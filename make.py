#!/usr/bin/env python3

import os
import shutil
import argparse
from typing import Optional
from distutils.dir_util import copy_tree

from fenautils import addrepr

import fenalib.logging_setup as logging_setup
logging_setup.setup_logging()

from fenalib.general import parse_text


"""
make.py lib [folder_name | all]
    - creates/updates the lib folder
    - used under folder_name/src
    - creates the lib folder in folder_name/src/lib

    - if folder_name is provided:
        - used under ego/general_event
        - creates the lib folder in general_event/folder_name/src/lib

make.py template <template_type> <folder_name>
    - creates a template for the given template type
    - template types are found under templates/template_type
    - all files inside the template folder will be copied
    - used under ego/general_event
    - creates the files under general_event/folder_name/src
    - eg. `template virus virus_2`

make.py build <folder_name | all>
    - all: runs fena.py on all folder names in the current working directory
    - folder_name: runs fena.py just on the folder
        - all ran specifically under folder_name/src/main.fena
"""



# where the python command in terminal was ran
# this should be under Fena/functions/ego/EVENT_NAME/src
current_working_dir = os.path.realpath(os.getcwd())

# where this python file was ran
python_file_dir = os.path.realpath(os.path.dirname(__file__))


@addrepr
class TemplateArgs:
    """
    Args:
        text (str)

    Attributes:
        template_type (str)
        replacements (Dict[str, str]):
    """
    def __init__(self, text):
        # removes all comments and whitespace
        lines = [line for line in text.splitlines() if line.strip() if not line.strip().startswith("#")]

        variables = {}
        for line in lines:
            arg, value = line.split("=")
            variables[arg] = value

        self.template_type = variables.pop("$TYPE")
        self.replacements = variables

    def use_replacements(self, text: str) -> str:
        for old, new in self.replacements.items():
            text = text.replace(old, new)

        return text

def get_args():
    # Usage: main.py fileName [output_path] [-d, --debug] [-c, --clean]
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title="subcommands", description="valid subcommands", dest="make_arg")
    subparsers.required = True

    lib_parser = subparsers.add_parser("lib", help="makes the library")
    lib_parser.add_argument("folder_name", nargs="?", help="The event folder you want to create/update the library in")

    template_parser = subparsers.add_parser("template", help="creates a given template in the file path")
    template_parser.add_argument("template_type", help="type of template you want to generate")
    template_parser.add_argument("folder_name", help="the file path that you want the following template to build in")

    build_parser = subparsers.add_parser("build", help="builds a given template")
    build_parser.add_argument("template_dir", help="template file base to build (virus_2.template -> 'virus_2', and all in directory -> '*')")
    build_parser.add_argument("-c", "--clean", help="removes all mcfunction files inside the output directory", action="store_true")
    build_parser.add_argument("-d", "--debug", help="puts say commands at the front of each mcfunction to show who is running it", action="store_true")

    args = parser.parse_args()
    return args


def main():
    # requires one positional argument
    # so far, the arguments are file_path, (update | clean)
    # so to run this file, do "python3 ../../../../lib.py update"
    # where "clean" just removes the event library
    args = get_args()

    if args.make_arg == "lib":
        update_lib(folder_name=args.folder_name)
    elif args.make_arg == "template":
        make_template(args.template_type, args.folder_name)
    else:
        assert args.make_arg == "build"
        build_template(args.template_dir, debug=args.debug, clean=args.clean)


def update_lib(folder_name: Optional[str] = None):
    """
    - Removes the old library if it exists
    - Copies the current library under Fena/lib to the lib directory
    """

    print("Updating the lib folder ...")

    regular_lib_path = os.path.join(python_file_dir, "lib")

    if folder_name is None:
        # makes sure that the last directory of the cwd is "src"
        assert os.path.basename(os.path.normpath(current_working_dir)) == "src", f"expected base dir in cwd to be 'src', but got {current_working_dir!r}"
        event_lib_dirs = [os.path.join(current_working_dir, "lib")]
    elif folder_name == "all":
        event_lib_dirs = [os.path.join(d, "src", "lib") for d in os.listdir(current_working_dir) if os.path.isdir(d)]
    else:
        event_lib_dirs = [os.path.join(current_working_dir, folder_name, "src", "lib")]

    for event_lib_path in event_lib_dirs:
        remove_lib(event_lib_path, folder_name)
        print(f"Creating the new lib folder for {event_lib_path!r} ...")

        # makes sure the library exists
        assert os.path.isdir(regular_lib_path)

        # gets the new version file
        version_file = os.path.join(regular_lib_path, "version.txt")
        if os.path.isfile(version_file):
            with open(version_file) as file:
                print(f"New lib version: {file.readline().strip()}")
        else:
            raise Exception("version.txt not found under global library")

        # copies the original library to the event library
        copy_tree(regular_lib_path, event_lib_path)

        print(f"Successfully updated the lib folder for {event_lib_path!r}")


def remove_lib(event_lib_path, folder_name):
    print(f"Removing old lib if it exists for {event_lib_path!r} ...")

    # makes sure the library path that will be removed is not the main library path
    assert event_lib_path != os.path.join(python_file_dir, "lib.py")

    # if the path exists, prints out the version file
    # and deletes the library path
    if os.path.isdir(event_lib_path):
        old_version_file = os.path.join(event_lib_path, "version.txt")
        if os.path.isfile(old_version_file):
            with open(old_version_file) as file:
                print(f"Old lib version: {file.readline().strip()}")

        shutil.rmtree(event_lib_path)
        print(f"Successfully removed the old lib folder for {event_lib_path!r}")


def make_template(template_type: str, template_dir: str):
    """
    Copies all the file in Fena/template/template_type folder
        into template_dir/src
    """
    print(f"Making template type {template_type!r} in {template_dir!r} ...")
    old_template_dir = os.path.join(python_file_dir, "templates", template_type)
    if not os.path.isdir(old_template_dir):
        raise SyntaxError(f"Invalid template type {template_type!r}, expected a template file path for {old_template_dir!r}")

    # the directory to paste the files from the old template dir
    new_template_dir = os.path.join(current_working_dir, template_dir, "src")

    # gets all template files from the main template folder
    template_files = os.listdir(old_template_dir)

    # makes the new directory if it doesn't exist
    if not os.path.isdir(new_template_dir):
        os.makedirs(new_template_dir)

    # copies eacn file individually the template files are actually files
    # since they can be directories
    for template_file in template_files:
        old_file = os.path.join(old_template_dir, template_file)
        new_file = os.path.join(new_template_dir, template_file)
        if os.path.isfile(old_file):

            shutil.copy(old_file, new_file)
            print(f"Copying {template_file!r} ...")

    # makes the library as well
    update_lib(template_dir)

    print(f"Successfully created the {template_type!r} template")


def build_template(template_dir: str, clean=False, debug=False):
    """
    Builds the template by creating the main.fena file and the lib folder

    Args:
        template_dir (str): The base file name of the template file to build
    """

    if template_dir == "all":
        # builds all directories
        directories = [d for d in os.listdir(current_working_dir) if os.path.isdir(d)]
    else:
        directories = [template_dir]

    for directory in directories:
        print(f"Building {directory!r} ...")
        output_path = os.path.realpath(os.path.join(current_working_dir, directory))
        fena_cwd = os.path.realpath(os.path.join(output_path, "src"))
        main_fena_file = os.path.realpath(os.path.join(fena_cwd, "main.fena"))

        with open(main_fena_file) as file:
            text = file.read()

        # changes current working directory for Fena
        os.chdir(fena_cwd)
        logging_setup.format_file_name(os.path.join(directory, "src", "main.fena"))
        parse_text(text, main_fena_file, output_path, clean=clean, debug=debug)

        print(f"Successfully built {directory!r}")


if __name__ == "__main__":
    main()


