from lib.location import LOCATION
from lib.floo_event import FlooEvent
from lib.const_ints import ConstInts
from lib.objective import Objectives
from lib.team import Teams
from lib.coords import Coords


location = LOCATION
floo_event = FlooEvent(location)
select_all = location.select_all
objectives = Objectives()
teams = Teams()
const_ints = ConstInts()


objectives.new("""
    # "Players":
    _ _ _

    # _stand (counter): increments in ticks as the round progresses
    # - also deincrements during the countdown until 0
    _ti _ Timer

    # _stand (state):
    # 0: waiting for the round to start
    # 1: during the countdown
    # 2: during round
    _st _ State

    # players (state): Player state for this event
    # 0 = being outside the event
    # 1 = part of the event, meaning the players are properly initialized
    # 2 = actually participating in the round
    _pl _ Player List
    _cl _ Calculations

""", display=location.name_str)
objectives["_"].setdisplay("sidebar")

const_ints.add_constants(-1)

teams.new("""
    _ _
        color = COLOR
        friendlyfire = false

""", display=location.name_str)

