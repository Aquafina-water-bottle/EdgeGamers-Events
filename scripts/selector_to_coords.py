import pyperclip

if __name__ == "__main__":
    import sys
    sys.path.append("..")

from lib.coords import Coords

input_list = """
x=19,y=6,z=-147,dx=88,dy=100,dz=27
x=36,y=16,z=-133,dy=2
x=36,y=16,z=-130,dy=2
x=36,y=16,z=-136,dy=2
""".splitlines()

input_list = [line for line in input_list if line if not line.startswith("#")]

def main():
    return_list = []
    for x in input_list:
        return_list.append(str(Coords(x)))

    pyperclip.copy("\n".join(return_list))

if __name__ == "__main__":
    main()
