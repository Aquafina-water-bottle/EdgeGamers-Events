import os
import json
import shutil

"""
- move data to json
- renames blocks.txt -> blocks.csv
"""

def main():
    blockhunt_dir = os.path.realpath("../functions/ego/blockhunt")
    dir_list = os.listdir(blockhunt_dir)
    dir_list.remove("a_small_village")
    print(dir_list)

    fena_template_path = os.path.realpath("../templates/blockhunt/main.fena")
    with open(fena_template_path) as file:
        blockhunt_template_str = file.read()
    print(blockhunt_template_str)

    for bh_dir in dir_list:
        src_dir = os.path.join(blockhunt_dir, bh_dir, "src")
        print(f"converting {src_dir} ...")
        # json_str = extract_json(src_dir)

        fena_file_path = os.path.join(src_dir, "main.fena")
        # json_file_path = os.path.join(src_dir, "main.json")
        # blocks_txt_file_path = os.path.join(src_dir, "blocks.txt")
        # blocks_csv_file_path = os.path.join(src_dir, "blocks.csv")

        with open(fena_file_path, "w") as file:
            file.write(blockhunt_template_str)
        # with open(json_file_path, "w") as file:
        #     file.write(json_str)

        # shallow file copy
        # shutil.copyfile(blocks_txt_file_path, blocks_csv_file_path)
        # os.remove(blocks_txt_file_path)

def extract_json(src_dir):
    LOCATION_START = "    from lib.location import "
    TP_WAIT_START = "        tp_to_wait_coords=Coords(\""
    TP_ARENA_START = "        tp_to_arena_coords=Coords(\""
    location = tp_to_wait_coords = tp_to_arena_coords = None

    # extracts the specific strings for json
    with open(os.path.join(src_dir, "main.fena")) as file:
        for line in file.read().splitlines():
            if LOCATION_START in line:
                location = line[len(LOCATION_START):]
                print(repr(location))
            if TP_WAIT_START in line:
                tp_to_wait_coords = line[len(TP_WAIT_START):len(line)-3]
                print(repr(tp_to_wait_coords))
            if TP_ARENA_START in line:
                tp_to_arena_coords = line[len(TP_ARENA_START):len(line)-3]
                print(repr(tp_to_arena_coords))

    # simply to make sure they were all found
    assert (location is not None) and (tp_to_wait_coords is not None) and (tp_to_arena_coords is not None)

    json_object = {
        "location": location,
        "tp_to_wait_coords": tp_to_wait_coords,
        "tp_to_arena_coords": tp_to_arena_coords
    }
    json_str = json.dumps(json_object, indent=4)

    print(json_str)
    return json_str

if __name__ == "__main__":
    main()

