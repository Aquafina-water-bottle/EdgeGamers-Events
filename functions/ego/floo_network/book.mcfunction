function ego:race/ice_race/book if @s[score_gp.bk_min=2065128021,score_gp.bk=2065128021]
function ego:race/slow_race/book if @s[score_gp.bk_min=526647444,score_gp.bk=526647444]
function ego:race/nether_race/book if @s[score_gp.bk_min=1892113393,score_gp.bk=1892113393]
function ego:race/evil_race/book if @s[score_gp.bk_min=296783358,score_gp.bk=296783358]
function ego:race/diamond_race/book if @s[score_gp.bk_min=401263828,score_gp.bk=401263828]
function ego:race/emerald_race/book if @s[score_gp.bk_min=1455341280,score_gp.bk=1455341280]
function ego:race/quartz_race/book if @s[score_gp.bk_min=365039744,score_gp.bk=365039744]
function ego:race/1_8_race/book if @s[score_gp.bk_min=190191959,score_gp.bk=190191959]
function ego:race/epic_race/book if @s[score_gp.bk_min=313853305,score_gp.bk=313853305]
function ego:race/salt_race/book if @s[score_gp.bk_min=1272235179,score_gp.bk=1272235179]
function ego:race/death_run/book if @s[score_gp.bk_min=1075455238,score_gp.bk=1075455238]
function ego:race/dirt_race/book if @s[score_gp.bk_min=738387099,score_gp.bk=738387099]
function ego:race/mycelium_race/book if @s[score_gp.bk_min=1967513996,score_gp.bk=1967513996]
function ego:race/frostburn_run/book if @s[score_gp.bk_min=1511902483,score_gp.bk=1511902483]
function ego:race/ever_changing_race/book if @s[score_gp.bk_min=302496294,score_gp.bk=302496294]
function ego:virus/virus_1/book if @s[score_gp.bk_min=2115807378,score_gp.bk=2115807378]
function ego:virus/virus_2/book if @s[score_gp.bk_min=1723054610,score_gp.bk=1723054610]
function ego:virus/virus_mansion/book if @s[score_gp.bk_min=1303306030,score_gp.bk=1303306030]
function ego:virus/virus_hell/book if @s[score_gp.bk_min=1521378220,score_gp.bk=1521378220]
function ego:pvp/old_ctf/book if @s[score_gp.bk_min=1793355344,score_gp.bk=1793355344]
function ego:capture_the_flag/book if @s[score_gp.bk_min=905070336,score_gp.bk=905070336]
function ego:the_pit_3/book if @s[score_gp.bk_min=2140363891,score_gp.bk=2140363891]
function ego:sand_tomb/book if @s[score_gp.bk_min=370149165,score_gp.bk=370149165]
function ego:anvil_drop/book if @s[score_gp.bk_min=2071894910,score_gp.bk=2071894910]
function ego:death_pit/book if @s[score_gp.bk_min=2007743934,score_gp.bk=2007743934]
function ego:rabbit_ball/book if @s[score_gp.bk_min=1405382069,score_gp.bk=1405382069]
function ego:mastermind/regular/book if @s[score_gp.bk_min=516294072,score_gp.bk=516294072]
function ego:mastermind/hell/book if @s[score_gp.bk_min=1606617780,score_gp.bk=1606617780]
function ego:pictionary/book if @s[score_gp.bk_min=1916439502,score_gp.bk=1916439502]
function ego:royal_rumble/book if @s[score_gp.bk_min=81659237,score_gp.bk=81659237]
function ego:blockhunt/a_small_village/book if @s[score_gp.bk_min=763457061,score_gp.bk=763457061]
function ego:blockhunt/apocalypse/book if @s[score_gp.bk_min=725637627,score_gp.bk=725637627]
function ego:blockhunt/castle_de_emmy/book if @s[score_gp.bk_min=2053033941,score_gp.bk=2053033941]
function ego:blockhunt/four_corners/book if @s[score_gp.bk_min=518834438,score_gp.bk=518834438]
function ego:blockhunt/hasdaa/book if @s[score_gp.bk_min=6296492,score_gp.bk=6296492]
function ego:blockhunt/hospital/book if @s[score_gp.bk_min=183650979,score_gp.bk=183650979]
function ego:blockhunt/jungle/book if @s[score_gp.bk_min=928077502,score_gp.bk=928077502]
function ego:blockhunt/mastermind/book if @s[score_gp.bk_min=772601972,score_gp.bk=772601972]
function ego:blockhunt/mushroom_village/book if @s[score_gp.bk_min=119885644,score_gp.bk=119885644]
function ego:blockhunt/old/book if @s[score_gp.bk_min=1452979271,score_gp.bk=1452979271]
function ego:blockhunt/park/book if @s[score_gp.bk_min=1278338284,score_gp.bk=1278338284]
function ego:blockhunt/rainbow/book if @s[score_gp.bk_min=376134566,score_gp.bk=376134566]
function ego:blockhunt/resort/book if @s[score_gp.bk_min=795649988,score_gp.bk=795649988]
function ego:blockhunt/train_station/book if @s[score_gp.bk_min=1846441401,score_gp.bk=1846441401]
function ego:blockhunt/virus_1/book if @s[score_gp.bk_min=942106152,score_gp.bk=942106152]
function ego:blockhunt/virus_2/book if @s[score_gp.bk_min=1356189675,score_gp.bk=1356189675]
function ego:blockhunt/zelda/book if @s[score_gp.bk_min=1233606541,score_gp.bk=1233606541]
function ego:birthday/book if @s[score_gp.bk_min=99934939,score_gp.bk=99934939]
function ego:ts_token/book if @s[score_gp.bk_min=425737659,score_gp.bk=425737659]
scoreboard players set @s gp.bk 0
