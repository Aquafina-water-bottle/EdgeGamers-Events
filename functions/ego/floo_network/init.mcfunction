scoreboard objectives add g.number dummy
scoreboard players set -1 g.number -1
scoreboard objectives add g.sa dummy Global Select All
scoreboard objectives add g.de deathCount Global Death Count
scoreboard objectives add g.cs stat.useItem.minecraft.carrot_on_a_stick Global Use Carrot Stick
scoreboard objectives add g.lg stat.leaveGame Global Leave Game
scoreboard objectives add g.to stat.useItem.minecraft.totem_of_undying Global Use Totem
scoreboard objectives add g.sc dummy Global CmdStat SuccessCount
scoreboard objectives add g.ab dummy Global CmdStat AffectedBlocks
scoreboard objectives add g.ai dummy Global CmdStat AffectedItems
scoreboard objectives add g.ae dummy Global CmdStat AffectedEntites
scoreboard objectives add g.qr dummy Global CmdStat QueryResult
scoreboard objectives add g.temp dummy Global Temp Calcs
scoreboard objectives add g.ftp dummy Global Teleported from Floo
scoreboard objectives add g.stp dummy Global Teleported from Spawn
scoreboard objectives add g.host dummy Global Host
scoreboard objectives add gr.at dummy Global Rank Admin Trainer
scoreboard objectives add gr.apps dummy Global Rank Panda Squad
scoreboard objectives add gr.ec dummy Global Rank Event Coordinator
scoreboard objectives add gr.le dummy Global Rank Leadership
scoreboard objectives add gr.cmd dummy Global Rank Special Command
scoreboard objectives add gr.total dummy Global Rank Total Rank
scoreboard objectives add gp.na dummy Global Python Names for IDs
scoreboard objectives add gp.id dummy Global Python Spawnpoint ID
scoreboard objectives add gp.tp dummy Global Python Teleport ID
scoreboard objectives add gp.bk dummy Global Python Book ID
scoreboard objectives add gp.trm dummy Global Python Termination Toggle
scoreboard objectives add gp.cgi dummy Global Python Current Game ID
scoreboard objectives add gp.pvp dummy Global Python PVP toggle
scoreboard objectives add gp.sat dummy Global Python Saturation toggle
scoreboard objectives add gp.rgt dummy Global Python Custom Regen Timer
scoreboard objectives add g.hp health Health
scoreboard objectives setdisplay list g.hp
scoreboard objectives add fl.rbs dummy Floo Network Rank Binary Score
scoreboard objectives add fl.rac dummy Floo Network Rank Calc
scoreboard objectives add fl.vot trigger Floo Network Voting
scoreboard objectives add fl.dly dummy Floo Network Spawn delay
scoreboard objectives add fl.dim dummy Floo Network Correct Dimension
scoreboard objectives add fl.rgt dummy Floo Network Calc Regen Time
scoreboard objectives add fl.rgd dummy Floo Network Calc Regen Diff
scoreboard teams add g.nopush Global No Push
scoreboard teams option g.nopush collisionRule never
scoreboard teams option g.nopush friendlyfire false
scoreboard teams add g.nopvp Global No PVP
scoreboard teams option g.nopvp friendlyfire false
scoreboard players set ir gp.na 2065128021
scoreboard players set race/ice_race gp.na 2065128021
scoreboard players set sr gp.na 526647444
scoreboard players set race/slow_race gp.na 526647444
scoreboard players set nr gp.na 1892113393
scoreboard players set race/nether_race gp.na 1892113393
scoreboard players set evilr gp.na 296783358
scoreboard players set race/evil_race gp.na 296783358
scoreboard players set dr gp.na 401263828
scoreboard players set race/diamond_race gp.na 401263828
scoreboard players set er gp.na 1455341280
scoreboard players set race/emerald_race gp.na 1455341280
scoreboard players set qr gp.na 365039744
scoreboard players set race/quartz_race gp.na 365039744
scoreboard players set 18r gp.na 190191959
scoreboard players set race/1_8_race gp.na 190191959
scoreboard players set epicr gp.na 313853305
scoreboard players set race/epic_race gp.na 313853305
scoreboard players set saltr gp.na 1272235179
scoreboard players set race/salt_race gp.na 1272235179
scoreboard players set deathr gp.na 1075455238
scoreboard players set race/death_run gp.na 1075455238
scoreboard players set dirtr gp.na 738387099
scoreboard players set race/dirt_race gp.na 738387099
scoreboard players set mr gp.na 1967513996
scoreboard players set race/mycelium_race gp.na 1967513996
scoreboard players set fbr gp.na 1511902483
scoreboard players set race/frostburn_run gp.na 1511902483
scoreboard players set ecr gp.na 302496294
scoreboard players set race/ever_changing_race gp.na 302496294
scoreboard players set vr1 gp.na 2115807378
scoreboard players set virus1 gp.na 2115807378
scoreboard players set virus/virus_1 gp.na 2115807378
scoreboard players set vr2 gp.na 1723054610
scoreboard players set virus2 gp.na 1723054610
scoreboard players set virus/virus_2 gp.na 1723054610
scoreboard players set vrm gp.na 1303306030
scoreboard players set virusm gp.na 1303306030
scoreboard players set virus/virus_mansion gp.na 1303306030
scoreboard players set vrh gp.na 1521378220
scoreboard players set virush gp.na 1521378220
scoreboard players set virus/virus_hell gp.na 1521378220
scoreboard players set pvp1 gp.na 1793355344
scoreboard players set oldctf gp.na 1793355344
scoreboard players set pvp/old_ctf gp.na 1793355344
scoreboard players set ctf gp.na 905070336
scoreboard players set capture_the_flag gp.na 905070336
scoreboard players set tpl3 gp.na 2140363891
scoreboard players set tp gp.na 2140363891
scoreboard players set the_pit_3 gp.na 2140363891
scoreboard players set st gp.na 370149165
scoreboard players set sand_tomb gp.na 370149165
scoreboard players set ad gp.na 2071894910
scoreboard players set anvil_drop gp.na 2071894910
scoreboard players set dp gp.na 2007743934
scoreboard players set death_pit gp.na 2007743934
scoreboard players set rb gp.na 1405382069
scoreboard players set rabbit_ball gp.na 1405382069
scoreboard players set mm gp.na 516294072
scoreboard players set mastermind/regular gp.na 516294072
scoreboard players set mmh gp.na 1606617780
scoreboard players set mm2 gp.na 1606617780
scoreboard players set mastermind/hell gp.na 1606617780
scoreboard players set pc gp.na 1916439502
scoreboard players set pictionary gp.na 1916439502
scoreboard players set rr gp.na 81659237
scoreboard players set royal_rumble gp.na 81659237
scoreboard players set bhasv gp.na 763457061
scoreboard players set bha gp.na 725637627
scoreboard players set bhcde gp.na 2053033941
scoreboard players set bhfc gp.na 518834438
scoreboard players set bhhd gp.na 6296492
scoreboard players set bhh gp.na 183650979
scoreboard players set bhj gp.na 928077502
scoreboard players set bhmm gp.na 772601972
scoreboard players set bhm gp.na 772601972
scoreboard players set bhmv gp.na 119885644
scoreboard players set bho gp.na 1452979271
scoreboard players set bhp gp.na 1278338284
scoreboard players set bhra gp.na 376134566
scoreboard players set bhre gp.na 795649988
scoreboard players set bhts gp.na 1846441401
scoreboard players set bhvr1 gp.na 942106152
scoreboard players set bhvr2 gp.na 1356189675
scoreboard players set bhz gp.na 1233606541
scoreboard players set bday gp.na 99934939
scoreboard players set birthday gp.na 99934939
scoreboard players set spawn gp.na 709458611
scoreboard players set cmd gp.na 1416570985
scoreboard players set floo gp.na 1416570985
scoreboard players set diagonally gp.na 1416570985
scoreboard players set floo_network gp.na 1416570985
scoreboard players set ts gp.na 425737659
scoreboard players set tstoken gp.na 425737659
scoreboard players set ts_token gp.na 425737659
scoreboard players set mghub gp.na 1476982272
scoreboard players set bhhub gp.na 1309475804
scoreboard players set otherhub gp.na 440755679
scoreboard players set racehub gp.na 143775834
kill @e[type=minecraft:armor_stand,tag=gp.stand]
summon armor_stand 348 4 114 {Tags:["gp.stand"],Invulnerable:1b,Invisible:1b,Marker:1b,NoGravity:1b}
scoreboard players set Acefire97 gr.apps 1
scoreboard players add Acefire97 gr.total 1
scoreboard players set ashtongreen00 gr.apps 1
scoreboard players add ashtongreen00 gr.total 1
scoreboard players set ENFORCER_GAMING gr.apps 1
scoreboard players add ENFORCER_GAMING gr.total 1
scoreboard players set VampireQueen94 gr.apps 1
scoreboard players add VampireQueen94 gr.total 1
scoreboard players set TedddyB gr.apps 1
scoreboard players add TedddyB gr.total 1
scoreboard players set ItsArk_ gr.apps 1
scoreboard players add ItsArk_ gr.total 1
scoreboard players set CMDZane gr.at 1
scoreboard players set CMDZane gr.apps 1
scoreboard players add CMDZane gr.total 3
scoreboard players set french_man gr.ec 1
scoreboard players add french_man gr.total 4
scoreboard players set jamboree_lee gr.ec 1
scoreboard players add jamboree_lee gr.total 4
scoreboard players set CynRyn gr.ec 1
scoreboard players add CynRyn gr.total 4
scoreboard players set Icohedron gr.ec 1
scoreboard players add Icohedron gr.total 4
scoreboard players set TheDarkOne239 gr.ec 1
scoreboard players add TheDarkOne239 gr.total 4
scoreboard players set T3ben gr.ec 1
scoreboard players add T3ben gr.total 4
scoreboard players set BUTTERLOVER7683 gr.at 1
scoreboard players set BUTTERLOVER7683 gr.le 1
scoreboard players add BUTTERLOVER7683 gr.total 10
scoreboard players set FoxyTheBoomQueen gr.at 1
scoreboard players set FoxyTheBoomQueen gr.le 1
scoreboard players add FoxyTheBoomQueen gr.total 10
scoreboard players set Daa_ gr.ec 1
scoreboard players set Daa_ gr.le 1
scoreboard players add Daa_ gr.total 12
scoreboard players set idk_lobsters gr.le 1
scoreboard players add idk_lobsters gr.total 8
scoreboard players set JadeofallTrades gr.le 1
scoreboard players add JadeofallTrades gr.total 8
scoreboard players set Witch_Doctor gr.le 1
scoreboard players set Witch_Doctor gr.cmd 1
scoreboard players add Witch_Doctor gr.total 24
scoreboard players set FirezFury gr.le 1
scoreboard players add FirezFury gr.total 8
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] fl.rbs 28
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.cgi 0
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"Floo","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Floo Network","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1416570985"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Floo Network","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Floo Network","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1416570985"}},{"text":" has been installed!","color":"gray"}]}
