scoreboard players set @a fl.rac 1
scoreboard players operation @s[score_fl.rbs_min=1] fl.rac = @s[score_fl.rbs_min=1] fl.rbs
execute @s[score_fl.rac_min=16] ~ ~ ~ scoreboard players set @a[score_gr.cmd_min=1,score_gr.cmd=1,score_fl.dim_min=1,score_fl.dim=1] fl.rac 0
scoreboard players remove @s[score_fl.rac_min=16] fl.rac 16
execute @s[score_fl.rac_min=8] ~ ~ ~ scoreboard players set @a[score_gr.le_min=1,score_gr.le=1,score_fl.dim_min=1,score_fl.dim=1] fl.rac 0
scoreboard players remove @s[score_fl.rac_min=8] fl.rac 8
execute @s[score_fl.rac_min=4] ~ ~ ~ scoreboard players set @a[score_gr.ec_min=1,score_gr.ec=1,score_fl.dim_min=1,score_fl.dim=1] fl.rac 0
scoreboard players remove @s[score_fl.rac_min=4] fl.rac 4
execute @s[score_fl.rac_min=2] ~ ~ ~ scoreboard players set @a[score_gr.at_min=1,score_gr.at=1,score_fl.dim_min=1,score_fl.dim=1] fl.rac 0
scoreboard players remove @s[score_fl.rac_min=2] fl.rac 2
execute @s[score_fl.rac_min=1] ~ ~ ~ scoreboard players set @a[score_gr.apps_min=1,score_gr.apps=1,score_fl.dim_min=1,score_fl.dim=1] fl.rac 0
scoreboard players remove @s[score_fl.rac_min=1] fl.rac 1
