# pylint: disable=import-error, no-name-in-module
from lib.floo_event import FlooEvent

class Virus(FlooEvent):
    """
    Args:
        location (Location)
        tp_to_wait_coords (Coords)
        tp_to_arena_coords (Optional[Coords])

    Attributes:
        tp_to_wait_coords (Coords): Teleport coordinates to teleport the virus to the
            waiting room
        tp_to_arena_coords (Optional[Coords]): The coords of the arena tp once the doors are
            locked
            - Used to put the virus in the arena after the countdown has started
    """

    def __init__(self, location, tp_to_wait_coords, tp_to_arena_coords, **options):
        # Automatically sets the pvp to true since that's how we roll
        options["pvp"] = "true"
        super().__init__(location, **options)

        self.tp_to_wait_coords = tp_to_wait_coords
        self.tp_to_arena_coords = tp_to_arena_coords

