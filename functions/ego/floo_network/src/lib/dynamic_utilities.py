from typing import List
from abc import ABC, abstractmethod

if __name__ == "__main__":
    import sys
    sys.path.append("..")
    del sys

from fenautils import Singleton, addrepr
from lib.command_group import CommandGroup

"""
General class for all minecraft dynamically created utilities that aren't entities (bossbars, utilities, teams)
    - Singular version is a 'MC dynamic utility'

What they all have in common:
    - Requires initialization and deletion
    - All can be initialized in one big chunk at the beginning
    - Easier to use a syntax programmed specifically for each in a multiline string
"""

class DynamicUtilityException(Exception):
    pass


# pylint: disable=abstract-method
@addrepr
class MCDynamicUtility(CommandGroup, ABC):
    """
    Singular version of the utility
    """
    @abstractmethod
    def __init__(self, name: str):
        super().__init__()
        self.name = name


@addrepr
class MCDynamicUtilityGroup(CommandGroup, Singleton, ABC):
    """
    Singleton class to hold multiple MC Dynamic Utilities in a dictionary-like fashion
    """
    def __init__(self):
        super().__init__()

        # the singleton apparently initializes every time it gets the same attribute
        # therefore, checks a hasattr so the utilities aren't cleared
        if not hasattr(self, "_utilities"):
            self._utilities = {}

    @abstractmethod
    def new(self, text: str, display="", remove=False, prefix=None):
        """
        Args:
            text (str): A multiline string that will be parsed for adding multiple utilities to the group.
                All text will have this format:
                    - surrounding whitespace is ignored
                    - empty lines are ignored
                    - all lines starting with "#" are ignored
            display (str): The display name that will be put in the beginning of each given display name
            remove (bool): Whether the utilities should be removed or not upon cmd_term
            prefix (Optional[str]): Sets the prefix for each utility (generally not used)
        """
        return [line.strip() for line in text.splitlines() if line.strip() if not line.strip().startswith("#")]

    def add(self, *utilities):
        """
        Args:
            *utilities: Any number of utilities to add to the group
        """
        for utility in utilities:
            if utility.name in self._utilities:
                raise DynamicUtilityException(f"Did not expect a repeat of a utility name {utility!r} inside {self._utilities}")
            self._utilities[utility.name] = utility

    def reset(self, reset=True):
        self._utilities.clear()

    @property
    def names(self) -> List[str]:
        """
        Returns:
            list of str objects: All utility names
        """
        return list(self._utilities.keys())

    def cmd_init(self) -> str:
        commands = []
        for utility in self._utilities.values():
            commands.append(utility.cmd_init())
        return CommandGroup.cmd_from_iterable(commands)

    def cmd_term(self, reset=True) -> str:
        commands = []
        for utility in self._utilities.values():
            commands.append(utility.cmd_term())

        # fully resets so you can't use utilities after a cmd_term
        if reset:
            self.reset()

        return CommandGroup.cmd_from_iterable(commands)

    def __getitem__(self, name: str):
        return self._utilities[name]

    def __len__(self):
        return len(self._utilities)

    def __contains__(self, name: str):
        return name in self._utilities


