# pylint: disable=unused-wildcard-import
# pylint: disable=wildcard-import
from lib.location import *
from lib.floo_event import FlooEvent
from lib.const_ints import ConstInts
from lib.objective import Objectives
from lib.team import Teams
from lib.coords import Coords
from lib.command_group import CommandGroup

# note that these are used inside main.fena
from lib.consts import Sounds, Effects # pylint: disable=unused-import

# creates the shortcuts text file
with open("shortcuts_for_spreadsheet.txt", "w") as file:
    for event in Location.members:
        file.write(event.name_str + "\t" + "\t".join(event.shortcuts) + "\n")

with open("shortcuts.txt", "w") as file:
    for event in Location.members:
        file.write(event.name_str + ": " + ", ".join(event.shortcuts) + "\n")

floo_event = FlooEvent(FLOO_NETWORK, pvp="false_teams")
select_all = FLOO_NETWORK.select_all
stand_coords = FLOO_NETWORK.tp_coords.pos
select_tree = "x=397,y=17,z=61,dist=..5"
select_portal = Coords("350 14 13 440 19 103").selector()
dimension_id = "0"


# regions where blocks are placable
# in this case, mastermind and pictionary
# placable_blocks = ["x=77,y=5,z=42,r=40"]

# all event id values as integers
event_id_list = sorted([loc.location_id for loc in Location.members if loc.is_event])

# General python code for the main start
objectives = Objectives()
teams = Teams()
const_ints = ConstInts(initialize_self=True)

const_ints.add_constants(-1)

# Global objectives
objectives.new("""
    # players (boolean): specifies whether they are in the game or not
    # 0 = not in the game
    # 1 = in the game
    g.sa _ Select All

    # players (auto): increments when they die
    # - immediately set back to 0
    g.de deathCount Death Count

    # players (auto): increments when they have right clicked with a carrot on a stick
    # - immediately set back to 0
    g.cs stat.useItem.minecraft.carrot_on_a_stick Use Carrot Stick

    # players (auto): increments when they leave the game
    # - because of the nature of commands, this actually detects when they rejoin the game
    # - immediately set back to 0
    g.lg stat.leaveGame Leave Game

    # players (auto): increments when they use a totem of undying
    # - immediately set back to 0
    g.to stat.useItem.minecraft.totem_of_undying Use Totem

    # any: stores various stats-related outputs
    # Note that no player is automatically set to store the command-stat on these objectives
    g.sc _ CmdStat SuccessCount
    g.ab _ CmdStat AffectedBlocks
    g.ai _ CmdStat AffectedItems
    g.ae _ CmdStat AffectedEntites
    g.qr _ CmdStat QueryResult

    # arbitrary: a global temp scoreboard to do whatever calculations your heart desires
    g.temp _ Temp Calcs

    # players (boolean): specifies that a player has teleported by `gp.tp`
    # - immediately set back to 0
    g.ftp _ Teleported from Floo

    # players (boolean): specifies that a player has teleported from the spawn tree
    # - immediately set back to 0
    g.stp _ Teleported from Spawn

    # players (boolean): specifies that a player is the host
    g.host _ Host

""", display="Global")


# sets rankings for individual players
RANKINGS = ["APPS", "AT", "EC", "LE", "CMD"]

def get_total_rank(*ranks):
    """
    Gets the sum given the rankings as strings

    Note that the highest boolean value is "CMD" at 16

    Examples:
        10110 -> 2 + 4 + 16 = 22
        >>> get_total_rank("AT", "EC", "CMD")
        22

    Examples:
        00001 -> 1
        >>> get_total_rank("APPS")
        1
    """
    total_rank = 0

    for rank in ranks:
        assert rank in RANKINGS
        total_rank += 2 ** RANKINGS.index(rank)
    return total_rank

def set_rankings(json_data):
    """
    Gets a text as a docstring and output the scoreboard settings for each player

    Args:
        text (dict)
    """
    commands = []

    for player_name, rankings in json_data.items():
        if not rankings:
            continue

        for rank in rankings:
            rank = rank.lower()
            commands.append(f"{player_name} gr.{rank} = 1")

        rank_num = get_total_rank(*rankings)
        commands.append(f"{player_name} gr.total += {rank_num}")

    return CommandGroup.cmd_from_iterable(commands)

# Global objectives for rankings
objectives.new("""
    # players (boolean): specifies their rank
    # note that g.cmd is for special command access (APWBS)
    gr.at _ Admin Trainer
    gr.apps _ Panda Squad
    gr.ec _ Event Coordinator
    gr.le _ Leadership
    gr.cmd _ Special Command

    # players (state): Holds the binary (as decimal) value to show their total rank
    # Bits are in order of `RANKINGS` as specified above
    # These numbers are calculated through `get_total_rank` as defined below
    # eg. a member of both AT and EC would be "00110" -> 6
    gr.total _ Total Rank

""", display="Global Rank")


# Global objectives for python
objectives.new("""
    # fake players: holds the event ID for fake player names as shortcuts
    # - This is used for the alias /sptp, ./sptpa, ./spbk, etc.
    gp.na _ Names for IDs

    # players: hold the event id for setting the spawn point
    # - set to the negative value of itself once the spawnpoint has been set
    gp.id _ Spawnpoint ID

    # gp.stand: holds the current event id to state that players will teleport
    #   to said event
    # players: temporarily hold the event id for teleporting
    # - immediately set back to 0
    gp.tp _ Teleport ID

    # players: the event id for getting a book
    # - set back to 0 afer getting the book
    gp.bk _ Book ID

    # gp.stand (boolean): holds the event id that is currently running
    # - once activated, fully terminates the current event
    # - immediately set back to 0
    gp.trm _ Termination Toggle

    # gp.stand (state): holds the event id that is currently running
    # 0: No event is running
    # >= 1: An event is running with said id
    gp.cgi _ Current Game ID

    # All the options set by each event
    # gp.stand (state): Holds what to do with PVP for the event
    # 0 ("false_teams"): Turn pvp off by having everyone join the g.nopvp team
    #   - default for when not running an event
    # 1 ("false_weak"): Turn pvp off by giving weakness to everyone (default for events)
    # 2 ("true"): Does nothing
    gp.pvp _ PVP toggle

    # gp.stand (boolean): Specifies whether saturation should be given or not
    gp.sat _ Saturation toggle

    # gp.stand: Holds the base time in ticks for custom regeneration
    # 0: No custom regeneration
    # 1 or over: Custom regen every said number of ticks
    gp.rgt _ Custom Regen Timer

""", display="Global Python")

# Global objectives that will not be removed
objectives.new("""
    # players (auto): stores health
    g.hp health Health

""", remove=False)
objectives["g.hp"].setdisplay("list")


# Sets the floo-network specific objectives used for general calculations
objectives.new("""
    # gp.stand: Holds the binary state of all ranks that have special
    #    EC-like access
    _rbs _ Rank Binary Score

    # gp.stand: Intermediate calculations to see which players have the
    #    special ranking
    # players (boolean): Whether they have special permissions or not
    _rac _ Rank Calc

    # players (boolean): Whether they have activated the `/vote` command or not
    _vot trigger Voting

    # players: Incremements up to `TELEPORT_DELAY` in ticks before teleporting
    #    players to the current event that is running
    _dly _ Spawn delay

    # players (boolean): Whether they are in the correct dimension for the main
    #    spawn or not
    _dim _ Correct Dimension

    # players: Holds the actual timer in ticks which increments until the
    #     gp.stand's base time
    _rgt _ Calc Regen Time

    # players (boolean): whether regeneration can be given or not
    # _rgc _ Can have Regen

    # players: calculates the difference between their regeneration timer and the
    # gp.stand's regeneration
    _rgd _ Calc Regen Diff

""", display="Floo Network")

# Global teams
teams.new("""
    g.nopush No Push
        collisionRule = never
        friendlyfire = false

    g.nopvp No PVP
        friendlyfire = false
""", display="Global", remove=False)


if __name__ == "__main__":
    import doctest
    doctest.testmod()



