Ice Race: ir, race/ice_race
Slow Race: sr, race/slow_race
Nether Race: nr, race/nether_race
Evil Race: evilr, race/evil_race
Diamond Race: dr, race/diamond_race
Emerald Race: er, race/emerald_race
Quartz Race: qr, race/quartz_race
1.8 Race: 18r, race/1_8_race
Epic Race: epicr, race/epic_race
Salt Race: saltr, race/salt_race
Death Run: deathr, race/death_run
Dirt Race: dirtr, race/dirt_race
Mycelium Race: mr, race/mycelium_race
Frostburn Run: fbr, race/frostburn_run
EverChanging Race: ecr, race/ever_changing_race
Virus 1: vr1, virus1, virus/virus_1
Virus 2: vr2, virus2, virus/virus_2
Mansion Virus: vrm, virusm, virus/virus_mansion
Virus Hell: vrh, virush, virus/virus_hell
PVP: Old CTF: pvp1, oldctf, pvp/old_ctf
Capture the flag: ctf, capture_the_flag
The Pit 3: tpl3, tp, the_pit_3
Sand Tomb: st, sand_tomb
Anvil Drop: ad, anvil_drop
Death Pit: dp, death_pit
Rabbit ball: rb, rabbit_ball
Mastermind: mm, mastermind/regular
Mastermind Hell: mmh, mm2, mastermind/hell
Pictonary: pc, pictionary
Royal Rumble: rr, royal_rumble
A Small Village: bhasv
Apocalypse: bha
Castle de Emmy: bhcde
Four Corners: bhfc
HASDaa: bhhd
Hospital: bhh
Jungle: bhj
BH Mastermind: bhmm, bhm
Mushroom Village: bhmv
Old: bho
Park: bhp
Rainbow: bhra
Resort: bhre
Train Station: bhts
Virus 1: bhvr1
Virus 2: bhvr2
Zelda: bhz
Jade's Birthday: bday, birthday
Spawn: spawn
Floo Network: cmd, floo, diagonally, floo_network
Teamspeak Token: ts, tstoken, ts_token
Minigame Hub: mghub
Blockhunt hub: bhhub
Other Games Hub: otherhub
Race Hub: racehub
