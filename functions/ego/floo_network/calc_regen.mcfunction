scoreboard players add @s fl.rgt 1
scoreboard players operation @s fl.rgd = @s gp.rgt
scoreboard players operation @s fl.rgd -= @s fl.rgt
execute @s[score_fl.rgd_min=0,score_fl.rgd=0] ~ ~ ~ effect @a[score_fl.dim_min=1,score_fl.dim=1,score_g.hp=19] minecraft:regeneration 2 10 true
execute @s[score_fl.rgd=-1] ~ ~ ~ effect @a[score_fl.dim_min=1,score_fl.dim=1] minecraft:regeneration 0 0 true
scoreboard players set @s[score_fl.rgd=-1] fl.rgt 0
