function ego:floo_network/stop_events
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLtp 1608458894
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLpvp 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLwea 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLreg 0
scoreboard players set @a gSA 0
scoreboard players set @a[x=838,y=89,z=133,dx=-74,dy=-85,dz=74] gSA 1
scoreboard objectives add constants dummy
scoreboard objectives add BD dummy Jade's Birthday
scoreboard objectives setdisplay sidebar BD
scoreboard objectives add BDst dummy Jade's Birthday State
scoreboard objectives add BDpl dummy Jade's Birthday Player List
scoreboard objectives add BDlec dummy Jade's Birthday Levitation Calc
scoreboard objectives add BDcl dummy Jade's Birthday Calculations
scoreboard objectives add BDsk stat.sneakTime Jade's Birthday Calculations
scoreboard teams add BD Jade's Birthday
scoreboard teams option BD color green
scoreboard teams option BD friendlyfire false
summon armor_stand ~ ~ ~ {Tags:["BDEntity","BDStand"],Invulnerable:1,NoGravity:1,Invisible:1,Marker:1b}
summon armor_stand 803 60 158 {Tags:["BDEntity","BDCandle"],Invulnerable:1b,NoGravity:1b,Invisible:1b,Fire:32767s}
summon armor_stand 799 60 158 {Tags:["BDEntity","BDCandle"],Invulnerable:1b,NoGravity:1b,Invisible:1b,Fire:32767s}
summon armor_stand 789 60 168 {Tags:["BDEntity","BDCandle"],Invulnerable:1b,NoGravity:1b,Invisible:1b,Fire:32767s}
summon armor_stand 789 60 172 {Tags:["BDEntity","BDCandle"],Invulnerable:1b,NoGravity:1b,Invisible:1b,Fire:32767s}
summon armor_stand 799 60 182 {Tags:["BDEntity","BDCandle"],Invulnerable:1b,NoGravity:1b,Invisible:1b,Fire:32767s}
summon armor_stand 803 60 182 {Tags:["BDEntity","BDCandle"],Invulnerable:1b,NoGravity:1b,Invisible:1b,Fire:32767s}
summon armor_stand 813 60 172 {Tags:["BDEntity","BDCandle"],Invulnerable:1b,NoGravity:1b,Invisible:1b,Fire:32767s}
summon armor_stand 813 60 168 {Tags:["BDEntity","BDCandle"],Invulnerable:1b,NoGravity:1b,Invisible:1b,Fire:32767s}
tellraw @a[score_EC_min=0,score_EC=0] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BDAY","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Jade's Birthday","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1608458894"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Jade's Birthday","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Jade's Birthday","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1608458894"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=armor_stand,tag=FlooStand,score_FLgam_min=0,score_FLgam=0] FLgam 1608458894
