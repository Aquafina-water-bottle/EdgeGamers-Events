class Block:
    """
    Attributes:
        unusable_blocks (set of str objects)
        block_type (str)
        block_states (dict mapping str to str)
    """

    unusable_block_types = {
        "minecraft:chest",
        "minecraft:bed"
    }

    def __init__(self, block_type, block_states={}):
        assert isinstance(block_type, str)
        assert isinstance(block_states, dict)

        if block_type in Block.unusable_block_types:
            raise TypeError(f"Unusable block type: {block_type}")
        
        # checks whether the block type has "minecraft:" before it
        if not block_type.startswith("minecraft:"):
            raise SyntaxError(f"Expected 'minecraft' before {block_type}")

        # checks whether each block state has "--" before it
        for block_state in block_states:
            if not block_state.startswith("--"):
                raise SyntaxError(f"Expected '--' before {block_state}")

        self.block_type = block_type
        self.block_states = block_states

    @property
    def block_states_str(self):
        """
        Returns:
            str: Its block states as a string value
        """
        assert self.block_states
        return " ".join(f"{key}={value}" for (key, value) in self.block_states.items())

    def disguise(self):
        """
        Returns:
            str: The full blockdisguise command to disguise a player
        """
        if self.block_states:
            return f"bd disguise {self.block_type} @p[r=0] {self.block_states_str}"
        return f"bd disguise {self.block_type} @p[r=0]"

    def __eq__(self, other):
        """
        Sees whether this block is equal to another
        """
        return (self.block_type == other.block_type) and (self.block_states == other.block_states)

    def __repr__(self):
        return f"Block[block_type={self.block_type}, block_states={self.block_states}]"
