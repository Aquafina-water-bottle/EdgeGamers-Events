class Sounds:
    """
    Shorthand constants for sounds
    """
    pling = "minecraft:block.note.pling"
    xp = "minecraft:entity.experience_orb.pickup"
    item = "minecraft:entity.item.pickup"
    tp = "minecraft:entity.endermen.teleport"
    wither = "minecraft:entity.wither.death"
    level = "minecraft:entity.player.levelup"
    explode = "minecraft:entity.generic.explode"
    hat = "minecraft:block.note.hat"
    primed = "minecraft:entity.tnt.primed"

class Effect:
    """
    Attributes:
        string (str)
        id (int)
    """
    _id_calc = 1
    def __init__(self, string):
        assert isinstance(string, str)
        self.string = string

        assert Effect._id_calc <= 30
        self.id = Effect._id_calc
        Effect._id_calc += 1

        # makes sure there aren't more effects created

    def __str__(self):
        return self.string

class Effects:
    """
    Shorthand constants for effects
    """
    spd = Effect("minecraft:speed")
    slow = Effect("minecraft:slowness")
    haste = Effect("minecraft:haste")
    mining_fatigue = Effect("minecraft:mining_fatigue")
    strength = Effect("minecraft:strength")
    hp = Effect("minecraft:instant_health")
    dmg = Effect("minecraft:instant_damage")
    jump = Effect("minecraft:jump_boost")
    nausea = Effect("minecraft:nausea")
    regen = Effect("minecraft:regeneration")
    resist = Effect("minecraft:resistance")
    fire_resist = Effect("minecraft:fire_resistance")
    water_breath = Effect("minecraft:water_breathing")
    invis = Effect("minecraft:invisibility")
    blind = Effect("minecraft:blindness")
    night_vision = Effect("minecraft:night_vision")
    hunger = Effect("minecraft:hunger")
    weak = Effect("minecraft:weakness")
    poison = Effect("minecraft:poison")
    wither = Effect("minecraft:wither")
    hp_boost = Effect("minecraft:health_boost")
    absorption = Effect("minecraft:absorption")
    saturation = Effect("minecraft:saturation")
    glow = Effect("minecraft:glowing")
    levitation = Effect("minecraft:levitation")
    luck = Effect("minecraft:luck")
    unluck = Effect("minecraft:unluck")
    slow_fall = Effect("minecraft:slow_falling")
    conduit = Effect("minecraft:conduit")
    dolphin = Effect("minecraft:dolphins_grace")


class Colors:
    """
    List of all colors
    """
    DARK_RED = "dark_red"
    RED = "red"
    GOLD = "gold"
    YELLOW = "yellow"
    GREEN = "green"
    DARK_GREEN = "dark_green"
    DARK_BLUE = "dark_blue"
    BLUE = "blue"
    DARK_AQUA = "dark_aqua"
    AQUA = "aqua"
    LIGHT_PURPLE = "light_purple"
    DARK_PURPLE = "dark_purple"
    WHITE = "white"
    GRAY = "gray"
    DARK_GRAY = "dark_gray"
    BLACK = "black"
    RESET = "reset"

    ALL = (DARK_RED, RED, GOLD, YELLOW, GREEN, DARK_GREEN, DARK_BLUE, BLUE, DARK_AQUA,
           AQUA, LIGHT_PURPLE, DARK_PURPLE, WHITE, GRAY, DARK_GRAY, BLACK, RESET)

    CODE_DICT = {
        DARK_RED: "&4",
        RED: "&c",
        GOLD: "&6",
        YELLOW: "&e",
        GREEN: "&a",
        DARK_GREEN: "&2",
        DARK_BLUE: "&1",
        BLUE: "&9",
        DARK_AQUA: "&3",
        AQUA: "&b",
        LIGHT_PURPLE: "&d",
        DARK_PURPLE: "&5",
        WHITE: "&f",
        GRAY: "&7",
        DARK_GRAY: "&8",
        BLACK: "&0",
        RESET: "&r",
    }

class Enchants:
    AQUA_AFFINITY = "6"
    BANE_OF_ARTHRO = "18"
    BLAST_PROT = "3"
    BIND = "10"
    VANISH = "71"
    DEPTH_STRIDER = "8"
    EFFICIENCY = "32"
    FEATHER_FALLING = "2"
    FIRE_ASPECT = "20"
    FIRE_PROT = "1"
    FLAME = "50"
    FORTUNE = "35"
    FROST_WALKER = "9"
    INFINITY = "51"
    KNOCKBACK = "19"
    LOOTING = "21"
    SEA_LUCK = "61"
    LURE = "62"
    MENDING = "70"
    POWER = "48"
    PROJECTILE_PROT = "4"
    PROT = "0"
    PUNCH = "49"
    RESPIRATION = "5"
    SHARP = "16"
    SILK = "33"
    SMITE = "17"
    SWEEPING_EDGE = "22"
    THORNS = "7"
    UNBREAKING = "34"

MAX_INT = (1<<31)-1