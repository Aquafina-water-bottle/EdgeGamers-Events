function ego:floo_network/stop_events
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLtp 1301307747
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLpvp 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLsat 1
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLwea 0
scoreboard players set @e[type=armor_stand,tag=FlooStand] FLreg 0
scoreboard players set @a gSA 0
scoreboard players set @a[x=110,y=3,z=148,dx=64,dy=15,dz=64] gSA 1
scoreboard objectives add constants dummy
scoreboard players set 20 constants 20
scoreboard players set 3 constants 3
scoreboard objectives add PC dummy Pictionary
scoreboard objectives setdisplay sidebar PC
scoreboard objectives add PCti dummy Pictionary Timer
scoreboard objectives add PCst dummy Pictionary State
scoreboard objectives add PCpl dummy Pictionary Player List
scoreboard objectives add PCrd dummy Pictionary Random Choose
scoreboard objectives add PCcl dummy Pictionary Calculations
scoreboard objectives add PCsc dummy Pictionary Success Count
scoreboard objectives add PCaf dummy Pictionary Affected Items
scoreboard teams add PC Pictionary
scoreboard teams option PC color green
scoreboard teams option PC friendlyfire false
scoreboard teams option PC collisionRule never
scoreboard teams add PCd_y Pictionary Display yellow
scoreboard teams option PCd_y color yellow
summon armor_stand ~ ~ ~ {Tags:["PCEntity","PCStand"],Invulnerable:1,NoGravity:1,Invisible:1,Marker:1b}
summon area_effect_cloud ~ ~0.01 ~ {Duration:2147483647,CustomName:"Almond Joy",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.02 ~ {Duration:2147483647,CustomName:"Atomic Fireball",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.03 ~ {Duration:2147483647,CustomName:"Baby Ruth",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.04 ~ {Duration:2147483647,CustomName:"Blow Pops",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.05 ~ {Duration:2147483647,CustomName:"Butterfinger",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.06 ~ {Duration:2147483647,CustomName:"Cadbury eggs",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.07 ~ {Duration:2147483647,CustomName:"Candy cigarettes",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.08 ~ {Duration:2147483647,CustomName:"Candy corn",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.09 ~ {Duration:2147483647,CustomName:"Circus peanuts",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.1 ~ {Duration:2147483647,CustomName:"Corgi",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.11 ~ {Duration:2147483647,CustomName:"Cotton candy",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.12 ~ {Duration:2147483647,CustomName:"Gummi bears",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.13 ~ {Duration:2147483647,CustomName:"Gummi worms",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.14 ~ {Duration:2147483647,CustomName:"Hershey Bar",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.15 ~ {Duration:2147483647,CustomName:"Hershey's Kiss",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.16 ~ {Duration:2147483647,CustomName:"Jawbreakers",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.17 ~ {Duration:2147483647,CustomName:"Jelly Belly",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.18 ~ {Duration:2147483647,CustomName:"Jolly Ranchers",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.19 ~ {Duration:2147483647,CustomName:"Junior Mints",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.2 ~ {Duration:2147483647,CustomName:"Kit-Kat",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.21 ~ {Duration:2147483647,CustomName:"M&Ms",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.22 ~ {Duration:2147483647,CustomName:"Milk Duds",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.23 ~ {Duration:2147483647,CustomName:"Milky Way",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.24 ~ {Duration:2147483647,CustomName:"Mr. Goodbar",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.25 ~ {Duration:2147483647,CustomName:"Necco wafers",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.26 ~ {Duration:2147483647,CustomName:"Nerds",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.27 ~ {Duration:2147483647,CustomName:"Nik-L-Nips",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.28 ~ {Duration:2147483647,CustomName:"Payday",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.29 ~ {Duration:2147483647,CustomName:"Peeps",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.3 ~ {Duration:2147483647,CustomName:"Pez",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.31 ~ {Duration:2147483647,CustomName:"Pixie Stix",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.32 ~ {Duration:2147483647,CustomName:"Pocky",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.33 ~ {Duration:2147483647,CustomName:"Pop Rocks",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.34 ~ {Duration:2147483647,CustomName:"Red Vines",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.35 ~ {Duration:2147483647,CustomName:"Reese's peanut butter cups",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.36 ~ {Duration:2147483647,CustomName:"Reese's pieces",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.37 ~ {Duration:2147483647,CustomName:"Salt water taffy",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.38 ~ {Duration:2147483647,CustomName:"Skittles",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.39 ~ {Duration:2147483647,CustomName:"Smarties",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.4 ~ {Duration:2147483647,CustomName:"Snickers",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.41 ~ {Duration:2147483647,CustomName:"Sour Patch Kids",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.42 ~ {Duration:2147483647,CustomName:"Starburst",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.43 ~ {Duration:2147483647,CustomName:"Sugar Daddy",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.44 ~ {Duration:2147483647,CustomName:"Swedish Fish",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.45 ~ {Duration:2147483647,CustomName:"Three Musketeers",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.46 ~ {Duration:2147483647,CustomName:"Toblerone",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.47 ~ {Duration:2147483647,CustomName:"Tootsie pop",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.48 ~ {Duration:2147483647,CustomName:"Tootsie roll",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.49 ~ {Duration:2147483647,CustomName:"Twix",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.5 ~ {Duration:2147483647,CustomName:"Twizzlers",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.51 ~ {Duration:2147483647,CustomName:"Wax lips",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.52 ~ {Duration:2147483647,CustomName:"Werther's Original",Tags:["PCEntity","PCTopic"]}
summon area_effect_cloud ~ ~0.53 ~ {Duration:2147483647,CustomName:"York Peppermint Patties",Tags:["PCEntity","PCTopic"]}
scoreboard teams join PC @e[type=area_effect_cloud,tag=PCTopic]
scoreboard teams join PCd_y Time_Elapsed
scoreboard players add @e[type=area_effect_cloud,tag=PCTopic] PCrd 0
setblock 158 1 180 air 0
title @a title {"text":"Tonight's Theme:","color":"dark_aqua"}
title @a subtitle {"text":"Candy","color":"gold","bold":"true"}
tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"PC","color":"dark_aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"dark_aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Tonight's Theme: ","color":"dark_aqua"},{"text":"Candy","color":"gold","bold":"true"}]}
scoreboard players set @s HOST 0
execute @e[type=armor_stand,tag=PCStand] ~ ~ ~ stats entity @s set SuccessCount @s PCsc
tellraw @a[score_EC_min=0,score_EC=0] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"PC","color":"dark_aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"dark_aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"]","color":"gray"},{"text":": "},{"text":"P","color":"light_purple","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"light_purple"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"i","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"c","color":"gold","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"gold"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"t","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"i","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"o","color":"dark_green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"dark_green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"n","color":"blue","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"blue"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"a","color":"dark_aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"dark_aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"r","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":"y","color":"white","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Pictionary","color":"white"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p FLtp 1301307747"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=armor_stand,tag=FlooStand,score_FLgam_min=0,score_FLgam=0] FLgam 1301307747
