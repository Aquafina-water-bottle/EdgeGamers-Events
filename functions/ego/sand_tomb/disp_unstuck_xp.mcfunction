scoreboard players operation @s STcl = @s STuti
scoreboard players operation @s STcl *= 100 constants
scoreboard players operation @s STcl /= 100 constants
xp -10000l @s
xp 1129l @s
xp 11 @s
xp 6336 @s[score_STcl_min=64]
scoreboard players remove @s[score_STcl_min=64] STcl 64
xp 3168 @s[score_STcl_min=32]
scoreboard players remove @s[score_STcl_min=32] STcl 32
xp 1584 @s[score_STcl_min=16]
scoreboard players remove @s[score_STcl_min=16] STcl 16
xp 792 @s[score_STcl_min=8]
scoreboard players remove @s[score_STcl_min=8] STcl 8
xp 396 @s[score_STcl_min=4]
scoreboard players remove @s[score_STcl_min=4] STcl 4
xp 198 @s[score_STcl_min=2]
scoreboard players remove @s[score_STcl_min=2] STcl 2
xp -1129l @s
