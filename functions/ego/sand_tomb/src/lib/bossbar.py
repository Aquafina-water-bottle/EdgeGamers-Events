"""
General module for dealing with minecraft bossbars introduced in 1.13

This will be used as the basis for other modules since there will be a few syntactical changes:
    - A singleton class will be used instead of a global object
    - Format of the new_str method will be slightly changed
"""

from lib.container import Container

class BossBar:
    pass

class BossBars:
    def new_str(self, text, prefix=None, display=None):
        """
        Initializes multiple bossbars inside a multiline string

        Format for declaration:
            id [json, string]?
             - string is gotten all the way until the newline
             - string is then formatted as a json {"text":"string"}
             - if no json or string is given, the json will be formatted as {"text":"id"}

        Format for options (include tab space):
            color <color>
            style <style>
            max <max>
            value <value>

        color ::= ["white", "pink", "red", "yellow", "green", "blue", "purple"]
        style ::= ["0": "progress", "6": "notched_6", "10": "notched_10", "12": "notched_12", "20": "notched_20"]
        max as positive integer less than (1<<31)-1
        value as positive integer less than (1<<31)-1

        Args:
            text

        Examples:
            >>> b = BossBars()
            >>> b.new_str('''
            ... _id {"text":"test"}
            ...     color yellow
            ...     visible false
            ...     style 20
            ... 
            ... _id2 Display Name
            ...     color green
            ...     max 216
            ...     value 25
            ... 
            ... _id3
            ...     style 0
            ...
            ... ''', prefix="rr", display="Royal Rumble")

            >>> for line in b.cmd_init().splitlines(): print(line.strip())
            bossbar create rr.id {"text":"test"}
            bossbar set rr.id color yellow
            bossbar set rr.id visible false
            bossbar set rr.id style notched_20
            bossbar create rr.id2 {"text":"Display Name"}
            bossbar set rr.id2 color green
            bossbar set rr.id2 max 216
            bossbar set rr.id2 value 25
            bossbar create rr.id3 {"text":"rr.id3"}
            bossbar set rr.id style progress


            >>> for line in b.cmd_term().splitlines(): print(line.strip())
            bossbar remove rr.id1
            bossbar remove rr.id2
            bossbar remove rr.id3
        """
        pass

