execute @e[type=armor_stand,tag=STStand,score_STbl_min=1,score_STbl=1] ~ ~ ~ effect @a[score_gSA_min=1,score_gSA=1,score_STpl_min=2,score_STpl=2] minecraft:blindness 2 0 true
effect @a[x=-207,y=10,z=16,dy=24,dz=3,m=2,score_STpl_min=2,score_STpl=2] minecraft:fire_resistance 15 0 true
effect @a[x=-207,y=10,z=16,dy=24,dz=3,m=2,score_STpl_min=2,score_STpl=2] minecraft:resistance 2 10 true
execute @s[score_STid_min=1,score_STid=1,score_STwc_min=1,score_STwc=1] ~ ~ ~ execute @a[c=1,x=-207,y=11,z=16,dy=1,dz=3,score_STpl_min=2,score_STpl=2] ~ ~ ~ function ego:sand_tomb/set_place_1
execute @s[score_STid_min=1,score_STid=1,score_STwc_min=2,score_STwc=2] ~ ~ ~ execute @a[c=1,x=-207,y=11,z=16,dy=1,dz=3,score_STpl_min=2,score_STpl=2] ~ ~ ~ function ego:sand_tomb/set_place_2
execute @s[score_STid_min=1,score_STid=1,score_STwc_min=3,score_STwc=3] ~ ~ ~ execute @a[c=1,x=-207,y=11,z=16,dy=1,dz=3,score_STpl_min=2,score_STpl=2] ~ ~ ~ function ego:sand_tomb/set_place_3
execute @s[score_STid_min=1,score_STid=1,score_STwc_min=4] ~ ~ ~ execute @a[c=1,x=-207,y=11,z=16,dy=1,dz=3,score_STpl_min=2,score_STpl=2] ~ ~ ~ function ego:sand_tomb/set_place_runnerup
effect @a[x=-272,y=41,z=40,dx=44,dy=1,m=2,score_STpl_min=2,score_STpl=2] minecraft:fire_resistance 15 0 true
effect @a[x=-272,y=41,z=40,dx=44,dy=1,m=2,score_STpl_min=2,score_STpl=2] minecraft:resistance 2 10 true
execute @s[score_STid_min=2,score_STid=2,score_STwc_min=1,score_STwc=1] ~ ~ ~ execute @a[c=1,x=-272,y=40,z=40,dx=44,score_STpl_min=2,score_STpl=2] ~ ~ ~ function ego:sand_tomb/set_place_1
execute @s[score_STid_min=2,score_STid=2,score_STwc_min=2,score_STwc=2] ~ ~ ~ execute @a[c=1,x=-272,y=40,z=40,dx=44,score_STpl_min=2,score_STpl=2] ~ ~ ~ function ego:sand_tomb/set_place_2
execute @s[score_STid_min=2,score_STid=2,score_STwc_min=3,score_STwc=3] ~ ~ ~ execute @a[c=1,x=-272,y=40,z=40,dx=44,score_STpl_min=2,score_STpl=2] ~ ~ ~ function ego:sand_tomb/set_place_3
execute @s[score_STid_min=2,score_STid=2,score_STwc_min=4] ~ ~ ~ execute @a[c=1,x=-272,y=40,z=40,dx=44,score_STpl_min=2,score_STpl=2] ~ ~ ~ function ego:sand_tomb/set_place_runnerup
scoreboard players add @s STti 1
scoreboard players operation @s STcl = &Delay STcl
scoreboard players operation @s STcl -= @s STti
execute @s[score_STcl=-1] ~ ~ ~ function ego:sand_tomb/set_sand
clear @a[score_gSA_min=1,score_gSA=1] sand
execute @a[score_gSA_min=1,score_gSA=1,m=2,score_STpl_min=2,score_STpl=2] ~ ~ ~ detect ~ ~1 ~ sand 0 scoreboard players add @s STti 1
execute @a[score_gSA_min=1,score_gSA=1,m=2,score_STpl_min=2,score_STpl=2] ~ ~ ~ detect ~ ~2 ~ air 0 scoreboard players set @s STti 0
kill @a[score_gSA_min=1,score_gSA=1,m=2,score_STpl_min=2,score_STpl=2,score_STti_min=100]
execute @a[score_gSA_min=1,score_gSA=1,m=2,score_STpl_min=2,score_STpl=2,score_gCS_min=1,score_STuti_min=0,score_STuti=0] ~ ~ ~ function ego:sand_tomb/make_unstuck
execute @a[score_gSA_min=1,score_gSA=1,m=2,score_STpl_min=2,score_STpl=2,score_STuti_min=1] ~ ~ ~ function ego:sand_tomb/disp_unstuck_xp
scoreboard players remove @a[score_gSA_min=1,score_gSA=1,m=2,score_STpl_min=2,score_STpl=2,score_STuti_min=1] STuti 1
