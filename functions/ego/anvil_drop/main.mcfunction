scoreboard players set @a g.sa 0
scoreboard players set @a[x=-38,y=0,z=76,dx=22,dy=35,dz=22] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-2071894911] gp.id 2071894910
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-2071894909] gp.id 2071894910
clear @a[score_g.sa_min=1,score_g.sa=1] minecraft:anvil -1 -1
kill @e[x=-38,y=0,z=76,dx=22,dy=35,dz=22,type=minecraft:item]
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_ad.pl_min=1] ad.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_ad.pl_min=1] ad.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_ad.pl_min=1] ad.pl 0
scoreboard players add @a[m=2,score_g.sa_min=1,score_g.sa=1] ad.pl 0
execute @a[m=2,score_g.sa_min=1,score_g.sa=1,score_ad.pl_min=0,score_ad.pl=0] ~ ~ ~ function ego:anvil_drop/init_player
execute @e[type=minecraft:armor_stand,score_ad.ipe1_min=0,score_ad.ipe1=100,tag=ad.stand] ~ ~ ~ function ego:anvil_drop/update_percent_1
execute @e[type=minecraft:armor_stand,score_ad.iti1_min=0,tag=ad.stand] ~ ~ ~ function ego:anvil_drop/update_timer_1
execute @e[type=minecraft:armor_stand,score_ad.ipe2_min=0,score_ad.ipe2=100,tag=ad.stand] ~ ~ ~ function ego:anvil_drop/update_percent_2
execute @e[type=minecraft:armor_stand,score_ad.iti2_min=0,tag=ad.stand] ~ ~ ~ function ego:anvil_drop/update_timer_2
execute @e[type=minecraft:armor_stand,score_ad.ipe3_min=0,score_ad.ipe3=100,tag=ad.stand] ~ ~ ~ function ego:anvil_drop/update_percent_3
execute @e[type=minecraft:armor_stand,score_ad.iti3_min=0,tag=ad.stand] ~ ~ ~ function ego:anvil_drop/update_timer_3
execute @e[type=minecraft:armor_stand,score_ad.st_min=0,score_ad.st=0,tag=ad.stand] ~ ~ ~ function ego:anvil_drop/wait_for_start
execute @e[type=minecraft:armor_stand,score_ad.st_min=1,score_ad.st=1,tag=ad.stand] ~ ~ ~ function ego:anvil_drop/countdown
execute @e[type=minecraft:armor_stand,score_ad.st_min=2,score_ad.st=2,tag=ad.stand] ~ ~ ~ function ego:anvil_drop/during_round
effect @a[m=2,score_g.sa_min=1,score_g.sa=1,score_ad.pl_min=1,score_ad.pl=1,score_g.hp=19] minecraft:instant_health 1 100 true
execute @a[score_g.host_min=1,score_g.host=1] ~ ~ ~ function ego:anvil_drop/display_settings
