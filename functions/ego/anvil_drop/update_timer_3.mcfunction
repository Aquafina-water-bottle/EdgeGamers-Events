scoreboard players operation &Timer3 ad.cl = @s ad.iti3
scoreboard players operation &Seconds3 ad.cl = &Timer3 ad.cl
scoreboard players operation &Seconds3 ad.cl /= 20 g.number
scoreboard players operation &Decimal3 ad.cl = &Timer3 ad.cl
scoreboard players operation &Decimal3 ad.cl %= 20 g.number
scoreboard players operation &Decimal3 ad.cl *= 5 g.number
scoreboard players reset &Additional3 ad.cl
scoreboard players operation @s ad.cl = &Decimal3 ad.cl
execute @s[score_ad.cl_min=5,score_ad.cl=5] ~ ~ ~ scoreboard players set &Additional3 ad.cl 0
scoreboard players set @s ad.iti3 -1
