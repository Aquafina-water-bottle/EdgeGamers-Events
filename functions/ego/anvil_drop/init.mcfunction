scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 2071894910
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration false
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 100
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-38,y=0,z=76,dx=22,dy=35,dz=22] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"AD","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Anvil Drop","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2071894910"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Anvil Drop","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Anvil Drop","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2071894910"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 2071894910
scoreboard players set 20 g.number 20
scoreboard players set 5 g.number 5
scoreboard objectives add ad. dummy Anvil Drop
scoreboard objectives setdisplay sidebar ad.
scoreboard objectives add ad.ti dummy Anvil Drop Timer
scoreboard objectives add ad.st dummy Anvil Drop State
scoreboard objectives add ad.pl dummy Anvil Drop Player List
scoreboard objectives add ad.dp dummy Anvil Drop Display
scoreboard objectives add ad.ipe1 dummy Anvil Drop Input Layer 1 %
scoreboard objectives add ad.ipe2 dummy Anvil Drop Input Layer 2 %
scoreboard objectives add ad.ipe3 dummy Anvil Drop Input Walls Toggle
scoreboard objectives add ad.iti1 dummy Anvil Drop Input Timer 1
scoreboard objectives add ad.iti2 dummy Anvil Drop Input Timer 2
scoreboard objectives add ad.iti3 dummy Anvil Drop Input Timer 3
scoreboard objectives add ad.ti1 dummy Anvil Drop Layer 1 Timer
scoreboard objectives add ad.ti2 dummy Anvil Drop Layer 2 Timer
scoreboard objectives add ad.ti3 dummy Anvil Drop Layer 3 Timer
scoreboard objectives add ad.cl dummy Anvil Drop Calculations
scoreboard teams add ad. Anvil Drop
scoreboard teams option ad. color green
scoreboard teams option ad. friendlyfire false
scoreboard teams add ad.d_y Anvil Drop display yellow
scoreboard teams option ad.d_y color yellow
scoreboard teams join ad.d_y Countdown
scoreboard teams join ad.d_y Players
scoreboard teams join ad.d_y Time_Elapsed
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["ad.stand","ad.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=ad.stand] ad.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=ad.stand] ad.ti 0
scoreboard players set @e[type=minecraft:armor_stand,tag=ad.stand] ad.ipe1 50
scoreboard players set @e[type=minecraft:armor_stand,tag=ad.stand] ad.ipe2 0
scoreboard players set @e[type=minecraft:armor_stand,tag=ad.stand] ad.ipe3 100
scoreboard players set @e[type=minecraft:armor_stand,tag=ad.stand] ad.iti1 60
scoreboard players set @e[type=minecraft:armor_stand,tag=ad.stand] ad.iti2 0
scoreboard players set @e[type=minecraft:armor_stand,tag=ad.stand] ad.iti3 0
