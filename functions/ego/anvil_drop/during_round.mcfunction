scoreboard players set Players ad.cl 0
execute @a[x=-33,y=3,z=81,dx=12,dy=10,dz=12,m=2,score_g.sa_min=1,score_g.sa=1,score_ad.pl_min=2,score_ad.pl=2] ~ ~ ~ scoreboard players add Players ad.cl 1
scoreboard players operation Players ad. = Players ad.cl
effect @a[m=2,score_g.sa_min=1,score_g.sa=1,score_ad.pl_min=2,score_ad.pl=2] minecraft:jump_boost 2 250 true
scoreboard players add @s ad.ti1 1
scoreboard players operation @s ad.cl = &Timer1 ad.cl
scoreboard players set @s[score_ad.cl_min=0,score_ad.cl=0] ad.ti1 -1
scoreboard players operation @s ad.cl -= @s ad.ti1
execute @s[score_ad.cl=0] ~ ~ ~ function ego:anvil_drop/run_layer_1
scoreboard players set @s[score_ad.cl=0] ad.ti1 0
scoreboard players add @s ad.ti2 1
scoreboard players operation @s ad.cl = &Timer2 ad.cl
scoreboard players set @s[score_ad.cl_min=0,score_ad.cl=0] ad.ti2 -1
scoreboard players operation @s ad.cl -= @s ad.ti2
execute @s[score_ad.cl=0] ~ ~ ~ function ego:anvil_drop/run_layer_2
scoreboard players set @s[score_ad.cl=0] ad.ti2 0
scoreboard players add @s ad.ti3 1
scoreboard players operation @s ad.cl = &Timer3 ad.cl
scoreboard players set @s[score_ad.cl_min=0,score_ad.cl=0] ad.ti3 -1
scoreboard players operation @s ad.cl -= @s ad.ti3
execute @s[score_ad.cl=0] ~ ~ ~ function ego:anvil_drop/run_layer_3
scoreboard players set @s[score_ad.cl=0] ad.ti3 0
scoreboard players add @s ad.ti 1
scoreboard players operation Time_Elapsed ad.cl = @s ad.ti
scoreboard players operation Time_Elapsed ad.cl /= 20 g.number
scoreboard players operation Time_Elapsed ad. = Time_Elapsed ad.cl
