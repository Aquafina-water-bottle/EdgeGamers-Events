from typing import NamedTuple, Iterable

from lib.location import ANVIL_DROP
from lib.floo_event import FlooEvent
from lib.const_ints import ConstInts
from lib.objective import Objectives
from lib.team import Teams
from lib.coords import Coords

"""
Fake player names:
    # holds the percent for each layer:
    &Percent1
    &Percent2
    &Percent3

    # holds the times for each layer:
    &Timer1
    &Timer2
    &Timer3

    # holds display information:
    # such that the display goes: Seconds, Additional, Decimal
    # where Additional is either 0 when decimal is 0 or 5, or null when decimal is over 5
    &Seconds1
    &Seconds2
    &Seconds3
    &Decimal1
    &Decimal2
    &Decimal3
    &Additional1
    &Additional2
    &Additional3

General notes:
    - layer 3 = walls
"""


location = ANVIL_DROP

# regen every 100 ticks, or 5 seconds
# this means 1 heart/10s
floo_event = FlooEvent(location, regen=100)
select_all = location.select_all
objectives = Objectives()
teams = Teams()
const_ints = ConstInts()
prefix = location.prefix_str


class Layer(NamedTuple):
    number: int
    percents: Iterable[int]
    struct_coords: Coords
    activate_coords: Coords

layers = [
    Layer(1, range(0, 101), Coords("-36 15 79"), Coords("-36 15 80")),
    Layer(2, range(0, 101), Coords("-36 14 79"), Coords("-36 14 80")),
    Layer(3, (0, 100), Coords("-36 13 79"), Coords("-36 13 80"))
    ]


# Anvil drop settings:
select_spawn_coords = Coords("-19 4 85 -12 9 89")
select_spawn = select_spawn_coords.selector()

select_arena_coords = Coords("-33 3 81 -21 13 93")
select_arena = select_arena_coords.selector()

door_region = Coords("-20 7 88 -20 5 86")
# specifically the region where people can stand in the doorway
# and be considered to be inside the game
door_arena_region = Coords("-21 5 86 -21 7 88")


objectives.new("""
    # The main display objective
    _ _ _

    # _stand (counter): count down the time on the countdown and counts up the gametime
    _ti _ Timer

    # _stand (state): stores the state of the round
    # 0: wait for start
    # 1: countdown
    # 2: during_round
    _st _ State

    # players (state): Player state for this event
    # 0 = being outside the map
    # 1 = part of the virus map, meaning the players are properly initialized
    #   players=1 means that they are either waiting or spectating
    # 2 = actually participating in the round
    _pl _ Player List

    # holds a bunch of fake names to be displayed for the ec
    _dp _ Display

    # _stand (temp): input a new percent for layer 1 and layer 2 respectively.
    # - default value is -1
    # - when a value is detected that is greater than -1, the percent is changed
    # this is temp since the percent values will be stored in the structure block
    # instead of a score
    _ipe1 _ Input Layer 1 %
    _ipe2 _ Input Layer 2 %

    # _stand (bool): note that although walls is either true or false, false=0 and true=100
    # as to preserve the 100% meaning in walls, and for general backwards compatability if
    # we ever want to section off walls
    _ipe3 _ Input Walls Toggle

    # _stand (temp): input a new time for all layers
    # - default value is -1
    # - when a value is greater than 0, the time is changed
    _iti1 _ Input Timer 1
    _iti2 _ Input Timer 2
    _iti3 _ Input Timer 3

    # _stand (counter): hold the timer for all 3 layers
    # every tick, this increments by one
    _ti1 _ Layer 1 Timer
    _ti2 _ Layer 2 Timer
    _ti3 _ Layer 3 Timer

    _cl _ Calculations

""", display=location.name_str)
objectives["_"].setdisplay("sidebar")

const_ints.add_constants(20, 5)

teams.new("""
    _ _
        color = green
        friendlyfire = false

    _d_y display yellow
        color = yellow

""", display=location.name_str)

