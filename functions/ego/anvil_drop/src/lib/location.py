from collections import Iterable
from typing import NamedTuple
import re

from lib.repr_utils import addrepr
from lib.assert_utils import assert_type, assert_tuple_types
from lib.hash import djb2
from lib.coords import Coords, TeleportCoords


class TextSegment(NamedTuple):
    text_slice: str
    color: str

@addrepr
class TextColorMap:
    """
    Simply a dictionary-like object that maps text to color.
    Unlike a dictionary, it cannot be indexed by getting a key, and the
        text slice can be the same as other text slices.
    Instead, the index is simply to get the position.

    A text segment is the combination of one text slice and color.
    A text slice is the text part of one text segment.

    Args:
        text_segments (Dict[str, str] or Iterable[Tuple[str, str]]):
            - Iterable that maps a piece of the text to some color so the
                piece of text displays said color
                - eg. {"C": "blue", "T": "white", "F": red"}
            - If there are repeating letters, it can be an iterable of tuples
                - eg. (("R", "blue"), ("R", "dark_green"))
                - eg. [("R", "blue"), ("R", "dark_green")]

    Attributes:
        text_segments (List[TextSegment])
    """
    def __init__(self, text_segments):
        self.text_segments = []
        assert_type(text_segments, Iterable)

        if isinstance(text_segments, dict):
            text_segments = text_segments.items()

        for text_slice, color in text_segments:
            text_segment = TextSegment(text_slice, color)
            self.text_segments.append(text_segment)

    @property
    def full_name(self):
        """
        Returns:
            str: all of the text slices joined together
        """
        return "".join(segment.text_slice for segment in self.text_segments)

    @property
    def color(self):
        """
        Returns:
            str: the first color value
        """
        return self[0].color

    def __getitem__(self, index):
        """
        Overload the index operator
        """
        return self.text_segments[index]


# pylint: disable=too-many-instance-attributes
# there are 12 instance attributes and your pathetic ass can only hold 11 >:c

@addrepr
class Location:
    """
    Holds folder location, coordinate and display info about a location

    Args:
        folder_name (str): The folder path relative to ego/functions
        formatted_name Iterable: Maps a piece of the text to some color
            so the piece of text displays said color
            - Used as an argument for `TextColorMap`
        coords (Coords): Teleport coordinates for the location

        *shortcuts (str): All possible lowercase shortcuts for the given
            location

        prefix (str or Iterable or None): Lowercase string or formatted in
            the same way as `formatted_name`
            - If it is an iterable, it is used as an argument for
                `TextColorMap`
            - If it is a string, the color will be the first color in the
                formatted name
            - If it is None, the string will be the first shortcut and the
                color will be the first color in the formatted name
            - Used to create `prefix_json` and `surround_json`

        select_coords (Coords or None): Coords to select the entire region of
            the location
            - This is none if there is no reason to select the entire region
                (generally if it isn't an event)

        is_event (bool): Whether the location is actually an event or not
            (since it can just be a location)
            - This is used by floo_network/main.fena

        tp_to_spawn (bool): Whether the location should teleport players at
            spawn to the location spawn by default or not
            - This is used by floo_network/main.fena

        use_folder_short (bool): Whether the folder name will be used as a
            shortcut for the location or not
            - This is used by floo_network/main.fena

    Attributes:
        location_id (int): A unique integer value determined by a hash function
        folder_name (str): The folder path relative to ego/functions
        name_json (str): Json objects that displays the name, connected
            together with commas
        name_str (str): The formatted name without color as a regular string
        name (TextColorMap): The fully formatted name containing easily
            accessable text to color mappings
        tp_coords (Coords): Coordinates of the location to teleport into
        tp_display_coords (str): Coordinate display of the teleport location
            (as full integer values with no rotation coordinates)
        shortcuts (list of str): All shortcuts that you can use to teleport
            to and get the book from manually
        prefix_json (str): Json objects that displays beginning prefix,
            connected together with commas
        prefix_str (str): The formatted prefix without color as a regular string
            This is generally used if "_" cannot be used before something
        prefix_disp (str): The display prefix without the color
        prefix (TextColorMap): The fully formatted prefix containing easily
            accessable text to color mappings
        select_coords (Coords or None): Coords to select the entire region
            of the location
            - This is none if there is no reason to select the entire region
                (generally if it isn't an event)
        select_all (str or None): Selector arguments to select the entire
            region (x,y,z,dx,dy,dz)
        is_event (bool): Whether the location is actually an event or not
            (since it can just be a location)
        tp_to_spawn (bool): Whether the location should teleport players at
            spawn to the location spawn by default or not
    """

    ids = set()
    members = []

    def __init__(self, folder_name, formatted_name, tp_coords, *shortcuts,
            prefix=None, select_coords=None, is_event=True,
            tp_to_spawn=True, use_folder_short=True):

        assert_type(folder_name, str)
        assert_type(formatted_name, Iterable)
        assert_type(tp_coords, Coords)
        assert_tuple_types(shortcuts, str)
        assert_type(prefix, str, Iterable, optional=True)
        assert_type(select_coords, Coords, optional=True)
        assert_type(is_event, bool)
        assert_type(tp_to_spawn, bool)
        assert_type(use_folder_short, bool)

        # Uses a unique ID value from the simple djb2 hash
        location_id = djb2(str(formatted_name) + folder_name)

        # rehash if a conflict somehow exists
        while location_id in Location.ids:
            print(f"WARNING: Rehashed {formatted_name}")
            location_id = djb2(str(location_id))
        Location.ids.add(location_id)

        self.location_id = location_id
        self.folder_name = folder_name

        name_map = TextColorMap(formatted_name)
        self.name_str = name_map.full_name
        self.name = name_map

        self.tp_coords = tp_coords

        if isinstance(tp_coords, TeleportCoords):
            display_coords = tp_coords.pos.copy()
        else:
            display_coords = tp_coords.copy()
        display_coords.to_int()
        self.tp_display_coords = str(display_coords)

        # only adds the folder name if it's not already there
        self.shortcuts = list(shortcuts)
        if folder_name not in shortcuts and use_folder_short:
            self.shortcuts.append(folder_name)

        if prefix is None:
            prefix_map = TextColorMap({shortcuts[0].upper(): name_map[0].color})
        elif isinstance(prefix, str):
            prefix_map = TextColorMap({prefix: name_map[0].color})
        else:
            prefix_map = TextColorMap(prefix)
        self.prefix = prefix_map
        self.prefix_str = prefix_map.full_name.lower()
        self.prefix_disp = prefix_map.full_name

        if select_coords is None:
            self.select_coords = self.select_all = None
        else:
            self.select_coords = select_coords
            self.select_all = select_coords.selector()

        self.is_event = is_event
        self.tp_to_spawn = tp_to_spawn

        # creates json last because it might require other attributes
        self.name_json = self._create_name_json(name_map)
        self.prefix_json = self._create_begin_json(prefix_map)

        # Adds each event to the members list
        Location.members.append(self)

    def _create_name_json(self, formatted_name):
        """
        Creates the part of the json text component that displays the full
            event name (eg. 'Pictionary')

        Args:
            formatted_name (TextColorMap)
        """
        assert_type(formatted_name, TextColorMap)

        hover_cmd = f"/@p gp.tp = {self.location_id}"

        # Nicely formatted placeholder for a json section
        # note that whitespace will be removed
        json_placeholder = r'''
            {"text":"%s","color":"%s","bold":"true",
                "hoverEvent":
                    {"action":"show_text","value":{"text":"%s","color":"%s"}},
                "clickEvent":
                    {"action":"run_command","value":"%s"}
            }'''
        json_placeholder = re.sub(r'\s+', '', json_placeholder)

        json_slices = []
        for name_slice, color in formatted_name:
            if isinstance(color, list):
                for individual_color in color:
                    json_slice = json_placeholder % (name_slice,
                            individual_color, self.name_str, color, hover_cmd)
                    json_slices.append(json_slice)
            else:
                json_slice = json_placeholder % (name_slice,
                        color, self.name_str, color, hover_cmd)
                json_slices.append(json_slice)

        return ",".join(json_slices)

    def _create_begin_json(self, formatted_prefix):
        """
        Creates the part of the json text component that displays the prefix
            - eg. '[PC]: '

        Args:
            formatted_prefix (TextColorMap)
        """
        return r'{"text":"[","color":"gray"},%s,{"text":"]","color":"gray"},{"text":": "}' % self._create_name_json(formatted_prefix)

    def surround_json(self, text):
        """
        Surrounds the inputted json objects with the beginning prefix json

        Args:
            text (str): String of json objects connected with commas
        """
        return r'{"text":"","extra":[%s,%s]}' % (self.prefix_json, text)

    def cmd_func(self, function_name):
        """
        Creates a function given the function name

        Args:
            function_name (str): The function section after the folder name
        """
        return f"function ego:{self.folder_name}/{function_name}"

    def cmd_tp(self, selector="@s"):
        """
        Teleports the selector to the current event spawn

        Args:
            selector (str): The selector that should be targetted
        """
        return f"{selector} gp.tp = {self.location_id}"

    def cmd_book(self, selector="@s[g.host=0]"):
        """
        Gives the book of the current event to the selector

        Args:
            selector (str): The selector that should be targetted
        """
        return f"{selector} gp.bk = {self.location_id}"


# Races
ICE_RACE = Location("race/ice_race",
        {"Ice Race": "aqua"},
        Coords("49 36 -45 -90 0"),
        "ir",
        select_coords=Coords("34 2 -64 175 71 -21"))

SLOW_RACE = Location("race/slow_race",
        {"Slow Race": "gray"},
        Coords("75 7 -80 90 0"),
        "sr",
        select_coords=Coords("21 4 -88 82 104 -67"))

NETHER_RACE = Location("race/nether_race",
        {"Nether Race": "red"},
        Coords("73 7 -102 90 0"),
        "nr",
        select_coords=Coords("32 4 -108 80 104 -93"))

EVIL_RACE = Location("race/evil_race",
        {"Evil Race": "gray"},
        Coords("23 7 -133 -90 0"),
        "evilr",
        select_coords=Coords("19 6 -147 107 106 -120"),
        prefix="EvilR")

DIAMOND_RACE = Location("race/diamond_race",
        {"Diamond Race": "aqua"},
        Coords("218 18 -37 -90 0"),
        "dr",
        select_coords=Coords("206 4 -57 255 104 -18"))

EMERALD_RACE = Location("race/emerald_race",
        {"Emerald Race": "green"},
        Coords("276 17 -96 90 0"),
        "er",
        select_coords=Coords("219 5 -121 294 105 -70"))

QUARTZ_RACE = Location("race/quartz_race",
        {"Quartz Race": "white"},
        Coords("229 8 -157 -90 0"),
        "qr",
        select_coords=Coords("219 4 -177 296 104 -139"))

V1_8_RACE = Location("race/1_8_race",
        {"1.8 Race": "gold"},
        Coords("242 26 -222 -90 0"),
        "18r",
        select_coords=Coords("212 7 -241 341 107 -191"))

EPIC_RACE = Location("race/epic_race",
        {"Epic Race": "gold"},
        Coords("424 12 -122 -90 0"),
        "epicr",
        select_coords=Coords("416 4 -135 476 33 -70"),
        prefix="EpicR")

SALT_RACE = Location("race/salt_race",
        {"Salt Race": "gray"},
        Coords("467 8 -33 -90 0"),
        "saltr",
        select_coords=Coords("419 4 -47 498 44 -15"),
        prefix="SaltR")

DEATH_RUN = Location("race/death_run",
        {"Death Run": "red"},
        Coords("31 20 -240 -45 0"),
        "deathr",
        select_coords=Coords("13 12 -260 148 62 -205"),
        prefix="DeathR")

DIRT_RACE = Location("race/dirt_race",
        {"Dirt Race": "gray"},
        Coords("23 15 -189.0 -90 0"),
        "dirtr",
        select_coords=Coords("18 11 -207 67 31 -162"),
        prefix="DirtR")

MYCELIUM_RACE = Location("race/mycelium_race",
        {"Mycelium Race": "dark_purple"},
        Coords("445 68 -669.0 -90 0"),
        "mr",
        select_coords=Coords("433 5 -739 473 85 -639"))

FROSTBURN_RUN = Location("race/frostburn_run",
        {"Frostburn Run": "aqua"},
        Coords("517 11 -827 90 0"),
        "fbr",
        select_coords=Coords("433 3 -841 523 53 -751"))

EVERCHANGING_RACE = Location("race/ever_changing_race",
        {"EverChanging Race": "yellow"},
        Coords("460 3 -331 -90 0"),
        "ecr",
        select_coords=Coords("450 0 -353 574 30 -308"))


# Minigames: Virus
VIRUS_1 = Location("virus/virus_1",
        {"Virus 1": "yellow"},
        Coords("-85 45 -241 -45 0"),
        "vr1", "virus1",
        select_coords=Coords("-130 4 -315 -9 64 -134"))

VIRUS_2 = Location("virus/virus_2",
        {"Virus 2": "yellow"},
        Coords("-77 56 -85 -45 0"),
        "vr2", "virus2",
        select_coords=Coords("-107 2 -130 86 102 111"))

VIRUS_MANSION = Location("virus/virus_mansion",
        {"Mansion Virus": "gold"},
        Coords("-164 14 -1211 -90 0"),
        "vrm", "virusm",
        select_coords=Coords("-46 78 -1170 -167 13 -1258"))

VIRUS_HELL = Location("virus/virus_hell",
        {"Virus Hell": "red"},
        Coords("-133 100 -1439 0 0"),
        "vrh", "virush",
        select_coords=Coords("-37 139 -1351 -150 50 -1464"))

# Minigames: PVP
PVP_OLD_CTF = Location("pvp/old_ctf",
        {"PVP: Old CTF": "red"},
        Coords("-268 49 -39 -90 0"),
        "pvp1", "oldctf")

CAPTURE_THE_FLAG = Location("capture_the_flag",
        {"Capture":"red", " the ":"white", "flag":"blue"},
        Coords("558 107 159.0 90 0"),
        "ctf",
        select_coords=Coords("548 106 148 568 111 169"),
        prefix={"c":"blue", "t":"white", "f":"blue"})

# Minigames: General
THE_PIT_3 = Location("the_pit_3",
        {"The Pit 3": "green"},
        Coords("-105 25 200 -135 0"),
        "tpl3", "tp",
        select_coords=Coords("-122 4 133 -43 54 224"))

SAND_TOMB = Location("sand_tomb",
        {"Sand Tomb": "yellow"},
        Coords("-250 25 18 -90 0"),
        "st",
        select_coords=Coords("-279 4 5 -196 54 49"))

ANVIL_DROP = Location("anvil_drop",
        {"Anvil Drop": "green"},
        Coords("-16 5 87 90 0"),
        "ad",
        select_coords=Coords("-38 0 76 -16 35 98"))

DEATH_PIT = Location("death_pit",
        {"Death Pit": "red"},
        Coords("-187 24 96 -90 0"),
        "dp")

RABBIT_BALL = Location("rabbit_ball",
        {"Rabbit":"red",
        " ":"white",
        "ball":"blue"},
        Coords("214 27 334 -90 0"),
        "rb",
        select_coords=Coords("206 73 262 320 56 415"))

# Minigames: Other
MASTERMIND = Location("mastermind/regular",
        {"Mastermind": "gold"},
        Coords("77 5 41 0 -15"),
        "mm",
        select_coords=Coords("45 3 22 109 33 87"))

MASTERMIND_HELL = Location("mastermind/hell",
        {"Mastermind Hell": "red"},
        Coords("177 5 45.0 -90 0"),
        "mmh",
        "mm2",
        select_coords=Coords("172 21 71 234 4 18"))

PICTIONARY = Location("pictionary",
        {
            "P":"light_purple",
            "i":"green",
            "c":"gold",
            "t":"yellow",
            "o":"dark_green",
            "n":"blue",
            "a":"dark_aqua",
            "r":"aqua",
            "y":"white"
        },
        Coords("161 4 180 90 0"),
        "pc",
        select_coords=Coords("110 3 148 174 18 212"),
        prefix={"PC":"dark_aqua"})

ROYAL_RUMBLE = Location("royal_rumble",
        {"Royal":"blue", " ":"white", "Rumble":"dark_green"},
        Coords("-103 19 482 -180 0"),
        "rr",
        select_coords=Coords("-153 0 299 5 110 494"),
        prefix=(("R", "blue"), ("R", "dark_green")))


# BlockHunt
BH_A_SMALL_VILLAGE = Location("blockhunt/a_small_village",
        {"A Small Village": "green"},
        Coords("-1439 12 -228 90 0"),
        "bhasv",
        use_folder_short=False,
        select_coords=Coords("-1393 38 -271 -1554 3 -109"))

BH_APOCALYPSE = Location("blockhunt/apocalypse",
        {"Apocalypse": "green"},
        Coords("-1530 45 -50"),
        "bha",
        use_folder_short=False,
        select_coords=Coords("-1565 47 -102 -1489 4 -6"))

BH_CASTLE_DE_EMMY = Location("blockhunt/castle_de_emmy",
        {"Castle de Emmy": "green"},
        Coords("-1607 64 -60 90 0"),
        "bhcde",
        use_folder_short=False,
        select_coords=Coords("-1570 73 -7 -1663 0 -102"))

BH_FOUR_CORNERS = Location("blockhunt/four_corners",
        {"Four Corners": "green"},
        Coords("-1313 102 -241 0 0"),
        "bhfc",
        use_folder_short=False,
        select_coords=Coords("-1358 111 -190 -1268 4 -280"))

BH_HASDAA = Location("blockhunt/hasdaa",
        {"HASDaa": "green"},
        Coords("-1107 24 -35 -90 0"),
        "bhhd",
        use_folder_short=False,
        select_coords=Coords("-1120 28 -10 -1044 4 -84"))

BH_HOSPITAL = Location("blockhunt/hospital",
        {"Hospital": "green"},
        Coords("-1073 26 -277 90 0"),
        "bhh",
        use_folder_short=False,
        select_coords=Coords("-1047 42 -290 -1137 3 -232"))

BH_JUNGLE = Location("blockhunt/jungle",
        {"Jungle": "green"},
        Coords("-1106 10 -97 180 0"),
        "bhj",
        use_folder_short=False,
        select_coords=Coords("-1046 55 -148 -1115 4 -88"))

BH_MASTERMIND = Location("blockhunt/mastermind",
        {"BH Mastermind": "green"},
        Coords("-1175 46 -264"),
        "bhmm", "bhm",
        use_folder_short=False,
        select_coords=Coords("-1213 51 -222 -1140 4 -294"))

BH_MUSHROOM_VILLAGE = Location("blockhunt/mushroom_village",
        {"Mushroom Village": "green"},
        Coords("-1081 43 -189 180 0"),
        "bhmv",
        use_folder_short=False,
        select_coords=Coords("-1117 48 -153 -1044 4 -225"))

BH_OLD = Location("blockhunt/old",
        {"Old": "green"},
        Coords("-1158 44 -139 180 0"),
        "bho",
        use_folder_short=False,
        select_coords=Coords("-1194 45 -175 -1122 3 -103"))

BH_PARK = Location("blockhunt/park",
        {"Park": "green"},
        Coords("-1242 37 -246 0 0"),
        "bhp",
        use_folder_short=False,
        select_coords=Coords("-1263 38 -189 -1221 4 -288"))

BH_RAINBOW = Location("blockhunt/rainbow",
        {"Rainbow": "green"},
        Coords("-1153 27 -69 -180 0"),
        "bhra",
        use_folder_short=False,
        select_coords=Coords("-1130 38 -93 -1172 4 -10"))

BH_RESORT = Location("blockhunt/resort",
        {"Resort": "green"},
        Coords("-1430 98 -46 0 0"),
        "bhre",
        use_folder_short=False,
        select_coords=Coords("-1390 101 -13 -1482 4 -105"))

BH_TRAIN_STATION = Location("blockhunt/train_station",
        {"Train Station": "green"},
        Coords("-1252 32 -88"),
        "bhts",
        use_folder_short=False,
        select_coords=Coords("-1208 34 -186 -1384 4 -10"))

BH_VIRUS_1 = Location("blockhunt/virus_1",
        {"Virus 1": "yellow"},
        Coords("-85 45 -241 -45 0"),
        "bhvr1",
        use_folder_short=False,
        select_coords=Coords("-130 4 -315 -9 64 -134"))

BH_VIRUS_2 = Location("blockhunt/virus_2",
        {"Virus 2": "yellow"},
        Coords("-77 56 -85 -45 0"),
        "bhvr2",
        use_folder_short=False,
        select_coords=Coords("-107 2 -130 86 102 111"))

BH_ZELDA = Location("blockhunt/zelda",
        {"Zelda": "green"},
        Coords("-1168 32 -191 -180 0"),
        "bhz",
        use_folder_short=False,
        select_coords=Coords("-1127 50 -218 -1213 3 -180"))


# Other
BIRTHDAY = Location("birthday",
        {"Jade's Birthday": "green"},
        Coords("801 70 135 0 0"),
        "bday",
        select_coords=Coords("838 89 133 764 4 207"))

SPAWN = Location("floo_network",
        {"Spawn": "dark_red"},
        Coords("397 17 61 90 0"),
        "spawn",
        is_event=False,
        use_folder_short=False,
        prefix="Spawn")

FLOO_NETWORK = Location("floo_network",
        {"Floo Network": "green"},
        Coords("348 4 114 -90 0"),
        "cmd", "floo", "diagonally",
        prefix="Floo",
        select_coords=Coords("350 14 13 440 44 103"),
        is_event=False)

TEAMSPEAK_TOKEN = Location("ts_token",
        {"Teamspeak Token": "dark_aqua"},
        Coords("549 20 42"),
        "ts",
        "tstoken",
        prefix="TSToken",
        select_coords=Coords("544 19 37 554 24 47"),
        tp_to_spawn=False)

MINIGAME_HUB = Location("floo_network",
        {"Minigame Hub": "green"},
        Coords("340 15 202 -35 0"),
        "mghub",
        prefix="MGhub",
        is_event=False,
        use_folder_short=False)

BLOCKHUNT_HUB = Location("floo_network",
        {"Blockhunt hub": "light_purple"},
        Coords("316 10 148"),
        "bhhub",
        prefix="BHhub",
        is_event=False,
        use_folder_short=False)

OTHER_GAMES_HUB = Location("floo_network",
        {"Other Games Hub": "gold"},
        Coords("276 5 194.0 -90 0"),
        "otherhub",
        prefix="Otherhub",
        is_event=False,
        use_folder_short=False)

RACE_HUB = Location("floo_network",
        {"Race Hub": "dark_aqua"},
        Coords("366 16 155.0 -90 0"),
        "racehub",
        prefix="RaceHub",
        is_event=False,
        use_folder_short=False)


