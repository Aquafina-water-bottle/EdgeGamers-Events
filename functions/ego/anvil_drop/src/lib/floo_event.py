from lib.assert_utils import assert_type
from lib.repr_utils import addrepr
from lib.command_group import CommandGroup
from lib.location import Location, FLOO_NETWORK

"""
Contains events related to the Floo Network, the main event teleportation and
maintenence system for the eGO server.

This entire module is useless if you're not working with the eGO server.
"""


@addrepr
class FlooEvent(CommandGroup):
    """
    Stores the general commands used by the Floo Network for every event.
        - Has multiple cmd_* methods to set various options
        - This isn't inheriting from Location because all Location objects are
            already created
        - Used as the main `event` variable for most main.fena event files

    Args:
        location (Location): Location of the floo event to hold the display
            name and teleport coords
        **options: Holds any floo event options

    Attributes:
        location (Location): Location of the floo event to hold the display
            name and teleport coords
        options (Dict[str, str]): Holds any floo event options as specified
            below
        summon_spawn (str): The beginning part of the execute command to ensure
            that the armor stand is summoned above the global python stand
        valid_options (Dict[str, List[str]]): Holds any floo event options as
            specified below
        required_funcs (Set[Str]): All the required functions for creating any
            floo event

    Floo Event Options:
        pvp:
            "false_teams" (0): Turn pvp off by having everyone join the
                g.nopvp team (default for when not running an event)
            "false_weak" (1): Turn pvp off by giving weakness to everyone
                (default for events)
            "true" (2): Does nothing

        saturation:
            "false" (0): Does nothing
            "true" (1): Gives everyone a saturation effect (default for all)

        regen:
            "false" (0): Sets naturalRegeneration to false
            "true" (0): Sets naturalRegeneration to true (default for all)
            POS_INT (#): Sets naturalRegeneration to false and gives 1/2 heart
                every specified number of ticks

    Example:
        FlooRace = FlooEvent(ICE_RACE)
        FlooPVP = FlooEvent(CAPTURE_THE_FLAG, pvp="false")
        FlooDeathPit = FlooEvent(DEATH_PIT, saturation="false")
    """

    # The set of required functions that must be ran for
    # literally any event to work
    required_funcs = {"cmd_init", "cmd_main", "cmd_term"}

    valid_options = {
        "pvp": ["false_teams", "false_weak", "true"],
        "saturation": ["false", "true"],
        "regen": ["POS_INT", "true", "false"],
    }

    def __init__(self, location, **options):
        super().__init__()
        assert_type(location, Location)

        self.location = location
        self.options = {}
        self.used_funcs = {}
        self.summon_spawn = "@e[gp.stand] ~ ~1 ~"

        for option, value in options.items():
            self.set_option(option, value)

    def set_option(self, option: str, option_value: str):
        """
        Sets a floo network option

        Args:
            option (str)
            option_value (str)
        """
        if option not in FlooEvent.valid_options:
            raise SyntaxError(f"Invalid option name '{option}' for the floo network")

        if (option_value not in FlooEvent.valid_options[option] and
                not ("POS_INT" in FlooEvent.valid_options[option] and
                     isinstance(option_value, int) and option_value > 0)):

            raise SyntaxError(f"Invalid option value for option '{option}' for the floo network: '{option_value}'")

        self.options[option] = option_value

    @staticmethod
    def cmd_set_score(objective, value):
        """
        Creates a command to set the score of the global python armor stand
        with an arbitrary objective
        """
        assert_type(objective, str)
        assert_type(value, int)
        return f"@e[type=armor_stand,gp.stand] {objective} = {value}"

    def cmd_option(self, option, option_value=None):
        """
        Gets a command string for a given option
            - Doesn't use the commands attribute because that is being modified
                currently with cmd_init()

        Args:
            option (str)
        """
        assert option in self.valid_options, f"Option {option} is not inside {tuple(self.valid_options)}"

        method_name = f"_cmd_option_{option}"
        cmd_option_get = getattr(self, method_name)
        return cmd_option_get(option_value)

    def _cmd_option_pvp(self, option_value=None):
        default = "false_weak"

        if option_value is None:
            pvp_option = self.options["pvp"] if "pvp" in self.options else default
        else:
            pvp_option = option_value

        pvp_score = FlooEvent.valid_options["pvp"].index(pvp_option)
        return FlooEvent.cmd_set_score("gp.pvp", pvp_score)

    def _cmd_option_saturation(self, option_value=None):
        default = "true"

        if option_value is None:
            saturation_option = self.options["saturation"] if "saturation" in self.options else default
        else:
            saturation_option = option_value

        saturation_score = FlooEvent.valid_options["saturation"].index(saturation_option)
        return FlooEvent.cmd_set_score("gp.sat", saturation_score)

    def _cmd_option_regen(self, option_value=None):
        commands = []
        default = "true"

        if option_value is None:
            regen_option = self.options["regen"] if "regen" in self.options else default
        else:
            regen_option = option_value

        if regen_option == "true":
            commands.append("gamerule naturalRegeneration true")
            commands.append(FlooEvent.cmd_set_score("gp.rgt", 0))
        elif regen_option == "false":
            commands.append("gamerule naturalRegeneration false")
            commands.append(FlooEvent.cmd_set_score("gp.rgt", 0))
        else:
            # positive integer
            commands.append("gamerule naturalRegeneration false")
            commands.append(FlooEvent.cmd_set_score("gp.rgt", regen_option))

        return CommandGroup.cmd_from_iterable(commands)

    def cmd_start_bin_search(self, num_list, folder_name): # pylint: disable=no-self-use
        """
        Simply provides the base function command for conducting a
            binary search

        Args:
            num_list (List[int]): A full sorted list of integers that will
                be used to search through the binary search
            folder_name (str): The folder that this search should be in
                - Literally all binary search mcfunctions should be in their
                    own folder anyways since it will mostly look like spam
        """
        if folder_name is None:
            return f"function search_{num_list[0]}_to_{num_list[-1]}"
        return f"function {folder_name}/search_{num_list[0]}_to_{num_list[-1]}"

    def _cmd_search_function(self, num_list, objective, arbitrary=False): # pylint: disable=no-self-use
        """
        Constructs the function commands splitting from the original
            mcfunction

        Args:
            num_list (List[Int]): All numbers that should be searched through
            objective (str): The objective that the numbers are assigned to
            arbitrary (bool): Whether the objective is used on itself or on anyone
        """
        # TODO: 1.13 arbitrary requires c=1
        selector = "@a" if arbitrary else "@s"
        num_min = num_list[0]
        num_max = num_list[-1]
        if num_min == num_max:
            # return f"function use_{num_min} if @s[{objective}={num_min}]"
            return f"function use_{num_min} if {selector}[{objective}={num_min}]"
        return f"function search_{num_min}_to_{num_max} if {selector}[{objective}={num_min}..{num_max}]"

    # constructs a binary search
    def mfunc_bin_search(self, num_list, objective, arbitrary=False, debug=True):
        """
        Args:
            num_list (list of sorted ints): Binary search to iterate through
            objective (str): Objective which will check the score number
            arbitrary (bool): Whether the objective is used on itself or on anyone
        """

        # base case
        if len(num_list) == 1:
            return ""

        split_num = len(num_list) // 2
        left_list = num_list[:split_num]
        right_list = num_list[split_num:]

        cmds = []
        if debug:
            cmds.append(f"!mfunc search_{num_list[0]}_to_{num_list[-1]}:")
        else:
            cmds.append(f"!mfunc search_{num_list[0]}_to_{num_list[-1]}: debug=false")
        cmds.append(self._cmd_search_function(left_list, objective, arbitrary=arbitrary))
        cmds.append(self._cmd_search_function(right_list, objective, arbitrary=arbitrary))

        left_cmds = self.mfunc_bin_search(left_list, objective, arbitrary=arbitrary)
        right_cmds = self.mfunc_bin_search(right_list, objective, arbitrary=arbitrary)
        return CommandGroup.cmd_from_args(CommandGroup.cmd_from_iterable(cmds), left_cmds, right_cmds)

    def cmd_main(self):
        """
        Used in the main loop to set everyone's floo network id value
        for spawning and teleportation
        """
        # global select all, specific for each event
        select_all = self.location.select_all
        event_id = self.location.location_id

        self.commands.append("@a g.sa = 0")
        self.commands.append(f"@a[{select_all}] g.sa = 1")

        self.commands.append("@a[g.sa=1] gp.id += 0")
        self.commands.append(f"@a[g.sa=1, gp.id=(..-{event_id+1})] gp.id = {event_id}")
        self.commands.append(f"@a[g.sa=1, gp.id=(-{event_id-1}..)] gp.id = {event_id}")

        return self.cmd_output()

    def mfunc_input(self):
        # return CommandGroup.cmd_from_args(self._funcs_input_init(), self._mfunc_input_term())
        return self._mfunc_input_init()

    def _mfunc_input_init(self):
        """
        Sets up the init input mcfunction

        If another game is currently running, it immediately stops it
        Otherwise, it runs the main init function
        """

        # gp.stand (state): holds the event id that is currently running
        # 0: No event is running
        # >= 1: An event is running with said id
        self.commands.append("!mfunc init:")
        self.commands.append("tag @e[type=armor_stand,gp.stand,gp.cgi=0] + gp.can_init")
        # self.commands.append(f"if(@e[type=armor_stand,gp.stand,gp.can_init]): {self.location.cmd_func('cmd_init')}")
        # self.commands.append(f"if(@e[type=armor_stand,gp.stand,!gp.can_init]): {FLOO_NETWORK.cmd_func('stop_events')}")
        self.commands.append(f"{self.location.cmd_func('init')} if @e[type=armor_stand,gp.can_init]")
        self.commands.append(f"@e[type=armor_stand,gp.stand]: {FLOO_NETWORK.cmd_func('stop_events')} if @s[!gp.can_init]")
        self.commands.append("tag @e[type=armor_stand,gp.can_init] - gp.can_init")
        return self.cmd_output()

    def cmd_init(self):
        # terminates any other games if they are running
        # self.commands.append("function ego:floo_network/stop_events")

        # sets the player as the host
        self.commands.append("@s g.host = 1")

        # setting up the teleport id
        if self.location.tp_to_spawn:
            self.commands.append(FlooEvent.cmd_set_score("gp.tp", self.location.location_id))

        # gets all the options
        for option in self.valid_options:
            self.commands.append(self.cmd_option(option))

        # global select all, specific for each event
        self.commands.append("@a g.sa = 0")
        self.commands.append(f"@a[{self.location.select_all}] g.sa = 1")

        self.commands.append(r'tellraw @a[gr.ec=1] {"text":"","extra":[%s,%s,%s]}' % (
            self.location.prefix_json,
            self.location.name_json,
            r'{"text":" has started!","color":"green"}')
        )

        self.commands.append(f"@e[type=armor_stand,gp.stand,gp.cgi=0] gp.cgi = {self.location.location_id}")
        return self.cmd_output()

    def _mfunc_input_term(self):
        # confirms termination
        self.commands.append("!mfunc term:")

        text_list = [
            self.location.prefix_json,
            r'''{"text":"Click here","color":"yellow","bold":"true",
                "clickEvent":{"action":"run_command","value":"/%s"}
                }''' % self.location.cmd_func("term"),

            r'{"text":" to confirm the termination of ","color":"gray"}',
            self.location.name_json,
            ]

        text_str = ",".join(text_list)
        self.commands.append(r'tellraw @s {"text":"","extra":[%s]}' % text_str)
        return self.cmd_output()

    def cmd_term(self):
        """
        Resets all options for the floo network
        """
        # resets host
        self.commands.append("* reset g.host")

        # Resets all options
        self.commands.append(self.cmd_option("pvp", "false_teams"))
        self.commands.append(self.cmd_option("saturation", "true"))
        self.commands.append(self.cmd_option("regen", "true"))

        # Sets the same game id and teleport status to 0
        self.commands.append(f"@e[type=armor_stand,gp.stand,gp.cgi={self.location.location_id}] gp.tp = 0")
        self.commands.append(f"@e[type=armor_stand,gp.stand,gp.cgi={self.location.location_id}] gp.cgi = 0")

        self.commands.append(r'tellraw @a[gr.ec=1] {"text":"","extra":[%s,%s,%s]}' % (
            self.location.prefix_json,
            self.location.name_json,
            r'{"text":" has stopped!","color":"red"}')
        )

        return self.cmd_output()

