scoreboard players operation &Timer1 ad.cl = @s ad.iti1
scoreboard players operation &Seconds1 ad.cl = &Timer1 ad.cl
scoreboard players operation &Seconds1 ad.cl /= 20 g.number
scoreboard players operation &Decimal1 ad.cl = &Timer1 ad.cl
scoreboard players operation &Decimal1 ad.cl %= 20 g.number
scoreboard players operation &Decimal1 ad.cl *= 5 g.number
scoreboard players reset &Additional1 ad.cl
scoreboard players operation @s ad.cl = &Decimal1 ad.cl
execute @s[score_ad.cl_min=5,score_ad.cl=5] ~ ~ ~ scoreboard players set &Additional1 ad.cl 0
scoreboard players set @s ad.iti1 -1
