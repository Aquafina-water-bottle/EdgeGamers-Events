scoreboard players set Players ad.cl 0
execute @a[x=-33,y=3,z=81,dx=12,dy=10,dz=12,m=2,score_g.sa_min=1,score_g.sa=1,score_ad.pl_min=2,score_ad.pl=2] ~ ~ ~ scoreboard players add Players ad.cl 1
scoreboard players operation Players ad. = Players ad.cl
effect @a[m=2,score_g.sa_min=1,score_g.sa=1,score_ad.pl_min=2,score_ad.pl=2] minecraft:jump_boost 2 250 true
scoreboard players remove @s[score_ad.ti_min=1] ad.ti 1
scoreboard players operation Countdown ad.cl = @s ad.ti
scoreboard players operation Countdown ad.cl /= 20 g.number
scoreboard players add Countdown ad.cl 1
scoreboard players operation Countdown ad. = Countdown ad.cl
execute @s[score_ad.ti_min=100,score_ad.ti=100] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"AD","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Anvil Drop","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2071894910"}},{"text":"]","color":"gray"},{"text":": "},{"text":"5","color":"yellow","bold":"true"}]}
execute @s[score_ad.ti_min=100,score_ad.ti=100] ~ ~ ~ execute @a ~ ~ ~ playsound minecraft:block.note.pling voice @s ~ ~ ~ 0.5
execute @s[score_ad.ti_min=80,score_ad.ti=80] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"AD","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Anvil Drop","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2071894910"}},{"text":"]","color":"gray"},{"text":": "},{"text":"4","color":"yellow","bold":"true"}]}
execute @s[score_ad.ti_min=80,score_ad.ti=80] ~ ~ ~ execute @a ~ ~ ~ playsound minecraft:block.note.pling voice @s ~ ~ ~ 1
execute @s[score_ad.ti_min=60,score_ad.ti=60] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"AD","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Anvil Drop","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2071894910"}},{"text":"]","color":"gray"},{"text":": "},{"text":"3","color":"yellow","bold":"true"}]}
execute @s[score_ad.ti_min=60,score_ad.ti=60] ~ ~ ~ execute @a ~ ~ ~ playsound minecraft:block.note.pling voice @s ~ ~ ~ 1.5
execute @s[score_ad.ti_min=40,score_ad.ti=40] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"AD","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Anvil Drop","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2071894910"}},{"text":"]","color":"gray"},{"text":": "},{"text":"2","color":"yellow","bold":"true"}]}
execute @s[score_ad.ti_min=40,score_ad.ti=40] ~ ~ ~ execute @a ~ ~ ~ playsound minecraft:block.note.pling voice @s ~ ~ ~ 2
execute @s[score_ad.ti_min=20,score_ad.ti=20] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"AD","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Anvil Drop","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2071894910"}},{"text":"]","color":"gray"},{"text":": "},{"text":"1","color":"yellow","bold":"true"}]}
execute @s[score_ad.ti_min=20,score_ad.ti=20] ~ ~ ~ execute @a ~ ~ ~ playsound minecraft:block.note.pling voice @s ~ ~ ~ 2
execute @s[score_ad.ti_min=0,score_ad.ti=0] ~ ~ ~ function ego:anvil_drop/end_countdown
