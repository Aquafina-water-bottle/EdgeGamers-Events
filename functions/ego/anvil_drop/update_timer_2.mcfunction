scoreboard players operation &Timer2 ad.cl = @s ad.iti2
scoreboard players operation &Seconds2 ad.cl = &Timer2 ad.cl
scoreboard players operation &Seconds2 ad.cl /= 20 g.number
scoreboard players operation &Decimal2 ad.cl = &Timer2 ad.cl
scoreboard players operation &Decimal2 ad.cl %= 20 g.number
scoreboard players operation &Decimal2 ad.cl *= 5 g.number
scoreboard players reset &Additional2 ad.cl
scoreboard players operation @s ad.cl = &Decimal2 ad.cl
execute @s[score_ad.cl_min=5,score_ad.cl=5] ~ ~ ~ scoreboard players set &Additional2 ad.cl 0
scoreboard players set @s ad.iti2 -1
