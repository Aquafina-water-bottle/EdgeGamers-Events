scoreboard players operation &Countdown vrm.cl = @s vrm.icd
scoreboard players operation &Countdown vrm.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds vrm.cl = @s vrm.icd
scoreboard players operation &CountdownMinutes vrm.cl = @s vrm.icd
scoreboard players operation &CountdownMinutes vrm.cl /= 60 g.number
scoreboard players operation @s vrm.icd %= 60 g.number
scoreboard players operation &CountdownSeconds vrm.cl = @s vrm.icd
scoreboard players reset &CountdownAdditional vrm.cl
execute @s[score_vrm.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional vrm.cl 0
scoreboard players set @s vrm.icd 0
