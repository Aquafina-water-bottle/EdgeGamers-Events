scoreboard players operation &GlowingUntil vrm.cl = @s vrm.igl
scoreboard players operation &GlowingUntil vrm.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds vrm.cl = @s vrm.igl
scoreboard players operation &GlowingUntilMinutes vrm.cl = @s vrm.igl
scoreboard players operation &GlowingUntilMinutes vrm.cl /= 60 g.number
scoreboard players operation @s vrm.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds vrm.cl = @s vrm.igl
scoreboard players reset &GlowingUntilAdditional vrm.cl
execute @s[score_vrm.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional vrm.cl 0
scoreboard players set @s vrm.igl 0
