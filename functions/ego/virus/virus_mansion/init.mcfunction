scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1303306030
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-46,y=78,z=-1170,dx=-121,dy=-65,dz=-88] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRM","color":"gold","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Mansion Virus","color":"gold"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1303306030"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Mansion Virus","color":"gold","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Mansion Virus","color":"gold"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1303306030"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1303306030
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add vrm. dummy Mansion Virus
scoreboard objectives setdisplay sidebar vrm.
scoreboard objectives add vrm.pl dummy Mansion Virus Player List
scoreboard objectives add vrm.ti dummy Mansion Virus Timer
scoreboard objectives add vrm.chi dummy Mansion Virus Count Hiders
scoreboard objectives add vrm.cvr dummy Mansion Virus Count Virus
scoreboard objectives add vrm.gl dummy Mansion Virus Virus Glowing
scoreboard objectives add vrm.cl dummy Mansion Virus Calculations
scoreboard objectives add vrm.igl dummy Mansion Virus Input Glowing
scoreboard objectives add vrm.igt dummy Mansion Virus Input Game Time
scoreboard objectives add vrm.icd dummy Mansion Virus Input Countdown
scoreboard objectives add vrm.st dummy Mansion Virus State
scoreboard teams add vrm.h Mansion Virus Hiders
scoreboard teams option vrm.h friendlyfire false
scoreboard teams option vrm.h collisionRule never
scoreboard teams option vrm.h deathMessageVisibility always
scoreboard teams option vrm.h nametagVisibility never
scoreboard teams option vrm.h color green
scoreboard teams add vrm.v Mansion Virus Virus
scoreboard teams option vrm.v friendlyfire false
scoreboard teams option vrm.v collisionRule never
scoreboard teams option vrm.v deathMessageVisibility always
scoreboard teams option vrm.v color yellow
scoreboard teams add vrm.d_y Mansion Virus Display Yellow
scoreboard teams option vrm.d_y color yellow
scoreboard teams add vrm.d_g Mansion Virus Display Green
scoreboard teams option vrm.d_g color green
scoreboard teams join vrm.d_y Countdown
scoreboard teams join vrm.d_y Minutes
scoreboard teams join vrm.d_y Seconds
scoreboard teams join vrm.d_y Virus
scoreboard teams join vrm.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["vrm.stand","vrm.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.igl 300
