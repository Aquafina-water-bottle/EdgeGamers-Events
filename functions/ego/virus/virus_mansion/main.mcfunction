scoreboard players set @a g.sa 0
scoreboard players set @a[x=-46,y=78,z=-1170,dx=-121,dy=-65,dz=-88] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-1303306031] gp.id 1303306030
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-1303306029] gp.id 1303306030
kill @e[x=-46,y=78,z=-1170,dx=-121,dy=-65,dz=-88,type=minecraft:item,tag=!vrm.entity]
execute @e[type=minecraft:armor_stand,score_vrm.icd_min=1,tag=vrm.stand] ~ ~ ~ function ego:virus/virus_mansion/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_vrm.igt_min=1,tag=vrm.stand] ~ ~ ~ function ego:virus/virus_mansion/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_vrm.igl_min=1,tag=vrm.stand] ~ ~ ~ function ego:virus/virus_mansion/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"vrm.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"vrm.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"vrm.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"vrm.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"vrm.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"vrm.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"vrm.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"vrm.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"vrm.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"vrm.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"vrm.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"vrm.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_vrm.pl_min=1,score_vrm.pl=2] vrm.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_vrm.pl_min=1,score_vrm.pl=2] vrm.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_vrm.pl_min=1,score_vrm.pl=2] vrm.pl 0
scoreboard players add @a vrm.pl 0
execute @e[type=minecraft:armor_stand,score_vrm.st_min=0,score_vrm.st=0,tag=vrm.stand] ~ ~ ~ function ego:virus/virus_mansion/wait_for_start
execute @e[type=minecraft:armor_stand,score_vrm.st_min=1,score_vrm.st=1,tag=vrm.stand] ~ ~ ~ function ego:virus/virus_mansion/countdown
execute @e[type=minecraft:armor_stand,score_vrm.st_min=2,score_vrm.st=2,tag=vrm.stand] ~ ~ ~ function ego:virus/virus_mansion/during_round
execute @e[type=minecraft:armor_stand,score_vrm.st_min=3,score_vrm.st=3,tag=vrm.stand] ~ ~ ~ function ego:virus/virus_mansion/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.chi 0
execute @a[m=2,team=vrm.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.chi 1
scoreboard players operation Hiders vrm. = @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.cvr 0
execute @a[m=2,team=vrm.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.cvr 1
scoreboard players operation Virus vrm. = @e[type=minecraft:armor_stand,tag=vrm.stand] vrm.cvr
minecraft:tp @a[x=-62,y=25,z=-1236,dx=0,dy=1,dz=0,m=2,score_vrm.pl_min=2,score_vrm.pl=2] -76 31 -1239
