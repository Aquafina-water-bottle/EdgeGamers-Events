scoreboard players operation &GameTime vrm.cl = @s vrm.igt
scoreboard players operation &GameTime vrm.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds vrm.cl = @s vrm.igt
scoreboard players operation &GameTimeMinutes vrm.cl = @s vrm.igt
scoreboard players operation &GameTimeMinutes vrm.cl /= 60 g.number
scoreboard players operation @s vrm.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds vrm.cl = @s vrm.igt
scoreboard players reset &GameTimeAdditional vrm.cl
execute @s[score_vrm.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional vrm.cl 0
scoreboard players set @s vrm.igt 0
