scoreboard players set @a g.sa 0
scoreboard players set @a[x=-130,y=4,z=-315,dx=121,dy=60,dz=181] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-2115807379] gp.id 2115807378
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-2115807377] gp.id 2115807378
kill @e[x=-130,y=4,z=-315,dx=121,dy=60,dz=181,type=minecraft:item,tag=!vr1.entity]
execute @e[type=minecraft:armor_stand,score_vr1.icd_min=1,tag=vr1.stand] ~ ~ ~ function ego:virus/virus_1/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_vr1.igt_min=1,tag=vr1.stand] ~ ~ ~ function ego:virus/virus_1/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_vr1.igl_min=1,tag=vr1.stand] ~ ~ ~ function ego:virus/virus_1/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"vr1.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"vr1.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"vr1.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"vr1.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"vr1.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"vr1.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"vr1.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"vr1.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"vr1.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"vr1.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"vr1.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"vr1.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_vr1.pl_min=1,score_vr1.pl=2] vr1.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_vr1.pl_min=1,score_vr1.pl=2] vr1.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_vr1.pl_min=1,score_vr1.pl=2] vr1.pl 0
scoreboard players add @a vr1.pl 0
execute @e[type=minecraft:armor_stand,score_vr1.st_min=0,score_vr1.st=0,tag=vr1.stand] ~ ~ ~ function ego:virus/virus_1/wait_for_start
execute @e[type=minecraft:armor_stand,score_vr1.st_min=1,score_vr1.st=1,tag=vr1.stand] ~ ~ ~ function ego:virus/virus_1/countdown
execute @e[type=minecraft:armor_stand,score_vr1.st_min=2,score_vr1.st=2,tag=vr1.stand] ~ ~ ~ function ego:virus/virus_1/during_round
execute @e[type=minecraft:armor_stand,score_vr1.st_min=3,score_vr1.st=3,tag=vr1.stand] ~ ~ ~ function ego:virus/virus_1/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.chi 0
execute @a[m=2,team=vr1.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.chi 1
scoreboard players operation Hiders vr1. = @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.cvr 0
execute @a[m=2,team=vr1.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.cvr 1
scoreboard players operation Virus vr1. = @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.cvr
