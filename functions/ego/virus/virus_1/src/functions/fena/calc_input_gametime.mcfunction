scoreboard players operation &GameTime vr1.cl = @s vr1.igt
scoreboard players operation &GameTime vr1.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds vr1.cl = @s vr1.igt
scoreboard players operation &GameTimeMinutes vr1.cl = @s vr1.igt
scoreboard players operation &GameTimeMinutes vr1.cl /= 60 g.number
scoreboard players operation @s vr1.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds vr1.cl = @s vr1.igt
scoreboard players reset &GameTimeAdditional vr1.cl
execute @s[score_vr1.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional vr1.cl 0
scoreboard players set @s vr1.igt 0
