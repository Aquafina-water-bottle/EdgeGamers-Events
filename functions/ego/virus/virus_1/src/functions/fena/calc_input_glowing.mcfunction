scoreboard players operation &GlowingUntil vr1.cl = @s vr1.igl
scoreboard players operation &GlowingUntil vr1.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds vr1.cl = @s vr1.igl
scoreboard players operation &GlowingUntilMinutes vr1.cl = @s vr1.igl
scoreboard players operation &GlowingUntilMinutes vr1.cl /= 60 g.number
scoreboard players operation @s vr1.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds vr1.cl = @s vr1.igl
scoreboard players reset &GlowingUntilAdditional vr1.cl
execute @s[score_vr1.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional vr1.cl 0
scoreboard players set @s vr1.igl 0
