scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 2115807378
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-130,y=4,z=-315,dx=121,dy=60,dz=181] g.sa 1
tellraw @a[score_g.ec_min=1,score_g.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VR1","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus 1","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2115807378"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Virus 1","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus 1","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2115807378"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 2115807378
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add vr1. dummy Virus 1
scoreboard objectives setdisplay sidebar vr1.
scoreboard objectives add vr1.pl dummy Virus 1 Player List
scoreboard objectives add vr1.ti dummy Virus 1 Timer
scoreboard objectives add vr1.chi dummy Virus 1 Count Hiders
scoreboard objectives add vr1.cvr dummy Virus 1 Count Virus
scoreboard objectives add vr1.gl dummy Virus 1 Virus Glowing
scoreboard objectives add vr1.cl dummy Virus 1 Calculations
scoreboard objectives add vr1.igl dummy Virus 1 Input Glowing
scoreboard objectives add vr1.igt dummy Virus 1 Input Game Time
scoreboard objectives add vr1.icd dummy Virus 1 Input Countdown
scoreboard objectives add vr1.st dummy Virus 1 State
scoreboard teams add vr1.h Virus 1 Hiders
scoreboard teams option vr1.h friendlyfire false
scoreboard teams option vr1.h collisionRule never
scoreboard teams option vr1.h deathMessageVisibility always
scoreboard teams option vr1.h nametagVisibility never
scoreboard teams option vr1.h color green
scoreboard teams add vr1.v Virus 1 Virus
scoreboard teams option vr1.v friendlyfire false
scoreboard teams option vr1.v collisionRule never
scoreboard teams option vr1.v deathMessageVisibility always
scoreboard teams option vr1.v color yellow
scoreboard teams add vr1.d_y Virus 1 Display Yellow
scoreboard teams option vr1.d_y color yellow
scoreboard teams add vr1.d_g Virus 1 Display Green
scoreboard teams option vr1.d_g color green
scoreboard teams join vr1.d_y Countdown
scoreboard teams join vr1.d_y Minutes
scoreboard teams join vr1.d_y Seconds
scoreboard teams join vr1.d_y Virus
scoreboard teams join vr1.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["vr1.stand","vr1.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=vr1.stand] vr1.igl 300
