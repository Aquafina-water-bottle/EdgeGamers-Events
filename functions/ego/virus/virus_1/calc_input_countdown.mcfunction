scoreboard players operation &Countdown vr1.cl = @s vr1.icd
scoreboard players operation &Countdown vr1.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds vr1.cl = @s vr1.icd
scoreboard players operation &CountdownMinutes vr1.cl = @s vr1.icd
scoreboard players operation &CountdownMinutes vr1.cl /= 60 g.number
scoreboard players operation @s vr1.icd %= 60 g.number
scoreboard players operation &CountdownSeconds vr1.cl = @s vr1.icd
scoreboard players reset &CountdownAdditional vr1.cl
execute @s[score_vr1.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional vr1.cl 0
scoreboard players set @s vr1.icd 0
