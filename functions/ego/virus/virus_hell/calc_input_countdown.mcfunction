scoreboard players operation &Countdown vrh.cl = @s vrh.icd
scoreboard players operation &Countdown vrh.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds vrh.cl = @s vrh.icd
scoreboard players operation &CountdownMinutes vrh.cl = @s vrh.icd
scoreboard players operation &CountdownMinutes vrh.cl /= 60 g.number
scoreboard players operation @s vrh.icd %= 60 g.number
scoreboard players operation &CountdownSeconds vrh.cl = @s vrh.icd
scoreboard players reset &CountdownAdditional vrh.cl
execute @s[score_vrh.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional vrh.cl 0
scoreboard players set @s vrh.icd 0
