scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1521378220
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-37,y=139,z=-1351,dx=-113,dy=-89,dz=-113] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Virus Hell","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1521378220
scoreboard players set 100 g.number 100
scoreboard players set 140 g.number 140
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add vrh. dummy Virus Hell
scoreboard objectives setdisplay sidebar vrh.
scoreboard objectives add vrh.pl dummy Virus Hell Player List
scoreboard objectives add vrh.ti dummy Virus Hell Timer
scoreboard objectives add vrh.chi dummy Virus Hell Count Hiders
scoreboard objectives add vrh.cvr dummy Virus Hell Count Virus
scoreboard objectives add vrh.gl dummy Virus Hell Virus Glowing
scoreboard objectives add vrh.cl dummy Virus Hell Calculations
scoreboard objectives add vrh.igl dummy Virus Hell Input Glowing
scoreboard objectives add vrh.igt dummy Virus Hell Input Game Time
scoreboard objectives add vrh.icd dummy Virus Hell Input Countdown
scoreboard objectives add vrh.st dummy Virus Hell State
scoreboard teams add vrh.h Virus Hell Hiders
scoreboard teams option vrh.h friendlyfire false
scoreboard teams option vrh.h collisionRule never
scoreboard teams option vrh.h deathMessageVisibility always
scoreboard teams option vrh.h nametagVisibility never
scoreboard teams option vrh.h color green
scoreboard teams add vrh.v Virus Hell Virus
scoreboard teams option vrh.v friendlyfire false
scoreboard teams option vrh.v collisionRule never
scoreboard teams option vrh.v deathMessageVisibility always
scoreboard teams option vrh.v color yellow
scoreboard teams add vrh.d_y Virus Hell Display Yellow
scoreboard teams option vrh.d_y color yellow
scoreboard teams add vrh.d_g Virus Hell Display Green
scoreboard teams option vrh.d_g color green
scoreboard teams join vrh.d_y Countdown
scoreboard teams join vrh.d_y Minutes
scoreboard teams join vrh.d_y Seconds
scoreboard teams join vrh.d_y Virus
scoreboard teams join vrh.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["vrh.stand","vrh.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=vrh.stand] vrh.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=vrh.stand] vrh.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=vrh.stand] vrh.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=vrh.stand] vrh.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=vrh.stand] vrh.igl 300
summon ghast -133 109 -1439 {Tags:["vrh.entity","vrh.ghast"],NoAI:1b,Invulnerable:1b}
scoreboard teams join g.nopush @e[type=minecraft:ghast,tag=vrh.ghast]
