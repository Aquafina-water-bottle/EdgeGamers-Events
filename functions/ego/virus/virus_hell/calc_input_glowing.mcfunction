scoreboard players operation &GlowingUntil vrh.cl = @s vrh.igl
scoreboard players operation &GlowingUntil vrh.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds vrh.cl = @s vrh.igl
scoreboard players operation &GlowingUntilMinutes vrh.cl = @s vrh.igl
scoreboard players operation &GlowingUntilMinutes vrh.cl /= 60 g.number
scoreboard players operation @s vrh.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds vrh.cl = @s vrh.igl
scoreboard players reset &GlowingUntilAdditional vrh.cl
execute @s[score_vrh.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional vrh.cl 0
scoreboard players set @s vrh.igl 0
