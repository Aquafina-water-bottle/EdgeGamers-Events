scoreboard players operation &GameTime vrh.cl = @s vrh.igt
scoreboard players operation &GameTime vrh.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds vrh.cl = @s vrh.igt
scoreboard players operation &GameTimeMinutes vrh.cl = @s vrh.igt
scoreboard players operation &GameTimeMinutes vrh.cl /= 60 g.number
scoreboard players operation @s vrh.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds vrh.cl = @s vrh.igt
scoreboard players reset &GameTimeAdditional vrh.cl
execute @s[score_vrh.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional vrh.cl 0
scoreboard players set @s vrh.igt 0
