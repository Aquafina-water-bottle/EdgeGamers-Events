particle reddust -133 102.0 -1432 1 1 0 0 20 force @a
scoreboard players set @a vrh.cl 0
execute @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.ti=179] ~ ~ ~ detect ~ ~1 ~ minecraft:lava * scoreboard players set @s vrh.cl 1
scoreboard players add @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=1,score_vrh.cl=1] vrh.ti 1
scoreboard players remove @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=0,score_vrh.cl=0,score_vrh.ti_min=1] vrh.ti 1
effect @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.ti_min=141] minecraft:glowing 1 0 true
tellraw @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.ti_min=141,score_vrh.ti=141,score_vrh.cl_min=1,score_vrh.cl=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Warning: You are ","color":"red"},{"text":"glowing!","color":"yellow","bold":"true"},{"text":" Get out of the lava!","color":"red"}]}
effect @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.ti=140] minecraft:glowing 0 0 true
execute @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2] ~ ~ ~ scoreboard players operation @s vrh.cl = @s vrh.ti
scoreboard players operation @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2] vrh.cl *= 100 g.number
scoreboard players operation @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2] vrh.cl /= 140 g.number
xp -100000L @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2]
xp 1129L @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2]
xp 11 @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2]
xp 6366 @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=64]
scoreboard players remove @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=64] vrh.cl 64
xp 3183 @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=32]
scoreboard players remove @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=32] vrh.cl 32
xp 1591 @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=16]
scoreboard players remove @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=16] vrh.cl 16
xp 795 @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=8]
scoreboard players remove @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=8] vrh.cl 8
xp 397 @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=4]
scoreboard players remove @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=4] vrh.cl 4
xp 198 @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=2]
scoreboard players remove @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=2] vrh.cl 2
xp 99 @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=1]
scoreboard players remove @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2,score_vrh.cl_min=1] vrh.cl 1
xp -1129L @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2]
scoreboard players remove @s[score_vrh.ti_min=1] vrh.ti 1
execute @s[score_vrh.ti_min=6000,score_vrh.ti=6000] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"5 minutes remain!","color":"yellow"}]}
execute @s[score_vrh.ti_min=4800,score_vrh.ti=4800] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"4 minutes remain!","color":"yellow"}]}
execute @s[score_vrh.ti_min=3600,score_vrh.ti=3600] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"3 minutes remain!","color":"yellow"}]}
execute @s[score_vrh.ti_min=2400,score_vrh.ti=2400] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"2 minutes remain!","color":"yellow"}]}
execute @s[score_vrh.ti_min=1200,score_vrh.ti=1200] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"1 minute remains!","color":"yellow"}]}
execute @s[score_vrh.ti_min=600,score_vrh.ti=600] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"30 seconds remain!","color":"yellow"}]}
execute @s[score_vrh.ti_min=300,score_vrh.ti=300] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"15 seconds remain!","color":"yellow"}]}
execute @s[score_vrh.ti_min=100,score_vrh.ti=100] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"5!","color":"yellow"}]}
execute @s[score_vrh.ti_min=80,score_vrh.ti=80] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"4!","color":"yellow"}]}
execute @s[score_vrh.ti_min=60,score_vrh.ti=60] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"3!","color":"yellow"}]}
execute @s[score_vrh.ti_min=40,score_vrh.ti=40] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"2!","color":"yellow"}]}
execute @s[score_vrh.ti_min=20,score_vrh.ti=20] ~ ~ ~ tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VRH","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus Hell","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1521378220"}},{"text":"]","color":"gray"},{"text":": "},{"text":"1!","color":"yellow"}]}
scoreboard players operation Seconds vrh.ti = @s vrh.ti
scoreboard players operation Seconds vrh.ti /= 20 g.number
scoreboard players operation Seconds vrh.ti %= 60 g.number
scoreboard players add Seconds vrh.ti 1
scoreboard players operation Seconds vrh. = Seconds vrh.ti
scoreboard players operation Minutes vrh.ti = @s vrh.ti
scoreboard players operation Minutes vrh.ti /= 1200 g.number
scoreboard players operation Minutes vrh. = Minutes vrh.ti
effect @a[m=2,team=vrh.v,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2] minecraft:strength 3 90 true
effect @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2] minecraft:resistance 3 3 true
effect @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=2,score_vrh.pl=2] minecraft:instant_health 100 100 true
execute @a[m=2,team=vrh.h,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=1,score_vrh.pl=1] ~ ~ ~ function ego:virus/virus_hell/add_to_round_as_hider
execute @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=0,score_vrh.pl=0] ~ ~ ~ function ego:virus/virus_hell/add_to_round_as_hider
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_vrh.pl_min=1,score_vrh.pl=1] vrh.pl 2
effect @a[m=2,team=vrh.v,score_g.sa_min=1,score_g.sa=1] minecraft:glowing 3 0 true
execute @s[score_vrh.ti=0] ~ ~ ~ function ego:virus/virus_hell/end_countdown
