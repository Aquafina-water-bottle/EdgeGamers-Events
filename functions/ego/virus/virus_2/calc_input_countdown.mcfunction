scoreboard players operation &Countdown vr2.cl = @s vr2.icd
scoreboard players operation &Countdown vr2.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds vr2.cl = @s vr2.icd
scoreboard players operation &CountdownMinutes vr2.cl = @s vr2.icd
scoreboard players operation &CountdownMinutes vr2.cl /= 60 g.number
scoreboard players operation @s vr2.icd %= 60 g.number
scoreboard players operation &CountdownSeconds vr2.cl = @s vr2.icd
scoreboard players reset &CountdownAdditional vr2.cl
execute @s[score_vr2.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional vr2.cl 0
scoreboard players set @s vr2.icd 0
