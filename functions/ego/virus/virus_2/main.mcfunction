scoreboard players set @a g.sa 0
scoreboard players set @a[x=-107,y=2,z=-130,dx=193,dy=100,dz=241] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-1723054611] gp.id 1723054610
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-1723054609] gp.id 1723054610
kill @e[x=-107,y=2,z=-130,dx=193,dy=100,dz=241,type=minecraft:item,tag=!vr2.entity]
execute @e[type=minecraft:armor_stand,score_vr2.icd_min=1,tag=vr2.stand] ~ ~ ~ function ego:virus/virus_2/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_vr2.igt_min=1,tag=vr2.stand] ~ ~ ~ function ego:virus/virus_2/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_vr2.igl_min=1,tag=vr2.stand] ~ ~ ~ function ego:virus/virus_2/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"vr2.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"vr2.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"vr2.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"vr2.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"vr2.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"vr2.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"vr2.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"vr2.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"vr2.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"vr2.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"vr2.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"vr2.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_vr2.pl_min=1,score_vr2.pl=2] vr2.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_vr2.pl_min=1,score_vr2.pl=2] vr2.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_vr2.pl_min=1,score_vr2.pl=2] vr2.pl 0
scoreboard players add @a vr2.pl 0
execute @e[type=minecraft:armor_stand,score_vr2.st_min=0,score_vr2.st=0,tag=vr2.stand] ~ ~ ~ function ego:virus/virus_2/wait_for_start
execute @e[type=minecraft:armor_stand,score_vr2.st_min=1,score_vr2.st=1,tag=vr2.stand] ~ ~ ~ function ego:virus/virus_2/countdown
execute @e[type=minecraft:armor_stand,score_vr2.st_min=2,score_vr2.st=2,tag=vr2.stand] ~ ~ ~ function ego:virus/virus_2/during_round
execute @e[type=minecraft:armor_stand,score_vr2.st_min=3,score_vr2.st=3,tag=vr2.stand] ~ ~ ~ function ego:virus/virus_2/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.chi 0
execute @a[m=2,team=vr2.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.chi 1
scoreboard players operation Hiders vr2. = @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.cvr 0
execute @a[m=2,team=vr2.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.cvr 1
scoreboard players operation Virus vr2. = @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.cvr
