scoreboard players operation &GlowingUntil vr2.cl = @s vr2.igl
scoreboard players operation &GlowingUntil vr2.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds vr2.cl = @s vr2.igl
scoreboard players operation &GlowingUntilMinutes vr2.cl = @s vr2.igl
scoreboard players operation &GlowingUntilMinutes vr2.cl /= 60 g.number
scoreboard players operation @s vr2.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds vr2.cl = @s vr2.igl
scoreboard players reset &GlowingUntilAdditional vr2.cl
execute @s[score_vr2.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional vr2.cl 0
scoreboard players set @s vr2.igl 0
