scoreboard players operation &GameTime vr2.cl = @s vr2.igt
scoreboard players operation &GameTime vr2.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds vr2.cl = @s vr2.igt
scoreboard players operation &GameTimeMinutes vr2.cl = @s vr2.igt
scoreboard players operation &GameTimeMinutes vr2.cl /= 60 g.number
scoreboard players operation @s vr2.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds vr2.cl = @s vr2.igt
scoreboard players reset &GameTimeAdditional vr2.cl
execute @s[score_vr2.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional vr2.cl 0
scoreboard players set @s vr2.igt 0
