scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1723054610
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-107,y=2,z=-130,dx=193,dy=100,dz=241] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"VR2","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus 2","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1723054610"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Virus 2","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus 2","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1723054610"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1723054610
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add vr2. dummy Virus 2
scoreboard objectives setdisplay sidebar vr2.
scoreboard objectives add vr2.pl dummy Virus 2 Player List
scoreboard objectives add vr2.ti dummy Virus 2 Timer
scoreboard objectives add vr2.chi dummy Virus 2 Count Hiders
scoreboard objectives add vr2.cvr dummy Virus 2 Count Virus
scoreboard objectives add vr2.gl dummy Virus 2 Virus Glowing
scoreboard objectives add vr2.cl dummy Virus 2 Calculations
scoreboard objectives add vr2.igl dummy Virus 2 Input Glowing
scoreboard objectives add vr2.igt dummy Virus 2 Input Game Time
scoreboard objectives add vr2.icd dummy Virus 2 Input Countdown
scoreboard objectives add vr2.st dummy Virus 2 State
scoreboard teams add vr2.h Virus 2 Hiders
scoreboard teams option vr2.h friendlyfire false
scoreboard teams option vr2.h collisionRule never
scoreboard teams option vr2.h deathMessageVisibility always
scoreboard teams option vr2.h nametagVisibility never
scoreboard teams option vr2.h color green
scoreboard teams add vr2.v Virus 2 Virus
scoreboard teams option vr2.v friendlyfire false
scoreboard teams option vr2.v collisionRule never
scoreboard teams option vr2.v deathMessageVisibility always
scoreboard teams option vr2.v color yellow
scoreboard teams add vr2.d_y Virus 2 Display Yellow
scoreboard teams option vr2.d_y color yellow
scoreboard teams add vr2.d_g Virus 2 Display Green
scoreboard teams option vr2.d_g color green
scoreboard teams join vr2.d_y Countdown
scoreboard teams join vr2.d_y Minutes
scoreboard teams join vr2.d_y Seconds
scoreboard teams join vr2.d_y Virus
scoreboard teams join vr2.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["vr2.stand","vr2.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=vr2.stand] vr2.igl 300
