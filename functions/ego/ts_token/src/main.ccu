$py(
from lib.floo_network import TEAMSPEAK_TOKEN, FlooEvent
from lib.const_ints import CONST_INTS
from lib.scoreboard import OBJECTIVES, TEAMS
from lib.consts import *
from lib.coords import Coords

event = TEAMSPEAK_TOKEN
floo_event = FlooEvent(TEAMSPEAK_TOKEN)
initials = "TS"
select_all = event.select_all


clay_block = "stained_hardened_clay color=lime"
coal_block = "coal_block 0"

clay_region = Coords("549 17 49")
floor_region = Coords("554 19 37 544 19 47")
glass_region = Coords("541 19 34 557 24 50")

# activates the structure block for resetting the arena's blocks
activate_struct_arena_reset = Coords("550 14 47")

# activates the structure block responsible to randomizing the floor layout
activate_struct_floor = Coords("548 14 47")

# activates the structure block to save the original arena
activate_struct_arena_save = Coords("547 5 102 535 5 102")

OBJECTIVES.new_str("""
    . _ .

    # 0 = password has not been set yet
    # 1 = password has been set
    st _ State

    # 0 = has not seen the password input request
    # 1 = has seen the password input request
    pl _ Player List

    # stores the number of guesses a player can have
    cn _ Player Counter

    # used by the TSStand to store the correct password after being inputted
    # since the input objective should never be used for storage
    pw _ Password storage

    # stores the display type
    # 0 = displays all players on coal
    # 1 = displays all players on stained clay
    dt _ Display Type

    # used by ECs to input the correct password by setting the TSStand
    # used by others to input their guess by setting themselves through trigger
    ipw trigger Input password
    
""", initials=initials, display=event.full_name)

OBJECTIVES[initials].setdisplay("sidebar")

CONST_INTS.add_consts(-1)
)


!mfunc init
    $(floo_event.cmd_init())
    $(OBJECTIVES.cmd_init())
    $(TEAMS.cmd_init())
    
    summon armor_stand ~ ~ ~ {Tags:["TSEntity","TSStand"],Invulnerable:1,NoGravity:1,Invisible:1,Marker:1b}

    # makes sure the display tyoe is set as coal blocks by default
    @e[type=armor_stand,TSStand] TSdt = 0
    @e[type=armor_stand,TSStand] TSst = 0

    # ensures the structure is saved
    fill $(activate_struct_arena_save) redstone_block 0 replace stonebrick 0
    fill $(activate_struct_arena_save) stonebrick 0 replace redstone_block 0

    function request_password

    $(floo_event.cmd_post_init())
    
!mfunc main
    $(floo_event.cmd_main())

    # finalizes the password once the password has been set
    @e[type=armor_stand,TSStand,TSipw=1..,TSst=0] function set_password

    # adds all players to the player list
    @a TSpl + 0

    # when the player rejoins the game, they are essentially fully reset
    @a[gLG=1..] TSpl = 0

    # once the password has been set, players are requested to guess the password
    @e[type=armor_stand,TSStand,TSst=1] @a[TSpl=0] function request_password_guess
    @e[type=armor_stand,TSStand,TSst=1] @a[TSpl=1,TSipw=1..] function validate_guess
    
    @e[type=armor_stand,TSStand,TSst=1,TSdt=0] function display_coal
    @e[type=armor_stand,TSStand,TSst=1,TSdt=1] function display_clay

!mfunc term
    $(floo_event.cmd_term())

    $(OBJECTIVES.cmd_term())
    $(TEAMS.cmd_term())

    function reset_arena
    
    kill @e[TSEntity]


# used on the host to request the password for the teamspeak token teleport
!mfunc request_password
    tellraw @s[EC=0] $(event.begin())
        {"text":"Click here!","color":"yellow","italic":"true",
            "hoverEvent":{"action":"show_text","value":{"text":"Click here to set the TS Token Password","color":"yellow"}},
            "clickEvent":{"action":"suggest_command","value":"/scoreboard players set @e[type=armor_stand,tag=TSStand] TSipw "}
        }
    ]}


# used on the TSStand to finalize the password setting
!mfunc set_password
    tellraw @a[EC=0] $(event.begin(r'{"text":"The password has been set!","color":"yellow"}'))
    ScOP @s TSpw = @s TSipw
    @s TSipw = 0
    @s TSst = 1
    

# used on to request them to guess the teamspeak password
!mfunc request_password_guess
    tellraw @s[TSpl=0] $(event.begin())
        {"text":"The ","color":"gray"},
        $(event.name_text()),
        {"text":" is now open! Click ","color":"gray"},
        {"text":"here","color":"yellow","bold":"true",
            "hoverEvent":{"action":"show_text","value":{"text":"Set the TSToken password with the format '/guess <number>'","color":"yellow"}},
            "clickEvent":{"action":"suggest_command","value":"/guess "}
        },
        {"text":" to submit the password.","color":"gray"}
    ]}

    # enables the password to be set
    @s enable TSipw
    @s TSpl = 1

    # resets their number of guesses in case they are being reset
    @s TScn = 0


# used on players when they guessed a teamspeak password
!mfunc validate_guess
    ScOP @s gTEMP = @s TSipw
    ScOP @s gTEMP -= @e[type=armor_stand,TSStand] TSpw
    @s[gTEMP=1..] function incorrect_guess
    @s[gTEMP=..-1] function incorrect_guess
    @s[gTEMP=0] function correct_guess

    # resets the password guess so it doesn't repeat in an infinite loop lol
    @s TSipw = 0


# used on players when they get a correct guess
!mfunc correct_guess
    $(event.cmd_spawn())


# used on players when they incorrectly guess the password
!mfunc incorrect_guess
    # outputs the fact that they have an invalid guess
    tellraw @s $(event.begin(r'{"text":"You have submitted an ","color":"gray"},{"text":"incorrect","color":"red"},{"text":" number.","color":"gray"}'))
    playsound $(Sounds.xp) voice @s ~ ~ ~ 1 0.5

    @s TScn + 1
    tellraw @s[TScn=10] $(event.begin(r'{"text":"You have submitted 10 incorrect answers, and have 0 tries left.","color":"gray"}'))

    # enables the password to be set again
    @s[TScn=..9] enable TSipw
    

# used on the TSStand to display players that are on top of a coal block
!mfunc display_coal
    @a gTEMP = 0
    @a[gSA=1,m=!3] ifblock ~ 19 ~ $(coal_block) @s gTEMP = 1
    @a[TS=0,gTEMP=0] reset TS
    @a[gTEMP=1] TS = 0


# used on the TSStand to display players that are on top of a clay or "losing" block
!mfunc display_clay
    @a gTEMP = 0
    @a[gSA=1,m=!3] ifblock ~ 19 ~ $(clay_block) @s gTEMP = 1
    @a[TS=0,gTEMP=0] reset TS
    @a[gTEMP=1] TS = 0


# used on arbitrary to reset the arena blocks
!mfunc reset_arena
    $(activate_struct_arena_reset.setblock("redstone_block"))
    $(activate_struct_arena_reset.setblock("air"))


!mfunc input_request_password
    function request_password

!mfunc input_fill_glass
    fill $(glass_region) glass 0 replace air 0

!mfunc input_reset_coal
    fill $(floor_region) $(clay_block) replace $(coal_block)

!mfunc input_reset_arena
    function reset_arena

!mfunc input_display_coal
    @e[type=armor_stand,TSStand] TSdt = 0

!mfunc input_display_clay
    @e[type=armor_stand,TSStand] TSdt = 1

!mfunc input_select_player
    @r[gSA=1,m=!3] setblock ~ 19 ~ $(coal_block)

!mfunc input_select_blocks
    $(activate_struct_floor.setblock("redstone_block"))
    $(activate_struct_floor.setblock("air"))

!mfunc input_reset_player
    tellraw @s $(event.begin())
        {"text":"Click ","color":"gray"},
        {"text":"here","color":"yellow","bold":"true",
            "hoverEvent":{"action":"show_text","value":{"text":"Reset a player's guess count","color":"yellow"}},
            "clickEvent":{"action":"suggest_command","value":"/scoreboard players set NAME TSpl 0"}
        },
        {"text":" to reset a player's guess count.","color":"gray"}
    ]}

!mfunc book
    replaceitem entity @s slot.weapon.offhand written_book 1 0 {
        title:"$(event.full_name) Book",author:"eGO",pages:["[\"\",
            {\"text\":\"TSToken Settings\\\n\",\"bold\":true},
            {\"text\":\"\\\n\"},
            
            {\"text\":\"TS: \",\"color\":\"dark_gray\"},
            {\"text\":\"Start\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("init"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Starts TSToken and requests the password\",\"color\":\"dark_green\"}}
            },

            {\"text\":\" \",\"color\":\"gray\"},
            {\"text\":\"[+]\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_request_password"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Manaully requests the password\",\"color\":\"gold\"}}
            },
            {\"text\":\" \",\"color\":\"gray\"},
            {\"text\":\"End\\\n\",\"color\":\"red\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("term"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Ends TSToken\",\"color\":\"red\"}}
            },
            
            {\"text\":\"\\\n\"},
            {\"text\":\"Arena: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_fill_glass"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Fills arena with glass\",\"color\":\"dark_green\"}}
            },
            
            {\"text\":\" \"},
            {\"text\":\"[R]\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_reset_coal"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Resets the coal blocks\",\"color\":\"gold\"}}
            },
            
            {\"text\":\" \"},
            {\"text\":\"[-]\\\n\",\"color\":\"red\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_reset_arena"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Resets the glass\",\"color\":\"red\"}}
            },
            
            {\"text\":\"Display Type: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_display_coal"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Regular (displays players with a coal block)\",\"color\":\"dark_green\"}}
            },
            
            {\"text\":\" \"},
            {\"text\":\"[+]\\\n\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_display_clay"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Opposite (displays all without a coal block)\",\"color\":\"gold\"}}
            },
            
            {\"text\":\"Select Type: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_select_player"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Regular (randomly selects one player)\",\"color\":\"dark_green\"}}
            },
            
            {\"text\":\" \"},
            {\"text\":\"[+]\\\n\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_select_blocks"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Worldedit (randomly selects some blocks regardless of player)\",\"color\":\"gold\"}}
            },
            
            {\"text\":\"Reset player: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\\\n\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_reset_player"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Resets a player's number of avaliable guesses back to 10\",\"color\":\"gold\"}}
            }
        ]"
    ]}

    


