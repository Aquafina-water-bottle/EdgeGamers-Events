scoreboard players operation @s gTEMP = @s TSipw
scoreboard players operation @s gTEMP -= @e[type=armor_stand,tag=TSStand] TSpw
execute @s[score_gTEMP_min=1] ~ ~ ~ function ego:ts_token/incorrect_guess
execute @s[score_gTEMP=-1] ~ ~ ~ function ego:ts_token/incorrect_guess
execute @s[score_gTEMP_min=0,score_gTEMP=0] ~ ~ ~ function ego:ts_token/correct_guess
scoreboard players set @s TSipw 0
