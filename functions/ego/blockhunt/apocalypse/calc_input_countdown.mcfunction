scoreboard players operation &Countdown bha.cl = @s bha.icd
scoreboard players operation &Countdown bha.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bha.cl = @s bha.icd
scoreboard players operation &CountdownMinutes bha.cl = @s bha.icd
scoreboard players operation &CountdownMinutes bha.cl /= 60 g.number
scoreboard players operation @s bha.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bha.cl = @s bha.icd
scoreboard players reset &CountdownAdditional bha.cl
execute @s[score_bha.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bha.cl 0
scoreboard players set @s bha.icd 0
