scoreboard players operation &GlowingUntil bha.cl = @s bha.igl
scoreboard players operation &GlowingUntil bha.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bha.cl = @s bha.igl
scoreboard players operation &GlowingUntilMinutes bha.cl = @s bha.igl
scoreboard players operation &GlowingUntilMinutes bha.cl /= 60 g.number
scoreboard players operation @s bha.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bha.cl = @s bha.igl
scoreboard players reset &GlowingUntilAdditional bha.cl
execute @s[score_bha.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bha.cl 0
scoreboard players set @s bha.igl 0
