scoreboard players operation &GameTime bha.cl = @s bha.igt
scoreboard players operation &GameTime bha.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bha.cl = @s bha.igt
scoreboard players operation &GameTimeMinutes bha.cl = @s bha.igt
scoreboard players operation &GameTimeMinutes bha.cl /= 60 g.number
scoreboard players operation @s bha.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bha.cl = @s bha.igt
scoreboard players reset &GameTimeAdditional bha.cl
execute @s[score_bha.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bha.cl 0
scoreboard players set @s bha.igt 0
