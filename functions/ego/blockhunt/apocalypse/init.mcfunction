scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 725637627
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1565,y=47,z=-102,dx=76,dy=-43,dz=96] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHA","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Apocalypse","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 725637627"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Apocalypse","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Apocalypse","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 725637627"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 725637627
scoreboard players set -1 g.number -1
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bha.prng dummy Pseudo-RNG
scoreboard players set &Increment bha.prng 12345
scoreboard players set &Modulus bha.prng 14
scoreboard players set &Multiplier bha.prng 1103515245
scoreboard players set &Offset bha.prng 0
scoreboard players set &Seed bha.prng 0
scoreboard objectives add bha. dummy Apocalypse
scoreboard objectives setdisplay sidebar bha.
scoreboard objectives add bha.pl dummy Apocalypse Player List
scoreboard objectives add bha.ti dummy Apocalypse Timer
scoreboard objectives add bha.chi dummy Apocalypse Count Hiders
scoreboard objectives add bha.cvr dummy Apocalypse Count Seekers
scoreboard objectives add bha.gl dummy Apocalypse Virus Glowing
scoreboard objectives add bha.cl dummy Apocalypse Calculations
scoreboard objectives add bha.igl dummy Apocalypse Input Glowing
scoreboard objectives add bha.igt dummy Apocalypse Input Game Time
scoreboard objectives add bha.icd dummy Apocalypse Input Countdown
scoreboard objectives add bha.st dummy Apocalypse State
scoreboard teams add bha.h Apocalypse Hiders
scoreboard teams option bha.h friendlyfire false
scoreboard teams option bha.h collisionRule never
scoreboard teams option bha.h deathMessageVisibility always
scoreboard teams option bha.h nametagVisibility never
scoreboard teams option bha.h color green
scoreboard teams option bha.h seeFriendlyInvisibles false
scoreboard teams add bha.v Apocalypse Seekers
scoreboard teams option bha.v friendlyfire false
scoreboard teams option bha.v collisionRule never
scoreboard teams option bha.v deathMessageVisibility always
scoreboard teams option bha.v color yellow
scoreboard teams add bha.d_y Apocalypse Display Yellow
scoreboard teams option bha.d_y color yellow
scoreboard teams add bha.d_g Apocalypse Display Green
scoreboard teams option bha.d_g color green
scoreboard teams join bha.d_y Countdown
scoreboard teams join bha.d_y Minutes
scoreboard teams join bha.d_y Seconds
scoreboard teams join bha.d_y Seekers
scoreboard teams join bha.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bha.stand","bha.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bha.stand] bha.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bha.stand] bha.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bha.stand] bha.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bha.stand] bha.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bha.stand] bha.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bha.prng","bha.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bha.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bha.prng] ~ ~ ~ execute @s[tag=bha.prng_true] ~ ~ ~ scoreboard players add &Seed bha.prng 1073741824
