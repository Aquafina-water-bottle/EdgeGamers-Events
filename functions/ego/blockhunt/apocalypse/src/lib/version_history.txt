0.1.0
    Actually made the version.txt file, so this is pretty much the start of the library lol

0.2.0
    Random changes to coords to make things work better idk

0.2.1
    Changed a bunch of events to use the new coordinate/vector system
        to_selector() -> selector()
        fixed a glitch with selector() not doing math properly
        doing math on coords -> doing math on coords.vec

0.3.0
    Changed virus.ccu to use the function shortcut
    Trying to the fact that players don't teleport where they should be teleported
    Added Objectives.merge(), Objectives.get_objectives(), Objectives.get_names()
    Added Teams.merge(), Teams.get_teams(), Teams.get_names()

0.4.0
    Fixed a bit of formatting with select all, by resetting their score at the end
    Fixed races.ccu, race_lap.ccu and race_regular.ccu by using gSA

0.5.0
    Removed a death specific objective in favor of gDE
    Changed the naming of some objectives inside races, so (name)cl for clearing --> (name)pl for player list
    Added a function as "reset_player" to reset any players

0.6.0
    Add PRNG module. Uses a Linear Congruential Generator to generate pseudo-random numbers.
    Add (Incomplete) BlockHunt module

0.7.0
    Remove prng.ccu in favor of a longer cmd_init in the PRNG class from prng.py

0.8.0
    Make BlockHunt work

0.9.0
    Add the rest of the BlockHunt maps

0.10.0
    floo_network.py
        Changed all the ID hashes to use the folder_name and name
        Changed all shortcuts to automatically include the folder name
        Renamed bho --> bh_office

0.10.1
    floo_network.py
        Renamed bh_office --> bh_old

0.10.2
    floo_network.py
        Added an option to not use the folder_name as a shortcut

0.11.0
    floo_network.py
        Added a custom regen option

    consts.py
        Added MAX_INT

0.12.0
    races.ccu
        Added a macro $edit_scoreboard_start() to initialize scoreboard teams and objectives

    race_lap.ccu, race_checkpoint.ccu, race_regular.ccu
        Added the macro in the copy/pasta region

0.12.1
    Fix BH_MASTERMIND select_coords

0.13.0
    Add .rotate and .rotated methods to Vector2

0.13.1
    Fix .rotate behavior on swizzled Vector2

0.13.2
    Fix coord import in vector.py

0.13.3
    Fix in-place arithmetic prefix assignment behavior with Coord

0.14.0
    floo_network.py
        Added mastermind hell
    
0.14.1
    consts.py
        Fixed a typo for fire resistance

0.14.2
    BlockHunt Hospital (BH_HOSPITAL) select_coords fixed
    virus.ccu and blockhunt.ccu
        Changed the team options deathMessageVisbility --> always

0.15.0
    virus.ccu and blockhunt.ccu
        Integrated blockhunt.ccu with virus.ccu so blockhunt.ccu completely depends on virus.ccu

0.16.0
    added virus_common.ccu and virus_common_macros.ccu to be used by both virus.ccu and blockhunt.ccu
    fixed a glitch where blockhunt does not properly give and remove disguises for all players

0.17.0
    floo_network.py
        added a custom way of setting a specific option and getting a singular option in command form

0.18.0
    floo_network.py
        added mansion virus

0.18.1
    floo_network.py
        updated tstoken
        -uses proper selection coordinates
        -set it to an event so other events can stop when tstoken starts

    virus_common.ccu and virus_common_macros.ccu
        added an edit_main macro so we can edit the main mcfunction

0.18.2
    floo_network.py
        Added an option to the Event class to determine whether the player should automatically teleport to the event spawn or not

0.18.3
    container.py
        Allows a command starting with "!" to not be preceeded with a tab space
        Added a not-working mastermind.ccu library entry

0.19.0
    floo_network.py
        Added a binary search method to make a bunch of functions for the searching part of the binary search in a separate specified folder

0.20.0
    blockhunt.ccu
        Added a tellraw message displaying what block you are supposed to be disguised as
        Changed the PRNG to only initialize at the total init
        Gives disguise to any hider that was previously a virus

    prng.py
        Changed some minor implementation details even though it literally does the same thing as before

    virus_common.ccu
        Fixed a glitch with glowing not being properly removed after 5 minutes if the 5 minute mark has not been reached exactly

0.21.0
    blockhunt.ccu
        Uses the new Block object found in blockhunt_block.py to make its commands
        Checks for duplicates and raises an error if one is found
        Checks for any invalid block types and raises an error if so

    blockhunt_block.py (created)
        Added a Block object to manipulate the block for each line in the file easier

0.21.1
    blockhunt.ccu
        Teleports them back down only -0.01 instead of -0.1 to prevent being stuck in a block
        Teleports hiders out of the seeker box if they are inside it

0.22.0
    floo_network.py
        added virus hell

    virus_common_macros.ccu and virus_common.ccu
        added edit macros for reset_player

1.0.0
    Removed:
        const_obj.py (never used)
        container.py (replaced with command_group.py)
        scoreboard.py (replaced with team.py and objective.py)
        floo_network.py (replaced with floo_event.py and locations.py)

    Added:
        assert_utils.py
        command_group.py
            - better name for container.py with changed variable names
        dynamic_utilities.py
            - provides base class for teams, objectives and bossbars, and their groups
        number_utils.py
        objective.py
        pylintrc
            - config file for pylint specifically for this library
        repr_utils.py
        singleton.py
        team.py

        events/
            - moved all event related files like virus and mastermind here

    Major changes:
        - Teams, Objectives and ConstInts are now singleton objects
        - `floo_network.Event` is now `location.Location` to better define what the object is
        - Coordinate library:
            - Removed a bunch of code base to make things much simplier
            - Coord objects are now immutable
            - RotationCoords and PositionCoords now inherit from Vector2 and Vector3 respectively
        - Practically all attributes and methods for every object have been renamed or changed to make more intuitive sense

1.1.0
    dynamic_utilities.py:
        - added a `reset()` method to MCDynamicUtilityGroup so the utilities in the group can be cleared
            - specifically to reset a singleton when multiple files are being built from fena

1.1.1
    event/virus_common.fena:
        - changed "Glowing To Virus" to "Virus Glowing" to shorten the objective display name

1.1.2
    event/blockhunt_block.py
        - added iron doors and wooden doors to the list of unusable blocks

1.1.3
    event/virus_common.fena
        - added the display of viruses and hiders at the end of each round

1.1.4
    location.py
        - changed the locations of the hubs
        - fixed a glitch with turning the teleport coordinates to integers instead of having them remain floats

    event/virus_common.fena and event/virus_common_macros.fena
        - Added macros for init_to_hider/virus and add_to_round_as_virus

1.2.0
    location.py
        - added the attribute "prefix" and "name" for their respective TextColorMap objects

    floo_event.py
        - fixed a bug with an invalid scoreboard objective
        - cmd_input -> funcs_input

    event/ (all races)
        - Updated all races so everything actually works now

1.2.1
    floo_event.py, event/race_post.fena
        - changed all "funcs_" -> "mfunc_"

1.2.2
    floo_event.py
        - added a debug option on whether a binary search should debug or not

    event/virus_common.fena
        - fixed a glitch where you couldn't manually add someone to a team
        - made it work with the new library (funcs_ -> mfunc_)
        - change the states to only include repeating functions

1.2.3
    event/race_lap.fena
        Fixed a bug with multiple places being added to lap races

    event/race_pre.fena
        Added further documentation for _stand with _pc

1.3.0
    - Changed the entire format of event/ directory:
        - Separated into their own race/, blockhunt/, virus/ and mastermind/ modules
        - Races now requires a main.json file to store information

1.3.1
    - Fixed a bug with coordinates not taking in the full amount of selector arguments

