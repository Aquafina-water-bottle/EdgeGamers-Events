"""
Creates a special objective to hold constant numbers

As soon as this is imported, it adds the "constants" objective to
the global scoreboard.OBJECTIVES variable
"""

from fenautils import Singleton
from lib.objective import Objective, Objectives

__all__ = [
    "ConstInts",
    ]


objectives = Objectives()

class ConstInts(Singleton):
    """
    Stores an objective name and a set (no duplicates) of integers as integer constants

    The general convention when using this is to not change the objective name
    """
    def __init__(self, objective_name="g.number", initialize_self=False):
        # makes sure the objective name is not inside since this __init__ can be called multiple times
        # note that this shouldn't be initialized since the global objective
        # should only be initialized in floo_network/main.fena
        if objective_name not in objectives:
            self.objective = Objective(objective_name, remove_self=False, initialize_self=initialize_self)
            objectives.add(self.objective)

    def add_constants(self, *constants: int):
        """
        Adds any number of integer constants, such that the objective maps the integer to the integer.
        (eg. self.add_constants(0) -> scoreboard players set 0 g.number 0)
        """
        for constant in constants:
            self.objective.add_score(str(constant), constant)

