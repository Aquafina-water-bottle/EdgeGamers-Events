scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1565,y=47,z=-102,dx=76,dy=-43,dz=96] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-725637628] gp.id 725637627
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-725637626] gp.id 725637627
kill @e[x=-1565,y=47,z=-102,dx=76,dy=-43,dz=96,type=minecraft:item,tag=!bha.entity]
execute @e[type=minecraft:armor_stand,score_bha.icd_min=1,tag=bha.stand] ~ ~ ~ function ego:blockhunt/apocalypse/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bha.igt_min=1,tag=bha.stand] ~ ~ ~ function ego:blockhunt/apocalypse/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bha.igl_min=1,tag=bha.stand] ~ ~ ~ function ego:blockhunt/apocalypse/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bha.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bha.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bha.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bha.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bha.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bha.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bha.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bha.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bha.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bha.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bha.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bha.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bha.pl_min=1,score_bha.pl=2] bha.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bha.pl_min=1,score_bha.pl=2] bha.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bha.pl_min=1,score_bha.pl=2] bha.pl 0
scoreboard players add @a bha.pl 0
execute @e[type=minecraft:armor_stand,score_bha.st_min=0,score_bha.st=0,tag=bha.stand] ~ ~ ~ function ego:blockhunt/apocalypse/wait_for_start
execute @e[type=minecraft:armor_stand,score_bha.st_min=1,score_bha.st=1,tag=bha.stand] ~ ~ ~ function ego:blockhunt/apocalypse/countdown
execute @e[type=minecraft:armor_stand,score_bha.st_min=2,score_bha.st=2,tag=bha.stand] ~ ~ ~ function ego:blockhunt/apocalypse/during_round
execute @e[type=minecraft:armor_stand,score_bha.st_min=3,score_bha.st=3,tag=bha.stand] ~ ~ ~ function ego:blockhunt/apocalypse/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bha.stand] bha.chi 0
execute @a[m=2,team=bha.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bha.stand] bha.chi 1
scoreboard players operation Hiders bha. = @e[type=minecraft:armor_stand,tag=bha.stand] bha.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bha.stand] bha.cvr 0
execute @a[m=2,team=bha.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bha.stand] bha.cvr 1
scoreboard players operation Seekers bha. = @e[type=minecraft:armor_stand,tag=bha.stand] bha.cvr
