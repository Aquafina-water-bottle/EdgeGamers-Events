scoreboard players operation &Countdown bhh.cl = @s bhh.icd
scoreboard players operation &Countdown bhh.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhh.cl = @s bhh.icd
scoreboard players operation &CountdownMinutes bhh.cl = @s bhh.icd
scoreboard players operation &CountdownMinutes bhh.cl /= 60 g.number
scoreboard players operation @s bhh.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhh.cl = @s bhh.icd
scoreboard players reset &CountdownAdditional bhh.cl
execute @s[score_bhh.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhh.cl 0
scoreboard players set @s bhh.icd 0
