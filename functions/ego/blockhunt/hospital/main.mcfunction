scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1047,y=42,z=-290,dx=-90,dy=-39,dz=58] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-183650980] gp.id 183650979
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-183650978] gp.id 183650979
kill @e[x=-1047,y=42,z=-290,dx=-90,dy=-39,dz=58,type=minecraft:item,tag=!bhh.entity]
execute @e[type=minecraft:armor_stand,score_bhh.icd_min=1,tag=bhh.stand] ~ ~ ~ function ego:blockhunt/hospital/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhh.igt_min=1,tag=bhh.stand] ~ ~ ~ function ego:blockhunt/hospital/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhh.igl_min=1,tag=bhh.stand] ~ ~ ~ function ego:blockhunt/hospital/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhh.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhh.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhh.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhh.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhh.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhh.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhh.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhh.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhh.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhh.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhh.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhh.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhh.pl_min=1,score_bhh.pl=2] bhh.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhh.pl_min=1,score_bhh.pl=2] bhh.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhh.pl_min=1,score_bhh.pl=2] bhh.pl 0
scoreboard players add @a bhh.pl 0
execute @e[type=minecraft:armor_stand,score_bhh.st_min=0,score_bhh.st=0,tag=bhh.stand] ~ ~ ~ function ego:blockhunt/hospital/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhh.st_min=1,score_bhh.st=1,tag=bhh.stand] ~ ~ ~ function ego:blockhunt/hospital/countdown
execute @e[type=minecraft:armor_stand,score_bhh.st_min=2,score_bhh.st=2,tag=bhh.stand] ~ ~ ~ function ego:blockhunt/hospital/during_round
execute @e[type=minecraft:armor_stand,score_bhh.st_min=3,score_bhh.st=3,tag=bhh.stand] ~ ~ ~ function ego:blockhunt/hospital/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.chi 0
execute @a[m=2,team=bhh.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.chi 1
scoreboard players operation Hiders bhh. = @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.cvr 0
execute @a[m=2,team=bhh.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.cvr 1
scoreboard players operation Seekers bhh. = @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.cvr
