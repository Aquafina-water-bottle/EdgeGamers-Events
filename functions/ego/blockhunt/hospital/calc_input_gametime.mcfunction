scoreboard players operation &GameTime bhh.cl = @s bhh.igt
scoreboard players operation &GameTime bhh.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhh.cl = @s bhh.igt
scoreboard players operation &GameTimeMinutes bhh.cl = @s bhh.igt
scoreboard players operation &GameTimeMinutes bhh.cl /= 60 g.number
scoreboard players operation @s bhh.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhh.cl = @s bhh.igt
scoreboard players reset &GameTimeAdditional bhh.cl
execute @s[score_bhh.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhh.cl 0
scoreboard players set @s bhh.igt 0
