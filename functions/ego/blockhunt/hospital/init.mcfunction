scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 183650979
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1047,y=42,z=-290,dx=-90,dy=-39,dz=58] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHH","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Hospital","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 183650979"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Hospital","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Hospital","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 183650979"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 183650979
scoreboard objectives add bhh.prng dummy Pseudo-RNG
scoreboard players set &Increment bhh.prng 12345
scoreboard players set &Modulus bhh.prng 18
scoreboard players set &Multiplier bhh.prng 1103515245
scoreboard players set &Offset bhh.prng 0
scoreboard players set &Seed bhh.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhh. dummy Hospital
scoreboard objectives setdisplay sidebar bhh.
scoreboard objectives add bhh.pl dummy Hospital Player List
scoreboard objectives add bhh.ti dummy Hospital Timer
scoreboard objectives add bhh.chi dummy Hospital Count Hiders
scoreboard objectives add bhh.cvr dummy Hospital Count Seekers
scoreboard objectives add bhh.gl dummy Hospital Virus Glowing
scoreboard objectives add bhh.cl dummy Hospital Calculations
scoreboard objectives add bhh.igl dummy Hospital Input Glowing
scoreboard objectives add bhh.igt dummy Hospital Input Game Time
scoreboard objectives add bhh.icd dummy Hospital Input Countdown
scoreboard objectives add bhh.st dummy Hospital State
scoreboard teams add bhh.h Hospital Hiders
scoreboard teams option bhh.h friendlyfire false
scoreboard teams option bhh.h collisionRule never
scoreboard teams option bhh.h deathMessageVisibility always
scoreboard teams option bhh.h nametagVisibility never
scoreboard teams option bhh.h color green
scoreboard teams option bhh.h seeFriendlyInvisibles false
scoreboard teams add bhh.v Hospital Seekers
scoreboard teams option bhh.v friendlyfire false
scoreboard teams option bhh.v collisionRule never
scoreboard teams option bhh.v deathMessageVisibility always
scoreboard teams option bhh.v color yellow
scoreboard teams add bhh.d_y Hospital Display Yellow
scoreboard teams option bhh.d_y color yellow
scoreboard teams add bhh.d_g Hospital Display Green
scoreboard teams option bhh.d_g color green
scoreboard teams join bhh.d_y Countdown
scoreboard teams join bhh.d_y Minutes
scoreboard teams join bhh.d_y Seconds
scoreboard teams join bhh.d_y Seekers
scoreboard teams join bhh.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhh.stand","bhh.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhh.stand] bhh.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhh.prng","bhh.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhh.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhh.prng] ~ ~ ~ execute @s[tag=bhh.prng_true] ~ ~ ~ scoreboard players add &Seed bhh.prng 1073741824
