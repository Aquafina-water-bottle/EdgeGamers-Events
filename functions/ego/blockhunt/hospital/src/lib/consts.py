"""
Holds constant values for multiple items
"""

from typing import NamedTuple


__all__ = [
    "Sounds",
    "Effects",
    "Enchants",
    "Colors",
    "MAX_INT",
    ]


class Sounds:
    """
    Shorthand constants for sounds
    """
    PLING = "minecraft:block.note.pling"
    XP = "minecraft:entity.experience_orb.pickup"
    ITEM = "minecraft:entity.item.pickup"
    TP = "minecraft:entity.endermen.teleport"
    WITHER = "minecraft:entity.wither.death"
    LEVEL = "minecraft:entity.player.levelup"
    EXPLODE = "minecraft:entity.generic.explode"
    HAT = "minecraft:block.note.hat"
    PRIMED = "minecraft:entity.tnt.primed"


class Effect:
    """
    Attributes:
        string (str)
        effect_id (int)
    """
    _id_calc = 1
    # total number of effects in minecraft
    MAX_EFFECTS = 30

    def __init__(self, string):
        assert Effect._id_calc <= Effect.MAX_EFFECTS

        self.effect_str = string
        self.effect_id = Effect._id_calc
        Effect._id_calc += 1

        # makes sure there aren't more effects created

    def __str__(self):
        return self.effect_str

class Effects:
    """
    Shorthand constants for effects
    """
    SPD = Effect("minecraft:speed")
    SLOW = Effect("minecraft:slowness")
    HASTE = Effect("minecraft:haste")
    MINING_FATIGUE = Effect("minecraft:mining_fatigue")
    STRENGTH = Effect("minecraft:strength")
    HP = Effect("minecraft:instant_health")
    DMG = Effect("minecraft:instant_damage")
    JUMP = Effect("minecraft:jump_boost")
    NAUSEA = Effect("minecraft:nausea")
    REGEN = Effect("minecraft:regeneration")
    RESIST = Effect("minecraft:resistance")
    FIRE_RESIST = Effect("minecraft:fire_resistance")
    WATER_BREATH = Effect("minecraft:water_breathing")
    INVIS = Effect("minecraft:invisibility")
    BLIND = Effect("minecraft:blindness")
    NIGHT_VISION = Effect("minecraft:night_vision")
    HUNGER = Effect("minecraft:hunger")
    WEAK = Effect("minecraft:weakness")
    POISON = Effect("minecraft:poison")
    WITHER = Effect("minecraft:wither")
    HP_BOOST = Effect("minecraft:health_boost")
    ABSORPTION = Effect("minecraft:absorption")
    SATURATION = Effect("minecraft:saturation")
    GLOW = Effect("minecraft:glowing")
    LEVITATION = Effect("minecraft:levitation")
    LUCK = Effect("minecraft:luck")
    UNLUCK = Effect("minecraft:unluck")
    SLOW_FALL = Effect("minecraft:slow_falling")
    CONDUIT = Effect("minecraft:conduit")
    DOLPHIN = Effect("minecraft:dolphins_grace")


class Colors:
    """
    List of all colors for text components
    """

    DARK_RED = "dark_red"
    RED = "red"
    GOLD = "gold"
    YELLOW = "yellow"
    GREEN = "green"
    DARK_GREEN = "dark_green"
    DARK_BLUE = "dark_blue"
    BLUE = "blue"
    DARK_AQUA = "dark_aqua"
    AQUA = "aqua"
    LIGHT_PURPLE = "light_purple"
    DARK_PURPLE = "dark_purple"
    WHITE = "white"
    GRAY = "gray"
    DARK_GRAY = "dark_gray"
    BLACK = "black"
    RESET = "reset"

    ALL = (DARK_RED, RED, GOLD, YELLOW, GREEN, DARK_GREEN, DARK_BLUE, BLUE, DARK_AQUA,
           AQUA, LIGHT_PURPLE, DARK_PURPLE, WHITE, GRAY, DARK_GRAY, BLACK, RESET)

    CODE_DICT = {
        DARK_RED: "&4",
        RED: "&c",
        GOLD: "&6",
        YELLOW: "&e",
        GREEN: "&a",
        DARK_GREEN: "&2",
        DARK_BLUE: "&1",
        BLUE: "&9",
        DARK_AQUA: "&3",
        AQUA: "&b",
        LIGHT_PURPLE: "&d",
        DARK_PURPLE: "&5",
        WHITE: "&f",
        GRAY: "&7",
        DARK_GRAY: "&8",
        BLACK: "&0",
        RESET: "&r",
    }


class EnchantBase(NamedTuple):
    enchant_id: int
    enchant_str: str

class Enchant(EnchantBase):
    def __str__(self):
        return self.enchant_str

class Enchants:
    AQUA_AFFINITY = Enchant(6, "minecraft:aqua_affinity")
    BANE_OF_ARTHRO = Enchant(18, "minecraft:bane_of_arthropods")
    BINDING = Enchant(10, "minecraft:binding_curse")
    BLAST_PROT = Enchant(3, "minecraft:blast_protection")
    DEPTH_STRIDER = Enchant(8, "minecraft:depth_strider")
    EFFICIENCY = Enchant(32, "minecraft:efficiency")
    FEATHER_FALLING = Enchant(2, "minecraft:feather_falling")
    FIRE_ASPECT = Enchant(20, "minecraft:fire_aspect")
    FIRE_PROT = Enchant(1, "minecraft:fire_protection")
    FLAME = Enchant(50, "minecraft:flame")
    FORTUNE = Enchant(35, "minecraft:fortune")
    FROST_WALKER = Enchant(9, "minecraft:frost_walker")
    INFINITY = Enchant(51, "minecraft:infinity")
    KNOCKBACK = Enchant(19, "minecraft:knockback")
    LOOTING = Enchant(21, "minecraft:looting")
    SEA_LUCK = Enchant(61, "minecraft:luck_of_the_sea")
    LURE = Enchant(62, "minecraft:lure")
    MENDING = Enchant(70, "minecraft:mending")
    POWER = Enchant(48, "minecraft:power")
    PROJECTILE_PROT = Enchant(4, "minecraft:projectile_protection")
    PROT = Enchant(0, "minecraft:protection")
    PUNCH = Enchant(49, "minecraft:punch")
    RESPIRATION = Enchant(5, "minecraft:respiration")
    SHARPNESS = Enchant(16, "minecraft:sharpness")
    SILK_TOUCH = Enchant(33, "minecraft:silk_touch")
    SMITE = Enchant(17, "minecraft:smite")
    SWEEPING = Enchant(22, "minecraft:sweeping")
    THORNS = Enchant(7, "minecraft:thorns")
    UNBREAKING = Enchant(34, "minecraft:unbreaking")
    VANISH = Enchant(71, "minecraft:vanishing_curse")

    # AQUA_AFFINITY = "minecraft:aqua_affinity"
    # BANE_OF_ARTHRO = "minecraft:bane_of_arthropods"
    # BINDING = "minecraft:binding_curse"
    # BLAST_PROT = "minecraft:blast_protection"
    # CHANNELING = "minecraft:channeling"
    # DEPTH_STRIDER = "minecraft:depth_strider"
    # EFFICIENCY = "minecraft:efficiency"
    # FEATHER_FALLING = "minecraft:feather_falling"
    # FIRE_ASPECT = "minecraft:fire_aspect"
    # FIRE_PROT = "minecraft:fire_protection"
    # FLAME = "minecraft:flame"
    # FORTUNE = "minecraft:fortune"
    # FROST_WALKER = "minecraft:frost_walker"
    # IMPALING = "minecraft:impaling"
    # INFINITY = "minecraft:infinity"
    # KNOCKBACK = "minecraft:knockback"
    # LOOTING = "minecraft:looting"
    # LOYALTY = "minecraft:loyalty"
    # SEA_LUCK = "minecraft:luck_of_the_sea"
    # LURE = "minecraft:lure"
    # MENDING = "minecraft:mending"
    # POWER = "minecraft:power"
    # PROJECTILE_PROT = "minecraft:projectile_protection"
    # PROT = "minecraft:protection"
    # PUNCH = "minecraft:punch"
    # RESPIRATION = "minecraft:respiration"
    # RIPTIDE = "minecraft:riptide"
    # SHARPNESS = "minecraft:sharpness"
    # SILK_TOUCH = "minecraft:silk_touch"
    # SMITE = "minecraft:smite"
    # SWEEPING = "minecraft:sweeping"
    # THORNS = "minecraft:thorns"
    # UNBREAKING = "minecraft:unbreaking"
    # VANISH = "minecraft:vanishing_curse"

MAX_INT = (1<<31)-1


