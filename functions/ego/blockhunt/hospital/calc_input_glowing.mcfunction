scoreboard players operation &GlowingUntil bhh.cl = @s bhh.igl
scoreboard players operation &GlowingUntil bhh.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhh.cl = @s bhh.igl
scoreboard players operation &GlowingUntilMinutes bhh.cl = @s bhh.igl
scoreboard players operation &GlowingUntilMinutes bhh.cl /= 60 g.number
scoreboard players operation @s bhh.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhh.cl = @s bhh.igl
scoreboard players reset &GlowingUntilAdditional bhh.cl
execute @s[score_bhh.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhh.cl 0
scoreboard players set @s bhh.igl 0
