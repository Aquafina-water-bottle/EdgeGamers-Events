scoreboard players operation &GameTime bhra.cl = @s bhra.igt
scoreboard players operation &GameTime bhra.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhra.cl = @s bhra.igt
scoreboard players operation &GameTimeMinutes bhra.cl = @s bhra.igt
scoreboard players operation &GameTimeMinutes bhra.cl /= 60 g.number
scoreboard players operation @s bhra.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhra.cl = @s bhra.igt
scoreboard players reset &GameTimeAdditional bhra.cl
execute @s[score_bhra.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhra.cl 0
scoreboard players set @s bhra.igt 0
