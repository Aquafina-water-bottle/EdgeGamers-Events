scoreboard players operation &Countdown bhra.cl = @s bhra.icd
scoreboard players operation &Countdown bhra.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhra.cl = @s bhra.icd
scoreboard players operation &CountdownMinutes bhra.cl = @s bhra.icd
scoreboard players operation &CountdownMinutes bhra.cl /= 60 g.number
scoreboard players operation @s bhra.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhra.cl = @s bhra.icd
scoreboard players reset &CountdownAdditional bhra.cl
execute @s[score_bhra.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhra.cl 0
scoreboard players set @s bhra.icd 0
