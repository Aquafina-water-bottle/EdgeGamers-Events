scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 376134566
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1130,y=38,z=-93,dx=-42,dy=-34,dz=83] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHRA","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Rainbow","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 376134566"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Rainbow","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Rainbow","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 376134566"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 376134566
scoreboard objectives add bhra.prng dummy Pseudo-RNG
scoreboard players set &Increment bhra.prng 12345
scoreboard players set &Modulus bhra.prng 9
scoreboard players set &Multiplier bhra.prng 1103515245
scoreboard players set &Offset bhra.prng 0
scoreboard players set &Seed bhra.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhra. dummy Rainbow
scoreboard objectives setdisplay sidebar bhra.
scoreboard objectives add bhra.pl dummy Rainbow Player List
scoreboard objectives add bhra.ti dummy Rainbow Timer
scoreboard objectives add bhra.chi dummy Rainbow Count Hiders
scoreboard objectives add bhra.cvr dummy Rainbow Count Seekers
scoreboard objectives add bhra.gl dummy Rainbow Virus Glowing
scoreboard objectives add bhra.cl dummy Rainbow Calculations
scoreboard objectives add bhra.igl dummy Rainbow Input Glowing
scoreboard objectives add bhra.igt dummy Rainbow Input Game Time
scoreboard objectives add bhra.icd dummy Rainbow Input Countdown
scoreboard objectives add bhra.st dummy Rainbow State
scoreboard teams add bhra.h Rainbow Hiders
scoreboard teams option bhra.h friendlyfire false
scoreboard teams option bhra.h collisionRule never
scoreboard teams option bhra.h deathMessageVisibility always
scoreboard teams option bhra.h nametagVisibility never
scoreboard teams option bhra.h color green
scoreboard teams option bhra.h seeFriendlyInvisibles false
scoreboard teams add bhra.v Rainbow Seekers
scoreboard teams option bhra.v friendlyfire false
scoreboard teams option bhra.v collisionRule never
scoreboard teams option bhra.v deathMessageVisibility always
scoreboard teams option bhra.v color yellow
scoreboard teams add bhra.d_y Rainbow Display Yellow
scoreboard teams option bhra.d_y color yellow
scoreboard teams add bhra.d_g Rainbow Display Green
scoreboard teams option bhra.d_g color green
scoreboard teams join bhra.d_y Countdown
scoreboard teams join bhra.d_y Minutes
scoreboard teams join bhra.d_y Seconds
scoreboard teams join bhra.d_y Seekers
scoreboard teams join bhra.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhra.stand","bhra.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhra.prng","bhra.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhra.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhra.prng] ~ ~ ~ execute @s[tag=bhra.prng_true] ~ ~ ~ scoreboard players add &Seed bhra.prng 1073741824
