scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1130,y=38,z=-93,dx=-42,dy=-34,dz=83] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-376134567] gp.id 376134566
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-376134565] gp.id 376134566
kill @e[x=-1130,y=38,z=-93,dx=-42,dy=-34,dz=83,type=minecraft:item,tag=!bhra.entity]
execute @e[type=minecraft:armor_stand,score_bhra.icd_min=1,tag=bhra.stand] ~ ~ ~ function ego:blockhunt/rainbow/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhra.igt_min=1,tag=bhra.stand] ~ ~ ~ function ego:blockhunt/rainbow/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhra.igl_min=1,tag=bhra.stand] ~ ~ ~ function ego:blockhunt/rainbow/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhra.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhra.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhra.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhra.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhra.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhra.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhra.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhra.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhra.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhra.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhra.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhra.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhra.pl_min=1,score_bhra.pl=2] bhra.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhra.pl_min=1,score_bhra.pl=2] bhra.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhra.pl_min=1,score_bhra.pl=2] bhra.pl 0
scoreboard players add @a bhra.pl 0
execute @e[type=minecraft:armor_stand,score_bhra.st_min=0,score_bhra.st=0,tag=bhra.stand] ~ ~ ~ function ego:blockhunt/rainbow/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhra.st_min=1,score_bhra.st=1,tag=bhra.stand] ~ ~ ~ function ego:blockhunt/rainbow/countdown
execute @e[type=minecraft:armor_stand,score_bhra.st_min=2,score_bhra.st=2,tag=bhra.stand] ~ ~ ~ function ego:blockhunt/rainbow/during_round
execute @e[type=minecraft:armor_stand,score_bhra.st_min=3,score_bhra.st=3,tag=bhra.stand] ~ ~ ~ function ego:blockhunt/rainbow/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.chi 0
execute @a[m=2,team=bhra.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.chi 1
scoreboard players operation Hiders bhra. = @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.cvr 0
execute @a[m=2,team=bhra.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.cvr 1
scoreboard players operation Seekers bhra. = @e[type=minecraft:armor_stand,tag=bhra.stand] bhra.cvr
