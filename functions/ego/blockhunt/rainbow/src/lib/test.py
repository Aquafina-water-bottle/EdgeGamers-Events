import collections

if __name__ == "__main__":
    import sys
    sys.path.append("..")
    del sys

from lib.repr_utils import addrepr
# from lib.vector import Vector2

# @addrepr
# class Vector:
#     def __init__(self, a, b):
#         self.a = a
#         self.b = b
# 
#     def __len__(self):
#         return 3
# 
#     def __iadd__(self, other):
#         self.a += other.a
#         self.b += other.b
#         return self
# 
# $for(i in 'xy')
# $for(j in 'xy')
#     @property
#     def $(i)$(j)(self):
#         """
#         Swizzle mask
# 
#         Returns:
#             Vector2(self.$(i), self.$(j))
#         """
#         return Vector2(self.$(i), self.$(j))
# 
# $endfor
# $endfor


Point = collections.namedtuple("Point", ["x", "y"])

@addrepr
class GetSwizzle:
    # failed :c
    def __init__(self, x, y):
        self.x = x
        self.y = y

        for i in 'xy':
            for j in 'xy':
                setattr(self, f"{i}{j}", lambda: Point(getattr(self, i), getattr(self, j)))


if __name__ == "__main__":
    g = GetSwizzle(0, 1)
    print(g)
    print(vars(g))

    #pylint: disable=no-member
    print(g.xy())
    print(g.yx())
    print(g.xx())
    print(g.yy())

    # print(len(Vector))
    # v = Vector2(1, 0)
    # print(repr(v))
    # v += 5
    # print(repr(v))

    # v = Vector(1, 2)
    # L = [1, 2]

    # v += v
    # L += L

    # print(v, L)



'''
>>> round(Vector2(1, 0).rotated(math.pi/2))
Vector2('0.0','-1.0')

>>> a = Vector3(1,0,0)
>>> a.xz.rotate(90, radians=False)
>>> a = Vector3(*map(int, a))
>>> a
Vector3('0','0','-1')

>>> Vector2(*map(int, Vector2(2,1).rotated(90, Vector2(1,1), False)))
Vector2('1','0')




>>> vec2 = Vector2(1, 0)
>>> vec2.rotate(math.pi/2, radians=True)
>>> vec2
Vector2(0.0, -1.0)

>>> vec3 = Vector3('^1', '^0', '^0')
>>> xz = vec3.xz
>>> xz.rotate(90, radians=False)
>>> xz.to_int()
>>> vec3 = Vector3(xz[0], vec3.y, xz[1])
>>> vec3
Vector3(^, ^, ^-1)

>>> vec2 = Vector2(2, 1)
>>> vec2.rotate(90, Vector2(1, 1))
>>> vec2
Vector2(1.0, 0.0)

'''

