$py:
    import json
    import lib.location
    from lib.race import LapRace
    from lib.coords import Coords

    with open("main.json") as file:
        race_json = json.load(file)

    # gets the location from lib.location, and assumes it exists
    # private so they aren't used
    _location_str = race_json["location"]
    _race_location = getattr(lib.location, _location_str)

    race = LapRace(
        location=_race_location,
        required_laps=race_json["required_laps"],

        finish_line_coords=Coords(race_json["finish_line_coords"]),
        lap_reset_coords=Coords(race_json["lap_reset_coords"]),
        spawn_coords=Coords(race_json["spawn_coords"]),
        )


$macro(edit_type_py_init):
    # scoreboard setting
    $py:
        objectives.new("""
            # players (bool): whether players are in a valid state to
            # complete a lap.
            # This is set to true when players g et to a point where they
            # cannot go backwards in the race to go over the finish line
            # _stand: stores the required number of laps
            _lp _ Laps

            # _stand: input the number of required laps for players to complete
            _ilp _ Input Laps

            # players (bool): whether they have already won or not
            # this is mainly here in case the EC sets the required_laps to
            # less than what a player already has for some reason
            _wn _ Has Won

        """, race.objective_disp)

        # creates four places: first, second, third, runnerup
        for _ in range(4):
            race.make_place()


    # _stand: updates the display of required laps
    !mfunc calc_required_laps:
        Required_Laps _ = @s _ilp
        team _r + Required_Laps
        @s _lp = @s _ilp
        @s _ilp = 0

    # used on players when a lap has been detected
    !mfunc add_lap:
        @s _ += 1
        tellraw @a $(race.location.surround_json(r'{"selector":"@s"},{"text":" has finished a lap!","color":"yellow"}'))
        playsound $(Sounds.XP) voice @a

        # calculates the difference between the armor stand and the player
        # if the difference is >= 0, and they haven't won yet,
        # that player finishes the race
        # reversed to prevent the same player from winning multiple times
        @s g.temp = @s _
        @s g.temp -= @e[type=armor_stand,_stand,c=1] _lp
        $for(index, place in enumerate(race.places)):
            @s[g.temp=(0..),_wn=0]: function set_place_$(index) if @e[type=armor_stand,_stand,_pc=$(index)]

        @s _lp = 0

    !folder input:
        !mfunc add_one_lap:
            playsound $(Sounds.XP) voice @s

            @e[type=armor_stand,_stand]: @s _ilp = @s _lp
            @e[type=armor_stand,_stand] _ilp += 1

        !mfunc remove_one_lap:
            playsound $(Sounds.XP) voice @s

            @e[type=armor_stand,_stand]: @s _ilp = @s _lp
            # Puts a range so you cannot remove a lap when there is only one lap left
            @e[type=armor_stand,_stand,_lp=2..] _ilp -= 1

    !mfunc book:
        replaceitem entity @s slot.weapon.offhand written_book 1 0 {
            title:"$(race.location.name_str) Book",author:"eGO",pages:["{\"text\":\"\",\"extra\":[
                {\"text\":\"$(race.location.name_str) Settings\\n\",\"bold\":true},
                {\"text\":\"\\n\"},

                {\"text\":\"$(race.location.prefix_disp): \",\"color\":\"dark_gray\"},
                {\"text\":\"Start\",\"color\":\"dark_green\",
                    \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/init\"},
                    \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Starts $(race.location.name_str)\",\"color\":\"green\"}}
                },
                {\"text\":\" / \",\"color\":\"gray\"},
                {\"text\":\"Stop\\n\\n\",\"color\":\"red\",
                    \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function term\"},
                    \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Ends $(race.location.name_str)\",\"color\":\"red\"}}
                },
                {\"text\":\"Required laps: \",\"color\":\"dark_gray\"},
                {\"text\":\"[+]\",\"color\":\"dark_green\",
                    \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/add_one_lap\"},
                    \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Adds one to the required laps\",\"color\":\"green\"}}
                },
                {\"text\":\" \",\"color\":\"gray\"},
                {\"text\":\"[-]\",\"color\":\"red\",
                    \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/remove_one_lap\"},
                    \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Removes one to the required laps\",\"color\":\"red\"}}
                }
            ]}"
        ]}



$macro(edit_type_clear_player):
    @s _lp += 0
    @s _wn += 0

$macro(edit_init):
    @e[type=armor_stand,_stand] _ilp = $(race.required_laps)
    @e[type=armor_stand,_stand] _pc = 0

$macro(edit_type_main):
    # calculates inputted laps
    @e[type=armor_stand,_stand,_ilp=1..]: function calc_required_laps

    # adds all players to the display objective to stopre the number of laps
    @a[g.sa=1,m=2,_pl=1] _ += 0

    # Sets the player lap score to false if they're in the spawn to prevent
    # them going backwards and getting a lap
    @a[$(race.select_spawn),m=2,_pl=1,_lp=1] _lp = 0

    # Sets the player lap score to true once they entered a region where they
    # cannot go backwards in the race
    @a[$(race.select_lap_reset),m=2,_pl=1,_lp=0] _lp = 1

    # lap detection
    @a[$(race.select_finish_line),m=2,_pl=1,_lp=1]: function add_lap


$macro(edit_type_set_place_common):
    # adds a place to the armor stand so it can give another player a
    # different place
    # only increments to 3 (0,1,2,3) so there can be infinite runnerups
    @e[type=armor_stand,_stand,_pc=..2] _pc += 1

    # sets _wn to -1 so a player cannot win more than once
    @s _wn = 1


$include("lib/race/main.fena")

