scoreboard players operation &GlowingUntil bhra.cl = @s bhra.igl
scoreboard players operation &GlowingUntil bhra.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhra.cl = @s bhra.igl
scoreboard players operation &GlowingUntilMinutes bhra.cl = @s bhra.igl
scoreboard players operation &GlowingUntilMinutes bhra.cl /= 60 g.number
scoreboard players operation @s bhra.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhra.cl = @s bhra.igl
scoreboard players reset &GlowingUntilAdditional bhra.cl
execute @s[score_bhra.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhra.cl 0
scoreboard players set @s bhra.igl 0
