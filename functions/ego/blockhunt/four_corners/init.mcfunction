scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 518834438
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1358,y=111,z=-190,dx=90,dy=-107,dz=-90] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHFC","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Four Corners","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 518834438"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Four Corners","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Four Corners","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 518834438"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 518834438
scoreboard objectives add bhfc.prng dummy Pseudo-RNG
scoreboard players set &Increment bhfc.prng 12345
scoreboard players set &Modulus bhfc.prng 21
scoreboard players set &Multiplier bhfc.prng 1103515245
scoreboard players set &Offset bhfc.prng 0
scoreboard players set &Seed bhfc.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhfc. dummy Four Corners
scoreboard objectives setdisplay sidebar bhfc.
scoreboard objectives add bhfc.pl dummy Four Corners Player List
scoreboard objectives add bhfc.ti dummy Four Corners Timer
scoreboard objectives add bhfc.chi dummy Four Corners Count Hiders
scoreboard objectives add bhfc.cvr dummy Four Corners Count Seekers
scoreboard objectives add bhfc.gl dummy Four Corners Virus Glowing
scoreboard objectives add bhfc.cl dummy Four Corners Calculations
scoreboard objectives add bhfc.igl dummy Four Corners Input Glowing
scoreboard objectives add bhfc.igt dummy Four Corners Input Game Time
scoreboard objectives add bhfc.icd dummy Four Corners Input Countdown
scoreboard objectives add bhfc.st dummy Four Corners State
scoreboard teams add bhfc.h Four Corners Hiders
scoreboard teams option bhfc.h friendlyfire false
scoreboard teams option bhfc.h collisionRule never
scoreboard teams option bhfc.h deathMessageVisibility always
scoreboard teams option bhfc.h nametagVisibility never
scoreboard teams option bhfc.h color green
scoreboard teams option bhfc.h seeFriendlyInvisibles false
scoreboard teams add bhfc.v Four Corners Seekers
scoreboard teams option bhfc.v friendlyfire false
scoreboard teams option bhfc.v collisionRule never
scoreboard teams option bhfc.v deathMessageVisibility always
scoreboard teams option bhfc.v color yellow
scoreboard teams add bhfc.d_y Four Corners Display Yellow
scoreboard teams option bhfc.d_y color yellow
scoreboard teams add bhfc.d_g Four Corners Display Green
scoreboard teams option bhfc.d_g color green
scoreboard teams join bhfc.d_y Countdown
scoreboard teams join bhfc.d_y Minutes
scoreboard teams join bhfc.d_y Seconds
scoreboard teams join bhfc.d_y Seekers
scoreboard teams join bhfc.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhfc.stand","bhfc.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhfc.prng","bhfc.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhfc.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhfc.prng] ~ ~ ~ execute @s[tag=bhfc.prng_true] ~ ~ ~ scoreboard players add &Seed bhfc.prng 1073741824
