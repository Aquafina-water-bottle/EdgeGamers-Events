scoreboard players operation &Countdown bhfc.cl = @s bhfc.icd
scoreboard players operation &Countdown bhfc.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhfc.cl = @s bhfc.icd
scoreboard players operation &CountdownMinutes bhfc.cl = @s bhfc.icd
scoreboard players operation &CountdownMinutes bhfc.cl /= 60 g.number
scoreboard players operation @s bhfc.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhfc.cl = @s bhfc.icd
scoreboard players reset &CountdownAdditional bhfc.cl
execute @s[score_bhfc.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhfc.cl 0
scoreboard players set @s bhfc.icd 0
