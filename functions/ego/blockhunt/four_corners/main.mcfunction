scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1358,y=111,z=-190,dx=90,dy=-107,dz=-90] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-518834439] gp.id 518834438
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-518834437] gp.id 518834438
kill @e[x=-1358,y=111,z=-190,dx=90,dy=-107,dz=-90,type=minecraft:item,tag=!bhfc.entity]
execute @e[type=minecraft:armor_stand,score_bhfc.icd_min=1,tag=bhfc.stand] ~ ~ ~ function ego:blockhunt/four_corners/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhfc.igt_min=1,tag=bhfc.stand] ~ ~ ~ function ego:blockhunt/four_corners/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhfc.igl_min=1,tag=bhfc.stand] ~ ~ ~ function ego:blockhunt/four_corners/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhfc.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhfc.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhfc.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhfc.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhfc.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhfc.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhfc.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhfc.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhfc.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhfc.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhfc.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhfc.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhfc.pl_min=1,score_bhfc.pl=2] bhfc.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhfc.pl_min=1,score_bhfc.pl=2] bhfc.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhfc.pl_min=1,score_bhfc.pl=2] bhfc.pl 0
scoreboard players add @a bhfc.pl 0
execute @e[type=minecraft:armor_stand,score_bhfc.st_min=0,score_bhfc.st=0,tag=bhfc.stand] ~ ~ ~ function ego:blockhunt/four_corners/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhfc.st_min=1,score_bhfc.st=1,tag=bhfc.stand] ~ ~ ~ function ego:blockhunt/four_corners/countdown
execute @e[type=minecraft:armor_stand,score_bhfc.st_min=2,score_bhfc.st=2,tag=bhfc.stand] ~ ~ ~ function ego:blockhunt/four_corners/during_round
execute @e[type=minecraft:armor_stand,score_bhfc.st_min=3,score_bhfc.st=3,tag=bhfc.stand] ~ ~ ~ function ego:blockhunt/four_corners/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.chi 0
execute @a[m=2,team=bhfc.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.chi 1
scoreboard players operation Hiders bhfc. = @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.cvr 0
execute @a[m=2,team=bhfc.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.cvr 1
scoreboard players operation Seekers bhfc. = @e[type=minecraft:armor_stand,tag=bhfc.stand] bhfc.cvr
