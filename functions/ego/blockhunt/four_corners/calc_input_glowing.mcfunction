scoreboard players operation &GlowingUntil bhfc.cl = @s bhfc.igl
scoreboard players operation &GlowingUntil bhfc.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhfc.cl = @s bhfc.igl
scoreboard players operation &GlowingUntilMinutes bhfc.cl = @s bhfc.igl
scoreboard players operation &GlowingUntilMinutes bhfc.cl /= 60 g.number
scoreboard players operation @s bhfc.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhfc.cl = @s bhfc.igl
scoreboard players reset &GlowingUntilAdditional bhfc.cl
execute @s[score_bhfc.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhfc.cl 0
scoreboard players set @s bhfc.igl 0
