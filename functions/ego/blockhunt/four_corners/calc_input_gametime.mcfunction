scoreboard players operation &GameTime bhfc.cl = @s bhfc.igt
scoreboard players operation &GameTime bhfc.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhfc.cl = @s bhfc.igt
scoreboard players operation &GameTimeMinutes bhfc.cl = @s bhfc.igt
scoreboard players operation &GameTimeMinutes bhfc.cl /= 60 g.number
scoreboard players operation @s bhfc.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhfc.cl = @s bhfc.igt
scoreboard players reset &GameTimeAdditional bhfc.cl
execute @s[score_bhfc.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhfc.cl 0
scoreboard players set @s bhfc.igt 0
