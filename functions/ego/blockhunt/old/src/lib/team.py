from typing import List

if __name__ == "__main__":
    import sys
    sys.path.append("..")
    del sys

from lib.dynamic_utilities import MCDynamicUtility, MCDynamicUtilityGroup, DynamicUtilityException
from lib.consts import Colors


__all__ = [
    "Team",
    "Teams",
    ]


class Team(MCDynamicUtility):
    """
    General object to represent a scoreboard team

    It is expected to have team options stated at the beginning and unchanging.
    However, if they have to be changed, change the team.options dict

    Args:
        name (str): team name
        display_name (str): team display name, defaults to empty string
        prefix (Optional[str]): Used for making the teams cache
        **options (str: str): values to hold all team options
    """

    def __init__(self, name: str, display_name="", remove_self=True, prefix=None, **options):
        super().__init__(name)

        if len(name) > 16:
            raise DynamicUtilityException(f"The team name {name!r} cannot be larger than 16 characters (it is {len(name)} chars long)")

        if len(display_name) > 32:
            raise DynamicUtilityException(f"The team display name {display_name!r} cannot be larger than 32 characters (it is {len(display_name)} chars long)")

        self.name = name
        self.display_name = display_name
        self.remove_self = remove_self
        self.prefix = prefix
        self.options = {}

        for option, value in options.items():
            self.add_option(option, value)

    def add_option(self, option: str, option_value: str):
        """
        Adds a single option to the options dictionary

        Args:
            option (str): Team option
            option_value (str): Team option value
        """
        self.options[option] = option_value

    def team_cache(self) -> str:
        """
        Creates the following line if the team has a color:
        (6 spaces) "Team_Name": "Color code"

        This is used by the BungeeTabListPlus plugin to set the team as the appropriate color.

        Otherwise, returns an empty string
        """
        if "color" in self.options:
            color = self.options["color"]
            color_code = Colors.CODE_DICT[color]

            # checks for prefix
            if self.name.startswith("_") and self.prefix is not None:
                name = f"{self.prefix}.{self.name[1:]}"
            else:
                name = self.name
            return f'      "{name}": "{color_code}"'
        return ""

    def cmd_init(self) -> str:
        """
        Creates "team add" and "team option" commands
        """
        # strips to remove the display name if it doesn't exist
        team_cmd = f"team add {self.name} {self.display_name}".strip()

        self.commands.append(team_cmd)

        # iterates through the options dictionary
        for option, option_value in self.options.items():
            option_cmd = f"team {self.name} {option} = {option_value}"
            self.commands.append(option_cmd)

        return self.cmd_output()

    def cmd_term(self) -> str:
        """
        Creates the "team remove" command
        """
        if self.remove_self:
            return f"team remove {self.name}"
        return ""


class Teams(MCDynamicUtilityGroup):
    """
    General container for holding Team objects
    """

    def new(self, text: str, display=None, remove=True, prefix=None):
        """
        Allows input of teams from multi-line string (without tab spaces)

        Note anything starting with "#" will be ignored

        Args:
            text (str): block of text to define multiple teams
            display (Optional[str]): display name that goes at the beginning of all team display names
                If the team display name is ".", it is completely replaced
            remove (bool): Whether the teams will be automatically removed or not
            prefix (Optional[str]): Used for making the teams cache

        Examples:
            >>> teams = Teams()

            Valid teams:
            >>> teams_str = '''
            ... # Base team for spectators
            ... _ _
            ...     color = white
            ...
            ... # Green team for royal rumble
            ... _g Green
            ...     color = green
            ...     nametagVisibility = hideForOtherTeams
            ...     friendlyfire = false
            ...     collisionRule = pushOwnTeam
            ...
            ... # blue team for royal rumble
            ... _b Blue
            ...     color = blue
            ...     nametagVisibility = hideForOtherTeams
            ...     friendlyfire = false
            ...     collisionRule = pushOwnTeam
            ... '''

            >>> teams.new(teams_str, display="Royal Rumble")
            >>> teams.new("_nopush No Push", display="Royal Rumble", remove=False)

            >>> print(teams.cmd_init())
            team add _ Royal Rumble
            team _ color = white
            team add _g Royal Rumble Green
            team _g color = green
            team _g nametagVisibility = hideForOtherTeams
            team _g friendlyfire = false
            team _g collisionRule = pushOwnTeam
            team add _b Royal Rumble Blue
            team _b color = blue
            team _b nametagVisibility = hideForOtherTeams
            team _b friendlyfire = false
            team _b collisionRule = pushOwnTeam
            team add _nopush Royal Rumble No Push

            >>> print(teams.cmd_term())
            team remove _
            team remove _g
            team remove _b

            Invalid teams:
            >>> teams.new("color = white")
            Traceback (most recent call last):
                ...
            lib.dynamic_utilities.DynamicUtilityException: A team must be defined before any team options can be set ('color = white')

            >>> teams.new("missing_params")
            Traceback (most recent call last):
                ...
            lib.dynamic_utilities.DynamicUtilityException: Missing parameter: display_name ('missing_params')
        """

        # strips the lines to remove the newlines at the end and any other whitespace
        # also only appends to list if the line is not empty, and doesn't start with #
        lines = super().new(text)

        # holds the current team
        current_team = None
        for line in lines:

            # splits in two in anticipation for a team option being set
            data = line.split(" ", 2)
            if len(data) == 1:
                raise DynamicUtilityException(f"Missing parameter: display_name ({line!r})")

            # if the given line is setting a constant (the second argument is "=")
            if data[1] == "=":
                if current_team is None:
                    raise DynamicUtilityException(f"A team must be defined before any team options can be set ({line!r})")
                option, option_value = data[0], data[2]
                current_team.add_option(option, option_value)
                continue

            # only splits once since now there should be two arguments: name, display_name
            data = line.split(" ", 1)
            team_name, team_display = data

            if team_display == "_":
                team_display = display
            elif display:
                team_display = f"{display} {team_display}"

            current_team = Team(team_name, display_name=team_display, remove_self=remove, prefix=prefix)
            self.add(current_team)

    def cmd_init(self):
        """
        Writes the teams_cache.txt file as well as create the commands
        """
        team_cache_lines = []
        for team in self.teams:
            line = team.team_cache()
            if line:
                team_cache_lines.append(line)

        if team_cache_lines:
            with open("teams_cache.txt", "w") as file:
                for line in team_cache_lines:
                    file.write(line + "\n")

        return super().cmd_init()

    @property
    def teams(self) -> List[Team]:
        """
        Returns:
            list of Team objects
        """
        return list(self._utilities.values())

if __name__ == "__main__":
    import doctest
    doctest.testmod()


