scoreboard players operation &GlowingUntil bho.cl = @s bho.igl
scoreboard players operation &GlowingUntil bho.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bho.cl = @s bho.igl
scoreboard players operation &GlowingUntilMinutes bho.cl = @s bho.igl
scoreboard players operation &GlowingUntilMinutes bho.cl /= 60 g.number
scoreboard players operation @s bho.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bho.cl = @s bho.igl
scoreboard players reset &GlowingUntilAdditional bho.cl
execute @s[score_bho.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bho.cl 0
scoreboard players set @s bho.igl 0
