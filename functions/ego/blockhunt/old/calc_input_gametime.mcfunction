scoreboard players operation &GameTime bho.cl = @s bho.igt
scoreboard players operation &GameTime bho.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bho.cl = @s bho.igt
scoreboard players operation &GameTimeMinutes bho.cl = @s bho.igt
scoreboard players operation &GameTimeMinutes bho.cl /= 60 g.number
scoreboard players operation @s bho.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bho.cl = @s bho.igt
scoreboard players reset &GameTimeAdditional bho.cl
execute @s[score_bho.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bho.cl 0
scoreboard players set @s bho.igt 0
