scoreboard players operation &Countdown bho.cl = @s bho.icd
scoreboard players operation &Countdown bho.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bho.cl = @s bho.icd
scoreboard players operation &CountdownMinutes bho.cl = @s bho.icd
scoreboard players operation &CountdownMinutes bho.cl /= 60 g.number
scoreboard players operation @s bho.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bho.cl = @s bho.icd
scoreboard players reset &CountdownAdditional bho.cl
execute @s[score_bho.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bho.cl 0
scoreboard players set @s bho.icd 0
