scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1194,y=45,z=-175,dx=72,dy=-42,dz=72] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-1452979272] gp.id 1452979271
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-1452979270] gp.id 1452979271
kill @e[x=-1194,y=45,z=-175,dx=72,dy=-42,dz=72,type=minecraft:item,tag=!bho.entity]
execute @e[type=minecraft:armor_stand,score_bho.icd_min=1,tag=bho.stand] ~ ~ ~ function ego:blockhunt/old/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bho.igt_min=1,tag=bho.stand] ~ ~ ~ function ego:blockhunt/old/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bho.igl_min=1,tag=bho.stand] ~ ~ ~ function ego:blockhunt/old/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bho.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bho.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bho.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bho.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bho.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bho.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bho.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bho.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bho.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bho.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bho.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bho.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bho.pl_min=1,score_bho.pl=2] bho.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bho.pl_min=1,score_bho.pl=2] bho.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bho.pl_min=1,score_bho.pl=2] bho.pl 0
scoreboard players add @a bho.pl 0
execute @e[type=minecraft:armor_stand,score_bho.st_min=0,score_bho.st=0,tag=bho.stand] ~ ~ ~ function ego:blockhunt/old/wait_for_start
execute @e[type=minecraft:armor_stand,score_bho.st_min=1,score_bho.st=1,tag=bho.stand] ~ ~ ~ function ego:blockhunt/old/countdown
execute @e[type=minecraft:armor_stand,score_bho.st_min=2,score_bho.st=2,tag=bho.stand] ~ ~ ~ function ego:blockhunt/old/during_round
execute @e[type=minecraft:armor_stand,score_bho.st_min=3,score_bho.st=3,tag=bho.stand] ~ ~ ~ function ego:blockhunt/old/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bho.stand] bho.chi 0
execute @a[m=2,team=bho.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bho.stand] bho.chi 1
scoreboard players operation Hiders bho. = @e[type=minecraft:armor_stand,tag=bho.stand] bho.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bho.stand] bho.cvr 0
execute @a[m=2,team=bho.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bho.stand] bho.cvr 1
scoreboard players operation Seekers bho. = @e[type=minecraft:armor_stand,tag=bho.stand] bho.cvr
