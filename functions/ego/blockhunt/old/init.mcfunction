scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1452979271
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1194,y=45,z=-175,dx=72,dy=-42,dz=72] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHO","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Old","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1452979271"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Old","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Old","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1452979271"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1452979271
scoreboard objectives add bho.prng dummy Pseudo-RNG
scoreboard players set &Increment bho.prng 12345
scoreboard players set &Modulus bho.prng 17
scoreboard players set &Multiplier bho.prng 1103515245
scoreboard players set &Offset bho.prng 0
scoreboard players set &Seed bho.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bho. dummy Old
scoreboard objectives setdisplay sidebar bho.
scoreboard objectives add bho.pl dummy Old Player List
scoreboard objectives add bho.ti dummy Old Timer
scoreboard objectives add bho.chi dummy Old Count Hiders
scoreboard objectives add bho.cvr dummy Old Count Seekers
scoreboard objectives add bho.gl dummy Old Virus Glowing
scoreboard objectives add bho.cl dummy Old Calculations
scoreboard objectives add bho.igl dummy Old Input Glowing
scoreboard objectives add bho.igt dummy Old Input Game Time
scoreboard objectives add bho.icd dummy Old Input Countdown
scoreboard objectives add bho.st dummy Old State
scoreboard teams add bho.h Old Hiders
scoreboard teams option bho.h friendlyfire false
scoreboard teams option bho.h collisionRule never
scoreboard teams option bho.h deathMessageVisibility always
scoreboard teams option bho.h nametagVisibility never
scoreboard teams option bho.h color green
scoreboard teams option bho.h seeFriendlyInvisibles false
scoreboard teams add bho.v Old Seekers
scoreboard teams option bho.v friendlyfire false
scoreboard teams option bho.v collisionRule never
scoreboard teams option bho.v deathMessageVisibility always
scoreboard teams option bho.v color yellow
scoreboard teams add bho.d_y Old Display Yellow
scoreboard teams option bho.d_y color yellow
scoreboard teams add bho.d_g Old Display Green
scoreboard teams option bho.d_g color green
scoreboard teams join bho.d_y Countdown
scoreboard teams join bho.d_y Minutes
scoreboard teams join bho.d_y Seconds
scoreboard teams join bho.d_y Seekers
scoreboard teams join bho.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bho.stand","bho.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bho.stand] bho.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bho.stand] bho.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bho.stand] bho.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bho.stand] bho.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bho.stand] bho.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bho.prng","bho.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bho.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bho.prng] ~ ~ ~ execute @s[tag=bho.prng_true] ~ ~ ~ scoreboard players add &Seed bho.prng 1073741824
