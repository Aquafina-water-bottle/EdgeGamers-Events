scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 942106152
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-130,y=4,z=-315,dx=121,dy=60,dz=181] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHVR1","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus 1","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 942106152"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Virus 1","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus 1","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 942106152"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 942106152
scoreboard objectives add bhvr1.prng dummy Pseudo-RNG
scoreboard players set &Increment bhvr1.prng 12345
scoreboard players set &Modulus bhvr1.prng 8
scoreboard players set &Multiplier bhvr1.prng 1103515245
scoreboard players set &Offset bhvr1.prng 0
scoreboard players set &Seed bhvr1.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhvr1. dummy Virus 1
scoreboard objectives setdisplay sidebar bhvr1.
scoreboard objectives add bhvr1.pl dummy Virus 1 Player List
scoreboard objectives add bhvr1.ti dummy Virus 1 Timer
scoreboard objectives add bhvr1.chi dummy Virus 1 Count Hiders
scoreboard objectives add bhvr1.cvr dummy Virus 1 Count Seekers
scoreboard objectives add bhvr1.gl dummy Virus 1 Virus Glowing
scoreboard objectives add bhvr1.cl dummy Virus 1 Calculations
scoreboard objectives add bhvr1.igl dummy Virus 1 Input Glowing
scoreboard objectives add bhvr1.igt dummy Virus 1 Input Game Time
scoreboard objectives add bhvr1.icd dummy Virus 1 Input Countdown
scoreboard objectives add bhvr1.st dummy Virus 1 State
scoreboard teams add bhvr1.h Virus 1 Hiders
scoreboard teams option bhvr1.h friendlyfire false
scoreboard teams option bhvr1.h collisionRule never
scoreboard teams option bhvr1.h deathMessageVisibility always
scoreboard teams option bhvr1.h nametagVisibility never
scoreboard teams option bhvr1.h color green
scoreboard teams option bhvr1.h seeFriendlyInvisibles false
scoreboard teams add bhvr1.v Virus 1 Seekers
scoreboard teams option bhvr1.v friendlyfire false
scoreboard teams option bhvr1.v collisionRule never
scoreboard teams option bhvr1.v deathMessageVisibility always
scoreboard teams option bhvr1.v color yellow
scoreboard teams add bhvr1.d_y Virus 1 Display Yellow
scoreboard teams option bhvr1.d_y color yellow
scoreboard teams add bhvr1.d_g Virus 1 Display Green
scoreboard teams option bhvr1.d_g color green
scoreboard teams join bhvr1.d_y Countdown
scoreboard teams join bhvr1.d_y Minutes
scoreboard teams join bhvr1.d_y Seconds
scoreboard teams join bhvr1.d_y Seekers
scoreboard teams join bhvr1.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhvr1.stand","bhvr1.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhvr1.prng","bhvr1.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhvr1.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhvr1.prng] ~ ~ ~ execute @s[tag=bhvr1.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr1.prng 1073741824
