scoreboard players operation &Countdown bhvr1.cl = @s bhvr1.icd
scoreboard players operation &Countdown bhvr1.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhvr1.cl = @s bhvr1.icd
scoreboard players operation &CountdownMinutes bhvr1.cl = @s bhvr1.icd
scoreboard players operation &CountdownMinutes bhvr1.cl /= 60 g.number
scoreboard players operation @s bhvr1.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhvr1.cl = @s bhvr1.icd
scoreboard players reset &CountdownAdditional bhvr1.cl
execute @s[score_bhvr1.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhvr1.cl 0
scoreboard players set @s bhvr1.icd 0
