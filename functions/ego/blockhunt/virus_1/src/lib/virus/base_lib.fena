$py:
    import json
    import lib.location
    from lib.virus import Virus
    from lib.coords import Coords

    with open("main.json") as file:
        virus_json = json.load(file)

    # gets the location from lib.location, and assumes it exists
    # private so they aren't used
    _location_str = virus_json["location"]
    _virus_location = getattr(lib.location, _location_str)

    virus = Virus(
        location=_virus_location,
        tp_to_wait_coords=Coords(virus_json["tp_to_wait_coords"]),
        tp_to_arena_coords=(None if virus_json["tp_to_arena_coords"] is None else Coords(virus_json["tp_to_arena_coords"])),
    )


$py:
    from typing import NamedTuple

    from lib.consts import Effects, Sounds, Enchants
    from lib.objective import Objectives
    from lib.team import Teams
    from lib.const_ints import ConstInts

    # general constants
    select_all = virus.location.select_all
    objectives = Objectives()
    teams = Teams()
    const_ints = ConstInts()

    prefix = virus.location.prefix_str

    # all input settings
    class MfuncInput(NamedTuple):
        mfunc_name: str
        var_name: str
        objective: str
        default_time: int

    countdown_mfunc_input = MfuncInput("countdown", "&Countdown", "_icd", 60)
    gametime_mfunc_input = MfuncInput("gametime", "&GameTime", "_igt", 600)
    glowing_mfunc_input = MfuncInput("glowing", "&GlowingUntil", "_igl", 300)
    mfunc_inputs = [countdown_mfunc_input, gametime_mfunc_input, glowing_mfunc_input]

    objectives.new(f"""
        # The main display objective
        _ _ _

        # players (state): Player state for this event
        # 0 = being outside the map
        # 1 = part of the virus map, meaning the players are properly initialized
        # 2 = actually participating in the round
        _pl _ Player List

        # _stand: count down the time on the countdown and the game time
        _ti _ Timer

        # _stand: count how many hiders and virus players exist
        _chi _ Count {hiders_name}
        _cvr _ Count {viruses_name}

        # _stand (boolean): show whether the players glow or not
        # specifically for during_round
        # 0 = None
        # 1 = Virus
        _gl _ Virus Glowing

        # _stand: misc. calculations
        _cl _ Calculations

        # _stand: Input for a custom glowing until setting
        _igl _ Input Glowing
        # _stand: Input for a custom game time setting
        _igt _ Input Game Time
        # _stand: Input for a custom game time setting
        _icd _ Input Countdown

        # _stand (state): stores the state of the round
        # 0 = waiting for the round to start
        # 1 = during countdown
        # 2 = after countdown, main game
        # 3 = during round end
        _st _ State

    """, virus.location.name_str)
    objectives["_"].setdisplay("sidebar")

    const_ints.add_constants(20, 60)

    teams.new(f"""
        _h {hiders_name}
            friendlyfire = false
            collisionRule = never
            deathMessageVisibility = always
            nametagVisibility = never
            color = green

        _v {viruses_name}
            friendlyfire = false
            collisionRule = never
            deathMessageVisibility = always
            color = yellow

        _d_y Display Yellow
            color = yellow

        _d_g Display Green
            color = green
    """, display=virus.location.name_str, prefix=prefix)


# arbitrary: displays the information for the host
$macro(display_host_info):
    title @a[g.host=1] actionbar {"text":"","extra":[
        {"text":"Initial game time: [","color":"green"},
            {"score":{"name":"&GameTimeMinutes","objective":"_cl"},"color":"yellow","bold":"true"},
            {"text":":","color":"yellow","bold":"true"},
            {"score":{"name":"&GameTimeAdditional","objective":"_cl"},"color":"yellow","bold":"true"},
            {"score":{"name":"&GameTimeSeconds","objective":"_cl"},"color":"yellow","bold":"true"},
            {"text":" (","color":"yellow"},
            {"score":{"name":"&GameTimeTotalSeconds","objective":"_cl"},"color":"yellow"},
            {"text":"s)","color":"yellow"},

        {"text":"], Glowing until: [","color":"green"},
            {"score":{"name":"&GlowingUntilMinutes","objective":"_cl"},"color":"yellow","bold":"true"},
            {"text":":","color":"yellow","bold":"true"},
            {"score":{"name":"&GlowingUntilAdditional","objective":"_cl"},"color":"yellow","bold":"true"},
            {"score":{"name":"&GlowingUntilSeconds","objective":"_cl"},"color":"yellow","bold":"true"},
            {"text":" (","color":"yellow"},
            {"score":{"name":"&GlowingUntilTotalSeconds","objective":"_cl"},"color":"yellow"},
            {"text":"s)","color":"yellow"},

        {"text":"], Countdown: [","color":"green"},
            {"score":{"name":"&CountdownMinutes","objective":"_cl"},"color":"yellow","bold":"true"},
            {"text":":","color":"yellow","bold":"true"},
            {"score":{"name":"&CountdownAdditional","objective":"_cl"},"color":"yellow","bold":"true"},
            {"score":{"name":"&CountdownSeconds","objective":"_cl"},"color":"yellow","bold":"true"},
            {"text":" (","color":"yellow"},
            {"score":{"name":"&CountdownTotalSeconds","objective":"_cl"},"color":"yellow"},
            {"text":"s)","color":"yellow"},

        {"text":"]","color":"green"}
    ]}

!set prefix = $(virus.location.prefix_str)

# as _stand: display the time in tellraw messages
$macro(disp_time_text):
    @s[_ti=$(5*60*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"5 minutes remain!","color":"yellow"}'))
    @s[_ti=$(4*60*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"4 minutes remain!","color":"yellow"}'))
    @s[_ti=$(3*60*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"3 minutes remain!","color":"yellow"}'))
    @s[_ti=$(2*60*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"2 minutes remain!","color":"yellow"}'))
    @s[_ti=$(1*60*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"1 minute remains!","color":"yellow"}'))

    @s[_ti=$(30*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"30 seconds remain!","color":"yellow"}'))
    @s[_ti=$(15*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"15 seconds remain!","color":"yellow"}'))
    @s[_ti=$(5*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"5!","color":"yellow"}'))
    @s[_ti=$(4*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"4!","color":"yellow"}'))
    @s[_ti=$(3*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"3!","color":"yellow"}'))
    @s[_ti=$(2*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"2!","color":"yellow"}'))
    @s[_ti=$(1*20)]: tellraw @a $(virus.location.surround_json(r'{"text":"1!","color":"yellow"}'))

# as _stand: display the time in the sidebar
$macro(disp_time_score):
    Seconds _ti = @s _ti
    Seconds _ti /= 20
    Seconds _ti %= 60
    Seconds _ti += 1
    Seconds _ = Seconds _ti

    Minutes _ti = @s _ti
    Minutes _ti /= 1200
    Minutes _ = Minutes _ti

# as _stand: to increment and display the time
$macro(use_timer):
    @s[_ti=1..] _ti -= 1
    $disp_time_text()
    $disp_time_score()


# arbitrary: set general player effects during the round
$macro(set_player_effects):
    effect @a[g.sa=1,m=2,_pl=2,team=_v] + $(Effects.STRENGTH) 3 90 true
    effect @a[g.sa=1,m=2,_pl=2] + $(Effects.RESIST) 3 3 true
    effect @a[g.sa=1,m=2,_pl=2] + $(Effects.HP) 100 100 true


$edit_type_py_init()
$edit_py_init()


!mfunc init:
    $(virus.cmd_init())
    $(objectives.cmd_init())
    $(teams.cmd_init())

    team _d_y + Countdown
    team _d_y + Minutes
    team _d_y + Seconds
    team _d_y + $(viruses_name)
    team _d_g + $(hiders_name)

    $(virus.summon_spawn): summon armor_stand ~ ~ ~ {Tags:["_stand","_entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
    @e[type=armor_stand,_stand] _gl = 0
    @e[type=armor_stand,_stand] _st = 0

    $for(mfunc_input in mfunc_inputs):
        $py:
            _, _, objective, default_time = mfunc_input
        @e[type=armor_stand,_stand] $(objective) = $(default_time)

    $edit_init()
    $edit_type_init()

!mfunc main debug=false:
    $(virus.cmd_main())

    # kills all items
    kill @e[$(select_all),!_entity,type=item]

    # calculates input from _i* objectives
    $for(mfunc_input in mfunc_inputs):
        $py:
            mfunc_name, _, objective, _ = mfunc_input
        @e[type=armor_stand,_stand,$(objective)=1..]: function calc_input_$(mfunc_name)

    # displays info for the host
    $display_host_info()


    # Initialization cases (set _pl = 0):
    # just rejoined as regular ranks: auto teleport to spawn
    #   covers players logging out in the arena
    # just rejoined inside arena as special ranks (g.sa=1,g.lg=1): reinitialize
    # just teleported from spawn (g.stp=1): initialize
    # dead (g.de=1): initialize
    # not initialized yet (_pl=null) : initialize

    @a[g.sa=1,m=2,g.lg=1,_pl=1..2] _pl = 0
    @a[g.sa=1,m=2,g.stp=1,_pl=1..2] _pl = 0
    @a[m=2,g.de=1,_pl=1..2] _pl = 0
    @a _pl += 0

    @e[type=armor_stand,_stand,_st=0]: function wait_for_start
    @e[type=armor_stand,_stand,_st=1]: function countdown
    @e[type=armor_stand,_stand,_st=2]: function during_round
    @e[type=armor_stand,_stand,_st=3]: function wait_for_reset

    # gets the number of hiders
    @e[type=armor_stand,_stand] _chi = 0
    @a[g.sa=1,m=2,team=_h]: @e[type=armor_stand,_stand] _chi += 1
    $(hiders_name) _ = @e[type=armor_stand,_stand] _chi

    # gets the number of virus
    @e[type=armor_stand,_stand] _cvr = 0
    @a[g.sa=1,m=2,team=_v]: @e[type=armor_stand,_stand] _cvr += 1
    $(viruses_name) _ = @e[type=armor_stand,_stand] _cvr

    $edit_type_main()
    $edit_main()

!mfunc term:
    # clears player and teleports them back to the spawn
    # used before the cmd_term to so g.host isn't reset
    @a[_pl=1..2,m=2]: function reset_player
    $(virus.location.cmd_tp("@a[_pl=1..2,m=2]"))

    $(virus.cmd_term())

    kill @e[_entity]

    $edit_type_term()
    $edit_term()

    $(objectives.cmd_term())
    $(teams.cmd_term())

    # requires resets because they are singleton classes
    # when multiple files are built using `make.py build all`
    # the singleton actually carries through that lmfao
    $py:
        objectives.reset()
        teams.reset()


# _stand: waiting period before the game starts
!mfunc wait_for_start debug=false:
    # initializes by:
    # - clear inventory, effects
    # initializes as whatever their current team is
    # - if they don't have a team, initializes as hider
    # this is to ensure that if a virus attempts to relog, they will remain a virus
    @a[g.sa=1,m=2,_pl=0,team=_v]: function init_player_as_virus
    @a[g.sa=1,m=2,_pl=0]: function init_player_as_hider

    # gives glowing to virus
    effect @a[g.sa=1,m=2,team=_v] + $(Effects.GLOW) 3 0 true


# _stand: starts the game
!mfunc start_round:
    # opens the doors
    title @a actionbar {"text":"The doors are now open","color":"green"}
    @a: playsound $(Sounds.LEVEL) voice @s

    $edit_type_round_start()
    $edit_round_start()

    # sets the round time
    @s _ti = &Countdown _cl

    # sets the state
    @s _st = 1


# _stand: used during the countdown
!mfunc countdown debug=false:
    $edit_type_countdown()
    $edit_countdown()

    $use_timer()
    $set_player_effects()

    # adds to round by:
    # - adding hiders to the round as a hider
    # - adding any just-initialized players as a hider
    # - simply sets their player state to 2 if they're a virus
    @a[g.sa=1,m=2,_pl=1,team=_h]: function add_to_round_as_hider
    @a[g.sa=1,m=2,_pl=0]: function add_to_round_as_hider
    @a[g.sa=1,m=2,_pl=1] _pl = 2

    # gives glowing to virus
    effect @a[g.sa=1,m=2,team=_v] + $(Effects.GLOW) 3 0 true

    # @a[g.sa=1,m=2,_pl=1]: function add_to_round
    @s[_ti=..0]: function end_countdown


# _stand: once the countdown ends
!mfunc end_countdown:
    Countdown reset _
    title @a subtitle {"text":""}
    title @a subtitle {"text":"The $(viruses_name.lower()) has been released!","color":"yellow"}
    @a: playsound $(Sounds.WITHER) voice @s

    $edit_type_countdown_end()
    $edit_countdown_end()

    @s _ti = &GameTime _cl
    @s _st = 2


# _stand: used during the round
!mfunc during_round debug=false:
    $use_timer()
    @s[_ti=..0]: function stop_round

    $set_player_effects()

    # adds to round by:
    # - clear inventory, effects
    # - joining virus
    # - teleporting the virus to the expected location
    @a[g.sa=1,m=2,_pl=0..1]: function add_to_round_as_virus

    # calculates the value of _gl
    # difference of &GlowingUntil - _ti
    # such that positive means virus still has glowing, and negative means glowing is removed
    @s _cl = &GlowingUntil _cl
    @s _cl -= @s _ti

    @s[_cl=0..,_gl=1..]: tellraw @a $(virus.location.surround_json(r'{"text":"Glowing has been removed from all ' + viruses_name.lower() + r'!","color":"yellow"}'))
    @s[_cl=0..,_gl=1..] @a: playsound $(Sounds.XP) voice @a[c=1]
    @s[_cl=0..,_gl=1..] _gl = 0

    # adds back glowing if necessary
    @s[_cl=..-1,_gl=..0] _gl = 1

    # finally sets the glowing effect
    @s[_gl=1]: effect @a[g.sa=1,m=2,team=_v] + $(Effects.GLOW) 3 0 true

    $edit_type_during_round()
    $edit_during_round()

    # detects whether the virus won
    @e[type=armor_stand,_stand,_chi=0]: function stop_round


# _stand: when there are no more hiders or when the timer runs out
!mfunc stop_round:
    # if the virus won, the hider number is 0
    @s[_chi=0]: title @a title {"text":"The $(viruses_name.lower()) won!","color":"yellow"}
    @s[_chi=0]: tellraw @a $(virus.location.surround_json(r'{"text":"The %s won!","color":"yellow"}' % viruses_name.lower()))

    # if the hiders won, then is more than one hider
    @s[_chi=1..]: title @a title {"text":"Time!","color":"green"}
    @s[_chi=1..]: title @a subtitle {"text":"The $(hiders_name.lower()) won!","color":"green"}
    @s[_chi=1..]: tellraw @a $(virus.location.surround_json(r'{"text":"The time is up! The %s won!","color":"yellow"}' % hiders_name.lower()))
    @s[_chi=1..]: Seconds reset _
    @s[_chi=1..]: Minutes reset _

    tellraw @a[g.host=1] {"text":"","extra":[
        $(virus.location.prefix_json),
        {"text":"$(hiders_name): ","color":"green"},
        {"selector":"@a[g.sa=1,m=2,_pl=2,team=_h]"}
    ]}

    tellraw @a[g.host=1] {"text":"","extra":[
        $(virus.location.prefix_json),
        {"text":"$(viruses_name): ","color":"yellow"},
        {"selector":"@a[g.sa=1,m=2,_pl=2,team=_v]"}
    ]}

    $edit_type_stop_round()
    $edit_stop_round()

    @s _st = 3


# _stand: waiting for the round to reset
!mfunc wait_for_reset debug=false:
    # gives all glowing and resistance
    effect @a[g.sa=1,m=2,_pl=1..2] + $(Effects.GLOW) 3 0 true
    effect @a[g.sa=1,m=2,_pl=1..2] + $(Effects.RESIST) 3 10 true
    effect @a[g.sa=1,m=2,_pl=1..2] + $(Effects.HP) 100 100 true

    # initializes anyone new by:
    # - clear inventory, effects
    # - joining hider
    @a[g.sa=1,m=2,_pl=0]: function init_player_as_hider

    $edit_wait_for_reset()
    $edit_type_wait_for_reset()


# _stand: when the round actually stops
!mfunc reset_round:
    # teleports anyone in the game back to spawn, joins hiders and reinitializes them
    $(virus.location.cmd_tp("@a[g.sa=1,m=2,_pl=2]"))
    team _h + @a[g.sa=1,m=2,_pl=2,team=_v]
    @a[g.sa=1,m=2,_pl=2] _pl = 0

    $edit_type_round_reset()
    $edit_round_reset()

    Countdown reset _
    Minutes reset _
    Seconds reset _
    @s _st = 0



# as players: clears their inventory and gives the book to the host
$macro(clear_player):
    item @s - *
    effect @s - *

    # gives the book only to the host
    $(virus.location.cmd_book("@s[g.host=1]"))

    $edit_type_clear_player()
    $edit_clear_player()


# as players: sets their team as a hider
$macro(join_hider):
    team _h + @s

# as players: sets their team as a virus
$macro(join_virus):
    # adds golden armor
    item @s armor.head = golden_helmet      {Unbreakable:1,ench:[{id:$(Enchants.BINDING.enchant_id),lvl:1},{id:$(Enchants.VANISH.enchant_id),lvl:1}]}
    item @s armor.chest = golden_chestplate {Unbreakable:1,ench:[{id:$(Enchants.BINDING.enchant_id),lvl:1},{id:$(Enchants.VANISH.enchant_id),lvl:1}]}
    item @s armor.legs = golden_leggings    {Unbreakable:1,ench:[{id:$(Enchants.BINDING.enchant_id),lvl:1},{id:$(Enchants.VANISH.enchant_id),lvl:1}]}
    item @s armor.feet = golden_boots       {Unbreakable:1,ench:[{id:$(Enchants.BINDING.enchant_id),lvl:1},{id:$(Enchants.VANISH.enchant_id),lvl:1}]}

    # actually joins the team
    team _v + @s

    # displays that they are now a virus
    title @s title {"text":"You are now","color":"yellow"}
    title @s subtitle {"text":"the $(virus_name.lower())!","color":"yellow"}
    tellraw @a {"text":"","extra":[
        $(virus.location.prefix_json),
        {"selector":"@s"},
        {"text":" became a ","color":"gray"},
        {"text":"$(virus_name.lower())","color":"yellow"},
        {"text":"!","color":"gray"}
    ]}


# as players: resets the player when the entire game ends
!mfunc reset_player:
    $clear_player()

# as players: initializes them only as a hider
!mfunc init_player_as_hider:
    $clear_player()
    $join_hider()

    $edit_type_init_player_as_hider()
    $edit_init_player_as_hider()

    # sets their player state to 1 to indicate they are initialized
    @s _pl = 1

# as players: initializes them as a hider from the virus (teleports to spawn)
!mfunc init_player_as_hider_from_virus:
    function init_player_as_hider
    $(virus.location.cmd_tp("@s"))

# as players: initializes them as a virus
!mfunc init_player_as_virus:
    $clear_player()
    $join_virus()

    # teleports to the waiting room
    tp @s $(virus.tp_to_wait_coords)

    $edit_type_init_player_as_virus()
    $edit_init_player_as_virus()

    # sets their player state to 1 to indicate they are initialized
    @s _pl = 1


!mfunc add_to_round_as_hider:
    $clear_player()
    $join_hider()

    $edit_type_add_to_round_as_hider()
    $edit_add_to_round_as_hider()

    # sets their player state to 1 to indicate they are added to the round
    @s _pl = 2

!mfunc add_to_round_as_virus:
    $clear_player()
    $join_virus()

    team _v + @s

    # teleports to the arena
    $if(virus.tp_to_arena_coords is not None):
        tp @s $(virus.tp_to_arena_coords)

    $edit_type_add_to_round_as_virus()
    $edit_add_to_round_as_virus()

    # sets their player state to 1 to indicate they are added to the round
    @s _pl = 2


$for(mfunc_input in mfunc_inputs):
    $py:
        mfunc_name, var_name, objective, _ = mfunc_input

    # _stand: sets the variable name and displays the minutes/seconds
    !mfunc calc_input_$(mfunc_name):
        $(var_name) _cl = @s $(objective)
        $(var_name) _cl *= 20

        # sets total seconds
        $(var_name)TotalSeconds _cl = @s $(objective)

        # sets general minutes
        $(var_name)Minutes _cl = @s $(objective)
        $(var_name)Minutes _cl /= 60

        # sets display seconds to armor stand and variable
        @s $(objective) %= 60
        $(var_name)Seconds _cl = @s $(objective)

        # sets the first digit of the seconds as 0 if seconds < 10
        # so x:9 displays as :09, and x:20 displays as x:20 (reset)
        $(var_name)Additional reset _cl
        @s[$(objective)=..9]: $(var_name)Additional _cl = 0

        @s $(objective) = 0

# player: arbitrary add to the hider team
# - this should only be used when manually adding someone to the hider team
!mfunc to_hider:
    @s[team=!_v]: function init_player_as_hider if @e[type=armor_stand,_stand,_st=0..1]
    @s[team=_v]: function init_player_as_hider_from_virus if @e[type=armor_stand,_stand,_st=0..1]
    function add_to_round_as_hider if @e[type=armor_stand,_stand,_st=2..]

# player: arbitrary add to the virus team
# - this should only be used when manually adding someone to the virus team
!mfunc to_virus:
    function init_player_as_virus if @e[type=armor_stand,_stand,_st=0..1]
    function add_to_round_as_virus if @e[type=armor_stand,_stand,_st=2..]


!folder input:
    $(virus.mfunc_input())

    !mfunc start_round:
        @e[type=armor_stand,_stand]: function start_round

    !mfunc stop_round:
        @e[type=armor_stand,_stand]: function stop_round

    !mfunc reset_round:
        @e[type=armor_stand,_stand]: function reset_round

    # randomly select a virus
    !mfunc select_rand_virus:
        # ensures that this function is not used during the round
        @e[type=armor_stand,_stand,_st=0..1] @r[g.sa=1,m=2,team=_h]: function init_player_as_virus
        @e[type=armor_stand,_stand,_st=2..]: tellraw @a[g.host=1] $(virus.location.surround_json(r'{"text":"You cannot randomly select a virus during the round!","color":"red"}'))

    # randomly convert a virus to a hider
    !mfunc remove_one_virus:
        # ensures that this function is not used during the round
        @e[type=armor_stand,_stand,_st=0..1] @r[g.sa=1,m=2,team=_v]: function init_player_as_hider_from_virus
        @e[type=armor_stand,_stand,_st=2..]: tellraw @a[g.host=1] $(virus.location.surround_json(r'{"text":"You cannot randomly remove a virus during the round!","color":"red"}'))

    # displays a tellraw so the host can input a team name
    !mfunc select_team:
        tellraw @a[g.host=1] {"text":"","extra":[
            $(virus.location.prefix_json),
            {"text":"Choose team: ","color":"gray"},
            {"text":"$(hider_name)","color":"green","italic":true,
                "clickEvent":{"action":"suggest_command","value":"/execute PLAYER_NAME ~ ~ ~ $(virus.location.cmd_func('to_hider'))"},
                "hoverEvent":{"action":"show_text","value":{"text":"Force someone to join the $(hiders_name.lower())","color":"green"}}
            },
            {"text":", ","color":"gray","italic":true},
            {"text":"$(virus_name)","color":"yellow","italic":true,
                "clickEvent":{"action":"suggest_command","value":"/execute PLAYER_NAME ~ ~ ~ $(virus.location.cmd_func('to_virus'))"},
                "hoverEvent":{"action":"show_text","value":{"text":"Force someone to join the $(viruses_name.lower())","color":"yellow"}}
            }
        ]}

    !mfunc to_default_settings:
        $for(mfunc_input in mfunc_inputs):
            $py:
                _, _, objective, default_time = mfunc_input
            @e[type=armor_stand,_stand] $(objective) = $(default_time)

    !mfunc change_settings:
        tellraw @s[g.host=1] {"text":"","extra":[
            $(virus.location.prefix_json),

            {"text":"Game time: ","color":"gray"},
            {"text":"[+]","color":"yellow",
                "hoverEvent":{"action":"show_text","value":{"text":"Edit game time for $(virus.location.name_str) in seconds","color":"yellow"}},
                "clickEvent":{"action":"suggest_command","value":"/scoreboard players set @e[type=armor_stand,tag=$(prefix).stand] $(prefix).igt #"}
            },

            {"text":"    Glowing until: ","color":"gray"},
            {"text":"[+]","color":"yellow",
                "hoverEvent":{"action":"show_text","value":{"text":"Edit when glowing stops for $(virus.location.name_str) in seconds","color":"yellow"}},
                "clickEvent":{"action":"suggest_command","value":"/scoreboard players set @e[type=armor_stand,tag=$(prefix).stand] $(prefix).igl #"}
            },

            {"text":"    Countdown: ","color":"gray"},
            {"text":"[+]","color":"yellow",
                "hoverEvent":{"action":"show_text","value":{"text":"Edit how long the countdown lasts for $(virus.location.name_str) in seconds","color":"yellow"}},
                "clickEvent":{"action":"suggest_command","value":"/scoreboard players set @e[type=armor_stand,tag=$(prefix).stand] $(prefix).icd #"}
            }
        ]}

    !mfunc add_one_min:
        @e[type=armor_stand,_stand] _ti += $(20*60)

    !mfunc add_30s:
        @e[type=armor_stand,_stand] _ti += $(20*30)

    !mfunc remove_30s:
        @e[type=armor_stand,_stand] _ti -= $(20*30)

    !mfunc remove_one_min:
        @e[type=armor_stand,_stand] _ti -= $(20*60)



!mfunc book:
    replaceitem entity @s slot.weapon.offhand written_book 1 0 {
        title:"$(virus.location.name_str) Book",author:"eGO",pages:["{\"text\":\"\",\"extra\":[
            {\"text\":\"$(virus.location.name_str) Settings\\n\",\"bold\":true},
            {\"text\":\"\\n\"},

            {\"text\":\"$(virus.location.prefix_disp): \",\"color\":\"dark_gray\"},
            {\"text\":\"Start\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function init\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Starts the $(type_name.lower()) map so it can be ran\",\"color\":\"green\"}}
            },
            {\"text\":\" / \",\"color\":\"gray\"},
            {\"text\":\"Stop\\n\",\"color\":\"red\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function term\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Ends the $(type_name.lower()) map\",\"color\":\"red\"}}
            },

            {\"text\":\"Round: \",\"color\":\"dark_gray\"},
            {\"text\":\"Start\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/start_round\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Starts an individual round\",\"color\":\"dark_green\"}}
            },
            {\"text\":\" / \",\"color\":\"gray\"},
            {\"text\":\"Stop\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/stop_round\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Stops an individual round\",\"color\":\"gold\"}}
            },
            {\"text\":\" / \",\"color\":\"gray\"},
            {\"text\":\"Reset\\n\\n\",\"color\":\"red\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/reset_round\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Resets an individual round\",\"color\":\"red\"}}
            },


            {\"text\":\"Select $(virus_name): \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/select_rand_virus\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Chooses a $(virus_name.lower())\",\"color\":\"dark_green\"}}
            },

            {\"text\":\" \",\"color\":\"gray\"},
            {\"text\":\"[-]\\n\",\"color\":\"red\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/remove_one_virus\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Removes a $(virus_name.lower())\",\"color\":\"red\"}}
            },

            {\"text\":\"Edit Teams: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\\n\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/select_team\"}
            },

            {\"text\":\"Time: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/add_one_min\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Adds 1 minute\",\"color\":\"dark_green\"}}
            },
            {\"text\":\" \",\"color\":\"gray\"},
            {\"text\":\"[+]\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/add_30s\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Adds 30 seconds\",\"color\":\"gold\"}}
            },
            {\"text\":\" \",\"color\":\"gray\"},
            {\"text\":\"[-]\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/remove_30s\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Removes 30 seconds\",\"color\":\"gold\"}}
            },
            {\"text\":\" \",\"color\":\"gray\"},
            {\"text\":\"[-]\\n\",\"color\":\"red\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/remove_one_min\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Removes 1 minute\",\"color\":\"red\"}}
            },

            {\"text\":\"Settings: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/change_settings\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Changes settings\",\"color\":\"dark_green\"}}
            },
            {\"text\":\" \",\"color\":\"gray\"},
            {\"text\":\"[-]\\n\",\"color\":\"red\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/to_default_settings\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Resets settings to their defaults\",\"color\":\"red\"}}
            }

        ]}"
    ]}

