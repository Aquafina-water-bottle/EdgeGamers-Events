scoreboard players operation &GlowingUntil bhvr1.cl = @s bhvr1.igl
scoreboard players operation &GlowingUntil bhvr1.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhvr1.cl = @s bhvr1.igl
scoreboard players operation &GlowingUntilMinutes bhvr1.cl = @s bhvr1.igl
scoreboard players operation &GlowingUntilMinutes bhvr1.cl /= 60 g.number
scoreboard players operation @s bhvr1.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhvr1.cl = @s bhvr1.igl
scoreboard players reset &GlowingUntilAdditional bhvr1.cl
execute @s[score_bhvr1.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhvr1.cl 0
scoreboard players set @s bhvr1.igl 0
