scoreboard players operation &GameTime bhvr1.cl = @s bhvr1.igt
scoreboard players operation &GameTime bhvr1.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhvr1.cl = @s bhvr1.igt
scoreboard players operation &GameTimeMinutes bhvr1.cl = @s bhvr1.igt
scoreboard players operation &GameTimeMinutes bhvr1.cl /= 60 g.number
scoreboard players operation @s bhvr1.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhvr1.cl = @s bhvr1.igt
scoreboard players reset &GameTimeAdditional bhvr1.cl
execute @s[score_bhvr1.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhvr1.cl 0
scoreboard players set @s bhvr1.igt 0
