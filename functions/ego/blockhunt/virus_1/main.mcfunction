scoreboard players set @a g.sa 0
scoreboard players set @a[x=-130,y=4,z=-315,dx=121,dy=60,dz=181] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-942106153] gp.id 942106152
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-942106151] gp.id 942106152
kill @e[x=-130,y=4,z=-315,dx=121,dy=60,dz=181,type=minecraft:item,tag=!bhvr1.entity]
execute @e[type=minecraft:armor_stand,score_bhvr1.icd_min=1,tag=bhvr1.stand] ~ ~ ~ function ego:blockhunt/virus_1/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhvr1.igt_min=1,tag=bhvr1.stand] ~ ~ ~ function ego:blockhunt/virus_1/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhvr1.igl_min=1,tag=bhvr1.stand] ~ ~ ~ function ego:blockhunt/virus_1/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhvr1.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhvr1.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhvr1.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhvr1.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhvr1.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhvr1.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhvr1.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhvr1.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhvr1.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhvr1.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhvr1.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhvr1.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhvr1.pl_min=1,score_bhvr1.pl=2] bhvr1.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhvr1.pl_min=1,score_bhvr1.pl=2] bhvr1.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhvr1.pl_min=1,score_bhvr1.pl=2] bhvr1.pl 0
scoreboard players add @a bhvr1.pl 0
execute @e[type=minecraft:armor_stand,score_bhvr1.st_min=0,score_bhvr1.st=0,tag=bhvr1.stand] ~ ~ ~ function ego:blockhunt/virus_1/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhvr1.st_min=1,score_bhvr1.st=1,tag=bhvr1.stand] ~ ~ ~ function ego:blockhunt/virus_1/countdown
execute @e[type=minecraft:armor_stand,score_bhvr1.st_min=2,score_bhvr1.st=2,tag=bhvr1.stand] ~ ~ ~ function ego:blockhunt/virus_1/during_round
execute @e[type=minecraft:armor_stand,score_bhvr1.st_min=3,score_bhvr1.st=3,tag=bhvr1.stand] ~ ~ ~ function ego:blockhunt/virus_1/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.chi 0
execute @a[m=2,team=bhvr1.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.chi 1
scoreboard players operation Hiders bhvr1. = @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.cvr 0
execute @a[m=2,team=bhvr1.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.cvr 1
scoreboard players operation Seekers bhvr1. = @e[type=minecraft:armor_stand,tag=bhvr1.stand] bhvr1.cvr
