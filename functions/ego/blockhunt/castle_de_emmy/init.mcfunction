scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 2053033941
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1570,y=73,z=-7,dx=-93,dy=-73,dz=-95] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHCDE","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Castle de Emmy","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2053033941"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Castle de Emmy","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Castle de Emmy","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2053033941"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 2053033941
scoreboard objectives add bhcde.prng dummy Pseudo-RNG
scoreboard players set &Increment bhcde.prng 12345
scoreboard players set &Modulus bhcde.prng 24
scoreboard players set &Multiplier bhcde.prng 1103515245
scoreboard players set &Offset bhcde.prng 0
scoreboard players set &Seed bhcde.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhcde. dummy Castle de Emmy
scoreboard objectives setdisplay sidebar bhcde.
scoreboard objectives add bhcde.pl dummy Castle de Emmy Player List
scoreboard objectives add bhcde.ti dummy Castle de Emmy Timer
scoreboard objectives add bhcde.chi dummy Castle de Emmy Count Hiders
scoreboard objectives add bhcde.cvr dummy Castle de Emmy Count Seekers
scoreboard objectives add bhcde.gl dummy Castle de Emmy Virus Glowing
scoreboard objectives add bhcde.cl dummy Castle de Emmy Calculations
scoreboard objectives add bhcde.igl dummy Castle de Emmy Input Glowing
scoreboard objectives add bhcde.igt dummy Castle de Emmy Input Game Time
scoreboard objectives add bhcde.icd dummy Castle de Emmy Input Countdown
scoreboard objectives add bhcde.st dummy Castle de Emmy State
scoreboard teams add bhcde.h Castle de Emmy Hiders
scoreboard teams option bhcde.h friendlyfire false
scoreboard teams option bhcde.h collisionRule never
scoreboard teams option bhcde.h deathMessageVisibility always
scoreboard teams option bhcde.h nametagVisibility never
scoreboard teams option bhcde.h color green
scoreboard teams option bhcde.h seeFriendlyInvisibles false
scoreboard teams add bhcde.v Castle de Emmy Seekers
scoreboard teams option bhcde.v friendlyfire false
scoreboard teams option bhcde.v collisionRule never
scoreboard teams option bhcde.v deathMessageVisibility always
scoreboard teams option bhcde.v color yellow
scoreboard teams add bhcde.d_y Castle de Emmy Display Yellow
scoreboard teams option bhcde.d_y color yellow
scoreboard teams add bhcde.d_g Castle de Emmy Display Green
scoreboard teams option bhcde.d_g color green
scoreboard teams join bhcde.d_y Countdown
scoreboard teams join bhcde.d_y Minutes
scoreboard teams join bhcde.d_y Seconds
scoreboard teams join bhcde.d_y Seekers
scoreboard teams join bhcde.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhcde.stand","bhcde.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhcde.prng","bhcde.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhcde.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhcde.prng] ~ ~ ~ execute @s[tag=bhcde.prng_true] ~ ~ ~ scoreboard players add &Seed bhcde.prng 1073741824
