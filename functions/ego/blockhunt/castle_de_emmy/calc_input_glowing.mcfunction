scoreboard players operation &GlowingUntil bhcde.cl = @s bhcde.igl
scoreboard players operation &GlowingUntil bhcde.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhcde.cl = @s bhcde.igl
scoreboard players operation &GlowingUntilMinutes bhcde.cl = @s bhcde.igl
scoreboard players operation &GlowingUntilMinutes bhcde.cl /= 60 g.number
scoreboard players operation @s bhcde.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhcde.cl = @s bhcde.igl
scoreboard players reset &GlowingUntilAdditional bhcde.cl
execute @s[score_bhcde.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhcde.cl 0
scoreboard players set @s bhcde.igl 0
