scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1570,y=73,z=-7,dx=-93,dy=-73,dz=-95] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-2053033942] gp.id 2053033941
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-2053033940] gp.id 2053033941
kill @e[x=-1570,y=73,z=-7,dx=-93,dy=-73,dz=-95,type=minecraft:item,tag=!bhcde.entity]
execute @e[type=minecraft:armor_stand,score_bhcde.icd_min=1,tag=bhcde.stand] ~ ~ ~ function ego:blockhunt/castle_de_emmy/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhcde.igt_min=1,tag=bhcde.stand] ~ ~ ~ function ego:blockhunt/castle_de_emmy/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhcde.igl_min=1,tag=bhcde.stand] ~ ~ ~ function ego:blockhunt/castle_de_emmy/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhcde.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhcde.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhcde.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhcde.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhcde.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhcde.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhcde.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhcde.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhcde.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhcde.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhcde.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhcde.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhcde.pl_min=1,score_bhcde.pl=2] bhcde.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhcde.pl_min=1,score_bhcde.pl=2] bhcde.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhcde.pl_min=1,score_bhcde.pl=2] bhcde.pl 0
scoreboard players add @a bhcde.pl 0
execute @e[type=minecraft:armor_stand,score_bhcde.st_min=0,score_bhcde.st=0,tag=bhcde.stand] ~ ~ ~ function ego:blockhunt/castle_de_emmy/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhcde.st_min=1,score_bhcde.st=1,tag=bhcde.stand] ~ ~ ~ function ego:blockhunt/castle_de_emmy/countdown
execute @e[type=minecraft:armor_stand,score_bhcde.st_min=2,score_bhcde.st=2,tag=bhcde.stand] ~ ~ ~ function ego:blockhunt/castle_de_emmy/during_round
execute @e[type=minecraft:armor_stand,score_bhcde.st_min=3,score_bhcde.st=3,tag=bhcde.stand] ~ ~ ~ function ego:blockhunt/castle_de_emmy/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.chi 0
execute @a[m=2,team=bhcde.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.chi 1
scoreboard players operation Hiders bhcde. = @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.cvr 0
execute @a[m=2,team=bhcde.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.cvr 1
scoreboard players operation Seekers bhcde. = @e[type=minecraft:armor_stand,tag=bhcde.stand] bhcde.cvr
