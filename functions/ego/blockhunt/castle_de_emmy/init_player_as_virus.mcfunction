clear @s
effect @s clear
scoreboard players set @s[score_g.host_min=1,score_g.host=1] gp.bk 2053033941
function ego:blockhunt/castle_de_emmy/undisguise
replaceitem entity @s slot.armor.head minecraft:golden_helmet 1 0 {Unbreakable:1,ench:[{id:10,lvl:1},{id:71,lvl:1}]}
replaceitem entity @s slot.armor.chest minecraft:golden_chestplate 1 0 {Unbreakable:1,ench:[{id:10,lvl:1},{id:71,lvl:1}]}
replaceitem entity @s slot.armor.legs minecraft:golden_leggings 1 0 {Unbreakable:1,ench:[{id:10,lvl:1},{id:71,lvl:1}]}
replaceitem entity @s slot.armor.feet minecraft:golden_boots 1 0 {Unbreakable:1,ench:[{id:10,lvl:1},{id:71,lvl:1}]}
scoreboard teams join bhcde.v @s
title @s title {"text":"You are now","color":"yellow"}
title @s subtitle {"text":"the seeker!","color":"yellow"}
tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHCDE","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Castle de Emmy","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2053033941"}},{"text":"]","color":"gray"},{"text":": "},{"selector":"@s"},{"text":" became a ","color":"gray"},{"text":"seeker","color":"yellow"},{"text":"!","color":"gray"}]}
minecraft:tp @s -1660 4 -99 -90 0
scoreboard players set @s bhcde.pl 1
