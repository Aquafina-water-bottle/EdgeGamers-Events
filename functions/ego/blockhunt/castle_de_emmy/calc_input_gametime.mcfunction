scoreboard players operation &GameTime bhcde.cl = @s bhcde.igt
scoreboard players operation &GameTime bhcde.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhcde.cl = @s bhcde.igt
scoreboard players operation &GameTimeMinutes bhcde.cl = @s bhcde.igt
scoreboard players operation &GameTimeMinutes bhcde.cl /= 60 g.number
scoreboard players operation @s bhcde.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhcde.cl = @s bhcde.igt
scoreboard players reset &GameTimeAdditional bhcde.cl
execute @s[score_bhcde.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhcde.cl 0
scoreboard players set @s bhcde.igt 0
