scoreboard players operation &Countdown bhcde.cl = @s bhcde.icd
scoreboard players operation &Countdown bhcde.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhcde.cl = @s bhcde.icd
scoreboard players operation &CountdownMinutes bhcde.cl = @s bhcde.icd
scoreboard players operation &CountdownMinutes bhcde.cl /= 60 g.number
scoreboard players operation @s bhcde.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhcde.cl = @s bhcde.icd
scoreboard players reset &CountdownAdditional bhcde.cl
execute @s[score_bhcde.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhcde.cl 0
scoreboard players set @s bhcde.icd 0
