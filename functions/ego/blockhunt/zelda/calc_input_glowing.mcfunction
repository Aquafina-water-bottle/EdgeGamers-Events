scoreboard players operation &GlowingUntil bhz.cl = @s bhz.igl
scoreboard players operation &GlowingUntil bhz.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhz.cl = @s bhz.igl
scoreboard players operation &GlowingUntilMinutes bhz.cl = @s bhz.igl
scoreboard players operation &GlowingUntilMinutes bhz.cl /= 60 g.number
scoreboard players operation @s bhz.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhz.cl = @s bhz.igl
scoreboard players reset &GlowingUntilAdditional bhz.cl
execute @s[score_bhz.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhz.cl 0
scoreboard players set @s bhz.igl 0
