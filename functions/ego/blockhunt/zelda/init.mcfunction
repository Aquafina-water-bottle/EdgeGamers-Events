scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1233606541
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1127,y=50,z=-218,dx=-86,dy=-47,dz=38] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHZ","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Zelda","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1233606541"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Zelda","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Zelda","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1233606541"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1233606541
scoreboard objectives add bhz.prng dummy Pseudo-RNG
scoreboard players set &Increment bhz.prng 12345
scoreboard players set &Modulus bhz.prng 11
scoreboard players set &Multiplier bhz.prng 1103515245
scoreboard players set &Offset bhz.prng 0
scoreboard players set &Seed bhz.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhz. dummy Zelda
scoreboard objectives setdisplay sidebar bhz.
scoreboard objectives add bhz.pl dummy Zelda Player List
scoreboard objectives add bhz.ti dummy Zelda Timer
scoreboard objectives add bhz.chi dummy Zelda Count Hiders
scoreboard objectives add bhz.cvr dummy Zelda Count Seekers
scoreboard objectives add bhz.gl dummy Zelda Virus Glowing
scoreboard objectives add bhz.cl dummy Zelda Calculations
scoreboard objectives add bhz.igl dummy Zelda Input Glowing
scoreboard objectives add bhz.igt dummy Zelda Input Game Time
scoreboard objectives add bhz.icd dummy Zelda Input Countdown
scoreboard objectives add bhz.st dummy Zelda State
scoreboard teams add bhz.h Zelda Hiders
scoreboard teams option bhz.h friendlyfire false
scoreboard teams option bhz.h collisionRule never
scoreboard teams option bhz.h deathMessageVisibility always
scoreboard teams option bhz.h nametagVisibility never
scoreboard teams option bhz.h color green
scoreboard teams option bhz.h seeFriendlyInvisibles false
scoreboard teams add bhz.v Zelda Seekers
scoreboard teams option bhz.v friendlyfire false
scoreboard teams option bhz.v collisionRule never
scoreboard teams option bhz.v deathMessageVisibility always
scoreboard teams option bhz.v color yellow
scoreboard teams add bhz.d_y Zelda Display Yellow
scoreboard teams option bhz.d_y color yellow
scoreboard teams add bhz.d_g Zelda Display Green
scoreboard teams option bhz.d_g color green
scoreboard teams join bhz.d_y Countdown
scoreboard teams join bhz.d_y Minutes
scoreboard teams join bhz.d_y Seconds
scoreboard teams join bhz.d_y Seekers
scoreboard teams join bhz.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhz.stand","bhz.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhz.prng","bhz.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhz.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhz.prng] ~ ~ ~ execute @s[tag=bhz.prng_true] ~ ~ ~ scoreboard players add &Seed bhz.prng 1073741824
