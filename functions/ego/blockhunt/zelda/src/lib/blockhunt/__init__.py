from typing import List

from fenautils import assert_type, addrepr


@addrepr
class Block:
    """
    Attributes:
        unusable_blocks (Set[str])
        block_type (str)
        block_states (dict str: str)
    """

    unusable_block_types = {
        "minecraft:chest",
        "minecraft:bed",
        "minecraft:wooden_door",
        "minecraft:iron_door",
    }

    def __init__(self, block_type, block_states=None):
        assert_type(block_type, str)
        assert_type(block_states, dict, optional=True)
        if block_states is None:
            block_states = {}

        if block_type in Block.unusable_block_types:
            raise TypeError(f"Unusable block type: {block_type}")

        # checks whether the block type has "minecraft:" before it
        if not block_type.startswith("minecraft:"):
            raise SyntaxError(f"Expected 'minecraft' before {block_type}")

        # checks whether each block state has "--" before it
        for block_state in block_states:
            if not block_state.startswith("--"):
                raise SyntaxError(f"Expected '--' before {block_state}")

        self.block_type = block_type
        self.block_states = block_states

    @property
    def block_states_str(self) -> str:
        """
        Returns:
            str: Its block states as a string value
        """
        assert self.block_states
        return " ".join(f"{key}={value}" for (key, value) in self.block_states.items())

    def disguise(self) -> str:
        """
        Returns:
            str: The full blockdisguise command to disguise a player
        """
        if self.block_states:
            return f"bd disguise {self.block_type} @p[dist=..0] {self.block_states_str}"
        return f"bd disguise {self.block_type} @p[dist=..0]"

    def __eq__(self, other):
        """
        Sees whether this block is equal to another
        """
        return (self.block_type == other.block_type) and (self.block_states == other.block_states)

    def __str__(self):
        """
        Returns a readable string for minecraft
        """
        if self.block_states:
            return f"'{self.block_type} {self.block_states_str}'"
        return f"'{self.block_type}'"


def get_blocks_from_file(file_name: str) -> List[Block]:
    """
    - blocks.txt is formatted as a file with each block separated by newline
        - each block must be preceeded by "minecraft:"
        - all optional block states must be precceeded with "--"
        eg.
            minecraft:diamond_block
            minecraft:gold_block
            minecraft:leaves --variant=jungle
            minecraft:leaves --variant=oak
    """

    with open(file_name) as blocks_file:
        # removes all empty newlines and creates a list of all lines in blocks.txt
        lines = [line for line in blocks_file.read().splitlines() if line.strip()]
        blocks = []

        for line in lines:
            # full_block is a list of strings separated by whitespace
            full_block = line.split()

            # gets the block type and any block states
            block_type = full_block[0]
            block_states = {}

            for full_block_state in full_block[1:]:
                # requires there to be only one "="
                if len(full_block_state.split("=")) != 2:
                    raise SyntaxError(f"Expected a block state and value separated with '=', but got '{full_block_state}'")
                block_state, value = full_block_state.split("=")

                block_states[block_state] = value

            block = Block(block_type, block_states)

            if block in blocks:
                raise SyntaxError(f"Found duplicate block: {block}")

            blocks.append(block)

    return blocks

