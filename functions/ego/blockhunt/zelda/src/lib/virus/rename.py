import os

files = [f for f in os.listdir() if os.path.isfile(f)]

for file_str in files:
    file_name, extension = os.path.splitext(file_str)
    if extension == ".ccu":
        os.rename(file_str, f"{file_name}.fena")
