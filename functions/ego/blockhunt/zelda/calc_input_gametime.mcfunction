scoreboard players operation &GameTime bhz.cl = @s bhz.igt
scoreboard players operation &GameTime bhz.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhz.cl = @s bhz.igt
scoreboard players operation &GameTimeMinutes bhz.cl = @s bhz.igt
scoreboard players operation &GameTimeMinutes bhz.cl /= 60 g.number
scoreboard players operation @s bhz.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhz.cl = @s bhz.igt
scoreboard players reset &GameTimeAdditional bhz.cl
execute @s[score_bhz.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhz.cl 0
scoreboard players set @s bhz.igt 0
