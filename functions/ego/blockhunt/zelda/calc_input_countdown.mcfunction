scoreboard players operation &Countdown bhz.cl = @s bhz.icd
scoreboard players operation &Countdown bhz.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhz.cl = @s bhz.icd
scoreboard players operation &CountdownMinutes bhz.cl = @s bhz.icd
scoreboard players operation &CountdownMinutes bhz.cl /= 60 g.number
scoreboard players operation @s bhz.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhz.cl = @s bhz.icd
scoreboard players reset &CountdownAdditional bhz.cl
execute @s[score_bhz.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhz.cl 0
scoreboard players set @s bhz.icd 0
