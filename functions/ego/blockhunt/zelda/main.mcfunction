scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1127,y=50,z=-218,dx=-86,dy=-47,dz=38] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-1233606542] gp.id 1233606541
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-1233606540] gp.id 1233606541
kill @e[x=-1127,y=50,z=-218,dx=-86,dy=-47,dz=38,type=minecraft:item,tag=!bhz.entity]
execute @e[type=minecraft:armor_stand,score_bhz.icd_min=1,tag=bhz.stand] ~ ~ ~ function ego:blockhunt/zelda/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhz.igt_min=1,tag=bhz.stand] ~ ~ ~ function ego:blockhunt/zelda/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhz.igl_min=1,tag=bhz.stand] ~ ~ ~ function ego:blockhunt/zelda/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhz.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhz.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhz.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhz.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhz.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhz.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhz.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhz.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhz.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhz.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhz.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhz.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhz.pl_min=1,score_bhz.pl=2] bhz.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhz.pl_min=1,score_bhz.pl=2] bhz.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhz.pl_min=1,score_bhz.pl=2] bhz.pl 0
scoreboard players add @a bhz.pl 0
execute @e[type=minecraft:armor_stand,score_bhz.st_min=0,score_bhz.st=0,tag=bhz.stand] ~ ~ ~ function ego:blockhunt/zelda/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhz.st_min=1,score_bhz.st=1,tag=bhz.stand] ~ ~ ~ function ego:blockhunt/zelda/countdown
execute @e[type=minecraft:armor_stand,score_bhz.st_min=2,score_bhz.st=2,tag=bhz.stand] ~ ~ ~ function ego:blockhunt/zelda/during_round
execute @e[type=minecraft:armor_stand,score_bhz.st_min=3,score_bhz.st=3,tag=bhz.stand] ~ ~ ~ function ego:blockhunt/zelda/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.chi 0
execute @a[m=2,team=bhz.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.chi 1
scoreboard players operation Hiders bhz. = @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.cvr 0
execute @a[m=2,team=bhz.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.cvr 1
scoreboard players operation Seekers bhz. = @e[type=minecraft:armor_stand,tag=bhz.stand] bhz.cvr
