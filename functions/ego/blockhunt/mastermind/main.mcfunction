scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1213,y=51,z=-222,dx=73,dy=-47,dz=-72] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-772601973] gp.id 772601972
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-772601971] gp.id 772601972
kill @e[x=-1213,y=51,z=-222,dx=73,dy=-47,dz=-72,type=minecraft:item,tag=!bhmm.entity]
execute @e[type=minecraft:armor_stand,score_bhmm.icd_min=1,tag=bhmm.stand] ~ ~ ~ function ego:blockhunt/mastermind/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhmm.igt_min=1,tag=bhmm.stand] ~ ~ ~ function ego:blockhunt/mastermind/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhmm.igl_min=1,tag=bhmm.stand] ~ ~ ~ function ego:blockhunt/mastermind/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhmm.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhmm.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhmm.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhmm.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhmm.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhmm.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhmm.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhmm.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhmm.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhmm.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhmm.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhmm.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhmm.pl_min=1,score_bhmm.pl=2] bhmm.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhmm.pl_min=1,score_bhmm.pl=2] bhmm.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhmm.pl_min=1,score_bhmm.pl=2] bhmm.pl 0
scoreboard players add @a bhmm.pl 0
execute @e[type=minecraft:armor_stand,score_bhmm.st_min=0,score_bhmm.st=0,tag=bhmm.stand] ~ ~ ~ function ego:blockhunt/mastermind/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhmm.st_min=1,score_bhmm.st=1,tag=bhmm.stand] ~ ~ ~ function ego:blockhunt/mastermind/countdown
execute @e[type=minecraft:armor_stand,score_bhmm.st_min=2,score_bhmm.st=2,tag=bhmm.stand] ~ ~ ~ function ego:blockhunt/mastermind/during_round
execute @e[type=minecraft:armor_stand,score_bhmm.st_min=3,score_bhmm.st=3,tag=bhmm.stand] ~ ~ ~ function ego:blockhunt/mastermind/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.chi 0
execute @a[m=2,team=bhmm.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.chi 1
scoreboard players operation Hiders bhmm. = @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.cvr 0
execute @a[m=2,team=bhmm.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.cvr 1
scoreboard players operation Seekers bhmm. = @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.cvr
