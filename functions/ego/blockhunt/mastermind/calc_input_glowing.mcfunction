scoreboard players operation &GlowingUntil bhmm.cl = @s bhmm.igl
scoreboard players operation &GlowingUntil bhmm.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhmm.cl = @s bhmm.igl
scoreboard players operation &GlowingUntilMinutes bhmm.cl = @s bhmm.igl
scoreboard players operation &GlowingUntilMinutes bhmm.cl /= 60 g.number
scoreboard players operation @s bhmm.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhmm.cl = @s bhmm.igl
scoreboard players reset &GlowingUntilAdditional bhmm.cl
execute @s[score_bhmm.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhmm.cl 0
scoreboard players set @s bhmm.igl 0
