scoreboard players operation &GameTime bhmm.cl = @s bhmm.igt
scoreboard players operation &GameTime bhmm.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhmm.cl = @s bhmm.igt
scoreboard players operation &GameTimeMinutes bhmm.cl = @s bhmm.igt
scoreboard players operation &GameTimeMinutes bhmm.cl /= 60 g.number
scoreboard players operation @s bhmm.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhmm.cl = @s bhmm.igt
scoreboard players reset &GameTimeAdditional bhmm.cl
execute @s[score_bhmm.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhmm.cl 0
scoreboard players set @s bhmm.igt 0
