execute @a[m=2,score_bhmm.pl_min=1,score_bhmm.pl=2] ~ ~ ~ function ego:blockhunt/mastermind/reset_player
scoreboard players set @a[m=2,score_bhmm.pl_min=1,score_bhmm.pl=2] gp.tp 772601972
scoreboard players reset * g.host
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=772601972,score_gp.cgi=772601972,tag=gp.stand] gp.tp 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=772601972,score_gp.cgi=772601972,tag=gp.stand] gp.cgi 0
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHMM","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"BH Mastermind","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 772601972"}},{"text":"]","color":"gray"},{"text":": "},{"text":"BH Mastermind","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"BH Mastermind","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 772601972"}},{"text":" has stopped!","color":"red"}]}
kill @e[tag=bhmm.entity]
scoreboard objectives remove bhmm.prng
scoreboard objectives remove bhmm.
scoreboard objectives remove bhmm.pl
scoreboard objectives remove bhmm.ti
scoreboard objectives remove bhmm.chi
scoreboard objectives remove bhmm.cvr
scoreboard objectives remove bhmm.gl
scoreboard objectives remove bhmm.cl
scoreboard objectives remove bhmm.igl
scoreboard objectives remove bhmm.igt
scoreboard objectives remove bhmm.icd
scoreboard objectives remove bhmm.st
scoreboard teams remove bhmm.h
scoreboard teams remove bhmm.v
scoreboard teams remove bhmm.d_y
scoreboard teams remove bhmm.d_g
