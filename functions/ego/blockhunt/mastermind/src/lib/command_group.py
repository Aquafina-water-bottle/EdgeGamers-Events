from collections import deque
from abc import ABC, abstractmethod

class CommandGroup(ABC):
    """
    General class to be used if the python object is going to output Fena commands

    Methods like `cmd_from_iterable` are used to string together multiple fena commands.
        - Big emphasis on fena commands. Use shortcuts even when defining commands in python when possible.

    Attributes:
        commands (deque): Holds all the commands that will be outputted from
    """

    def __init__(self):
        self.commands = deque()

    @staticmethod
    def cmd_from_iterable(iterable) -> str:
        """
        Joins multiple fena commands and possibly mcfunction statements

        If the first in the iterable is a mcfunction statement,
            commands will be indented with 4 spaces.
        Otherwise, nothing will be indented even if mcfunction
            statements are found elsewhere.

        Attributes:
            iterable (any iterable): Any container of commands

        Returns:
            str: All commands separated by newlines if the command isn't just whitespace

        Examples:
            >>> l = ["say 1", "say 2", "say 3"]
            >>> print(CommandGroup.cmd_from_iterable(l))
            say 1
            say 2
            say 3

            >>> l = ["!mfunc first:", "say 1", "say 2", "say 3", "!mfunc second:", "say 1", "say 2", "say 3"]
            >>> print(CommandGroup.cmd_from_iterable(l))
            !mfunc first:
                say 1
                say 2
                say 3
            !mfunc second:
                say 1
                say 2
                say 3

            Make sure your list starts with a mcfunction declaration if you want indentation:
            >>> l = ["say 1", "say 2", "say 3", "!mfunc second:", "say 1", "say 2", "say 3"]
            >>> print(CommandGroup.cmd_from_iterable(l))
            say 1
            say 2
            say 3
            !mfunc second:
            say 1
            say 2
            say 3
        """
        if iterable and iterable[0].startswith("!mfunc"):
            return_cmds = ""

            newline_sep = ""
            for cmd in iterable:
                if cmd.startswith("!"):
                    return_cmds += newline_sep + cmd
                else:
                    return_cmds += "\n    " + cmd

                # defines newline sep here so the newline doesn't affect the first line
                newline_sep = "\n"
            return return_cmds

        return "\n".join(cmd for cmd in iterable if cmd.strip())

    @staticmethod
    def cmd_from_args(*commands: str) -> str:
        """
        Attributes:
            *commands (str): Any number of commands that should be outputted

        Returns:
            str: All commands separated by newlines
        """
        return CommandGroup.cmd_from_iterable(commands)

    def cmd_output(self, empty=True) -> str:
        """
        Outputs commands from the `commands` attribute

        Attributes:
            empty (bool): Whether the commands should be emptied or not

        Returns:
            str: All commands in the deque separated by newlines
        """
        cmd_str = CommandGroup.cmd_from_iterable(self.commands)
        if empty:
            self.commands = deque()

        return cmd_str

    @abstractmethod
    def cmd_init(self) -> str:
        pass

    def cmd_main(self) -> str:
        pass

    @abstractmethod
    def cmd_term(self) -> str:
        pass


if __name__ == "__main__":
    import doctest
    doctest.testmod()

