minecraft:tp @s[team=bhmm.h] ~ ~0.1 ~
scoreboard players operation &Seed bhmm.prng *= &Multiplier bhmm.prng
scoreboard players operation &Seed bhmm.prng += &Increment bhmm.prng
scoreboard players operation @s bhmm.prng = &Seed bhmm.prng
scoreboard players operation @s bhmm.prng %= &Modulus bhmm.prng
scoreboard players operation @s[score_bhmm.prng=-1] bhmm.prng *= -1 g.number
scoreboard players operation @s bhmm.prng += &Offset bhmm.prng
execute @s[score_bhmm.prng_min=0,score_bhmm.prng=0] ~ ~ ~ function ego:blockhunt/mastermind/disguise/0
execute @s[score_bhmm.prng_min=1,score_bhmm.prng=1] ~ ~ ~ function ego:blockhunt/mastermind/disguise/1
execute @s[score_bhmm.prng_min=2,score_bhmm.prng=2] ~ ~ ~ function ego:blockhunt/mastermind/disguise/2
execute @s[score_bhmm.prng_min=3,score_bhmm.prng=3] ~ ~ ~ function ego:blockhunt/mastermind/disguise/3
execute @s[score_bhmm.prng_min=4,score_bhmm.prng=4] ~ ~ ~ function ego:blockhunt/mastermind/disguise/4
execute @s[score_bhmm.prng_min=5,score_bhmm.prng=5] ~ ~ ~ function ego:blockhunt/mastermind/disguise/5
execute @s[score_bhmm.prng_min=6,score_bhmm.prng=6] ~ ~ ~ function ego:blockhunt/mastermind/disguise/6
execute @s[score_bhmm.prng_min=7,score_bhmm.prng=7] ~ ~ ~ function ego:blockhunt/mastermind/disguise/7
execute @s[score_bhmm.prng_min=8,score_bhmm.prng=8] ~ ~ ~ function ego:blockhunt/mastermind/disguise/8
execute @s[score_bhmm.prng_min=9,score_bhmm.prng=9] ~ ~ ~ function ego:blockhunt/mastermind/disguise/9
execute @s[score_bhmm.prng_min=10,score_bhmm.prng=10] ~ ~ ~ function ego:blockhunt/mastermind/disguise/10
execute @s[score_bhmm.prng_min=11,score_bhmm.prng=11] ~ ~ ~ function ego:blockhunt/mastermind/disguise/11
execute @s[score_bhmm.prng_min=12,score_bhmm.prng=12] ~ ~ ~ function ego:blockhunt/mastermind/disguise/12
execute @s[score_bhmm.prng_min=13,score_bhmm.prng=13] ~ ~ ~ function ego:blockhunt/mastermind/disguise/13
execute @s[score_bhmm.prng_min=14,score_bhmm.prng=14] ~ ~ ~ function ego:blockhunt/mastermind/disguise/14
execute @s[score_bhmm.prng_min=15,score_bhmm.prng=15] ~ ~ ~ function ego:blockhunt/mastermind/disguise/15
execute @s[score_bhmm.prng_min=16,score_bhmm.prng=16] ~ ~ ~ function ego:blockhunt/mastermind/disguise/16
execute @s[score_bhmm.prng_min=17,score_bhmm.prng=17] ~ ~ ~ function ego:blockhunt/mastermind/disguise/17
execute @s[score_bhmm.prng_min=18,score_bhmm.prng=18] ~ ~ ~ function ego:blockhunt/mastermind/disguise/18
execute @s[score_bhmm.prng_min=19,score_bhmm.prng=19] ~ ~ ~ function ego:blockhunt/mastermind/disguise/19
execute @s[score_bhmm.prng_min=20,score_bhmm.prng=20] ~ ~ ~ function ego:blockhunt/mastermind/disguise/20
execute @s[score_bhmm.prng_min=21,score_bhmm.prng=21] ~ ~ ~ function ego:blockhunt/mastermind/disguise/21
execute @s[score_bhmm.prng_min=22,score_bhmm.prng=22] ~ ~ ~ function ego:blockhunt/mastermind/disguise/22
minecraft:tp @s[team=bhmm.h] ~ ~-0.01 ~
