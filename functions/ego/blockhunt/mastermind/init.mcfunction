scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 772601972
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1213,y=51,z=-222,dx=73,dy=-47,dz=-72] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHMM","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"BH Mastermind","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 772601972"}},{"text":"]","color":"gray"},{"text":": "},{"text":"BH Mastermind","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"BH Mastermind","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 772601972"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 772601972
scoreboard objectives add bhmm.prng dummy Pseudo-RNG
scoreboard players set &Increment bhmm.prng 12345
scoreboard players set &Modulus bhmm.prng 23
scoreboard players set &Multiplier bhmm.prng 1103515245
scoreboard players set &Offset bhmm.prng 0
scoreboard players set &Seed bhmm.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhmm. dummy BH Mastermind
scoreboard objectives setdisplay sidebar bhmm.
scoreboard objectives add bhmm.pl dummy BH Mastermind Player List
scoreboard objectives add bhmm.ti dummy BH Mastermind Timer
scoreboard objectives add bhmm.chi dummy BH Mastermind Count Hiders
scoreboard objectives add bhmm.cvr dummy BH Mastermind Count Seekers
scoreboard objectives add bhmm.gl dummy BH Mastermind Virus Glowing
scoreboard objectives add bhmm.cl dummy BH Mastermind Calculations
scoreboard objectives add bhmm.igl dummy BH Mastermind Input Glowing
scoreboard objectives add bhmm.igt dummy BH Mastermind Input Game Time
scoreboard objectives add bhmm.icd dummy BH Mastermind Input Countdown
scoreboard objectives add bhmm.st dummy BH Mastermind State
scoreboard teams add bhmm.h BH Mastermind Hiders
scoreboard teams option bhmm.h friendlyfire false
scoreboard teams option bhmm.h collisionRule never
scoreboard teams option bhmm.h deathMessageVisibility always
scoreboard teams option bhmm.h nametagVisibility never
scoreboard teams option bhmm.h color green
scoreboard teams option bhmm.h seeFriendlyInvisibles false
scoreboard teams add bhmm.v BH Mastermind Seekers
scoreboard teams option bhmm.v friendlyfire false
scoreboard teams option bhmm.v collisionRule never
scoreboard teams option bhmm.v deathMessageVisibility always
scoreboard teams option bhmm.v color yellow
scoreboard teams add bhmm.d_y BH Mastermind Display Yellow
scoreboard teams option bhmm.d_y color yellow
scoreboard teams add bhmm.d_g BH Mastermind Display Green
scoreboard teams option bhmm.d_g color green
scoreboard teams join bhmm.d_y Countdown
scoreboard teams join bhmm.d_y Minutes
scoreboard teams join bhmm.d_y Seconds
scoreboard teams join bhmm.d_y Seekers
scoreboard teams join bhmm.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhmm.stand","bhmm.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmm.stand] bhmm.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhmm.prng","bhmm.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhmm.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhmm.prng] ~ ~ ~ execute @s[tag=bhmm.prng_true] ~ ~ ~ scoreboard players add &Seed bhmm.prng 1073741824
