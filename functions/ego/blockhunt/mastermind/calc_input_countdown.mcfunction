scoreboard players operation &Countdown bhmm.cl = @s bhmm.icd
scoreboard players operation &Countdown bhmm.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhmm.cl = @s bhmm.icd
scoreboard players operation &CountdownMinutes bhmm.cl = @s bhmm.icd
scoreboard players operation &CountdownMinutes bhmm.cl /= 60 g.number
scoreboard players operation @s bhmm.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhmm.cl = @s bhmm.icd
scoreboard players reset &CountdownAdditional bhmm.cl
execute @s[score_bhmm.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhmm.cl 0
scoreboard players set @s bhmm.icd 0
