scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 763457061
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1393,y=38,z=-271,dx=-161,dy=-35,dz=162] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHASV","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"A Small Village","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 763457061"}},{"text":"]","color":"gray"},{"text":": "},{"text":"A Small Village","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"A Small Village","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 763457061"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 763457061
scoreboard objectives add bhasv.prng dummy Pseudo-RNG
scoreboard players set &Increment bhasv.prng 12345
scoreboard players set &Modulus bhasv.prng 25
scoreboard players set &Multiplier bhasv.prng 1103515245
scoreboard players set &Offset bhasv.prng 0
scoreboard players set &Seed bhasv.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhasv. dummy A Small Village
scoreboard objectives setdisplay sidebar bhasv.
scoreboard objectives add bhasv.pl dummy A Small Village Player List
scoreboard objectives add bhasv.ti dummy A Small Village Timer
scoreboard objectives add bhasv.chi dummy A Small Village Count Hiders
scoreboard objectives add bhasv.cvr dummy A Small Village Count Seekers
scoreboard objectives add bhasv.gl dummy A Small Village Virus Glowing
scoreboard objectives add bhasv.cl dummy A Small Village Calculations
scoreboard objectives add bhasv.igl dummy A Small Village Input Glowing
scoreboard objectives add bhasv.igt dummy A Small Village Input Game Time
scoreboard objectives add bhasv.icd dummy A Small Village Input Countdown
scoreboard objectives add bhasv.st dummy A Small Village State
scoreboard teams add bhasv.h A Small Village Hiders
scoreboard teams option bhasv.h friendlyfire false
scoreboard teams option bhasv.h collisionRule never
scoreboard teams option bhasv.h deathMessageVisibility always
scoreboard teams option bhasv.h nametagVisibility never
scoreboard teams option bhasv.h color green
scoreboard teams option bhasv.h seeFriendlyInvisibles false
scoreboard teams add bhasv.v A Small Village Seekers
scoreboard teams option bhasv.v friendlyfire false
scoreboard teams option bhasv.v collisionRule never
scoreboard teams option bhasv.v deathMessageVisibility always
scoreboard teams option bhasv.v color yellow
scoreboard teams add bhasv.d_y A Small Village Display Yellow
scoreboard teams option bhasv.d_y color yellow
scoreboard teams add bhasv.d_g A Small Village Display Green
scoreboard teams option bhasv.d_g color green
scoreboard teams join bhasv.d_y Countdown
scoreboard teams join bhasv.d_y Minutes
scoreboard teams join bhasv.d_y Seconds
scoreboard teams join bhasv.d_y Seekers
scoreboard teams join bhasv.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhasv.stand","bhasv.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhasv.prng","bhasv.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhasv.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhasv.prng] ~ ~ ~ execute @s[tag=bhasv.prng_true] ~ ~ ~ scoreboard players add &Seed bhasv.prng 1073741824
