scoreboard players operation &Countdown bhasv.cl = @s bhasv.icd
scoreboard players operation &Countdown bhasv.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhasv.cl = @s bhasv.icd
scoreboard players operation &CountdownMinutes bhasv.cl = @s bhasv.icd
scoreboard players operation &CountdownMinutes bhasv.cl /= 60 g.number
scoreboard players operation @s bhasv.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhasv.cl = @s bhasv.icd
scoreboard players reset &CountdownAdditional bhasv.cl
execute @s[score_bhasv.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhasv.cl 0
scoreboard players set @s bhasv.icd 0
