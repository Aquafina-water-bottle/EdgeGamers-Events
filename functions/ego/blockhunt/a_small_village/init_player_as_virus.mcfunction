clear @s
effect @s clear
scoreboard players set @s[score_g.host_min=1,score_g.host=1] gp.bk 763457061
function ego:blockhunt/a_small_village/undisguise
replaceitem entity @s slot.armor.head minecraft:golden_helmet 1 0 {Unbreakable:1,ench:[{id:10,lvl:1},{id:71,lvl:1}]}
replaceitem entity @s slot.armor.chest minecraft:golden_chestplate 1 0 {Unbreakable:1,ench:[{id:10,lvl:1},{id:71,lvl:1}]}
replaceitem entity @s slot.armor.legs minecraft:golden_leggings 1 0 {Unbreakable:1,ench:[{id:10,lvl:1},{id:71,lvl:1}]}
replaceitem entity @s slot.armor.feet minecraft:golden_boots 1 0 {Unbreakable:1,ench:[{id:10,lvl:1},{id:71,lvl:1}]}
scoreboard teams join bhasv.v @s
title @s title {"text":"You are now","color":"yellow"}
title @s subtitle {"text":"the seeker!","color":"yellow"}
tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHASV","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"A Small Village","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 763457061"}},{"text":"]","color":"gray"},{"text":": "},{"selector":"@s"},{"text":" became a ","color":"gray"},{"text":"seeker","color":"yellow"},{"text":"!","color":"gray"}]}
minecraft:tp @s -1539 4 -159 180 0
scoreboard players set @s bhasv.pl 1
