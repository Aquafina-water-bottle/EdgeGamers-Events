scoreboard players operation &GameTime bhasv.cl = @s bhasv.igt
scoreboard players operation &GameTime bhasv.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhasv.cl = @s bhasv.igt
scoreboard players operation &GameTimeMinutes bhasv.cl = @s bhasv.igt
scoreboard players operation &GameTimeMinutes bhasv.cl /= 60 g.number
scoreboard players operation @s bhasv.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhasv.cl = @s bhasv.igt
scoreboard players reset &GameTimeAdditional bhasv.cl
execute @s[score_bhasv.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhasv.cl 0
scoreboard players set @s bhasv.igt 0
