scoreboard players operation &GlowingUntil bhasv.cl = @s bhasv.igl
scoreboard players operation &GlowingUntil bhasv.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhasv.cl = @s bhasv.igl
scoreboard players operation &GlowingUntilMinutes bhasv.cl = @s bhasv.igl
scoreboard players operation &GlowingUntilMinutes bhasv.cl /= 60 g.number
scoreboard players operation @s bhasv.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhasv.cl = @s bhasv.igl
scoreboard players reset &GlowingUntilAdditional bhasv.cl
execute @s[score_bhasv.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhasv.cl 0
scoreboard players set @s bhasv.igl 0
