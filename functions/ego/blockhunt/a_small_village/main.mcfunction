scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1393,y=38,z=-271,dx=-161,dy=-35,dz=162] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-763457062] gp.id 763457061
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-763457060] gp.id 763457061
kill @e[x=-1393,y=38,z=-271,dx=-161,dy=-35,dz=162,type=minecraft:item,tag=!bhasv.entity]
execute @e[type=minecraft:armor_stand,score_bhasv.icd_min=1,tag=bhasv.stand] ~ ~ ~ function ego:blockhunt/a_small_village/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhasv.igt_min=1,tag=bhasv.stand] ~ ~ ~ function ego:blockhunt/a_small_village/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhasv.igl_min=1,tag=bhasv.stand] ~ ~ ~ function ego:blockhunt/a_small_village/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhasv.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhasv.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhasv.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhasv.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhasv.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhasv.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhasv.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhasv.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhasv.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhasv.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhasv.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhasv.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhasv.pl_min=1,score_bhasv.pl=2] bhasv.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhasv.pl_min=1,score_bhasv.pl=2] bhasv.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhasv.pl_min=1,score_bhasv.pl=2] bhasv.pl 0
scoreboard players add @a bhasv.pl 0
execute @e[type=minecraft:armor_stand,score_bhasv.st_min=0,score_bhasv.st=0,tag=bhasv.stand] ~ ~ ~ function ego:blockhunt/a_small_village/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhasv.st_min=1,score_bhasv.st=1,tag=bhasv.stand] ~ ~ ~ function ego:blockhunt/a_small_village/countdown
execute @e[type=minecraft:armor_stand,score_bhasv.st_min=2,score_bhasv.st=2,tag=bhasv.stand] ~ ~ ~ function ego:blockhunt/a_small_village/during_round
execute @e[type=minecraft:armor_stand,score_bhasv.st_min=3,score_bhasv.st=3,tag=bhasv.stand] ~ ~ ~ function ego:blockhunt/a_small_village/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.chi 0
execute @a[m=2,team=bhasv.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.chi 1
scoreboard players operation Hiders bhasv. = @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.cvr 0
execute @a[m=2,team=bhasv.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.cvr 1
scoreboard players operation Seekers bhasv. = @e[type=minecraft:armor_stand,tag=bhasv.stand] bhasv.cvr
