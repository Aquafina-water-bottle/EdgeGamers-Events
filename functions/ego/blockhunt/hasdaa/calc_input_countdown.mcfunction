scoreboard players operation &Countdown bhhd.cl = @s bhhd.icd
scoreboard players operation &Countdown bhhd.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhhd.cl = @s bhhd.icd
scoreboard players operation &CountdownMinutes bhhd.cl = @s bhhd.icd
scoreboard players operation &CountdownMinutes bhhd.cl /= 60 g.number
scoreboard players operation @s bhhd.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhhd.cl = @s bhhd.icd
scoreboard players reset &CountdownAdditional bhhd.cl
execute @s[score_bhhd.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhhd.cl 0
scoreboard players set @s bhhd.icd 0
