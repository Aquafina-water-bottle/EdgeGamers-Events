from typing import List

if __name__ == "__main__":
    import sys
    sys.path.append("..")
    del sys

from lib.dynamic_utilities import MCDynamicUtility, MCDynamicUtilityGroup, DynamicUtilityException
from fenautils import addrepr


__all__ = [
    "Objective",
    "Objectives",
    ]


@addrepr
class DisplaySlot:
    """
    Scoreboard objective setdisplay slot
    """

    def __init__(self, value, reset=True):
        """
        Args:
            value (str): The specific slot that objective will be displayed on
            reset (bool): If the objective in the specified slot resets when it terminates
        """
        self.value = value
        self.reset = reset

    def __str__(self):
        return self.value


class Objective(MCDynamicUtility):
    """
    Represents a scoreboard objective

    Args:
        name (str): objective name
        criteria (str): objective criteria, defaults to "dummy"
            Note that "_" is the same as "dummy"
        display_name (str): objective display name, defaults to an empty string
        remove_self (bool): if the objective is removed when calling Objective.cmd_term()
        initialize_self (bool): if the objective is actually created when calling Objective.cmd_init()
        prefix (Optional[str]): General prefix for this objective (generally not used)

    Attributes:
        name (str): objective name
        criteria (str): objective criteria, defaults to "dummy"
            Note that "_" is the same as "dummy"
        display_name (str): objective display name, defaults to an empty string
        remove_self (bool): if the objective is removed when calling Objective.cmd_term()
        initialize_self (bool): if the objective is actually created when calling Objective.cmd_init()
        setdisplay_slots (str):
        slots (list of (str, bool)): Holds tuples containing the slot, and whether to reset said slot
        prefix (Optional[str]): General prefix for this objective (generally not used)
    """

    def __init__(self, name: str, criteria="_", display_name="", remove_self=True, initialize_self=True, prefix=None):
        super().__init__(name)

        if len(name) > 16:
            raise DynamicUtilityException(f"The objective name {name!r} cannot be larger than 16 characters (it is {len(name)} chars long)")

        if len(display_name) > 32:
            raise DynamicUtilityException(f"The objective display name {display_name!r} cannot be larger than 32 characters (it is {len(display_name)} chars long)")

        if criteria == "_":
            self.criteria = "dummy"
        else:
            self.criteria = criteria

        self.display_name = display_name
        self.remove_self = remove_self
        self.initialize_self = initialize_self
        self.prefix = prefix
        self._slots = []
        self._scores = {}

    def setdisplay(self, *slots: str, reset_slot=True):
        """
        Args:
            *slots (str): Slots that objective will be displayed on
            reset_slot (bool): If the objective in the specified slot resets when it terminates
                - This is only useful if the objective isn't going to be removed on cmd_term

        Examples:
            >>> objective = Objective("_test")
            >>> objective.setdisplay("sidebar")
            >>> objective.setdisplay("list", reset_slot=False)

            >>> objective2 = Objective("_stay", remove_self=False)
            >>> objective2.setdisplay("belowName")
            >>> objective2.setdisplay("sidebar.team.aqua", reset_slot=False)

            >>> print(objective.cmd_init())
            objective add _test dummy
            objective setdisplay sidebar _test
            objective setdisplay list _test

            >>> print(objective.cmd_term())
            objective remove _test

            >>> print(objective2.cmd_init())
            objective add _stay dummy
            objective setdisplay belowName _stay
            objective setdisplay sidebar.team.aqua _stay

            >>> print(objective2.cmd_term())
            objective setdisplay belowName
        """

        for slot in slots:
            display_slot = DisplaySlot(slot, reset_slot)
            self._slots.append(display_slot)

    def add_score(self, name: str, value: int):
        """
        Maps some name to some score in the objective

        Args:
            name (str): The target that will be used to set the objective
            value (int): The scoreboard value to be mapped to the target

        Examples:
            >>> objective = Objective("t.test")
            >>> objective.add_score("2", 2)
            >>> objective.add_score("&fake_player", 15)

            >>> print(objective.cmd_init())
            objective add t.test dummy
            &fake_player t.test = 15
            2 t.test = 2

            >>> print(objective.cmd_term())
            objective remove t.test
        """
        self._scores[name] = value

    def cmd_init(self) -> str:
        """
        Adds all objectives using:
            objective add
            objective setdisplay

        Adds all constants using:
            NAME OBJ_NAME = VALUE
        """
        # strips to remove the display name if it doesn't exist
        if self.initialize_self:
            cmd_obj_add = f"objective add {self.name} {self.criteria} {self.display_name}".strip()
            self.commands.append(cmd_obj_add)

        # If slots is not empty
        for slot in self._slots:
            self.commands.append(f"objective setdisplay {slot.value} {self.name}")

        # adds scores
        for name, value in sorted(self._scores.items()):
            self.commands.append(f"{name} {self.name} = {value}")

        return self.cmd_output()

    def cmd_term(self) -> str:
        """
        Removes objectives and resets setdisplay slots using:
            objective remove
            objective setdisplay
        """

        # Does not reset the setdisplay slot because removing
        # the objective automatically resets the slot
        if self.remove_self:
            return f"objective remove {self.name}"

        for slot in self._slots:
            if slot.reset:
                self.commands.append(f"objective setdisplay {slot.value}")
        return self.cmd_output()


class Objectives(MCDynamicUtilityGroup):
    """
    Represents a group of objectives
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def new(self, text: str, display=None, remove=True, prefix=None):
        """
        Allows input of objectives from a multi-line string
            - This ignores surrounding whitespace and lines starting with "#"

        Args:
            text: block of text to define multiple objectives
            display: display name that goes at the beginning of all objective display names
                If the objective display name is "_", it is replaced with "dummy"
            remove: Whether the objectives will be automatically removed or not
            prefix (Optional[str]): General prefix for these objectives (generally not used)

        Examples:
            >>> objectives = Objectives()

            Valid objectives:
            >>> objectives_str = '''
            ... # Display slot
            ... _ _ _
            ...
            ... # Used on players to store the player state
            ... # 0 = about to be initialized
            ... # 1 = initialized
            ... # 2 = in the round
            ... _pl _ Player List
            ...
            ... # Test constants (note that tabulation doesn't matter, although I'd advise you to format it with tabs)
            ... _const _ Constant Numbers
            ...     some_const = 25
            ...     another_const = 3
            ...
            ... # Custom criteria
            ... _cs stat.useItem.minecraft.carrot_on_a_stick Carrot Stick
            ... _xp _ XP calc
            ... '''
            >>> objectives.new(objectives_str, display="Royal Rumble")
            >>> objectives.new("_h health Health", display="Royal Rumble", remove=False)

            >>> print(objectives.cmd_init())
            objective add _ dummy Royal Rumble
            objective add _pl dummy Royal Rumble Player List
            objective add _const dummy Royal Rumble Constant Numbers
            another_const _const = 3
            some_const _const = 25
            objective add _cs stat.useItem.minecraft.carrot_on_a_stick Royal Rumble Carrot Stick
            objective add _xp dummy Royal Rumble XP calc
            objective add _h health Royal Rumble Health

            >>> print(objectives.cmd_term())
            objective remove _
            objective remove _pl
            objective remove _const
            objective remove _cs
            objective remove _xp

            Invalid objectives:
            >>> objectives.new("some_const = 25")
            Traceback (most recent call last):
                ...
            lib.dynamic_utilities.DynamicUtilityException: An objective must be defined before any constants can be set ('some_const = 25')

            >>> objectives.new("missing_params")
            Traceback (most recent call last):
                ...
            lib.dynamic_utilities.DynamicUtilityException: Missing parameters: criteria, display_name ('missing_params')

            >>> objectives.new("missing_params criteria")
            Traceback (most recent call last):
                ...
            lib.dynamic_utilities.DynamicUtilityException: Missing parameter: display_name ('missing_params criteria')
        """

        # strips the lines to remove the newlines at the end and any other whitespace
        # also only appends to list if the line is not empty, and doesn't start with #
        lines = super().new(text)
        current_obj = None

        for line in lines:
            data = line.split(" ", 2)
            if len(data) == 1:
                raise DynamicUtilityException(f"Missing parameters: criteria, display_name ({line!r})")
            elif len(data) == 2:
                raise DynamicUtilityException(f"Missing parameter: display_name ({line!r})")

            # if the given line is setting a constant (the second argument is "=")
            if data[1] == "=":
                if current_obj is None:
                    raise DynamicUtilityException(f"An objective must be defined before any constants can be set ({line!r})")
                name, value = data[0], int(data[2])
                current_obj.add_score(name, value)
                continue

            obj_name, obj_criteria, obj_display = data
            # removes the first "_" and replaces it with the provided display

            if obj_display == "_":
                obj_display = display
            elif display:
                obj_display = f"{display} {obj_display}"

            current_obj = Objective(obj_name, criteria=obj_criteria, display_name=obj_display, remove_self=remove, prefix=prefix)
            self.add(current_obj)

    @property
    def objectives(self) -> List[Objective]:
        """
        Returns:
            list of Objective objects
        """
        return list(self._utilities.values())


if __name__ == "__main__":
    import doctest
    doctest.testmod()


