scoreboard players operation &GameTime bhhd.cl = @s bhhd.igt
scoreboard players operation &GameTime bhhd.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhhd.cl = @s bhhd.igt
scoreboard players operation &GameTimeMinutes bhhd.cl = @s bhhd.igt
scoreboard players operation &GameTimeMinutes bhhd.cl /= 60 g.number
scoreboard players operation @s bhhd.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhhd.cl = @s bhhd.igt
scoreboard players reset &GameTimeAdditional bhhd.cl
execute @s[score_bhhd.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhhd.cl 0
scoreboard players set @s bhhd.igt 0
