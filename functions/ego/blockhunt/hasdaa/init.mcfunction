scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 6296492
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1120,y=28,z=-10,dx=76,dy=-24,dz=-74] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHHD","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"HASDaa","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 6296492"}},{"text":"]","color":"gray"},{"text":": "},{"text":"HASDaa","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"HASDaa","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 6296492"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 6296492
scoreboard objectives add bhhd.prng dummy Pseudo-RNG
scoreboard players set &Increment bhhd.prng 12345
scoreboard players set &Modulus bhhd.prng 16
scoreboard players set &Multiplier bhhd.prng 1103515245
scoreboard players set &Offset bhhd.prng 0
scoreboard players set &Seed bhhd.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhhd. dummy HASDaa
scoreboard objectives setdisplay sidebar bhhd.
scoreboard objectives add bhhd.pl dummy HASDaa Player List
scoreboard objectives add bhhd.ti dummy HASDaa Timer
scoreboard objectives add bhhd.chi dummy HASDaa Count Hiders
scoreboard objectives add bhhd.cvr dummy HASDaa Count Seekers
scoreboard objectives add bhhd.gl dummy HASDaa Virus Glowing
scoreboard objectives add bhhd.cl dummy HASDaa Calculations
scoreboard objectives add bhhd.igl dummy HASDaa Input Glowing
scoreboard objectives add bhhd.igt dummy HASDaa Input Game Time
scoreboard objectives add bhhd.icd dummy HASDaa Input Countdown
scoreboard objectives add bhhd.st dummy HASDaa State
scoreboard teams add bhhd.h HASDaa Hiders
scoreboard teams option bhhd.h friendlyfire false
scoreboard teams option bhhd.h collisionRule never
scoreboard teams option bhhd.h deathMessageVisibility always
scoreboard teams option bhhd.h nametagVisibility never
scoreboard teams option bhhd.h color green
scoreboard teams option bhhd.h seeFriendlyInvisibles false
scoreboard teams add bhhd.v HASDaa Seekers
scoreboard teams option bhhd.v friendlyfire false
scoreboard teams option bhhd.v collisionRule never
scoreboard teams option bhhd.v deathMessageVisibility always
scoreboard teams option bhhd.v color yellow
scoreboard teams add bhhd.d_y HASDaa Display Yellow
scoreboard teams option bhhd.d_y color yellow
scoreboard teams add bhhd.d_g HASDaa Display Green
scoreboard teams option bhhd.d_g color green
scoreboard teams join bhhd.d_y Countdown
scoreboard teams join bhhd.d_y Minutes
scoreboard teams join bhhd.d_y Seconds
scoreboard teams join bhhd.d_y Seekers
scoreboard teams join bhhd.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhhd.stand","bhhd.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhhd.prng","bhhd.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhhd.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhhd.prng] ~ ~ ~ execute @s[tag=bhhd.prng_true] ~ ~ ~ scoreboard players add &Seed bhhd.prng 1073741824
