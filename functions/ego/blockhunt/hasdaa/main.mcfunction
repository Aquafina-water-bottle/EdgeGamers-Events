scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1120,y=28,z=-10,dx=76,dy=-24,dz=-74] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-6296493] gp.id 6296492
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-6296491] gp.id 6296492
kill @e[x=-1120,y=28,z=-10,dx=76,dy=-24,dz=-74,type=minecraft:item,tag=!bhhd.entity]
execute @e[type=minecraft:armor_stand,score_bhhd.icd_min=1,tag=bhhd.stand] ~ ~ ~ function ego:blockhunt/hasdaa/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhhd.igt_min=1,tag=bhhd.stand] ~ ~ ~ function ego:blockhunt/hasdaa/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhhd.igl_min=1,tag=bhhd.stand] ~ ~ ~ function ego:blockhunt/hasdaa/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhhd.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhhd.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhhd.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhhd.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhhd.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhhd.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhhd.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhhd.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhhd.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhhd.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhhd.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhhd.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhhd.pl_min=1,score_bhhd.pl=2] bhhd.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhhd.pl_min=1,score_bhhd.pl=2] bhhd.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhhd.pl_min=1,score_bhhd.pl=2] bhhd.pl 0
scoreboard players add @a bhhd.pl 0
execute @e[type=minecraft:armor_stand,score_bhhd.st_min=0,score_bhhd.st=0,tag=bhhd.stand] ~ ~ ~ function ego:blockhunt/hasdaa/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhhd.st_min=1,score_bhhd.st=1,tag=bhhd.stand] ~ ~ ~ function ego:blockhunt/hasdaa/countdown
execute @e[type=minecraft:armor_stand,score_bhhd.st_min=2,score_bhhd.st=2,tag=bhhd.stand] ~ ~ ~ function ego:blockhunt/hasdaa/during_round
execute @e[type=minecraft:armor_stand,score_bhhd.st_min=3,score_bhhd.st=3,tag=bhhd.stand] ~ ~ ~ function ego:blockhunt/hasdaa/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.chi 0
execute @a[m=2,team=bhhd.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.chi 1
scoreboard players operation Hiders bhhd. = @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.cvr 0
execute @a[m=2,team=bhhd.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.cvr 1
scoreboard players operation Seekers bhhd. = @e[type=minecraft:armor_stand,tag=bhhd.stand] bhhd.cvr
