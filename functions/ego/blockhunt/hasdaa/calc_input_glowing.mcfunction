scoreboard players operation &GlowingUntil bhhd.cl = @s bhhd.igl
scoreboard players operation &GlowingUntil bhhd.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhhd.cl = @s bhhd.igl
scoreboard players operation &GlowingUntilMinutes bhhd.cl = @s bhhd.igl
scoreboard players operation &GlowingUntilMinutes bhhd.cl /= 60 g.number
scoreboard players operation @s bhhd.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhhd.cl = @s bhhd.igl
scoreboard players reset &GlowingUntilAdditional bhhd.cl
execute @s[score_bhhd.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhhd.cl 0
scoreboard players set @s bhhd.igl 0
