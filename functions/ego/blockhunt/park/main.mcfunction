scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1263,y=38,z=-189,dx=42,dy=-34,dz=-99] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-1278338285] gp.id 1278338284
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-1278338283] gp.id 1278338284
kill @e[x=-1263,y=38,z=-189,dx=42,dy=-34,dz=-99,type=minecraft:item,tag=!bhp.entity]
execute @e[type=minecraft:armor_stand,score_bhp.icd_min=1,tag=bhp.stand] ~ ~ ~ function ego:blockhunt/park/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhp.igt_min=1,tag=bhp.stand] ~ ~ ~ function ego:blockhunt/park/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhp.igl_min=1,tag=bhp.stand] ~ ~ ~ function ego:blockhunt/park/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhp.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhp.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhp.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhp.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhp.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhp.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhp.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhp.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhp.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhp.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhp.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhp.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhp.pl_min=1,score_bhp.pl=2] bhp.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhp.pl_min=1,score_bhp.pl=2] bhp.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhp.pl_min=1,score_bhp.pl=2] bhp.pl 0
scoreboard players add @a bhp.pl 0
execute @e[type=minecraft:armor_stand,score_bhp.st_min=0,score_bhp.st=0,tag=bhp.stand] ~ ~ ~ function ego:blockhunt/park/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhp.st_min=1,score_bhp.st=1,tag=bhp.stand] ~ ~ ~ function ego:blockhunt/park/countdown
execute @e[type=minecraft:armor_stand,score_bhp.st_min=2,score_bhp.st=2,tag=bhp.stand] ~ ~ ~ function ego:blockhunt/park/during_round
execute @e[type=minecraft:armor_stand,score_bhp.st_min=3,score_bhp.st=3,tag=bhp.stand] ~ ~ ~ function ego:blockhunt/park/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.chi 0
execute @a[m=2,team=bhp.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.chi 1
scoreboard players operation Hiders bhp. = @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.cvr 0
execute @a[m=2,team=bhp.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.cvr 1
scoreboard players operation Seekers bhp. = @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.cvr
