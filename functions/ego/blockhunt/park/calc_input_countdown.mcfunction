scoreboard players operation &Countdown bhp.cl = @s bhp.icd
scoreboard players operation &Countdown bhp.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhp.cl = @s bhp.icd
scoreboard players operation &CountdownMinutes bhp.cl = @s bhp.icd
scoreboard players operation &CountdownMinutes bhp.cl /= 60 g.number
scoreboard players operation @s bhp.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhp.cl = @s bhp.icd
scoreboard players reset &CountdownAdditional bhp.cl
execute @s[score_bhp.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhp.cl 0
scoreboard players set @s bhp.icd 0
