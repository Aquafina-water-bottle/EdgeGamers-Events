scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1278338284
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1263,y=38,z=-189,dx=42,dy=-34,dz=-99] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHP","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Park","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1278338284"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Park","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Park","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1278338284"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1278338284
scoreboard objectives add bhp.prng dummy Pseudo-RNG
scoreboard players set &Increment bhp.prng 12345
scoreboard players set &Modulus bhp.prng 7
scoreboard players set &Multiplier bhp.prng 1103515245
scoreboard players set &Offset bhp.prng 0
scoreboard players set &Seed bhp.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhp. dummy Park
scoreboard objectives setdisplay sidebar bhp.
scoreboard objectives add bhp.pl dummy Park Player List
scoreboard objectives add bhp.ti dummy Park Timer
scoreboard objectives add bhp.chi dummy Park Count Hiders
scoreboard objectives add bhp.cvr dummy Park Count Seekers
scoreboard objectives add bhp.gl dummy Park Virus Glowing
scoreboard objectives add bhp.cl dummy Park Calculations
scoreboard objectives add bhp.igl dummy Park Input Glowing
scoreboard objectives add bhp.igt dummy Park Input Game Time
scoreboard objectives add bhp.icd dummy Park Input Countdown
scoreboard objectives add bhp.st dummy Park State
scoreboard teams add bhp.h Park Hiders
scoreboard teams option bhp.h friendlyfire false
scoreboard teams option bhp.h collisionRule never
scoreboard teams option bhp.h deathMessageVisibility always
scoreboard teams option bhp.h nametagVisibility never
scoreboard teams option bhp.h color green
scoreboard teams option bhp.h seeFriendlyInvisibles false
scoreboard teams add bhp.v Park Seekers
scoreboard teams option bhp.v friendlyfire false
scoreboard teams option bhp.v collisionRule never
scoreboard teams option bhp.v deathMessageVisibility always
scoreboard teams option bhp.v color yellow
scoreboard teams add bhp.d_y Park Display Yellow
scoreboard teams option bhp.d_y color yellow
scoreboard teams add bhp.d_g Park Display Green
scoreboard teams option bhp.d_g color green
scoreboard teams join bhp.d_y Countdown
scoreboard teams join bhp.d_y Minutes
scoreboard teams join bhp.d_y Seconds
scoreboard teams join bhp.d_y Seekers
scoreboard teams join bhp.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhp.stand","bhp.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhp.stand] bhp.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhp.prng","bhp.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhp.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhp.prng] ~ ~ ~ execute @s[tag=bhp.prng_true] ~ ~ ~ scoreboard players add &Seed bhp.prng 1073741824
