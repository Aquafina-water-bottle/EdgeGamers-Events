scoreboard players operation &GameTime bhp.cl = @s bhp.igt
scoreboard players operation &GameTime bhp.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhp.cl = @s bhp.igt
scoreboard players operation &GameTimeMinutes bhp.cl = @s bhp.igt
scoreboard players operation &GameTimeMinutes bhp.cl /= 60 g.number
scoreboard players operation @s bhp.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhp.cl = @s bhp.igt
scoreboard players reset &GameTimeAdditional bhp.cl
execute @s[score_bhp.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhp.cl 0
scoreboard players set @s bhp.igt 0
