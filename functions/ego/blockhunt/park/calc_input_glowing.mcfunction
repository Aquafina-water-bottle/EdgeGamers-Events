scoreboard players operation &GlowingUntil bhp.cl = @s bhp.igl
scoreboard players operation &GlowingUntil bhp.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhp.cl = @s bhp.igl
scoreboard players operation &GlowingUntilMinutes bhp.cl = @s bhp.igl
scoreboard players operation &GlowingUntilMinutes bhp.cl /= 60 g.number
scoreboard players operation @s bhp.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhp.cl = @s bhp.igl
scoreboard players reset &GlowingUntilAdditional bhp.cl
execute @s[score_bhp.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhp.cl 0
scoreboard players set @s bhp.igl 0
