scoreboard players operation &GlowingUntil bhj.cl = @s bhj.igl
scoreboard players operation &GlowingUntil bhj.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhj.cl = @s bhj.igl
scoreboard players operation &GlowingUntilMinutes bhj.cl = @s bhj.igl
scoreboard players operation &GlowingUntilMinutes bhj.cl /= 60 g.number
scoreboard players operation @s bhj.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhj.cl = @s bhj.igl
scoreboard players reset &GlowingUntilAdditional bhj.cl
execute @s[score_bhj.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhj.cl 0
scoreboard players set @s bhj.igl 0
