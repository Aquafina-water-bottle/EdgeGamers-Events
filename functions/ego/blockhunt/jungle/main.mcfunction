scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1046,y=55,z=-148,dx=-69,dy=-51,dz=60] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-928077503] gp.id 928077502
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-928077501] gp.id 928077502
kill @e[x=-1046,y=55,z=-148,dx=-69,dy=-51,dz=60,type=minecraft:item,tag=!bhj.entity]
execute @e[type=minecraft:armor_stand,score_bhj.icd_min=1,tag=bhj.stand] ~ ~ ~ function ego:blockhunt/jungle/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhj.igt_min=1,tag=bhj.stand] ~ ~ ~ function ego:blockhunt/jungle/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhj.igl_min=1,tag=bhj.stand] ~ ~ ~ function ego:blockhunt/jungle/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhj.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhj.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhj.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhj.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhj.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhj.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhj.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhj.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhj.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhj.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhj.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhj.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhj.pl_min=1,score_bhj.pl=2] bhj.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhj.pl_min=1,score_bhj.pl=2] bhj.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhj.pl_min=1,score_bhj.pl=2] bhj.pl 0
scoreboard players add @a bhj.pl 0
execute @e[type=minecraft:armor_stand,score_bhj.st_min=0,score_bhj.st=0,tag=bhj.stand] ~ ~ ~ function ego:blockhunt/jungle/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhj.st_min=1,score_bhj.st=1,tag=bhj.stand] ~ ~ ~ function ego:blockhunt/jungle/countdown
execute @e[type=minecraft:armor_stand,score_bhj.st_min=2,score_bhj.st=2,tag=bhj.stand] ~ ~ ~ function ego:blockhunt/jungle/during_round
execute @e[type=minecraft:armor_stand,score_bhj.st_min=3,score_bhj.st=3,tag=bhj.stand] ~ ~ ~ function ego:blockhunt/jungle/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.chi 0
execute @a[m=2,team=bhj.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.chi 1
scoreboard players operation Hiders bhj. = @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.cvr 0
execute @a[m=2,team=bhj.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.cvr 1
scoreboard players operation Seekers bhj. = @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.cvr
