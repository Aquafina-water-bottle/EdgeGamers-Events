scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 928077502
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1046,y=55,z=-148,dx=-69,dy=-51,dz=60] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHJ","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Jungle","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 928077502"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Jungle","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Jungle","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 928077502"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 928077502
scoreboard objectives add bhj.prng dummy Pseudo-RNG
scoreboard players set &Increment bhj.prng 12345
scoreboard players set &Modulus bhj.prng 17
scoreboard players set &Multiplier bhj.prng 1103515245
scoreboard players set &Offset bhj.prng 0
scoreboard players set &Seed bhj.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhj. dummy Jungle
scoreboard objectives setdisplay sidebar bhj.
scoreboard objectives add bhj.pl dummy Jungle Player List
scoreboard objectives add bhj.ti dummy Jungle Timer
scoreboard objectives add bhj.chi dummy Jungle Count Hiders
scoreboard objectives add bhj.cvr dummy Jungle Count Seekers
scoreboard objectives add bhj.gl dummy Jungle Virus Glowing
scoreboard objectives add bhj.cl dummy Jungle Calculations
scoreboard objectives add bhj.igl dummy Jungle Input Glowing
scoreboard objectives add bhj.igt dummy Jungle Input Game Time
scoreboard objectives add bhj.icd dummy Jungle Input Countdown
scoreboard objectives add bhj.st dummy Jungle State
scoreboard teams add bhj.h Jungle Hiders
scoreboard teams option bhj.h friendlyfire false
scoreboard teams option bhj.h collisionRule never
scoreboard teams option bhj.h deathMessageVisibility always
scoreboard teams option bhj.h nametagVisibility never
scoreboard teams option bhj.h color green
scoreboard teams option bhj.h seeFriendlyInvisibles false
scoreboard teams add bhj.v Jungle Seekers
scoreboard teams option bhj.v friendlyfire false
scoreboard teams option bhj.v collisionRule never
scoreboard teams option bhj.v deathMessageVisibility always
scoreboard teams option bhj.v color yellow
scoreboard teams add bhj.d_y Jungle Display Yellow
scoreboard teams option bhj.d_y color yellow
scoreboard teams add bhj.d_g Jungle Display Green
scoreboard teams option bhj.d_g color green
scoreboard teams join bhj.d_y Countdown
scoreboard teams join bhj.d_y Minutes
scoreboard teams join bhj.d_y Seconds
scoreboard teams join bhj.d_y Seekers
scoreboard teams join bhj.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhj.stand","bhj.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhj.stand] bhj.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhj.prng","bhj.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhj.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhj.prng] ~ ~ ~ execute @s[tag=bhj.prng_true] ~ ~ ~ scoreboard players add &Seed bhj.prng 1073741824
