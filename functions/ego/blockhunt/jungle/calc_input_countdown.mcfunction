scoreboard players operation &Countdown bhj.cl = @s bhj.icd
scoreboard players operation &Countdown bhj.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhj.cl = @s bhj.icd
scoreboard players operation &CountdownMinutes bhj.cl = @s bhj.icd
scoreboard players operation &CountdownMinutes bhj.cl /= 60 g.number
scoreboard players operation @s bhj.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhj.cl = @s bhj.icd
scoreboard players reset &CountdownAdditional bhj.cl
execute @s[score_bhj.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhj.cl 0
scoreboard players set @s bhj.icd 0
