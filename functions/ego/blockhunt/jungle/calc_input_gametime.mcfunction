scoreboard players operation &GameTime bhj.cl = @s bhj.igt
scoreboard players operation &GameTime bhj.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhj.cl = @s bhj.igt
scoreboard players operation &GameTimeMinutes bhj.cl = @s bhj.igt
scoreboard players operation &GameTimeMinutes bhj.cl /= 60 g.number
scoreboard players operation @s bhj.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhj.cl = @s bhj.igt
scoreboard players reset &GameTimeAdditional bhj.cl
execute @s[score_bhj.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhj.cl 0
scoreboard players set @s bhj.igt 0
