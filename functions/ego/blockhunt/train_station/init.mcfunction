scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1846441401
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1208,y=34,z=-186,dx=-176,dy=-30,dz=176] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHTS","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Train Station","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1846441401"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Train Station","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Train Station","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1846441401"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1846441401
scoreboard objectives add bhts.prng dummy Pseudo-RNG
scoreboard players set &Increment bhts.prng 12345
scoreboard players set &Modulus bhts.prng 8
scoreboard players set &Multiplier bhts.prng 1103515245
scoreboard players set &Offset bhts.prng 0
scoreboard players set &Seed bhts.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhts. dummy Train Station
scoreboard objectives setdisplay sidebar bhts.
scoreboard objectives add bhts.pl dummy Train Station Player List
scoreboard objectives add bhts.ti dummy Train Station Timer
scoreboard objectives add bhts.chi dummy Train Station Count Hiders
scoreboard objectives add bhts.cvr dummy Train Station Count Seekers
scoreboard objectives add bhts.gl dummy Train Station Virus Glowing
scoreboard objectives add bhts.cl dummy Train Station Calculations
scoreboard objectives add bhts.igl dummy Train Station Input Glowing
scoreboard objectives add bhts.igt dummy Train Station Input Game Time
scoreboard objectives add bhts.icd dummy Train Station Input Countdown
scoreboard objectives add bhts.st dummy Train Station State
scoreboard teams add bhts.h Train Station Hiders
scoreboard teams option bhts.h friendlyfire false
scoreboard teams option bhts.h collisionRule never
scoreboard teams option bhts.h deathMessageVisibility always
scoreboard teams option bhts.h nametagVisibility never
scoreboard teams option bhts.h color green
scoreboard teams option bhts.h seeFriendlyInvisibles false
scoreboard teams add bhts.v Train Station Seekers
scoreboard teams option bhts.v friendlyfire false
scoreboard teams option bhts.v collisionRule never
scoreboard teams option bhts.v deathMessageVisibility always
scoreboard teams option bhts.v color yellow
scoreboard teams add bhts.d_y Train Station Display Yellow
scoreboard teams option bhts.d_y color yellow
scoreboard teams add bhts.d_g Train Station Display Green
scoreboard teams option bhts.d_g color green
scoreboard teams join bhts.d_y Countdown
scoreboard teams join bhts.d_y Minutes
scoreboard teams join bhts.d_y Seconds
scoreboard teams join bhts.d_y Seekers
scoreboard teams join bhts.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhts.stand","bhts.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhts.prng","bhts.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhts.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhts.prng] ~ ~ ~ execute @s[tag=bhts.prng_true] ~ ~ ~ scoreboard players add &Seed bhts.prng 1073741824
