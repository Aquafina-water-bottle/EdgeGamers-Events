scoreboard players operation &GameTime bhts.cl = @s bhts.igt
scoreboard players operation &GameTime bhts.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhts.cl = @s bhts.igt
scoreboard players operation &GameTimeMinutes bhts.cl = @s bhts.igt
scoreboard players operation &GameTimeMinutes bhts.cl /= 60 g.number
scoreboard players operation @s bhts.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhts.cl = @s bhts.igt
scoreboard players reset &GameTimeAdditional bhts.cl
execute @s[score_bhts.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhts.cl 0
scoreboard players set @s bhts.igt 0
