scoreboard players operation &GlowingUntil bhts.cl = @s bhts.igl
scoreboard players operation &GlowingUntil bhts.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhts.cl = @s bhts.igl
scoreboard players operation &GlowingUntilMinutes bhts.cl = @s bhts.igl
scoreboard players operation &GlowingUntilMinutes bhts.cl /= 60 g.number
scoreboard players operation @s bhts.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhts.cl = @s bhts.igl
scoreboard players reset &GlowingUntilAdditional bhts.cl
execute @s[score_bhts.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhts.cl 0
scoreboard players set @s bhts.igl 0
