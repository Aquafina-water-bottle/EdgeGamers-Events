scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1208,y=34,z=-186,dx=-176,dy=-30,dz=176] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-1846441402] gp.id 1846441401
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-1846441400] gp.id 1846441401
kill @e[x=-1208,y=34,z=-186,dx=-176,dy=-30,dz=176,type=minecraft:item,tag=!bhts.entity]
execute @e[type=minecraft:armor_stand,score_bhts.icd_min=1,tag=bhts.stand] ~ ~ ~ function ego:blockhunt/train_station/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhts.igt_min=1,tag=bhts.stand] ~ ~ ~ function ego:blockhunt/train_station/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhts.igl_min=1,tag=bhts.stand] ~ ~ ~ function ego:blockhunt/train_station/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhts.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhts.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhts.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhts.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhts.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhts.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhts.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhts.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhts.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhts.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhts.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhts.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhts.pl_min=1,score_bhts.pl=2] bhts.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhts.pl_min=1,score_bhts.pl=2] bhts.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhts.pl_min=1,score_bhts.pl=2] bhts.pl 0
scoreboard players add @a bhts.pl 0
execute @e[type=minecraft:armor_stand,score_bhts.st_min=0,score_bhts.st=0,tag=bhts.stand] ~ ~ ~ function ego:blockhunt/train_station/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhts.st_min=1,score_bhts.st=1,tag=bhts.stand] ~ ~ ~ function ego:blockhunt/train_station/countdown
execute @e[type=minecraft:armor_stand,score_bhts.st_min=2,score_bhts.st=2,tag=bhts.stand] ~ ~ ~ function ego:blockhunt/train_station/during_round
execute @e[type=minecraft:armor_stand,score_bhts.st_min=3,score_bhts.st=3,tag=bhts.stand] ~ ~ ~ function ego:blockhunt/train_station/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.chi 0
execute @a[m=2,team=bhts.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.chi 1
scoreboard players operation Hiders bhts. = @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.cvr 0
execute @a[m=2,team=bhts.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.cvr 1
scoreboard players operation Seekers bhts. = @e[type=minecraft:armor_stand,tag=bhts.stand] bhts.cvr
