minecraft:tp @s[team=bhts.h] ~ ~0.1 ~
scoreboard players operation &Seed bhts.prng *= &Multiplier bhts.prng
scoreboard players operation &Seed bhts.prng += &Increment bhts.prng
scoreboard players operation @s bhts.prng = &Seed bhts.prng
scoreboard players operation @s bhts.prng %= &Modulus bhts.prng
scoreboard players operation @s[score_bhts.prng=-1] bhts.prng *= -1 g.number
scoreboard players operation @s bhts.prng += &Offset bhts.prng
execute @s[score_bhts.prng_min=0,score_bhts.prng=0] ~ ~ ~ function ego:blockhunt/train_station/disguise/0
execute @s[score_bhts.prng_min=1,score_bhts.prng=1] ~ ~ ~ function ego:blockhunt/train_station/disguise/1
execute @s[score_bhts.prng_min=2,score_bhts.prng=2] ~ ~ ~ function ego:blockhunt/train_station/disguise/2
execute @s[score_bhts.prng_min=3,score_bhts.prng=3] ~ ~ ~ function ego:blockhunt/train_station/disguise/3
execute @s[score_bhts.prng_min=4,score_bhts.prng=4] ~ ~ ~ function ego:blockhunt/train_station/disguise/4
execute @s[score_bhts.prng_min=5,score_bhts.prng=5] ~ ~ ~ function ego:blockhunt/train_station/disguise/5
execute @s[score_bhts.prng_min=6,score_bhts.prng=6] ~ ~ ~ function ego:blockhunt/train_station/disguise/6
execute @s[score_bhts.prng_min=7,score_bhts.prng=7] ~ ~ ~ function ego:blockhunt/train_station/disguise/7
minecraft:tp @s[team=bhts.h] ~ ~-0.01 ~
