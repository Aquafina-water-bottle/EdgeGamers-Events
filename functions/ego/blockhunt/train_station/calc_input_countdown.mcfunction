scoreboard players operation &Countdown bhts.cl = @s bhts.icd
scoreboard players operation &Countdown bhts.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhts.cl = @s bhts.icd
scoreboard players operation &CountdownMinutes bhts.cl = @s bhts.icd
scoreboard players operation &CountdownMinutes bhts.cl /= 60 g.number
scoreboard players operation @s bhts.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhts.cl = @s bhts.icd
scoreboard players reset &CountdownAdditional bhts.cl
execute @s[score_bhts.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhts.cl 0
scoreboard players set @s bhts.icd 0
