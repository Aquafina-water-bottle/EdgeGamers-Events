# File system
```
race
L __init__.py (Holds all races)
L checkpoint.fena (defines a checkpoint race)
L lap.fena (defines a lap race)
L regular.fena (defines a regular race)
L common_macros.fena (imported by the main.fena file to ensure all macros are defined)
L pre.fena (common initial import for checkpoint.fena, lap.fena, and regular.fena)
L post.fena (common final import for checkpoint.fena, lap.fena, and regular.fena)
```
