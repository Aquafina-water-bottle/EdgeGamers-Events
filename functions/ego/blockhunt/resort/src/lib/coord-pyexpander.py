from numbers import Real
from typing import NamedTuple, Union

if __name__ == "__main__":
    import sys
    sys.path.append("..")
    del sys

import lib.number_utils
from lib.assert_utils import assert_type

__all__ = [
        "Coord"
        ]

class CoordError(Exception):
    pass

class InvalidNumberError(CoordError):
    pass

is_signed_int = lib.number_utils.is_signed_int

def is_number(value):
    """
    Examples:
        >>> is_number(25.6)
        True
        >>> is_number(-25.6)
        True
        >>> is_number(0)
        True
        >>> is_number(1964)
        True
        >>> is_number(-1964)
        True

        Note that the following numbers convert thusly:
        >>> 6e5
        600000.0
        >>> 6e-100
        6e-100
        >>> 1_964
        1964

        This means floating point values should be formatted with proper
        decimal points before entering this method
        >>> "%.10f" % 6e-100
        '0.0000000000'
        >>> round(6e-100, 10)
        0.0

        >>> is_number(6e5)
        True
        >>> is_number(6e100)
        False
        >>> is_number(1_964)
        True
        >>> is_number(float('nan'))
        False
        >>> is_number(None)
        False
        >>> is_number(27j+5)
        False
    """
    if not isinstance(value, str):
        value = str(value)

    return lib.number_utils.is_number(value)

def to_number(x):
    """
    Turn x into an int or float depending on its representation

    Args:
        x (str): A numeric representation of a number

    Returns:
        int(x) if x is an integer
        float(x) if x is a floating point number

    Examples:
        >>> to_number('5')
        5
        >>> to_number('3.14')
        3.14
        >>> to_number('-5')
        -5
        >>> to_number('-3.14')
        -3.14
    """

    if is_signed_int(x):
        return int(x)
    elif is_number(x):
        return float(x)
    raise InvalidNumberError(f"Expected a valid number for {x!r}")


class CoordBase(NamedTuple):
    prefix: str
    value: Union[int, float]


class Coord(CoordBase):
    """
    An immutable value container for Minecraft coordinates

    Args:
        value (optional): Value of this coordinate.
            If it is a float or int, this coordinate will be absolute.
            If a str, the value should be a number optionally prefixed by '~' (relative) or '^' (local).

    Attributes:
        value (numbers.Real): The value of this coordinate
        prefix (str): The prefix of this coordinate. Can be either "", "~", or "^"

    Examples:
        >>> def test(c):
        ...     coord = Coord(c)
        ...     print(f"{coord}: {coord!r}")

        >>> test(5)
        5: Coord(prefix='', value=5)

        >>> test(-5)
        -5: Coord(prefix='', value=-5)

        >>> test(5.5)
        5.5: Coord(prefix='', value=5.5)

        >>> test(-5.5)
        -5.5: Coord(prefix='', value=-5.5)


        >>> test('5')
        5: Coord(prefix='', value=5)

        >>> test('-5')
        -5: Coord(prefix='', value=-5)

        >>> test('5.5')
        5.5: Coord(prefix='', value=5.5)

        >>> test('-5.5')
        -5.5: Coord(prefix='', value=-5.5)


        >>> test('~5')
        ~5: Coord(prefix='~', value=5)

        >>> test('~-5')
        ~-5: Coord(prefix='~', value=-5)

        >>> test('~5.5')
        ~5.5: Coord(prefix='~', value=5.5)

        >>> test('~-5.5')
        ~-5.5: Coord(prefix='~', value=-5.5)


        >>> test('^5')
        ^5: Coord(prefix='^', value=5)

        >>> test('^-5')
        ^-5: Coord(prefix='^', value=-5)

        >>> test('^5.5')
        ^5.5: Coord(prefix='^', value=5.5)

        >>> test('^-5.5')
        ^-5.5: Coord(prefix='^', value=-5.5)

        >>> test('NaN')
        Traceback (most recent call last):
            ...
        CoordError: Invalid prefix 'N', expected one of ['', '~', '^']

        >>> test(float('NaN'))
        Traceback (most recent call last):
            ...
        InvalidNumberError: Expected a real number as nan


        As said before, this is very much immuatble.
        >>> c = Coord("~12.3")
        >>> c.prefix = "^"
        Traceback (most recent call last):
            ...
        AttributeError: can't set attribute

        >>> c.value = 12.4
        Traceback (most recent call last):
            ...
        AttributeError: can't set attribute


    In all numeric type operations, the prefix of the result is inherited from the left of the operator.
    Only other coord objects can be used when when doing numeric type operations.

    These methods should return NotImplemented if the operation isn't supported
        https://docs.python.org/3/reference/datamodel.html#emulating-numeric-types

    Binary Operator Examples:
        >>> c = Coord("^15") + Coord("~5")
        >>> c
        Coord(prefix='^', value=20)

        >>> c += Coord("~5.0")
        >>> c
        Coord(prefix='^', value=25.0)

        >>> c -= Coord("5")
        >>> c
        Coord(prefix='^', value=20.0)

        >>> c = 15
        >>> c += Coord("^5")
        Traceback (most recent call last):
            ...
        TypeError: unsupported operand type(s) for +=: 'int' and 'Coord'

        >>> 15 / Coord("^5")
        Traceback (most recent call last):
            ...
        TypeError: unsupported operand type(s) for /: 'int' and 'Coord'

        >>> c = Coord("5")
        >>> c -= 25
        Traceback (most recent call last):
            ...
        TypeError: unsupported operand type(s) for -=: 'Coord' and 'int'

        >>> c * 25
        Traceback (most recent call last):
            ...
        TypeError: unsupported operand type(s) for *: 'Coord' and 'int'

    In numeric type comparisons for greater or less than, the prefix of the Coord is ignored.
    However, for equality comparisons, the prefix is also compared.

    Comparison Examples:
        >>> Coord("~15") == Coord("^15")
        False

        >>> Coord("~15") != Coord("^15")
        True

        >>> Coord("~15") == Coord("~15")
        True

        >>> Coord("~10") <= Coord("~15")
        True

        >>> Coord("~9") <= Coord("9")
        True

        >>> Coord("~9") <= 25
        Traceback (most recent call last):
            ...
        TypeError: '<=' not supported between instances of 'Coord' and 'int'

        >>> Coord("^3") > Coord("3")
        False
    """

    valid_prefixes = ["", "~", "^"]

    def __new__(cls, value_combo):
        """
        Args:
            value_combo (numbers.Real, str, Coord): A combination of the coord prefix and its number
            - If value_combo is Coord, it will simply return itself back since an immutable object doesn't need copies
        """
        if isinstance(value_combo, Coord):
            return value_combo
        prefix, value = Coord._separate_prefix_value(value_combo)
        return cls.from_value_prefix(value, prefix)

    @classmethod
    def from_value_prefix(cls, value=0, prefix=""):
        """
        Args:
            value (numbers.Real, str): A real number or a string of a real number
            prefix ("", "~", or "^")
        """
        Coord._check_value(value)
        Coord._check_prefix(prefix)
        return super().__new__(cls, prefix, value)

    @staticmethod
    def _separate_prefix_value(value_combo):
        """
        Args:
            value_combo (numbers.Real, str)
        """
        if isinstance(value_combo, Real):
            return "", value_combo

        # String can either be a full number value_combo, a full prefix or a combination of both
        if isinstance(value_combo, str):
            # only a number: no prefix
            if value_combo[0].isdigit() or value_combo[0] == "-":
                return "", to_number(value_combo)

            # only prefix because the entire string isn't a number
            if len(value_combo) == 1:
                return value_combo, 0

            # makes sure the first character is a prefix for easier debugging
            Coord._check_prefix(value_combo[0])

            # gets the first character as the prefix, and the rest as a number
            return value_combo[0], to_number(value_combo[1:])

        raise CoordError(f"Expected a numbers.Real, Coord or str but got {value_combo!r}")

    @staticmethod
    def _check_prefix(prefix):
        """
        Makes sure the prefix is a valid prefix

        Args:
            prefix (str)
        """
        if prefix not in Coord.valid_prefixes:
            raise CoordError(f"Invalid prefix {prefix!r}, expected one of {Coord.valid_prefixes}")

    @staticmethod
    def _check_value(value):
        """
        Makes sure the value is a valid value

        Args:
            prefix (Real)
        """
        if not isinstance(value, Real) or not is_number(value):
            raise InvalidNumberError(f"Expected a real number as {value!r}")

    def __str__(self):
        """
        Gets the minecraft coordinate representation of the coordinate

        Examples:
            >>> str(Coord('^5'))
            '^5'
            >>> str(Coord(2))
            '2'
            >>> str(Coord('~'))
            '~'
        """
        if self.value == 0 and not self.absolute:
            return self.prefix
        return self.prefix + str(self.value)

    @property
    def absolute(self):
        """
        Returns:
            bool: Whether the coordinate has a prefix or not
        """
        return self.prefix == ""

    def as_absolute(self):
        """
        Changes the coord to absolute

        Examples:
            >>> c = Coord("^25")

            >>> c
            Coord(prefix='^', value=25)

            >>> c.as_absolute()
            Coord(prefix='', value=25)
        """
        if self.absolute:
            return self
        return Coord.from_value_prefix(self.value, "")

    @property
    def relative(self):
        """
        Returns:
            bool: Whether the coordinate is equal to '~'
        """
        return self.prefix == "~"

    def as_relative(self):
        """
        Changes the coord to relative

        Examples:
            >>> c = Coord("^25")

            >>> c
            Coord(prefix='^', value=25)

            >>> c.as_relative()
            Coord(prefix='~', value=25)
        """
        if self.relative:
            return self
        return Coord.from_value_prefix(self.value, "~")

    @property
    def local(self):
        """
        Returns:
            bool: Whether the coordinate is equal to '^'
        """
        return self.prefix == "^"

    def as_local(self):
        """
        Changes the coord to local

        Examples:
            >>> c = Coord("25")

            >>> c
            Coord(prefix='', value=25)

            >>> c.as_local()
            Coord(prefix='^', value=25)
        """
        if self.local:
            return self
        return Coord.from_value_prefix(self.value, "^")

    def as_int(self):
        """
        Turns the value into an integer

        Examples:
            >>> Coord("-7").as_int()
            Coord(prefix='', value=-7)

            >>> Coord("~-6.5").as_int()
            Coord(prefix='~', value=-6)

            >>> Coord("^6").as_int()
            Coord(prefix='^', value=6)
        """
        return Coord.from_value_prefix(int(self.value), self.prefix)

    def as_float(self):
        """
        Turns the value into a float

        Examples:
            >>> Coord("-7").as_float()
            Coord(prefix='', value=-7.0)

            >>> Coord("~-6.5").as_float()
            Coord(prefix='~', value=-6.5)

            >>> Coord("^6").as_float()
            Coord(prefix='^', value=6.0)
        """
        return Coord.from_value_prefix(float(self.value), self.prefix)


# comparison operators
$py(comparisons = {
    "__lt__": "<",
    "__le__": "<=",
    "__gt__": ">",
    "__ge__": ">="}
)

$for(name, symbol in comparisons.items())
    def $(name)(self, other):
        if isinstance(other, Coord):
            return self.value $(symbol) other.value
        return NotImplemented
$endfor

    def __eq__(self, other):
        """
        If you want to compare a coordinate with a numeric value, compare it with `self.value`
        """
        if isinstance(other, Coord):
            return self.value == other.value and self.prefix == other.prefix
        return NotImplemented


# binary operators
$py(binary_ops = {
    "__add__": "+",
    "__sub__": "-",
    "__mul__": "*",
    "__truediv__": "/",
    "__floordiv__": "//",
    "__mod__": "%",
    "__pow__": "**",
    "__lshift__": "<<",
    "__rshift__": ">>",
    "__and__": "&",
    "__xor__": "^",
    "__or__": "|"}
)

$for(name, symbol in binary_ops.items())
    def $(name)(self, other):
        if isinstance(other, Coord):
            result_value = self.value $(symbol) other.value
            return Coord.from_value_prefix(result_value, self.prefix)
        return NotImplemented
$endfor

    def __divmod__(self, other):
        if isinstance(other, Coord):
            result_value = divmod(self.value, other.value)
            return Coord.from_value_prefix(result_value, self.prefix)
        return NotImplemented

    def __neg__(self):
        """
        Makes the value negative, as expected

        Examples:
            >>> -Coord("5")
            Coord(prefix='', value=-5)

            >>> -Coord("^7")
            Coord(prefix='^', value=-7)

            >>> -Coord("^-7")
            Coord(prefix='^', value=7)
        """
        return Coord.from_value_prefix(-self.value, self.prefix)

    def __pos__(self):
        """
        Honestly this does nothing but whatever

        Examples:
            >>> +Coord("5")
            Coord(prefix='', value=5)

            >>> +Coord("^7")
            Coord(prefix='^', value=7)

            >>> +Coord("^-7")
            Coord(prefix='^', value=-7)
        """
        return Coord.from_value_prefix(self.value, self.prefix)

    def __invert__(self):
        """
        This '~' operator is not bitwise not, but makes the coordinate relative.
        If you're really craving for that sweet bitwise not, get coord.value instead ya doofus

        Examples:
            >>> ~Coord("^7")
            Coord(prefix='~', value=7)

            >>> ~Coord("~7")
            Coord(prefix='~', value=7)

            >>> ~Coord("7")
            Coord(prefix='~', value=7)
        """
        return Coord.from_value_prefix(self.value, "~")

    def __abs__(self):
        """
        Takes the absolute value of this Coord's value

        Examples:
            >>> abs(Coord("-7"))
            Coord(prefix='', value=7)

            >>> abs(Coord("~-6.5"))
            Coord(prefix='~', value=6.5)

            >>> abs(Coord("^6"))
            Coord(prefix='^', value=6)
        """
        return Coord.from_value_prefix(abs(self.value), self.prefix)

    def __round__(self, n=0):
        """Rounds this Coord's value to the n'th decimal place

        Examples:
            >>> round(Coord("3.141592653589793"), 2)
            Coord(prefix='', value=3.14)
        """
        return Coord.from_value_prefix(round(self.value, n), self.prefix)


if __name__ == "__main__":
    import doctest
    doctest.testmod()


