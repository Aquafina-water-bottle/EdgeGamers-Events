scoreboard players operation &GlowingUntil bhre.cl = @s bhre.igl
scoreboard players operation &GlowingUntil bhre.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhre.cl = @s bhre.igl
scoreboard players operation &GlowingUntilMinutes bhre.cl = @s bhre.igl
scoreboard players operation &GlowingUntilMinutes bhre.cl /= 60 g.number
scoreboard players operation @s bhre.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhre.cl = @s bhre.igl
scoreboard players reset &GlowingUntilAdditional bhre.cl
execute @s[score_bhre.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhre.cl 0
scoreboard players set @s bhre.igl 0
