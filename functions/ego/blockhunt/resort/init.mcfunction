scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 795649988
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1390,y=101,z=-13,dx=-92,dy=-97,dz=-92] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHRE","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Resort","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 795649988"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Resort","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Resort","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 795649988"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 795649988
scoreboard objectives add bhre.prng dummy Pseudo-RNG
scoreboard players set &Increment bhre.prng 12345
scoreboard players set &Modulus bhre.prng 30
scoreboard players set &Multiplier bhre.prng 1103515245
scoreboard players set &Offset bhre.prng 0
scoreboard players set &Seed bhre.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhre. dummy Resort
scoreboard objectives setdisplay sidebar bhre.
scoreboard objectives add bhre.pl dummy Resort Player List
scoreboard objectives add bhre.ti dummy Resort Timer
scoreboard objectives add bhre.chi dummy Resort Count Hiders
scoreboard objectives add bhre.cvr dummy Resort Count Seekers
scoreboard objectives add bhre.gl dummy Resort Virus Glowing
scoreboard objectives add bhre.cl dummy Resort Calculations
scoreboard objectives add bhre.igl dummy Resort Input Glowing
scoreboard objectives add bhre.igt dummy Resort Input Game Time
scoreboard objectives add bhre.icd dummy Resort Input Countdown
scoreboard objectives add bhre.st dummy Resort State
scoreboard teams add bhre.h Resort Hiders
scoreboard teams option bhre.h friendlyfire false
scoreboard teams option bhre.h collisionRule never
scoreboard teams option bhre.h deathMessageVisibility always
scoreboard teams option bhre.h nametagVisibility never
scoreboard teams option bhre.h color green
scoreboard teams option bhre.h seeFriendlyInvisibles false
scoreboard teams add bhre.v Resort Seekers
scoreboard teams option bhre.v friendlyfire false
scoreboard teams option bhre.v collisionRule never
scoreboard teams option bhre.v deathMessageVisibility always
scoreboard teams option bhre.v color yellow
scoreboard teams add bhre.d_y Resort Display Yellow
scoreboard teams option bhre.d_y color yellow
scoreboard teams add bhre.d_g Resort Display Green
scoreboard teams option bhre.d_g color green
scoreboard teams join bhre.d_y Countdown
scoreboard teams join bhre.d_y Minutes
scoreboard teams join bhre.d_y Seconds
scoreboard teams join bhre.d_y Seekers
scoreboard teams join bhre.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhre.stand","bhre.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhre.prng","bhre.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhre.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhre.prng] ~ ~ ~ execute @s[tag=bhre.prng_true] ~ ~ ~ scoreboard players add &Seed bhre.prng 1073741824
