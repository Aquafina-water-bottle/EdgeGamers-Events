scoreboard players operation &GameTime bhre.cl = @s bhre.igt
scoreboard players operation &GameTime bhre.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhre.cl = @s bhre.igt
scoreboard players operation &GameTimeMinutes bhre.cl = @s bhre.igt
scoreboard players operation &GameTimeMinutes bhre.cl /= 60 g.number
scoreboard players operation @s bhre.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhre.cl = @s bhre.igt
scoreboard players reset &GameTimeAdditional bhre.cl
execute @s[score_bhre.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhre.cl 0
scoreboard players set @s bhre.igt 0
