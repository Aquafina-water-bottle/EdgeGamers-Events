scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1390,y=101,z=-13,dx=-92,dy=-97,dz=-92] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-795649989] gp.id 795649988
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-795649987] gp.id 795649988
kill @e[x=-1390,y=101,z=-13,dx=-92,dy=-97,dz=-92,type=minecraft:item,tag=!bhre.entity]
execute @e[type=minecraft:armor_stand,score_bhre.icd_min=1,tag=bhre.stand] ~ ~ ~ function ego:blockhunt/resort/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhre.igt_min=1,tag=bhre.stand] ~ ~ ~ function ego:blockhunt/resort/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhre.igl_min=1,tag=bhre.stand] ~ ~ ~ function ego:blockhunt/resort/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhre.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhre.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhre.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhre.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhre.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhre.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhre.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhre.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhre.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhre.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhre.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhre.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhre.pl_min=1,score_bhre.pl=2] bhre.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhre.pl_min=1,score_bhre.pl=2] bhre.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhre.pl_min=1,score_bhre.pl=2] bhre.pl 0
scoreboard players add @a bhre.pl 0
execute @e[type=minecraft:armor_stand,score_bhre.st_min=0,score_bhre.st=0,tag=bhre.stand] ~ ~ ~ function ego:blockhunt/resort/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhre.st_min=1,score_bhre.st=1,tag=bhre.stand] ~ ~ ~ function ego:blockhunt/resort/countdown
execute @e[type=minecraft:armor_stand,score_bhre.st_min=2,score_bhre.st=2,tag=bhre.stand] ~ ~ ~ function ego:blockhunt/resort/during_round
execute @e[type=minecraft:armor_stand,score_bhre.st_min=3,score_bhre.st=3,tag=bhre.stand] ~ ~ ~ function ego:blockhunt/resort/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.chi 0
execute @a[m=2,team=bhre.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.chi 1
scoreboard players operation Hiders bhre. = @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.cvr 0
execute @a[m=2,team=bhre.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.cvr 1
scoreboard players operation Seekers bhre. = @e[type=minecraft:armor_stand,tag=bhre.stand] bhre.cvr
