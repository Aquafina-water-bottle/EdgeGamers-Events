scoreboard players operation &Countdown bhre.cl = @s bhre.icd
scoreboard players operation &Countdown bhre.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhre.cl = @s bhre.icd
scoreboard players operation &CountdownMinutes bhre.cl = @s bhre.icd
scoreboard players operation &CountdownMinutes bhre.cl /= 60 g.number
scoreboard players operation @s bhre.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhre.cl = @s bhre.icd
scoreboard players reset &CountdownAdditional bhre.cl
execute @s[score_bhre.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhre.cl 0
scoreboard players set @s bhre.icd 0
