scoreboard players operation &Countdown bhmv.cl = @s bhmv.icd
scoreboard players operation &Countdown bhmv.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhmv.cl = @s bhmv.icd
scoreboard players operation &CountdownMinutes bhmv.cl = @s bhmv.icd
scoreboard players operation &CountdownMinutes bhmv.cl /= 60 g.number
scoreboard players operation @s bhmv.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhmv.cl = @s bhmv.icd
scoreboard players reset &CountdownAdditional bhmv.cl
execute @s[score_bhmv.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhmv.cl 0
scoreboard players set @s bhmv.icd 0
