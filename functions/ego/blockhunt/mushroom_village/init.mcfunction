scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 119885644
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1117,y=48,z=-153,dx=73,dy=-44,dz=-72] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHMV","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Mushroom Village","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 119885644"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Mushroom Village","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Mushroom Village","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 119885644"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 119885644
scoreboard objectives add bhmv.prng dummy Pseudo-RNG
scoreboard players set &Increment bhmv.prng 12345
scoreboard players set &Modulus bhmv.prng 16
scoreboard players set &Multiplier bhmv.prng 1103515245
scoreboard players set &Offset bhmv.prng 0
scoreboard players set &Seed bhmv.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhmv. dummy Mushroom Village
scoreboard objectives setdisplay sidebar bhmv.
scoreboard objectives add bhmv.pl dummy Mushroom Village Player List
scoreboard objectives add bhmv.ti dummy Mushroom Village Timer
scoreboard objectives add bhmv.chi dummy Mushroom Village Count Hiders
scoreboard objectives add bhmv.cvr dummy Mushroom Village Count Seekers
scoreboard objectives add bhmv.gl dummy Mushroom Village Virus Glowing
scoreboard objectives add bhmv.cl dummy Mushroom Village Calculations
scoreboard objectives add bhmv.igl dummy Mushroom Village Input Glowing
scoreboard objectives add bhmv.igt dummy Mushroom Village Input Game Time
scoreboard objectives add bhmv.icd dummy Mushroom Village Input Countdown
scoreboard objectives add bhmv.st dummy Mushroom Village State
scoreboard teams add bhmv.h Mushroom Village Hiders
scoreboard teams option bhmv.h friendlyfire false
scoreboard teams option bhmv.h collisionRule never
scoreboard teams option bhmv.h deathMessageVisibility always
scoreboard teams option bhmv.h nametagVisibility never
scoreboard teams option bhmv.h color green
scoreboard teams option bhmv.h seeFriendlyInvisibles false
scoreboard teams add bhmv.v Mushroom Village Seekers
scoreboard teams option bhmv.v friendlyfire false
scoreboard teams option bhmv.v collisionRule never
scoreboard teams option bhmv.v deathMessageVisibility always
scoreboard teams option bhmv.v color yellow
scoreboard teams add bhmv.d_y Mushroom Village Display Yellow
scoreboard teams option bhmv.d_y color yellow
scoreboard teams add bhmv.d_g Mushroom Village Display Green
scoreboard teams option bhmv.d_g color green
scoreboard teams join bhmv.d_y Countdown
scoreboard teams join bhmv.d_y Minutes
scoreboard teams join bhmv.d_y Seconds
scoreboard teams join bhmv.d_y Seekers
scoreboard teams join bhmv.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhmv.stand","bhmv.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhmv.prng","bhmv.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhmv.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhmv.prng] ~ ~ ~ execute @s[tag=bhmv.prng_true] ~ ~ ~ scoreboard players add &Seed bhmv.prng 1073741824
