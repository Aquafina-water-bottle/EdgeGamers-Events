scoreboard players operation &GlowingUntil bhmv.cl = @s bhmv.igl
scoreboard players operation &GlowingUntil bhmv.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhmv.cl = @s bhmv.igl
scoreboard players operation &GlowingUntilMinutes bhmv.cl = @s bhmv.igl
scoreboard players operation &GlowingUntilMinutes bhmv.cl /= 60 g.number
scoreboard players operation @s bhmv.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhmv.cl = @s bhmv.igl
scoreboard players reset &GlowingUntilAdditional bhmv.cl
execute @s[score_bhmv.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhmv.cl 0
scoreboard players set @s bhmv.igl 0
