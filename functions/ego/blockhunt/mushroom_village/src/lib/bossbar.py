"""
General module for dealing with minecraft bossbars introduced in 1.13

This will be used as the basis for other modules since there will be a few syntactical changes:
    - A singleton class will be used instead of a global object
    - Format of the new_str method will be slightly changed
"""

class BossBar:
    def __init__(self, *args, **kwargs):
        raise NotImplementedError()

class BossBars:
    def __init__(self, *args, **kwargs):
        raise NotImplementedError()

    def new(self, text: str, display=None, remove=True):
        """
        Initializes multiple bossbars inside a multiline string

        Format for declaration:
            id [json, string]?
             - string is gotten all the way until the newline
             - string is then formatted as a json {"text":"string"}
             - if no json or string is given, the json will be formatted as {"text":"id"}

        Args:
            text

        Examples:
            >>> bossbars = BossBars()
            >>> bossbars.new_str('''
            ... _ _
            ... _id1 {"text":"test"}
            ...     color = yellow
            ...     visible = false
            ...     style = 20
            ...
            ... _id2 Display Name
            ...     color = green
            ...     max = 216
            ...     value = 25
            ...
            ... _id3 _
            ...     style 0
            ...
            ... ''', display="Royal Rumble")

            >>> print(bossbars.cmd_init())
            bossbar add _ {"text":"Royal Rumble"}
            bossbar add _id1 {"text":"test"}
            bossbar _id1 color = yellow
            bossbar _id1 visible = false
            bossbar _id1 style = 20
            bossbar add _id2 {"text":"Royal Rumble Display Name"}
            bossbar _id2 color = green
            bossbar _id2 max = 216
            bossbar _id2 value = 25
            bossbar add _id3 {"text":"Royal Rumble"}
            bossbar _id3 style = 0

            >>> print(bossbars.cmd_term())
            bossbar remove _
            bossbar remove _id1
            bossbar remove _id2
            bossbar remove _id3
        """
        pass

