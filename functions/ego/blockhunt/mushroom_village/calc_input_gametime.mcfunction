scoreboard players operation &GameTime bhmv.cl = @s bhmv.igt
scoreboard players operation &GameTime bhmv.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhmv.cl = @s bhmv.igt
scoreboard players operation &GameTimeMinutes bhmv.cl = @s bhmv.igt
scoreboard players operation &GameTimeMinutes bhmv.cl /= 60 g.number
scoreboard players operation @s bhmv.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhmv.cl = @s bhmv.igt
scoreboard players reset &GameTimeAdditional bhmv.cl
execute @s[score_bhmv.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhmv.cl 0
scoreboard players set @s bhmv.igt 0
