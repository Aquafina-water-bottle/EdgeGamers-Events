scoreboard players set @a g.sa 0
scoreboard players set @a[x=-1117,y=48,z=-153,dx=73,dy=-44,dz=-72] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-119885645] gp.id 119885644
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-119885643] gp.id 119885644
kill @e[x=-1117,y=48,z=-153,dx=73,dy=-44,dz=-72,type=minecraft:item,tag=!bhmv.entity]
execute @e[type=minecraft:armor_stand,score_bhmv.icd_min=1,tag=bhmv.stand] ~ ~ ~ function ego:blockhunt/mushroom_village/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhmv.igt_min=1,tag=bhmv.stand] ~ ~ ~ function ego:blockhunt/mushroom_village/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhmv.igl_min=1,tag=bhmv.stand] ~ ~ ~ function ego:blockhunt/mushroom_village/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhmv.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhmv.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhmv.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhmv.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhmv.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhmv.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhmv.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhmv.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhmv.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhmv.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhmv.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhmv.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhmv.pl_min=1,score_bhmv.pl=2] bhmv.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhmv.pl_min=1,score_bhmv.pl=2] bhmv.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhmv.pl_min=1,score_bhmv.pl=2] bhmv.pl 0
scoreboard players add @a bhmv.pl 0
execute @e[type=minecraft:armor_stand,score_bhmv.st_min=0,score_bhmv.st=0,tag=bhmv.stand] ~ ~ ~ function ego:blockhunt/mushroom_village/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhmv.st_min=1,score_bhmv.st=1,tag=bhmv.stand] ~ ~ ~ function ego:blockhunt/mushroom_village/countdown
execute @e[type=minecraft:armor_stand,score_bhmv.st_min=2,score_bhmv.st=2,tag=bhmv.stand] ~ ~ ~ function ego:blockhunt/mushroom_village/during_round
execute @e[type=minecraft:armor_stand,score_bhmv.st_min=3,score_bhmv.st=3,tag=bhmv.stand] ~ ~ ~ function ego:blockhunt/mushroom_village/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.chi 0
execute @a[m=2,team=bhmv.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.chi 1
scoreboard players operation Hiders bhmv. = @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.cvr 0
execute @a[m=2,team=bhmv.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.cvr 1
scoreboard players operation Seekers bhmv. = @e[type=minecraft:armor_stand,tag=bhmv.stand] bhmv.cvr
