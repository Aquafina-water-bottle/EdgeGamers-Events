scoreboard players set @a g.sa 0
scoreboard players set @a[x=-107,y=2,z=-130,dx=193,dy=100,dz=241] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-1356189676] gp.id 1356189675
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-1356189674] gp.id 1356189675
kill @e[x=-107,y=2,z=-130,dx=193,dy=100,dz=241,type=minecraft:item,tag=!bhvr2.entity]
execute @e[type=minecraft:armor_stand,score_bhvr2.icd_min=1,tag=bhvr2.stand] ~ ~ ~ function ego:blockhunt/virus_2/calc_input_countdown
execute @e[type=minecraft:armor_stand,score_bhvr2.igt_min=1,tag=bhvr2.stand] ~ ~ ~ function ego:blockhunt/virus_2/calc_input_gametime
execute @e[type=minecraft:armor_stand,score_bhvr2.igl_min=1,tag=bhvr2.stand] ~ ~ ~ function ego:blockhunt/virus_2/calc_input_glowing
title @a[score_g.host_min=1,score_g.host=1] actionbar {"text":"","extra":[{"text":"Initial game time: [","color":"green"},{"score":{"name":"&GameTimeMinutes","objective":"bhvr2.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GameTimeAdditional","objective":"bhvr2.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GameTimeSeconds","objective":"bhvr2.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GameTimeTotalSeconds","objective":"bhvr2.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Glowing until: [","color":"green"},{"score":{"name":"&GlowingUntilMinutes","objective":"bhvr2.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilAdditional","objective":"bhvr2.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&GlowingUntilSeconds","objective":"bhvr2.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&GlowingUntilTotalSeconds","objective":"bhvr2.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"], Countdown: [","color":"green"},{"score":{"name":"&CountdownMinutes","objective":"bhvr2.cl"},"color":"yellow","bold":"true"},{"text":":","color":"yellow","bold":"true"},{"score":{"name":"&CountdownAdditional","objective":"bhvr2.cl"},"color":"yellow","bold":"true"},{"score":{"name":"&CountdownSeconds","objective":"bhvr2.cl"},"color":"yellow","bold":"true"},{"text":" (","color":"yellow"},{"score":{"name":"&CountdownTotalSeconds","objective":"bhvr2.cl"},"color":"yellow"},{"text":"s)","color":"yellow"},{"text":"]","color":"green"}]}
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.lg_min=1,score_g.lg=1,score_bhvr2.pl_min=1,score_bhvr2.pl=2] bhvr2.pl 0
scoreboard players set @a[m=2,score_g.sa_min=1,score_g.sa=1,score_g.stp_min=1,score_g.stp=1,score_bhvr2.pl_min=1,score_bhvr2.pl=2] bhvr2.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_bhvr2.pl_min=1,score_bhvr2.pl=2] bhvr2.pl 0
scoreboard players add @a bhvr2.pl 0
execute @e[type=minecraft:armor_stand,score_bhvr2.st_min=0,score_bhvr2.st=0,tag=bhvr2.stand] ~ ~ ~ function ego:blockhunt/virus_2/wait_for_start
execute @e[type=minecraft:armor_stand,score_bhvr2.st_min=1,score_bhvr2.st=1,tag=bhvr2.stand] ~ ~ ~ function ego:blockhunt/virus_2/countdown
execute @e[type=minecraft:armor_stand,score_bhvr2.st_min=2,score_bhvr2.st=2,tag=bhvr2.stand] ~ ~ ~ function ego:blockhunt/virus_2/during_round
execute @e[type=minecraft:armor_stand,score_bhvr2.st_min=3,score_bhvr2.st=3,tag=bhvr2.stand] ~ ~ ~ function ego:blockhunt/virus_2/wait_for_reset
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.chi 0
execute @a[m=2,team=bhvr2.h,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.chi 1
scoreboard players operation Hiders bhvr2. = @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.chi
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.cvr 0
execute @a[m=2,team=bhvr2.v,score_g.sa_min=1,score_g.sa=1] ~ ~ ~ scoreboard players add @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.cvr 1
scoreboard players operation Seekers bhvr2. = @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.cvr
