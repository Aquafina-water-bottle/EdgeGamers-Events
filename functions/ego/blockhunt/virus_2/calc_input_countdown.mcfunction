scoreboard players operation &Countdown bhvr2.cl = @s bhvr2.icd
scoreboard players operation &Countdown bhvr2.cl *= 20 g.number
scoreboard players operation &CountdownTotalSeconds bhvr2.cl = @s bhvr2.icd
scoreboard players operation &CountdownMinutes bhvr2.cl = @s bhvr2.icd
scoreboard players operation &CountdownMinutes bhvr2.cl /= 60 g.number
scoreboard players operation @s bhvr2.icd %= 60 g.number
scoreboard players operation &CountdownSeconds bhvr2.cl = @s bhvr2.icd
scoreboard players reset &CountdownAdditional bhvr2.cl
execute @s[score_bhvr2.icd=9] ~ ~ ~ scoreboard players set &CountdownAdditional bhvr2.cl 0
scoreboard players set @s bhvr2.icd 0
