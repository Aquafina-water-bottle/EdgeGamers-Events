scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1356189675
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=-107,y=2,z=-130,dx=193,dy=100,dz=241] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"BHVR2","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus 2","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1356189675"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Virus 2","color":"yellow","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Virus 2","color":"yellow"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1356189675"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1356189675
scoreboard objectives add bhvr2.prng dummy Pseudo-RNG
scoreboard players set &Increment bhvr2.prng 12345
scoreboard players set &Modulus bhvr2.prng 6
scoreboard players set &Multiplier bhvr2.prng 1103515245
scoreboard players set &Offset bhvr2.prng 0
scoreboard players set &Seed bhvr2.prng 0
scoreboard players set 20 g.number 20
scoreboard players set 60 g.number 60
scoreboard objectives add bhvr2. dummy Virus 2
scoreboard objectives setdisplay sidebar bhvr2.
scoreboard objectives add bhvr2.pl dummy Virus 2 Player List
scoreboard objectives add bhvr2.ti dummy Virus 2 Timer
scoreboard objectives add bhvr2.chi dummy Virus 2 Count Hiders
scoreboard objectives add bhvr2.cvr dummy Virus 2 Count Seekers
scoreboard objectives add bhvr2.gl dummy Virus 2 Virus Glowing
scoreboard objectives add bhvr2.cl dummy Virus 2 Calculations
scoreboard objectives add bhvr2.igl dummy Virus 2 Input Glowing
scoreboard objectives add bhvr2.igt dummy Virus 2 Input Game Time
scoreboard objectives add bhvr2.icd dummy Virus 2 Input Countdown
scoreboard objectives add bhvr2.st dummy Virus 2 State
scoreboard teams add bhvr2.h Virus 2 Hiders
scoreboard teams option bhvr2.h friendlyfire false
scoreboard teams option bhvr2.h collisionRule never
scoreboard teams option bhvr2.h deathMessageVisibility always
scoreboard teams option bhvr2.h nametagVisibility never
scoreboard teams option bhvr2.h color green
scoreboard teams option bhvr2.h seeFriendlyInvisibles false
scoreboard teams add bhvr2.v Virus 2 Seekers
scoreboard teams option bhvr2.v friendlyfire false
scoreboard teams option bhvr2.v collisionRule never
scoreboard teams option bhvr2.v deathMessageVisibility always
scoreboard teams option bhvr2.v color yellow
scoreboard teams add bhvr2.d_y Virus 2 Display Yellow
scoreboard teams option bhvr2.d_y color yellow
scoreboard teams add bhvr2.d_g Virus 2 Display Green
scoreboard teams option bhvr2.d_g color green
scoreboard teams join bhvr2.d_y Countdown
scoreboard teams join bhvr2.d_y Minutes
scoreboard teams join bhvr2.d_y Seconds
scoreboard teams join bhvr2.d_y Seekers
scoreboard teams join bhvr2.d_g Hiders
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["bhvr2.stand","bhvr2.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.gl 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.st 0
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.icd 60
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.igt 600
scoreboard players set @e[type=minecraft:armor_stand,tag=bhvr2.stand] bhvr2.igl 300
summon area_effect_cloud ~ ~ ~ {Tags:["bhvr2.prng","bhvr2.prng_true"],Duration:5}
summon area_effect_cloud ~ ~ ~ {Tags:["bhvr2.prng"],Duration:5}
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 1
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 2
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 4
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 8
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 16
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 32
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 64
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 128
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 256
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 512
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 1024
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 2048
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 4096
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 8192
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 16384
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 32768
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 65536
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 131072
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 262144
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 524288
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 1048576
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 2097152
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 4194304
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 8388608
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 16777216
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 33554432
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 67108864
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 134217728
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 268435456
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 536870912
execute @r[type=minecraft:area_effect_cloud,tag=bhvr2.prng] ~ ~ ~ execute @s[tag=bhvr2.prng_true] ~ ~ ~ scoreboard players add &Seed bhvr2.prng 1073741824
