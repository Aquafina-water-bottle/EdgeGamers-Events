scoreboard players operation &GameTime bhvr2.cl = @s bhvr2.igt
scoreboard players operation &GameTime bhvr2.cl *= 20 g.number
scoreboard players operation &GameTimeTotalSeconds bhvr2.cl = @s bhvr2.igt
scoreboard players operation &GameTimeMinutes bhvr2.cl = @s bhvr2.igt
scoreboard players operation &GameTimeMinutes bhvr2.cl /= 60 g.number
scoreboard players operation @s bhvr2.igt %= 60 g.number
scoreboard players operation &GameTimeSeconds bhvr2.cl = @s bhvr2.igt
scoreboard players reset &GameTimeAdditional bhvr2.cl
execute @s[score_bhvr2.igt=9] ~ ~ ~ scoreboard players set &GameTimeAdditional bhvr2.cl 0
scoreboard players set @s bhvr2.igt 0
