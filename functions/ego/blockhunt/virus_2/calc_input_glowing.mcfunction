scoreboard players operation &GlowingUntil bhvr2.cl = @s bhvr2.igl
scoreboard players operation &GlowingUntil bhvr2.cl *= 20 g.number
scoreboard players operation &GlowingUntilTotalSeconds bhvr2.cl = @s bhvr2.igl
scoreboard players operation &GlowingUntilMinutes bhvr2.cl = @s bhvr2.igl
scoreboard players operation &GlowingUntilMinutes bhvr2.cl /= 60 g.number
scoreboard players operation @s bhvr2.igl %= 60 g.number
scoreboard players operation &GlowingUntilSeconds bhvr2.cl = @s bhvr2.igl
scoreboard players reset &GlowingUntilAdditional bhvr2.cl
execute @s[score_bhvr2.igl=9] ~ ~ ~ scoreboard players set &GlowingUntilAdditional bhvr2.cl 0
scoreboard players set @s bhvr2.igl 0
