from typing import NamedTuple, Union
from abc import ABC

from lib.floo_event import FlooEvent
from lib.coords import PositionCoords, RegionCoords, Coords
from lib.repr_utils import addrepr


__all__ = [
    "Race",
    "Fill",
    "Place",
    "Checkpoint",
    "RegularRace",
    "CheckpointRace",
    "LapRace",
    ]


class Fill(NamedTuple):
    region: Union[PositionCoords, RegionCoords]
    block: str
    term_block: str = "air"

@addrepr
class Place:
    """
    Attributes:
        select_coords (Coords)
        message (str)
        place_num (int)
        fills (List[str])
        additional_cmds (List[str])
    """

    default_messages = (
        "took first place!",
        "took second place!",
        "took third place!",
        "got runnerup!"
    )

    def __init__(self, place_num, message=None, select_coords=None, fills=None, additional_cmds=None):
        """
        Args:
            place_num (int): The place place_num that will be shown on display
            message (str): The message that will appear when a player finishes the race,
                formatted as (player) message
            select_coords (Coords): The selection region for each player
            fills (List[Fill]): Any additional commands that should be added
            additional_cmds (List[str]): Any additional commands that should be added
        """
        super().__init__()
        self.place_num = place_num

        # The message defaults to the messages defined in Place.default_messages
        if message is None:
            if place_num > len(Place.default_messages):
                self.message = Place.default_messages[-1]
            else:
                self.message = Place.default_messages[place_num - 1]
        else:
            self.message = message

        self.select_coords = select_coords

        if fills is None:
            self.fills = []
        else:
            self.fills = fills

        if additional_cmds is None:
            self.additional_cmds = []
        else:
            self.additional_cmds = additional_cmds


@addrepr
class Race(FlooEvent, ABC):
    """
    Storage class to contain multiple race properties

    Attributes:
        fill_air (Coords region)
        fill_block (Coords region)
        objective_disp (str): A possibly shortened version of display name to be put into objective display names
        places (List[Place]): Holds the nth place information
    """

    def __init__(self, event, objective_disp=None, **options):
        """
        Args:
            event (floo_network.Event)
            fill_air (Coords region)
            fill_block (Coords region)
        """
        super().__init__(event, **options)
        self.places = []

        if objective_disp is None:
            self.objective_disp = self.location.name_str
        else:
            self.objective_disp = objective_disp

    def make_place(self, **kwargs):
        """
        See Place.__init__
        """
        place_num = len(self.places) + 1

        return Place(place_num, **kwargs)


class RegularRace(Race):
    pass


class Checkpoint(NamedTuple):
    """
    Attributes:
        select_coords (Coords): Selection to set the checkpoint
        tp_coords (Coords): Teleport coordinates when they fall into water or lava

    Args:
        select_coords (Coords): Selection to set the checkpoint
        tp_coords (Coords): Teleport coordinates when they fall into water or lava
    """
    select_coords: Coords
    tp_coords: Coords


class CheckpointRace(Race):
    def __init__(self, event, spawn, objective_disp=None, **options):
        super().__init__(event, objective_disp, **options)
        self.spawn = spawn
        self.checkpoints = []


class LapRace(Race):
    """
    Container class to get lap race properties
    Attributes:
        lap (str): selector region when completing a lap
        lap_coords (Coords)
        lap_reset (str): selector region when resetting the lap selection
        lap_reset_coords (Coords)
        spawn (str): selector region for the spawn area
        spawn_coords (Coords)
    """

    def __init__(self, event, lap_coords, lap_reset_coords, spawn_coords,
                 objective_disp=None, required_laps=None, **options):
        """
        Args:
            event (floo_network.Event)
            lap (Coords region)
            lap_reset (Coords region)
            spawn (Coords region)
        """
        super().__init__(event, objective_disp, **options)

        self.lap_coords = lap_coords
        self.select_lap = lap_coords.selector()

        self.lap_reset_coords = lap_reset_coords
        self.select_lap_reset = lap_reset_coords.selector()

        self.spawn_coords = spawn_coords
        self.select_spawn = spawn_coords.selector()

        self.required_laps = required_laps

