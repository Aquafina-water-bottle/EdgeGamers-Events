from abc import ABC


'''
class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

'''
class Singleton(ABC):
    """
    Ensures that any class that is inherited from this Singleton class is a singleton

    Examples:
        >>> class Group1(Singleton): pass
        >>> class Group2(Singleton): pass

        >>> g1_1 = Group1()
        >>> g1_2 = Group1()
        >>> g2_1 = Group2()
        >>> g2_2 = Group2()

        IDs (as addresses to memory) are the same if the singleton class is the same
        >>> id(g1_1) == id(g1_2)
        True

        >>> id(g2_1) == id(g2_2)
        True

        IDs are different if the singleton class is different
        >>> id(g2_1) == id(g1_1)
        False
    """

    # pylint: disable=unused-argument
    def __new__(cls, *args, **kwargs):
        """
        Ensures that the class is a singleton with the same id
        """
        singleton_var = f"_{cls.__name__}"
        if not hasattr(cls, singleton_var):
            setattr(cls, singleton_var, super().__new__(cls))
        # print(singleton_var, cls, id(getattr(cls, singleton_var)))
        return getattr(cls, singleton_var)


if __name__ == "__main__":
    import doctest
    doctest.testmod()

