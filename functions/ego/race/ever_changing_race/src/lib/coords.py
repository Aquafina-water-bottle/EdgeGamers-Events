import re
import sys
from abc import ABC, abstractmethod

if __name__ == "__main__":
    sys.path.append("..")

from lib.assert_utils import assert_type
from lib.coord import Coord
from lib.vector import Vector2, Vector3

"""
Coordinates group objects
"""


def _get_vec3(dictionary, x_key, y_key, z_key):
    """
    Returns:
        Vector3: Vector containing values from the dictionary
            given the specified keys
    """
    x = dictionary[x_key]
    y = dictionary[y_key]
    z = dictionary[z_key]
    return Vector3(x, y, z)



class CoordsError(Exception):
    pass

class CoordsNotAbsoluteError(Exception):
    pass


def check_coord_type(coords_group, coord_type):
    assert_type(coords_group, coord_type, CoordsError)


class CoordsBase(ABC):
    """
    Coords base class
    """

    @abstractmethod
    def cmd(self, args):
        """
        Returns:
            str: The full fena command
        """
        pass

    @property
    def absolute(self):
        for coord in self:
            if not coord.absolute:
                return False
        return True

    @property
    def relative(self):
        for coord in self:
            if not coord.relative:
                return False
        return True

    @property
    def local(self):
        for coord in self:
            if not coord.local:
                return False
        return True


class NonVectorCoordsBase(CoordsBase, ABC):
    """
    Base class for a coordinate that doesn't inherit from a Vector class

    Attributes:
        _coords_groups (list of CoordsBase objects)
    """
    @abstractmethod
    def __init__(self, *coords_groups):
        self._coords_groups = list(coords_groups)

    @abstractmethod
    def __len__(self):
        pass

    def __iter__(self):
        for vector in self._coords_groups:
            for coord in vector:
                yield coord

    def __str__(self):
        return " ".join(str(c) for c in self)

    def __repr__(self):
        class_name = type(self).__name__
        return f"{class_name}({', '.join(str(c) for c in self)})"


class PositionCoords(CoordsBase, Vector3):
    """
    Examples:
        >>> p = Coords('^', '^-2', Coord.from_value_prefix(round(3.5e-100, 1), '^'))
        >>> p
        PositionCoords(^, ^-2, ^)

        >>> str(p)
        '^ ^-2 ^'

        >>> p.local
        True

        >>> p.relative
        False

        >>> p.absolute
        False

        >>> list(p)
        [Coord(prefix='^', value=0), Coord(prefix='^', value=-2), Coord(prefix='^', value=0.0)]

        >>> len(p)
        3

        >>> p.xxy
        Vector3(^, ^, ^-2)

        >>> p.cmd('stone_brick')
        'setblock ^ ^-2 ^ stone_brick'
    """
    def cmd(self, args):
        return f"setblock {self} {args}"

    setblock = cmd


class RotationCoords(CoordsBase, Vector2):
    """
    Examples:
        >>> r = Coords(1, 2)
        >>> r
        RotationCoords(1, 2)

        >>> str(r)
        '1 2'

        >>> r.local
        False

        >>> r.relative
        False

        >>> r.absolute
        True

        >>> list(r)
        [Coord(prefix='', value=1), Coord(prefix='', value=2)]

        >>> len(r)
        2

        >>> r.yx
        Vector2(2, 1)

        >>> r.cmd('@s')
        'tp @s ~ ~ ~ 1 2'
    """

    # pylint: disable=arguments-differ
    def cmd(self, selector):
        return f"tp {selector} ~ ~ ~ {self}"

    tp_rot = cmd


class TeleportCoords(NonVectorCoordsBase):
    """
    Examples:
        >>> t = Coords(1, 2, '~3', Coord(4), -5)
        >>> t
        TeleportCoords(1, 2, ~3, 4, -5)

        >>> str(t)
        '1 2 ~3 4 -5'

        >>> t.local
        False

        >>> t.relative
        False

        >>> t.absolute
        False

        >>> list(t)
        [Coord(prefix='', value=1), Coord(prefix='', value=2), Coord(prefix='~', value=3), Coord(prefix='', value=4), Coord(prefix='', value=-5)]

        >>> len(t)
        5

        >>> t.pos
        PositionCoords(1, 2, ~3)

        >>> t.rot
        RotationCoords(4, -5)

        >>> t.pos.zyx
        Vector3(~3, 2, 1)

        >>> t.cmd('@s')
        'tp @s 1 2 ~3 4 -5'

        >>> rot = Coords("~", "~")
        >>> t.rot = rot
        >>> t
        TeleportCoords(1, 2, ~3, ~, ~)

        >>> t.rot
        RotationCoords(~, ~)
    """
    def __init__(self, x, y, z, xr, yr):
        pos = PositionCoords(x, y, z)
        rot = RotationCoords(xr, yr)
        super().__init__(pos, rot)

    @property
    def pos(self):
        return self._coords_groups[0]

    @pos.setter
    def pos(self, pos):
        check_coord_type(pos, PositionCoords)
        self._coords_groups[0] = pos

    @property
    def rot(self):
        return self._coords_groups[1]

    @rot.setter
    def rot(self, rot):
        check_coord_type(rot, RotationCoords)
        self._coords_groups[1] = rot

    # pylint: disable=arguments-differ
    def cmd(self, selector):
        return f"tp {selector} {self}"

    tp = cmd

    def __len__(self):
        return 5


class RegionCoords(NonVectorCoordsBase):
    """
    Examples:
        >>> r = Coords(1, 2, '3', Coord(4), -5.5, 6)
        >>> r
        RegionCoords(1, 2, 3, 4, -5.5, 6)

        >>> str(r)
        '1 2 3 4 -5.5 6'

        >>> r.local
        False

        >>> r.relative
        False

        >>> r.absolute
        True

        >>> list(r)
        [Coord(prefix='', value=1), Coord(prefix='', value=2), Coord(prefix='', value=3), Coord(prefix='', value=4), Coord(prefix='', value=-5.5), Coord(prefix='', value=6)]

        >>> len(r)
        6

        >>> r.pos
        PositionCoords(1, 2, 3)

        >>> r.pos2
        PositionCoords(4, -5.5, 6)

        >>> r.pos.zyz
        Vector3(3, 2, 3)

        >>> r.cmd('stone_brick')
        'fill 1 2 3 4 -5.5 6 stone_brick'

        >>> pos2 = Coords("8", "7", "6")
        >>> r.pos2 = pos2
        >>> r
        RegionCoords(1, 2, 3, 8, 7, 6)

        >>> r.pos2
        PositionCoords(8, 7, 6)

        >>> r = Coords("x=6,y=7,z=8,dx=10,dy=10,dz=10")
        >>> r
        RegionCoords(6, 7, 8, 16, 17, 18)

        >>> r.selector()
        'x=6,y=7,z=8,dx=10,dy=10,dz=10'
    """
    def __init__(self, x, y, z, x2, y2, z2):
        pos = PositionCoords(x, y, z)
        pos2 = PositionCoords(x2, y2, z2)
        super().__init__(pos, pos2)

    @property
    def pos(self):
        return self._coords_groups[0]

    @pos.setter
    def pos(self, pos):
        check_coord_type(pos, PositionCoords)
        self._coords_groups[0] = pos

    @property
    def pos2(self):
        return self._coords_groups[1]

    @pos2.setter
    def pos2(self, pos2):
        check_coord_type(pos2, PositionCoords)
        self._coords_groups[1] = pos2

    def selector(self):
        if not self.absolute:
            raise CoordsNotAbsoluteError(f"Expected {self} to be absolute")

        selector_args = []

        vec3_1 = _get_vec3(self.pos, "x", "y", "z")
        vec3_2 = _get_vec3(self.pos2, "x", "y", "z")
        offset = vec3_2 - vec3_1

        for arg, value in zip(("x", "y", "z", "dx", "dy", "dz"), list(vec3_1) + list(offset)):
            selector_args.append(f"{arg}={value}")
        return ",".join(selector_args)

    def cmd(self, args):
        return f"fill {self} {args}"

    fill = cmd

    def __len__(self):
        return 6


class CloneCoords(NonVectorCoordsBase):
    """
    Examples:
        >>> c = Coords("~1 ~1 ~1 ~-1 ~-1 ~-1 346 -12 56")
        >>> c
        CloneCoords(~1, ~1, ~1, ~-1, ~-1, ~-1, 346, -12, 56)

        >>> str(c)
        '~1 ~1 ~1 ~-1 ~-1 ~-1 346 -12 56'

        >>> c.local
        False

        >>> c.relative
        False

        >>> c.absolute
        False

        >>> len(c)
        9

        >>> c.region
        RegionCoords(~1, ~1, ~1, ~-1, ~-1, ~-1)

        >>> c.region.relative
        True

        >>> c.pos
        PositionCoords(346, -12, 56)

        >>> c.pos.absolute
        True

        >>> c.region.pos.zy
        Vector2(~1, ~1)

        >>> c.cmd('masked')
        'clone ~1 ~1 ~1 ~-1 ~-1 ~-1 346 -12 56 masked'

        >>> pos = Coords("-1 -2 -3")
        >>> c.pos = pos
        >>> c
        CloneCoords(~1, ~1, ~1, ~-1, ~-1, ~-1, -1, -2, -3)

        >>> c.pos
        PositionCoords(-1, -2, -3)
    """
    def __init__(self, x, y, z, x2, y2, z2, x3, y3, z3):
        region = RegionCoords(x, y, z, x2, y2, z2)
        pos = PositionCoords(x3, y3, z3)
        super().__init__(region, pos)

    @property
    def region(self):
        return self._coords_groups[0]

    @region.setter
    def region(self, region):
        check_coord_type(region, RegionCoords)
        self._coords_groups[0] = region

    @property
    def pos(self):
        return self._coords_groups[1]

    @pos.setter
    def pos(self, pos):
        check_coord_type(pos, PositionCoords)
        self._coords_groups[1] = pos

    def cmd(self, args):
        return f"clone {self} {args}"

    clone = cmd

    def __len__(self):
        return 9


# simple definition since we really shouldn't call a recursive function every time we use Coords()
_ALL_SUBCLASSES = {
        2: RotationCoords,
        3: PositionCoords,
        5: TeleportCoords,
        6: RegionCoords,
        9: CloneCoords,
        }



rx_CSV_COMMA_SEP = re.compile(r'\s*,\s*')
class Coords(ABC):
    """
    Coords Factory
    """

    def __new__(cls, *args):
        """
        Create a new Coords object

        Args:
            *args (coord-like object or str): Each individual coordinates as a
                coord-like object or a full string containing multiple values
                - If this is a single string, can be parsed in two different ways:
                    - selector args 'x=#,y=#,z=#,dx=#,dy=#,dz=#'
                    - full coordinates '~1 ~-3 ~0.2'

        Returns:
            One of any subclasses of Coords based on the length of args (or length of args.split())
        """

        if len(args) == 1 and isinstance(args[0], str):
            string = args[0]

            # Creating coords based on selector.
            if "=" in string:
                selector_args = {}
                for selector_arg in re.split(rx_CSV_COMMA_SEP, string):
                    argument, value = selector_arg.split("=")
                    selector_args[argument] = value

                if len(selector_args) == 3:
                    coords = list(selector_args.values())

                elif len(selector_args) == 6:
                    position_vector = _get_vec3(selector_args, "x", "y", "z")
                    offset_vector = _get_vec3(selector_args, "dx", "dy", "dz")
                    position2_vector = offset_vector + position_vector
                    coords = list(position_vector) + list(position2_vector)

                else:
                    raise CoordsError(f"Unexpected length of arguments for {args}")

            else:
                coords = string.split()

        else:
            coords = list(args)

        length = len(coords)
        try:
            CoordsObject = _ALL_SUBCLASSES[length]
            return CoordsObject(*coords)
        except KeyError:
            # https://docs.python.org/3/library/exceptions.html#BaseException.with_traceback
            tb = sys.exc_info()[2]
        raise CoordsError(f"Expected a length of {list(_ALL_SUBCLASSES)} but got {length} for arguments {args} -> {coords}").with_traceback(tb)

# Register each subclass of CoordsBase to also be a subclass of Coords
#   Rationale: So isinstance will work
# e.g. `isinstance(PositionCoords, Coords) == True`
# even though PositionCoords extends CoordsBase
for subclass in _ALL_SUBCLASSES.values():
    Coords.register(subclass)

def _test_time():
    """
    Results indicate that the try statement is faster for valid lengths,
    meaning the try/except statement will be used over the if statement
    """
    import timeit

    class Factory1:
        def __new__(cls, *coords):
            length = len(coords)
            if length in _ALL_SUBCLASSES:
                return _ALL_SUBCLASSES[length]
            return None

    class Factory2:
        def __new__(cls, *coords):
            length = len(coords)
            try:
                return _ALL_SUBCLASSES[length]
            except KeyError:
                return None

    def test(code_str):
        time = timeit.timeit(code_str, globals=globals())
        print(f"{code_str!r}: {time}")

    print(Factory1(1, 2, 3))
    print(Factory1(1, 2, 3, 4))
    print(Factory2(1, 2, 3))
    print(Factory2(1, 2, 3, 4))

    test("Factory1(1, 2, 3)")
    test("Factory1(1, 2, 3, 4)")

    test("Factory2(1, 2, 3)")
    test("Factory2(1, 2, 3, 4)")


if __name__ == "__main__":
    import doctest
    doctest.testmod()

    c = Coords("366 16 155.0 -90 0")
    print(str(c))

    # _test_time()
    # print(Coords("x=6,y=7,z=8,dx=10,dy=10,dz=10"))






