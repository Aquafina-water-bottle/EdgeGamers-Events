import math
from abc import ABC, abstractmethod

if __name__ == "__main__":
    import sys
    sys.path.append("..")
    del sys

from lib.coord import Coord

"""
Note that with mutability, it implies that it won't return
    itself to prevent confusion.
For example, python implements their lists are mutable by methods
    like `list.sort()` not returning anything.

Similarly, methods solely made to affect only the object itself will not
    return anything.
"""

class VectorError(Exception):
    pass

class InvalidVectorError(VectorError):
    pass


class CoordVector(ABC):
    """
    Implements a general list that holds all coord objects in
    """

    def __init__(self, length):
        self._coords = [None] * length

    @abstractmethod
    def dot(self, vec):
        """
        Return the dot product between this vector and another
        """
        pass

    def norm(self):
        """
        Return the norm of this vector

        Returns:
            The length / norm / magnitude of this vector
        """
        return math.sqrt(self.dot(self))

    def normalized(self):
        """
        Return a normalized version of this Vector

        Returns:
            This vector but with a length of 1
        """
        return self / self.norm()

    def copy(self):
        """
        Return a copy of this vector

        Returns:
            A copy of this vector such that the components of
            the vector are of equal value but do not refer to
            the same objects.
        """
        VectorObject = type(self)
        return VectorObject(*self._coords)

    def to_int(self):
        """
        Sets itself with only integer coord values

        Examples:
            >>> vec2 = Vector2(1.0, 2)
            >>> vec2.to_int()
            >>> vec2
            Vector2(1, 2)

            >>> vec3 = Vector3('~1', '~0', '-3')
            >>> vec3.to_int()
            >>> vec3
            Vector3(~1, ~, -3)

            >>> vec3 = Vector3('~1.0', '~0.0', '-3.0')
            >>> vec3.to_int()
            >>> vec3
            Vector3(~1, ~, -3)
        """
        for index, coord in enumerate(self._coords):
            self._coords[index] = coord.as_int()

    def to_float(self):
        """
        Sets itself with only integer coord values

        Examples:
            >>> vec2 = Vector2(1.0, 2)
            >>> vec2.to_float()
            >>> vec2
            Vector2(1.0, 2.0)

            >>> vec3 = Vector3('~1', '~0', '-3')
            >>> vec3.to_float()
            >>> vec3
            Vector3(~1.0, ~, -3.0)

            >>> vec3 = Vector3('~1.0', '~0.0', '-3.0')
            >>> vec3.to_float()
            >>> vec3
            Vector3(~1.0, ~, -3.0)
        """
        for index, coord in enumerate(self._coords):
            self._coords[index] = coord.as_float()

    def __len__(self):
        """
        Returns:
            The size of this vector (as the number of components)
                - eg. if it stores x, y, z, it returns 3.
        """
        return len(self._coords)

    def __iter__(self):
        """
        Return an iterable of the components of this vector
        """
        yield from self._coords

    def __getitem__(self, key):
        """
        Allow retriving components of this vector via indexing in
        a list-like fashion and with 'x','y','z','w', ... ,etc.

        Raises:
            IndexError when out of bounds or vector does not contain the key
            https://docs.python.org/3/reference/datamodel.html#object.__getitem__
        """
        # slices up to the length so a length of 2 only gets xy, and a length of 3 gets xyz
        keys = "xyz"[0:len(self)]
        if isinstance(key, str):
            if len(key) != 1:
                raise IndexError(f"Expected a key that is one character long instead of {key!r}")

            if key not in keys:
                raise IndexError(f"Expected a key that is a inside {list(keys)} but got {key!r}")

            index = keys.index(key)
            return self._coords[index]

        return self._coords[key]

    def __setitem__(self, key, value):
        """
        Allow modification of components of this vector via indexing in
        a list-like fashion and with 'x','y','z','w', ... ,etc.

        Raises:
            IndexError when out of bounds or vector does not contain the key
            https://docs.python.org/3/reference/datamodel.html#object.__setitem__
        """
        keys = "xyz"[0:len(self)]
        if isinstance(key, str):
            if len(key) != 1:
                raise IndexError(f"Expected a key that is one character long instead of {key!r}")

            if key not in keys:
                raise IndexError(f"Expected a key that is a inside {list(keys)}")

            index = keys.index(key)
            self._coords[index] = value

        self._coords[key] = value

    def __str__(self):
        """
        Returns:
            str: Minecraft formatted coordinates
        """
        return " ".join(str(c) for c in self._coords)

    def __repr__(self):
        """
        Return a string that looks like a valid Python expression to
        recreate this vector
        """

        vector_name = type(self).__name__
        return f"{vector_name}({', '.join(str(c) for c in self._coords)})"


    """
    Arthimetic comparisons
    """

    def __eq__(self, other):
        """
        If each component of this vector equals (in value) with each component
        of the other vector, return True

        Examples:
            >>> vec3_1 = Vector3('~0', 2, '3')
            >>> vec3_2 = Vector3('~', '2.0', 3)
            >>> vec3_3 = Vector3('~1', 2, '3')

            >>> vec2_1 = Vector2('~0', 2)
            >>> vec2_2 = Vector2('~', '2.0')
            >>> vec2_3 = Vector2('~1', 2)

            >>> vec3_1 == vec3_2
            True
            >>> vec3_2 == vec3_1
            True
            >>> vec3_2 != vec3_3
            True
            >>> vec3_1 != vec3_3
            True

            >>> vec2_1 == vec2_2
            True
            >>> vec2_2 == vec2_1
            True
            >>> vec2_2 != vec2_3
            True
            >>> vec2_1 != vec2_3
            True

            >>> vec2_1 != vec3_1
            True
            >>> vec2_1 == vec2_1
            True
        """
        # fast
        if self is other:
            return True

        # fast
        if type(other) != type(self):
            return False

        # slow
        return other._coords == self._coords

    """
    Matrix multiplication
    """

    @abstractmethod
    def __matmul__(self, other):
        """
        Vector * Matrix
        """
        pass

    """
    Arithmetic operations are performed component-wise
    """


    @abstractmethod
    def __add__(self, other):
        pass

    @abstractmethod
    def __iadd__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __sub__(self, other):
        pass

    @abstractmethod
    def __isub__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __mul__(self, other):
        pass

    @abstractmethod
    def __imul__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __truediv__(self, other):
        pass

    @abstractmethod
    def __itruediv__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __floordiv__(self, other):
        pass

    @abstractmethod
    def __ifloordiv__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __mod__(self, other):
        pass

    @abstractmethod
    def __imod__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __pow__(self, other):
        pass

    @abstractmethod
    def __ipow__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __lshift__(self, other):
        pass

    @abstractmethod
    def __ilshift__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __rshift__(self, other):
        pass

    @abstractmethod
    def __irshift__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __and__(self, other):
        pass

    @abstractmethod
    def __iand__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __xor__(self, other):
        pass

    @abstractmethod
    def __ixor__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass

    @abstractmethod
    def __or__(self, other):
        pass

    @abstractmethod
    def __ior__(self, other):
        """
        Should return NotImplemented if any of the vector
        components in self refer to the same object.
        """
        pass


    @abstractmethod
    def __neg__(self):
        """
        Apply the unary arithmetic operation to each component
        """
        pass

    @abstractmethod
    def __pos__(self):
        """
        Apply the unary arithmetic operation to each component
        """
        pass

    @abstractmethod
    def __invert__(self):
        """
        Apply the unary arithmetic operation to each component
        """
        pass



# pylint: disable=too-many-public-methods
# has a butt load of methods because of all the swizzles
class Vector3(CoordVector):
    """
    Base class for a PositionCoordinate, which holds 3 individual coordinates

    Args:
        x (Coord-like object): x-coordinate
        y (Coord-like object): y-coordinate
        z (Coord-like object): z-coordinate

    Attributes:
        _coords (List[Coord]): Full list of all coordinates found in the vector

    Examples:
        >>> Vector3(1, 2.0, -3.0)
        Vector3(1, 2.0, -3.0)

        >>> Vector3('~5', '^6', 2)
        Vector3(~5, ^6, 2)
    """

    def __init__(self, x, y, z):
        super().__init__(3)
        self.x = x
        self.y = y
        self.z = z

    @property
    def x(self):
        return self._coords[0]

    @x.setter
    def x(self, value):
        self._coords[0] = Coord(value)

    @property
    def y(self):
        return self._coords[1]

    @y.setter
    def y(self, value):
        self._coords[1] = Coord(value)

    @property
    def z(self):
        return self._coords[2]

    @z.setter
    def z(self, value):
        self._coords[2] = Coord(value)

    def cross(self, vec3):
        """
        Return the cross product

        Args:
            vec3 (Vector3): A Vector3

        Returns:
            A Vector3 that is the cross product of this vector and vec3
        """
        if not isinstance(vec3, Vector3):
            raise InvalidVectorError(f"A cross product is only applicable to a Vector3 and not {vec3!r}")
        x = self.y * vec3.z - self.z * vec3.y
        y = self.z * vec3.x - self.x * vec3.z
        z = self.x * vec3.y - self.y * vec3.x
        return Vector3(x, y, z)

    def dot(self, vec):
        if not isinstance(vec, Vector3):
            raise InvalidVectorError(f"A dot product is only applicable to a Vector3 and not {vec!r}")
        return self.x * vec.x + self.y * vec.y + self.z * vec.z

    def __matmul__(self, other):
        return NotImplemented


    @property
    def xxx(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.x, self.x, self.x)
        """
        return Vector3(self.x, self.x, self.x)

    @property
    def xxy(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.x, self.x, self.y)
        """
        return Vector3(self.x, self.x, self.y)

    @property
    def xxz(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.x, self.x, self.z)
        """
        return Vector3(self.x, self.x, self.z)

    @property
    def xyx(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.x, self.y, self.x)
        """
        return Vector3(self.x, self.y, self.x)

    @property
    def xyy(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.x, self.y, self.y)
        """
        return Vector3(self.x, self.y, self.y)

    @property
    def xyz(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.x, self.y, self.z)
        """
        return Vector3(self.x, self.y, self.z)

    @property
    def xzx(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.x, self.z, self.x)
        """
        return Vector3(self.x, self.z, self.x)

    @property
    def xzy(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.x, self.z, self.y)
        """
        return Vector3(self.x, self.z, self.y)

    @property
    def xzz(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.x, self.z, self.z)
        """
        return Vector3(self.x, self.z, self.z)

    @property
    def yxx(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.y, self.x, self.x)
        """
        return Vector3(self.y, self.x, self.x)

    @property
    def yxy(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.y, self.x, self.y)
        """
        return Vector3(self.y, self.x, self.y)

    @property
    def yxz(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.y, self.x, self.z)
        """
        return Vector3(self.y, self.x, self.z)

    @property
    def yyx(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.y, self.y, self.x)
        """
        return Vector3(self.y, self.y, self.x)

    @property
    def yyy(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.y, self.y, self.y)
        """
        return Vector3(self.y, self.y, self.y)

    @property
    def yyz(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.y, self.y, self.z)
        """
        return Vector3(self.y, self.y, self.z)

    @property
    def yzx(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.y, self.z, self.x)
        """
        return Vector3(self.y, self.z, self.x)

    @property
    def yzy(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.y, self.z, self.y)
        """
        return Vector3(self.y, self.z, self.y)

    @property
    def yzz(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.y, self.z, self.z)
        """
        return Vector3(self.y, self.z, self.z)

    @property
    def zxx(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.z, self.x, self.x)
        """
        return Vector3(self.z, self.x, self.x)

    @property
    def zxy(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.z, self.x, self.y)
        """
        return Vector3(self.z, self.x, self.y)

    @property
    def zxz(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.z, self.x, self.z)
        """
        return Vector3(self.z, self.x, self.z)

    @property
    def zyx(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.z, self.y, self.x)
        """
        return Vector3(self.z, self.y, self.x)

    @property
    def zyy(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.z, self.y, self.y)
        """
        return Vector3(self.z, self.y, self.y)

    @property
    def zyz(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.z, self.y, self.z)
        """
        return Vector3(self.z, self.y, self.z)

    @property
    def zzx(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.z, self.z, self.x)
        """
        return Vector3(self.z, self.z, self.x)

    @property
    def zzy(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.z, self.z, self.y)
        """
        return Vector3(self.z, self.z, self.y)

    @property
    def zzz(self):
        """
        Swizzle mask

        Returns:
            Vector3(self.z, self.z, self.z)
        """
        return Vector3(self.z, self.z, self.z)


    @property
    def xx(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.x, self.x)
        """
        return Vector2(self.x, self.x)

    @property
    def xy(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.x, self.y)
        """
        return Vector2(self.x, self.y)

    @property
    def xz(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.x, self.z)
        """
        return Vector2(self.x, self.z)

    @property
    def yx(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.y, self.x)
        """
        return Vector2(self.y, self.x)

    @property
    def yy(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.y, self.y)
        """
        return Vector2(self.y, self.y)

    @property
    def yz(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.y, self.z)
        """
        return Vector2(self.y, self.z)

    @property
    def zx(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.z, self.x)
        """
        return Vector2(self.z, self.x)

    @property
    def zy(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.z, self.y)
        """
        return Vector2(self.z, self.y)

    @property
    def zz(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.z, self.z)
        """
        return Vector2(self.z, self.z)


    """
    Arithmetic operations are performed component-wise
    """

    def __add__(self, other):
        if isinstance(other, Vector3):
            x = self.x + other.x
            y = self.y + other.y
            z = self.z + other.z
        else:
            coord = Coord(other)
            x = self.x + coord
            y = self.y + coord
            z = self.z + coord
        return Vector3(x, y, z)

    def __iadd__(self, other):
        if isinstance(other, Vector3):
            self.x += other.x
            self.y += other.y
            self.z += other.z
        else:
            coord = Coord(other)
            self.x += coord
            self.y += coord
            self.z += coord
        return self

    def __sub__(self, other):
        if isinstance(other, Vector3):
            x = self.x - other.x
            y = self.y - other.y
            z = self.z - other.z
        else:
            coord = Coord(other)
            x = self.x - coord
            y = self.y - coord
            z = self.z - coord
        return Vector3(x, y, z)

    def __isub__(self, other):
        if isinstance(other, Vector3):
            self.x -= other.x
            self.y -= other.y
            self.z -= other.z
        else:
            coord = Coord(other)
            self.x -= coord
            self.y -= coord
            self.z -= coord
        return self

    def __mul__(self, other):
        if isinstance(other, Vector3):
            x = self.x * other.x
            y = self.y * other.y
            z = self.z * other.z
        else:
            coord = Coord(other)
            x = self.x * coord
            y = self.y * coord
            z = self.z * coord
        return Vector3(x, y, z)

    def __imul__(self, other):
        if isinstance(other, Vector3):
            self.x *= other.x
            self.y *= other.y
            self.z *= other.z
        else:
            coord = Coord(other)
            self.x *= coord
            self.y *= coord
            self.z *= coord
        return self

    def __truediv__(self, other):
        if isinstance(other, Vector3):
            x = self.x / other.x
            y = self.y / other.y
            z = self.z / other.z
        else:
            coord = Coord(other)
            x = self.x / coord
            y = self.y / coord
            z = self.z / coord
        return Vector3(x, y, z)

    def __itruediv__(self, other):
        if isinstance(other, Vector3):
            self.x /= other.x
            self.y /= other.y
            self.z /= other.z
        else:
            coord = Coord(other)
            self.x /= coord
            self.y /= coord
            self.z /= coord
        return self

    def __floordiv__(self, other):
        if isinstance(other, Vector3):
            x = self.x // other.x
            y = self.y // other.y
            z = self.z // other.z
        else:
            coord = Coord(other)
            x = self.x // coord
            y = self.y // coord
            z = self.z // coord
        return Vector3(x, y, z)

    def __ifloordiv__(self, other):
        if isinstance(other, Vector3):
            self.x //= other.x
            self.y //= other.y
            self.z //= other.z
        else:
            coord = Coord(other)
            self.x //= coord
            self.y //= coord
            self.z //= coord
        return self

    def __mod__(self, other):
        if isinstance(other, Vector3):
            x = self.x % other.x
            y = self.y % other.y
            z = self.z % other.z
        else:
            coord = Coord(other)
            x = self.x % coord
            y = self.y % coord
            z = self.z % coord
        return Vector3(x, y, z)

    def __imod__(self, other):
        if isinstance(other, Vector3):
            self.x %= other.x
            self.y %= other.y
            self.z %= other.z
        else:
            coord = Coord(other)
            self.x %= coord
            self.y %= coord
            self.z %= coord
        return self

    def __pow__(self, other):
        if isinstance(other, Vector3):
            x = self.x ** other.x
            y = self.y ** other.y
            z = self.z ** other.z
        else:
            coord = Coord(other)
            x = self.x ** coord
            y = self.y ** coord
            z = self.z ** coord
        return Vector3(x, y, z)

    def __ipow__(self, other):
        if isinstance(other, Vector3):
            self.x **= other.x
            self.y **= other.y
            self.z **= other.z
        else:
            coord = Coord(other)
            self.x **= coord
            self.y **= coord
            self.z **= coord
        return self

    def __lshift__(self, other):
        if isinstance(other, Vector3):
            x = self.x << other.x
            y = self.y << other.y
            z = self.z << other.z
        else:
            coord = Coord(other)
            x = self.x << coord
            y = self.y << coord
            z = self.z << coord
        return Vector3(x, y, z)

    def __ilshift__(self, other):
        if isinstance(other, Vector3):
            self.x <<= other.x
            self.y <<= other.y
            self.z <<= other.z
        else:
            coord = Coord(other)
            self.x <<= coord
            self.y <<= coord
            self.z <<= coord
        return self

    def __rshift__(self, other):
        if isinstance(other, Vector3):
            x = self.x >> other.x
            y = self.y >> other.y
            z = self.z >> other.z
        else:
            coord = Coord(other)
            x = self.x >> coord
            y = self.y >> coord
            z = self.z >> coord
        return Vector3(x, y, z)

    def __irshift__(self, other):
        if isinstance(other, Vector3):
            self.x >>= other.x
            self.y >>= other.y
            self.z >>= other.z
        else:
            coord = Coord(other)
            self.x >>= coord
            self.y >>= coord
            self.z >>= coord
        return self

    def __and__(self, other):
        if isinstance(other, Vector3):
            x = self.x & other.x
            y = self.y & other.y
            z = self.z & other.z
        else:
            coord = Coord(other)
            x = self.x & coord
            y = self.y & coord
            z = self.z & coord
        return Vector3(x, y, z)

    def __iand__(self, other):
        if isinstance(other, Vector3):
            self.x &= other.x
            self.y &= other.y
            self.z &= other.z
        else:
            coord = Coord(other)
            self.x &= coord
            self.y &= coord
            self.z &= coord
        return self

    def __xor__(self, other):
        if isinstance(other, Vector3):
            x = self.x ^ other.x
            y = self.y ^ other.y
            z = self.z ^ other.z
        else:
            coord = Coord(other)
            x = self.x ^ coord
            y = self.y ^ coord
            z = self.z ^ coord
        return Vector3(x, y, z)

    def __ixor__(self, other):
        if isinstance(other, Vector3):
            self.x ^= other.x
            self.y ^= other.y
            self.z ^= other.z
        else:
            coord = Coord(other)
            self.x ^= coord
            self.y ^= coord
            self.z ^= coord
        return self

    def __or__(self, other):
        if isinstance(other, Vector3):
            x = self.x | other.x
            y = self.y | other.y
            z = self.z | other.z
        else:
            coord = Coord(other)
            x = self.x | coord
            y = self.y | coord
            z = self.z | coord
        return Vector3(x, y, z)

    def __ior__(self, other):
        if isinstance(other, Vector3):
            self.x |= other.x
            self.y |= other.y
            self.z |= other.z
        else:
            coord = Coord(other)
            self.x |= coord
            self.y |= coord
            self.z |= coord
        return self


    def __neg__(self):
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z
        return self
    def __pos__(self):
        self.x = +self.x
        self.y = +self.y
        self.z = +self.z
        return self
    def __invert__(self):
        self.x = ~self.x
        self.y = ~self.y
        self.z = ~self.z
        return self



class Vector2(CoordVector):
    """
    A class for storing and manipulating two coordinates

    Args:
        x (optional): x-coordinate. Has a default value of 0
        y (optional): y-coordinate. If None, will inherit x's value

    Attributes:
        x (Coord): x-coordinate
        y (Coord): y-coordinate

    Examples:
        >>> Vector2(1.0, 2.0)
        Vector2(1.0, 2.0)
    """

    def __init__(self, x, y):
        super().__init__(2)
        self.x = x
        self.y = y

    @property
    def x(self):
        return self._coords[0]

    @x.setter
    def x(self, value):
        self._coords[0] = Coord(value)

    @property
    def y(self):
        return self._coords[1]

    @y.setter
    def y(self, value):
        self._coords[1] = Coord(value)

    def dot(self, vec):
        if not isinstance(vec, Vector2):
            raise InvalidVectorError(f"A dot product is only applicable to a Vector2 and not {vec!r}")
        return self.x * vec.x + self.y * vec.y

    def __matmul__(self, other):
        return NotImplemented

    @property
    def xx(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.x, self.x)
        """
        return Vector2(self.x, self.x)

    @property
    def xy(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.x, self.y)
        """
        return Vector2(self.x, self.y)

    @property
    def yx(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.y, self.x)
        """
        return Vector2(self.y, self.x)

    @property
    def yy(self):
        """
        Swizzle mask

        Returns:
            Vector2(self.y, self.y)
        """
        return Vector2(self.y, self.y)



    """
    Arithmetic operations are performed component-wise
    """

    def __add__(self, other):
        if isinstance(other, Vector2):
            x = self.x + other.x
            y = self.y + other.y
        else:
            coord = Coord(other)
            x = self.x + coord
            y = self.y + coord
        return Vector2(x, y)

    def __iadd__(self, other):
        if isinstance(other, Vector2):
            self.x += other.x
            self.y += other.y
        else:
            coord = Coord(other)
            self.x += coord
            self.y += coord
        return self

    def __sub__(self, other):
        if isinstance(other, Vector2):
            x = self.x - other.x
            y = self.y - other.y
        else:
            coord = Coord(other)
            x = self.x - coord
            y = self.y - coord
        return Vector2(x, y)

    def __isub__(self, other):
        if isinstance(other, Vector2):
            self.x -= other.x
            self.y -= other.y
        else:
            coord = Coord(other)
            self.x -= coord
            self.y -= coord
        return self

    def __mul__(self, other):
        if isinstance(other, Vector2):
            x = self.x * other.x
            y = self.y * other.y
        else:
            coord = Coord(other)
            x = self.x * coord
            y = self.y * coord
        return Vector2(x, y)

    def __imul__(self, other):
        if isinstance(other, Vector2):
            self.x *= other.x
            self.y *= other.y
        else:
            coord = Coord(other)
            self.x *= coord
            self.y *= coord
        return self

    def __truediv__(self, other):
        if isinstance(other, Vector2):
            x = self.x / other.x
            y = self.y / other.y
        else:
            coord = Coord(other)
            x = self.x / coord
            y = self.y / coord
        return Vector2(x, y)

    def __itruediv__(self, other):
        if isinstance(other, Vector2):
            self.x /= other.x
            self.y /= other.y
        else:
            coord = Coord(other)
            self.x /= coord
            self.y /= coord
        return self

    def __floordiv__(self, other):
        if isinstance(other, Vector2):
            x = self.x // other.x
            y = self.y // other.y
        else:
            coord = Coord(other)
            x = self.x // coord
            y = self.y // coord
        return Vector2(x, y)

    def __ifloordiv__(self, other):
        if isinstance(other, Vector2):
            self.x //= other.x
            self.y //= other.y
        else:
            coord = Coord(other)
            self.x //= coord
            self.y //= coord
        return self

    def __mod__(self, other):
        if isinstance(other, Vector2):
            x = self.x % other.x
            y = self.y % other.y
        else:
            coord = Coord(other)
            x = self.x % coord
            y = self.y % coord
        return Vector2(x, y)

    def __imod__(self, other):
        if isinstance(other, Vector2):
            self.x %= other.x
            self.y %= other.y
        else:
            coord = Coord(other)
            self.x %= coord
            self.y %= coord
        return self

    def __pow__(self, other):
        if isinstance(other, Vector2):
            x = self.x ** other.x
            y = self.y ** other.y
        else:
            coord = Coord(other)
            x = self.x ** coord
            y = self.y ** coord
        return Vector2(x, y)

    def __ipow__(self, other):
        if isinstance(other, Vector2):
            self.x **= other.x
            self.y **= other.y
        else:
            coord = Coord(other)
            self.x **= coord
            self.y **= coord
        return self

    def __lshift__(self, other):
        if isinstance(other, Vector2):
            x = self.x << other.x
            y = self.y << other.y
        else:
            coord = Coord(other)
            x = self.x << coord
            y = self.y << coord
        return Vector2(x, y)

    def __ilshift__(self, other):
        if isinstance(other, Vector2):
            self.x <<= other.x
            self.y <<= other.y
        else:
            coord = Coord(other)
            self.x <<= coord
            self.y <<= coord
        return self

    def __rshift__(self, other):
        if isinstance(other, Vector2):
            x = self.x >> other.x
            y = self.y >> other.y
        else:
            coord = Coord(other)
            x = self.x >> coord
            y = self.y >> coord
        return Vector2(x, y)

    def __irshift__(self, other):
        if isinstance(other, Vector2):
            self.x >>= other.x
            self.y >>= other.y
        else:
            coord = Coord(other)
            self.x >>= coord
            self.y >>= coord
        return self

    def __and__(self, other):
        if isinstance(other, Vector2):
            x = self.x & other.x
            y = self.y & other.y
        else:
            coord = Coord(other)
            x = self.x & coord
            y = self.y & coord
        return Vector2(x, y)

    def __iand__(self, other):
        if isinstance(other, Vector2):
            self.x &= other.x
            self.y &= other.y
        else:
            coord = Coord(other)
            self.x &= coord
            self.y &= coord
        return self

    def __xor__(self, other):
        if isinstance(other, Vector2):
            x = self.x ^ other.x
            y = self.y ^ other.y
        else:
            coord = Coord(other)
            x = self.x ^ coord
            y = self.y ^ coord
        return Vector2(x, y)

    def __ixor__(self, other):
        if isinstance(other, Vector2):
            self.x ^= other.x
            self.y ^= other.y
        else:
            coord = Coord(other)
            self.x ^= coord
            self.y ^= coord
        return self

    def __or__(self, other):
        if isinstance(other, Vector2):
            x = self.x | other.x
            y = self.y | other.y
        else:
            coord = Coord(other)
            x = self.x | coord
            y = self.y | coord
        return Vector2(x, y)

    def __ior__(self, other):
        if isinstance(other, Vector2):
            self.x |= other.x
            self.y |= other.y
        else:
            coord = Coord(other)
            self.x |= coord
            self.y |= coord
        return self


    def __neg__(self):
        self.x = -self.x
        self.y = -self.y
        return self
    def __pos__(self):
        self.x = +self.x
        self.y = +self.y
        return self
    def __invert__(self):
        self.x = ~self.x
        self.y = ~self.y
        return self

    def rotate(self, angle, point_vec=None, radians=False, round_to=10):
        """
        Performs a clockwise rotation of this vector about the given point_vec with the given angle

        Args:
            angle (numbers.Real, str, Coord): the angle to rotate by
            point_vec (Vector2, optional): vector/point to rotate about
            radians (bool): if False, angle will be converted to degrees for internal calculations
            round_to (int): Whether the result should have its decimals rounded or not
                This defaults to 10.

        Examples:
            >>> vec2 = Vector2(1, 0)
            >>> vec2.rotate(math.pi/2, radians=True)
            >>> vec2
            Vector2(0.0, -1.0)

            >>> vec3 = Vector3('^1', '^0', '^0')
            >>> xz = vec3.xz
            >>> xz.rotate(90, radians=False)
            >>> xz.to_int()
            >>> vec3 = Vector3(xz[0], vec3.y, xz[1])
            >>> vec3
            Vector3(^, ^, ^-1)

            >>> vec2 = Vector2(2, 1)
            >>> vec2.rotate(90, Vector2(1, 1))
            >>> vec2
            Vector2(1.0, 0.0)
        """
        if not radians:
            angle = math.radians(angle)
        angle = Coord(angle).value

        if point_vec is None:
            point_vec = Vector2(0, 0)
        if not isinstance(point_vec, Vector2):
            raise InvalidVectorError(f"Expected 'point_vec' to be a Vector2 but got {point_vec!r}")

        # Create a copy of self to prevent overwritting self values
        result = self.copy()

        # Make point_vec the new origin
        result -= point_vec

        # Perform clockwise rotation about the origin
        new_x_value = result.x.value * math.cos(angle) + result.y.value * math.sin(angle)
        new_y_value = result.x.value * -math.sin(angle) + result.y.value * math.cos(angle)

        # Rounds said x value
        if not isinstance(round_to, int):
            raise VectorError(f"Expected 'round_to' to be an int but got {round_to!r}")
        new_x_value = round(new_x_value, round_to)
        new_y_value = round(new_y_value, round_to)

        # Assign the new rotated coordinates to the result vector
        self.x = Coord.from_value_prefix(new_x_value, self.x.prefix)
        self.y = Coord.from_value_prefix(new_y_value, self.y.prefix)
        self += point_vec


if __name__ == "__main__":
    import doctest
    doctest.testmod()


