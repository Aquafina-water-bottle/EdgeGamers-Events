execute @a[m=2,score_ir.pl_min=1,score_ir.pl=1] ~ ~ ~ function ego:race/ice_race/reset_player
scoreboard players reset * g.host
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=2065128021,score_gp.cgi=2065128021,tag=gp.stand] gp.tp 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=2065128021,score_gp.cgi=2065128021,tag=gp.stand] gp.cgi 0
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"IR","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Ice Race","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2065128021"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Ice Race","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Ice Race","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2065128021"}},{"text":" has stopped!","color":"red"}]}
scoreboard objectives remove ir.
scoreboard objectives remove ir.cd
scoreboard objectives remove ir.pl
scoreboard objectives remove ir.pc
scoreboard teams remove ir.r
kill @e[tag=ir.entity]
fill 42 33 -31 42 33 -32 air
setblock 42 33 -28 air
setblock 42 33 -35 air
fill 66 34 -42 66 34 -48 redstone_block 0 replace stonebrick 0
