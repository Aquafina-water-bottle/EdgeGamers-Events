scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 2065128021
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=34,y=2,z=-64,dx=141,dy=69,dz=43] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"IR","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Ice Race","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2065128021"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Ice Race","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Ice Race","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 2065128021"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 2065128021
scoreboard players set -1 g.number -1
scoreboard objectives add ir. dummy Ice Race
scoreboard objectives setdisplay sidebar ir.
scoreboard objectives add ir.cd dummy Ice Race Countdown Timer
scoreboard objectives add ir.pl dummy Ice Race Player List
scoreboard objectives add ir.pc dummy Ice Race Place
scoreboard teams add ir.r Ice Race
scoreboard teams option ir.r color aqua
scoreboard teams option ir.r seeFriendlyInvisibles true
scoreboard teams option ir.r collisionRule never
scoreboard teams option ir.r friendlyfire false
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["ir.stand","ir.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players add @e[type=minecraft:armor_stand,tag=ir.stand] ir.cd 0
