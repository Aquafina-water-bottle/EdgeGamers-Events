MAX_INT = (1<<31)-1


def djb2(string):
    hash_int = 5381

    for char in string:
        char = ord(char)
        hash_int = ((hash_int << 5) + hash_int) + char

    return hash_int % MAX_INT


if __name__ == "__main__":
    def test(string):
        print(djb2(string))
    test("asdf")
    test("123456")
    test("123457")
    test("223456")
    test("{'test': 'white', 'testing': 'green'}")

