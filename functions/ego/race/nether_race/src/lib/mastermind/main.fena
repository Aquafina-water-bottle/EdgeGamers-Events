$py(r"""
Copy/pasta the following:

$py(
from lib.floo_network import MASTERMIND_NAME, FlooEvent
from lib.const_ints import CONST_INTS
from lib.scoreboard import OBJECTIVES, TEAMS
from lib.coords import Coords
from lib.vector import Vector3
from lib.consts import *


# activating the structure blocks to save the rooms
activate_struct_save = Coords("107 4 82 96 4 82")

# activates the structure blocks underneath the map to reset the rooms
activate_struct_reset = Coords("45 2 60 109 2 23")

# region to fill the buttons
north_buttons_region = Coords("46 6 26 101 6 34")
south_buttons_region = Coords("53 6 57 108 6 49")

# size of the room from the 
room_size_north = Vector3(7, 5, 12)
room_size_south = Vector3(-7, 5, -12)


# region to fill the doors with bedrock
north_door_region = Coords("49 8 37 105 6 37")
south_door_region = Coords("49 8 46 105 6 46")


dist_to_ans_north = relative(-5, 5, -12)
dist_to_ans_south = relative(5, 5, 12)

# room closest to the west
north_room = Coords("53 4 36")
south_room = Coords("46 4 47")

select_hallway = Coords("46 5 38 108 10 45").selector()

# difference of blocks between each room (+ 1)
room_difference = 11


# how many blocks are between each guess (+ 1)
guess_difference = 2

# how many blocks are between the guessing bar and the stone button (+ 1)
guess_button_difference = 2


# total number of rooms
# iterated with "num"
total_rooms = 12

# number of guesses in total
# iterated with "guess"
total_guesses = 5

# number of blocks (slots for blocks) per guess
# iterated with "guess_block_num"
blocks_per_guess = 4


# block type that we are supposed to place our guess on
place_on = "minecraft:" + "glass"

# each individual block that is avaliable for detecting the solid block
# all blocks will have "minecraft:" added automatically to the beginning
# this list will be used for setting the breakable blocks for the breaking tool
# iterated with "block"
blocks = ["minecraft:" + block for block in ("wool color=red", "wool color=lime", "wool color=cyan", "wool color=magenta", "wool 6", "wool 1")]

# each individual block as their item id
# all items will have "minecraft:" added automatically to the beginning
blocks_item = ["minecraft:" + block_item for block_item in ("wool 14", "wool 5", "wool 9", "wool 2", "wool 6", "wool 1")]

# item id of the special tool to break their block
# all will have unbreaking and efficiency, using
breaking_tool = shears


# formatted with (team_name used as the display name, true text color)
# the team name will automatically add the initials to the beginning
# iterated through "team"
block_team_colors = [("red", "red"), ("lime", "green"), ("cyan", "dark_aqua"), ("magenta", "dark_purple"), ("pink", "light_purple"), ("orange", "gold")]
)


$include("lib/mastermind_macros.ccu")

# TODO macros

$include("lib/mastermind.ccu")
""")


$py(
event = MASTERMIND_NAME
floo_event = FlooEvent(MASTERMIND_NAME)
initials = event.initials
select_all = event.select_all


# total number of possible choices of blocks
# iterated with "block_num"
blocks_total = len(blocks)


def relative(*coords):
    return Coords(*map(lambda x: "~" + str(x), coords))

def rotate_180(coords):
    # you literally can't use round since that returns a float, and I need an int
    # coords.vec.xz.rotate(180, radians=False)
    # coords.vec = round(coords.vec)

    coords.vec.xz *= -1

    # this is in case the glitch somehow appears again
    # coords.vec.xz = coords.vec.xz * -1


class Room:
    def __init__(self, number, select_coords):
        self.number = number
        self.select_coords = select_coords
        self.corner = select_coords.pos
        self.select = select_coords.selector()

    def __str__(self):
        return "Room({0}: corner='{1}', selection='{2}')".format(self.number, self.corner, self.select)


# rooms is a dictionary mapping the room number with the room object
# iterated with "room"
rooms = {}

for num in range(1, (total_rooms // 2) + 1):
    position = (north_room.vec + Coords((num - 1) * room_difference, 0, 0).vec)
    selection = Coords(*position, *(position + room_size_south))
    room = Room(num, selection)
    rooms[num] = room

for num in range((total_rooms // 2) + 1, total_rooms + 1):
    position = south_room.vec + Coords(((num - 1) - total_rooms // 2) * room_difference, 0, 0).vec
    selection = Coords(*position, *(position + room_size_north))
    room = Room(num, selection)
    rooms[num] = room


OBJECTIVES.new_str("""
    . _ .

    # Used on players to store their player state
    # Used on the Room Stand to calculate what to display on the EC board
    pl _ Player List

    # Used on items to change their entity data so they are proper placable wool
    # Used on players to temporarily set themselves to be used for validating the guess
    # Used on the Room Stand = 1 to temporarily see if their guess is in the proper order
    # Also used on the Room Stand = 2 to show that a guess has been successfully validated
    # Used on the White and Red clouds to count how many white/red there are in a guess
    # Used on the Repeating clouds to count the number of unique blocks in their guess
    cl _ Calculations
    st _ State

    # used on the host to see what is going on
    ec _ EC scoreboard

    # used on players and on the Room Stands
    # to specify what room id they are in
    rn _ Room Number

    # used on the Room Stand to store what guess is expected
    gn _ Guess Number

    # used on the Room Stand to store what guess has
    # been inputted by the player
    ign _ Guess Number Input

    # used on Guess clouds to store what block id was guessed at that area
    bi _ Block ID

    # Used on the Guess clouds and the Correct stands to
    # store what block id is correct at their position
    # Used on the Room Stand to show that the correct answer has been reached
    ca _ Correct Answer

    # checks for a repeated answer or no answer at all
    ra _ Repeated Answer
    na _ No Answer

    # rs _ Room Select
    # pa _ Predicted Answer
    # sd _ Sign Display
    # gc _ Guess Check
    # gl _ Guess Calc
    # it _ Item
    # op _ Options
    # rg _ Right Guess
    
""", initials=initials, display=event.full_name)

OBJECTIVES[initials].setdisplay("sidebar")
OBJECTIVES[initials + "ec"].setdisplay("sidebar.team.white")

CONST_INTS.add_consts(-1)

for team in block_team_colors:
    team_name, color = block_team_colors
    team_name = initials + team_name
    TEAMS.new(team_name, display="{} {}".format(event.full_name, color), color=color)

# host team
TEAMS.new(initials + "host", display=event.full_name + " Host, color="white")
)




# used on a player to initialize the game and set themselves as the host
!mfunc init
    $(floo_event.cmd_init())
    $(OBJECTIVES.cmd_init())
    $(TEAMS.cmd_init())

    # sets the player as the host
    @s HOST = 0

    # set tiledrops to true so wool drops when you break it
    gamerule doTileDrops true

    # activates the save structure blocks
    fill $(activate_struct_save) redstone_block 0 replace stonebrick 0
    fill $(activate_struct_save) stonebrick 0 replace redstone_block 0
    
    summon armor_stand ~ ~ ~ {Tags:["$(initials)Entity","$(initials)Stand"],Invulnerable:1,NoGravity:1,Invisible:1,Marker:1b}
    $(floo_event.cmd_post_init())

    $for(room in rooms.values())
    summon armor_stand $(room.corner) {Tags:["$(initials)Entity","$(initials)Room","$(initials)Room$(room.number)","$(initials)InRoom$(room.number)"],
        Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}

    $for(guess_block_num in range(1, blocks_per_guess+1))
    summon armor_stand $(room.corner) {Tags:["$(initials)Entity","$(initials)Correct","$(initials)Correct$(guess_block_num)","$(initials)InRoom$(room.number)"],
        Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
    $endfor
    @e[type=armor_stand,$(initials)InRoom$(room.number)] $(initials)rn = $(room.number)
    $endfor

    # @e[type=armor_stand,$(initials)Correct] function teleport_correct
    function teleport_correct
    function randomize
    

!mfunc main
    $(floo_event.cmd_main())
    
    # initializes the player
    @a[gSA=1] $(initials)pl + 0
    @a[gSA=1,$(initials)pl=0] function reset_player

    # @e[type=armor_stand,$(initials)Stand,$(initials)st=0] function wait_for_start
    @e[type=armor_stand,$(initials)Stand,$(initials)st=1] function start_round
    @e[type=armor_stand,$(initials)Stand,$(initials)st=2] function during_round
    @e[type=armor_stand,$(initials)Stand,$(initials)st=3] function stop_round

    # checks the guess if an input is found
    @e[type=armor_stand,$(initials)Room,$(initials)ign=1..] function check_guess
    
    @a[m=2,$(initials)pl=2,gDE=1..] function reset_player

    @e[$(select_all),type=item] $(initials)cl + 0 {Item:{id:"minecraft:wool"}}
    @e[$(select_all),type=item,$(initials)cl=0] function change_wool

    # sets the display for the player list
    @a[$(select_hallway),m=1] $(initials)rn = 0
    @a[$(select_hallway),m=2] $(initials)rn = 0
    @a[gSA=1,m=3] $(initials)rn = 0

    $for(room in rooms.values())
    @a[$(room.select),m=2] $(initials)rn = $(room.number)
    @a[$(room.select),m=1] $(initials)rn = $(room.number)
    $endfor

    # displays the correct answer for the host
    @a[HOST=0] function display_answer_actionbar

    # joins the correct team for the host and players
    @a gTEMP = 0
    @a[HOST=0] gTEMP = 1
    join NoPVP @a[gTEMP=0]
    join $(initials)white @a[gTEMP=1]
    
    @a[$(initials)rn=1..] ScOP @s $(initials) = @s $(initials)rn
    @a[$(initials)rn=0] reset $(initials)


!mfunc term
    $(floo_event.cmd_term())

    # all players are removed from hosting mastermind
    * reset HOST

    gamerule doTileDrops false

    function clear_lanes
    function open_doors
    @a[m=2,$(initials)pl=1..] function full_reset_player

    $(OBJECTIVES.cmd_term())
    $(TEAMS.cmd_term())
    
    kill @e[$(initials)Entity]


# Used on the $(initials)Stand when the round is starting
!mfunc start_round
    function close_doors
    function randomize
    function set_buttons

    # sets the guess number as 1
    @e[type=armor_stand,$(initials)Room] $(initials)gn = 1

    # resets whether the correct answer has been gotten or not
    @e[type=armor_stand,$(initials)Room] $(initials)ca = 0
    @e[type=armor_stand,$(initials)Room] reset $(initials)pl

    # resets their inventories for next round
    @a[gSA=1,$(initials)pl=2] $(initials)pl = 1


    @s $(initials)st = 2


!mfunc during_round
    @a[gSA=1,$(initials)pl=1,$(initials)rn=1..,m=1] function add_to_round
    @a[gSA=1,$(initials)pl=1,$(initials)rn=1..,m=2] function add_to_round

!mfunc stop_round
    function clear_lanes
    @s $(initials)st = 0


# creates a bunch of input_guess mcfunctions
$for(num in range(1, total_rooms + 1))
$for(guess in range(1, total_guesses + 1))
!mfunc input_guess_$(num)_$(guess)
    @e[type=armor_stand,$(initials)Room$(num)] $(initials)ign = $(guess)
$endfor
$endfor


# used on the Room Stand to determine what guess number is being ran
!mfunc check_guess
    $for(num in range(1, total_rooms + 1))
    @s[$(initials)Room$(num)] function check_guess_$(num)
    $endfor
    @s $(initials)ign = 0


# used on the Room Stand to check whether they guessed in the correct order
$for(num in range(1, total_rooms + 1))
!mfunc check_guess_$(num)
    # say @s check_guess_$(num)
    # sets their calc score to 0
    @s $(initials)cl = 0

    # sets the calc store to 1 if the inputted guess number ($(initials)ign)
    # matches the expected guess number ($(initials)gn)
    $for(guess in range(1, total_guesses + 1))
    @s[$(initials)ign=$(guess),$(initials)gn=$(guess)] $(initials)cl = 1
    $endfor
    
    # tellraw message to display the expected guess number ($(initials)gn)
    $for(guess in range(1, total_guesses + 1))
    @s[$(initials)cl=0,$(initials)gn=$(guess)] tellraw @a[$(initials)rn=$(num)] $(event.begin())
        {"text":"Warning: ","color":"red","bold":"true"},
        {"text":"Please use guess $(guess).","color":"gold"}]}
    $endfor

    @s[$(initials)cl=1] function select_player_for_guess_$(num)

    # -1 is guess on first try
    # -2 is guess on second try
    # -3 is guess on third try
    # -4 is guess on fourth try
    # -5 is guess on fifth try
    
    # -100 normal
    # -102 is when first guess is successfully done
    # -103 is when second guess is successfully done
    # -104 is when third guess is successfully done
    # -105 is when fourth guess is successfully done
    # -106 is when fifth guess is successfully done
    
    # guess number is normally positive
    ScOP @s[$(initials)cl=2] $(initials)pl = @s $(initials)gn

    # makes it negative
    ScOP @s[$(initials)cl=2] $(initials)pl *= -1 constants

    # subtracts 100 if the guess has been validated but not a winning guess
    @s[$(initials)cl=2,$(initials)ca=0] $(initials)pl - 100
    
    # displays it on the EC board
    ScOP @a[$(initials)rn=$(num)] $(initials)ec = @s $(initials)pl

    # Increases the guess number by one
    @s[$(initials)cl=2] $(initials)gn + 1

    # sets the calc back to 0
    @s $(initials)cl = 0


# used on the Room Stand to select the player to be used for validating the guess
!mfunc select_player_for_guess_$(num)
    # say @s select_player_for_guess_$(num)

    # summons an area effect cloud on the correct button
    $for(guess in range(1, total_guesses + 1))

    $py(summon_coords = relative(7, 2, ((total_guesses-guess)*guess_difference) + 2))
    $if(num <= (total_rooms//2))
    # rotate around origin
    $py(rotate_180(summon_coords))
    $endif

    @s[$(initials)gn=$(guess)] summon area_effect_cloud $(summon_coords) {Tags:["$(initials)Entity","$(initials)GuessTP$(num)"],Duration:5}
    $endfor

    @e[$(initials)GuessTP$(num)] @p[r=5,$(initials)rn=$(num)] $(initials)cl = 1
    tp @a[$(initials)rn=$(num),$(initials)cl=1] @e[$(initials)GuessTP$(num)]

    $if(num <= (total_rooms//2))
    tp @a[$(initials)rn=$(num),$(initials)cl=1] ~ ~ ~ ~180 ~
    $endif

    @a[$(initials)rn=$(num),$(initials)cl=1] function validate_guess_$(num)

    kill @e[$(initials)GuessTP$(num)]


# used on a player to validate the guess they are standing on
!mfunc validate_guess_$(num)
    # say @s validate_guess_$(num)

    # summons area effect clouds on top of the guess
    # for 0 to 3, summons area effect clouds over the blocks of the guess
    $for(guess_block_num in range(blocks_per_guess))

    $py(summon_coords = relative(-(guess_button_difference + guess_block_num), 0, 0))
    $if(num <= (total_rooms//2))
    # rotate around origin
    $py(rotate_180(summon_coords))
    $endif

    summon area_effect_cloud $(summon_coords) {Tags:["$(initials)Entity","$(initials)Guess","$(initials)Guess$(guess_block_num+1)"],Duration:5}
    $endfor

    # summons one for each block possibility to check for repeating answers
    $for(block_num in range(1, blocks_total+1))
    summon area_effect_cloud ~ ~ ~ {Tags:["$(initials)Entity","$(initials)Repeat","$(initials)Repeat$(block_num)"],Duration:5}
    $endfor

    # summons this right on the player
    summon area_effect_cloud ~ ~ ~ {Tags:["$(initials)Entity","$(initials)Display","$(initials)White"],Duration:5}
    summon area_effect_cloud ~ ~ ~ {Tags:["$(initials)Entity","$(initials)Display","$(initials)Red"],Duration:5}

    # sets the score of $(initials)rn for each temporary area effect cloud
    @e[$(rooms[num].select),type=area_effect_cloud] $(initials)rn = $(num)

    # the block id defaults to 0 if there was no correct block detected
    @e[$(initials)rn=$(num),$(initials)Guess] $(initials)bi + 0

    # sets the block id for the Guess clouds corresponding to the block
    # their block id is the index+1 of the block list (1-6)
    $for(index, block in enumerate(blocks))
    @e[$(initials)rn=$(num),$(initials)Guess] ifblock ~ ~-1 ~ $(block) @s $(initials)bi = $(index+1)
    $endfor

    # detects any repeating blocks by adding to a Repeating cloud with
    # the tag Repeat(id), with each guess, and seeing if there are 2 or more of any
    $for(block_num in range(1, blocks_total+1))
    @e[$(initials)rn=$(num),$(initials)Guess,$(initials)bi=$(block_num)] @e[$(initials)rn=$(num),$(initials)Repeat$(block_num)] $(initials)cl + 1
    $endfor

    @s $(initials)ra + 0
    @e[$(initials)rn=$(num),$(initials)Repeat,$(initials)cl=2..] @a[$(initials)rn=$(num),$(initials)cl=1] $(initials)ra = 1
    
    # Detects any non-wool blocks
    @s $(initials)na + 0
    @e[$(initials)Guess,$(initials)bi=0] @a[$(initials)rn=$(num),$(initials)cl=1] $(initials)na = 1

    # Shows the warning
    @s[$(initials)ra=1] tellraw @a[$(initials)rn=$(num)] $(event.begin())
        {"text":"Warning: ","color":"red","bold":"true"},
        {"text":"There are repeated colors. Please redo your guess.","color":"gold"}]}

    @s[$(initials)na=1] tellraw @a[$(initials)rn=$(num)] $(event.begin())
        {"text":"Warning: ","color":"red","bold":"true"},
        {"text":"There is at least one missing block. Please redo your guess.","color":"gold"}]}

    # only displays the number of red and white if there
    # are no repeated colors or missing blocks
    @s[$(initials)ra=0,$(initials)na=0] function display_guess_$(num)

    kill @e[type=area_effect_cloud,$(initials)rn=$(num)]
    @s reset $(initials)ra
    @s reset $(initials)na

    @s $(initials)cl = 0
    

# used on the player to display the guess
!mfunc display_guess_$(num)
    # say @s display_guess_$(num)

    # Sets the correct answer to the Guess clouds to match the armor stand
    $for(guess_block_num in range(1, blocks_per_guess+1))
    ScOP @e[$(initials)rn=$(num),$(initials)Guess$(guess_block_num)] $(initials)ca = @e[$(initials)rn=$(num),$(initials)Correct$(guess_block_num)] $(initials)ca
    $endfor
    
    # Counts the number of white and red
    $for(block_num in range(1, blocks_total+1))

    # counts the number of white by counting the clouds
    # around but not including the correct answer
    @e[$(initials)rn=$(num),$(initials)Guess,$(initials)ca=$(block_num)] @e[$(initials)rn=$(num),$(initials)Guess,dist=1..$(blocks_per_guess-1),$(initials)bi=$(block_num)] @e[$(initials)rn=$(num),$(initials)White] $(initials)cl + 1

    # counts the number of red by counting the number of clouds
    # that have the same block id and correct answer id
    @e[$(initials)rn=$(num),$(initials)Guess,$(initials)ca=$(block_num),$(initials)bi=$(block_num)] @e[$(initials)rn=$(num),$(initials)Red] $(initials)cl + 1
    $endfor
    
    # Adds everything to 0 (to display 0 red/white, and to detect if there isn't a wool block placed)
    @e[$(initials)rn=$(num),$(initials)Red] $(initials)cl + 0
    @e[$(initials)rn=$(num),$(initials)White] $(initials)cl + 0
    
    # sets the sign if the guess is filled and has no repeating blocks
    # sets a sign with both red and white

    $if(num <= (total_rooms//2))
    $py(set_sign = "setblock ~7 ~ ~ standing_sign 2")
    $py(set_air = "setblock ~7 ~ ~ air 0")
    $else
    $py(set_sign = "setblock ~-7 ~ ~ standing_sign 10")
    $py(set_air = "setblock ~-7 ~ ~ air 0")
    $endif

    $(set_sign) replace {
        Text1:"{\"text\":\"-=-\"}",
        Text2:"{\"text\":\"\",\"extra\":[
            {\"score\":{\"name\":\"@e[type=area_effect_cloud,tag=$(initials)Red]\",\"objective\":\"$(initials)cl\"}},{\"text\":\" red\"}]}",
        Text3:"{\"text\":\"\",\"extra\":[
            {\"score\":{\"name\":\"@e[type=area_effect_cloud,tag=$(initials)White]\",\"objective\":\"$(initials)cl\"}},{\"text\":\" white\"}]}",
        Text4:"{\"text\":\"-=-\"}"}

    # sets a winning red sign
    @e[$(initials)rn=$(num),$(initials)Red,$(initials)cl=$(blocks_per_guess)] $(set_air)
    @e[$(initials)rn=$(num),$(initials)Red,$(initials)cl=$(blocks_per_guess)] $(set_sign) replace {
        Text1:"{\"text\":\"--=--\"}",
        Text2:"{\"text\":\"$(blocks_per_guess) red\",\"bold\":\"true\"}",
        Text3:"{\"text\":\"Shhhh\",\"bold\":\"true\"}",
        Text4:"{\"text\":\"--=--\"}"}

    # sets the sign will only show if you have red blocks and no white blocks
    @e[$(initials)rn=$(num),$(initials)White,$(initials)cl=0] @e[$(initials)rn=$(num),$(initials)Red,$(initials)cl=1..$(blocks_per_guess-1)] $(set_air)
    @e[$(initials)rn=$(num),$(initials)White,$(initials)cl=0] @e[$(initials)rn=$(num),$(initials)Red,$(initials)cl=1..$(blocks_per_guess-1)] $(set_sign) replace {
        Text1:"{\"text\":\"-=-\"}",
        Text2:"{\"text\":\"\",\"extra\":[{\"score\":{\"name\":\"@e[type=area_effect_cloud,tag=$(initials)Red]\",\"objective\":\"$(initials)cl\"}},{\"text\":\" red\"}]}",
        Text3:"{\"text\":\"-=-\"}",
        Text4:"{\"text\":\"\"}"}

    # sets the sign will only show if you have white blocks and no red blocks
    @e[$(initials)rn=$(num),$(initials)White,$(initials)cl=1..$(blocks_per_guess)] @e[$(initials)rn=$(num),$(initials)Red,$(initials)cl=0] $(set_air)
    @e[$(initials)rn=$(num),$(initials)White,$(initials)cl=1..$(blocks_per_guess)] @e[$(initials)rn=$(num),$(initials)Red,$(initials)cl=0] $(set_sign) replace {
        Text1:"{\"text\":\"-=-\"}",
        Text2:"{\"text\":\"\",\"extra\":[{\"score\":{\"name\":\"@e[type=area_effect_cloud,tag=$(initials)White]\",\"objective\":\"$(initials)cl\"}},{\"text\":\" white\"}]}",
        Text3:"{\"text\":\"-=-\"}",
        Text4:"{\"text\":\"\"}"}
    
    setblock ~ ~ ~ tripwire 0 replace stone_button
    
    # sets the room stand to $(initials)cl=2 when a guess has been successfully validated
    @e[type=armor_stand,$(initials)Room$(num),$(initials)ca=0,$(initials)cl=1] $(initials)cl = 2

    # sets the room stand to $(initials)ca=1 if an answer has been found
    @e[$(initials)rn=$(num),$(initials)Red,$(initials)cl=$(blocks_per_guess)] @e[type=armor_stand,$(initials)Room$(num),$(initials)ca=0] $(initials)ca = 1

    @e[$(initials)rn=$(num),$(initials)Red,$(initials)cl=$(blocks_per_guess)] fill ~ ~4 ~ ~ ~6 ~ sea_lantern 0 replace redstone_lamp

    $if(num <= (total_rooms//2))
    clone ~2 ~-1 ~ ~5 ~-1 ~ ~2 ~-3 ~
    $else
    clone ~-2 ~-1 ~ ~-5 ~-1 ~ ~-5 ~-3 ~
    $endif

    setblock ~ ~5 ~ redstone_block 0

$endfor



# used on arbitrary to teleport the Correct stands
# to the correct places
!mfunc teleport_correct
    # teleports all to one corner of the guess display
    tp @e[type=armor_stand,$(initials)Correct,$(initials)rn=1..$(total_rooms // 2)] $(dist_to_ans_north)
    tp @e[type=armor_stand,$(initials)Correct,$(initials)rn=$(total_rooms//2 + 1)..$(total_rooms)] $(dist_to_ans_south)
    
    # teleports relative so each armor stand can be alligned
    # to the correct slot in the guess display
    $for(guess_block_num in range(blocks_per_guess))
    tp @e[type=armor_stand,$(initials)Correct,$(initials)Correct$(guess_block_num+1),$(initials)rn=1..$(total_rooms // 2)] ~$(guess_block_num) ~ ~
    $endfor

    $for(guess_block_num in range(blocks_per_guess))
    tp @e[type=armor_stand,$(initials)Correct,$(initials)Correct$(guess_block_num+1),$(initials)rn=$(total_rooms//2 + 1)..$(total_rooms)] ~$(-guess_block_num) ~ ~
    $endfor


# used on arbitrary to randomize all rooms
!mfunc randomize
    $for(num in range(1, total_rooms + 1))
    @e[type=armor_stand,$(initials)Room$(num)] function randomize_$(num)
    $endfor

    # sets their names and team color equal to the correct answer
    function update_colors


# used on the Room Stands to randomize their specific room
$for(num in range(1, total_rooms + 1))
!mfunc randomize_$(num)

    # summons 6 area effect clouds to be chosen from to randomize each room
    $for(block_num in range(1, blocks_total+1))
    summon area_effect_cloud ~ ~ ~ {Duration:5,Tags:["$(initials)Random","$(initials)Random$(block_num)"]}
    $endfor

    # sets their scores depending on their tag
    # so $(initials)Random2 will have a $(initials)ca score of 2
    $for(block_num in range(1, blocks_total+1))
    @e[type=area_effect_cloud,$(initials)Random$(block_num)] $(initials)ca = $(block_num)
    $endfor
    
    # goes through all guess block numbers other than the last one (1, 2, 3)
    # and randomly chooses an area effect cloud using $(initials)cl=0
    $for(guess_block_num in range(1, blocks_per_guess))
    @r[type=area_effect_cloud,$(initials)Random] $(initials)cl + 0
    ScOP @e[$(initials)rn=$(num),type=armor_stand,$(initials)Correct$(guess_block_num)] $(initials)ca = @e[type=area_effect_cloud,$(initials)Random,$(initials)cl=0] $(initials)ca
    kill @e[type=area_effect_cloud,$(initials)cl=0]
    $endfor

    # uses the last number as the total blocks per guess (4) and
    # kills off any remaining area effect clouds
    @r[type=area_effect_cloud,$(initials)Random] $(initials)cl + 0
    ScOP @e[$(initials)rn=$(num),type=armor_stand,$(initials)Correct$(blocks_per_guess)] $(initials)ca = @e[type=area_effect_cloud,$(initials)Random,$(initials)cl=0] $(initials)ca
    kill @e[type=area_effect_cloud,$(initials)Random]

$endfor

# used on arbitrary to update their names and team colors
!mfunc update_colors
    $for(block_num in range(1, blocks_total+1))
    $py(color, team = blocks_disp[block_num-1])

    entitydata @e[type=armor_stand,$(initials)Correct,$(initials)ca=$(block_num)] {CustomName:"$(color)",Team:"$(team)"}
    $endfor


!mfunc reset_player
    clear @s
    $(event.cmd_book("@s[EC=0]"))

    @s $(initials)ec = -100

    effect @s clear
    effect @s $(Effects.hp) 1 100 true
    xp -1000L @s

    @s $(initials)pl = 1


# Used on players to reset them fully
!mfunc full_reset_player
    function reset_player
    $(event.cmd_spawn())


# used on players to add them to the round
!mfunc add_to_round
    function reset_player

    # gives 64 wool to gamemode 2, but 1 wool to gamemode 1
    replaceitem entity @s[m=2] slot.hotbar.0 wool 64 1 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=2] slot.hotbar.1 wool 64 2 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=2] slot.hotbar.2 wool 64 6 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=2] slot.hotbar.3 wool 64 9 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=2] slot.hotbar.4 wool 64 5 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=2] slot.hotbar.5 wool 64 14 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}

    replaceitem entity @s[m=1] slot.hotbar.0 wool 1 1 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=1] slot.hotbar.1 wool 1 2 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=1] slot.hotbar.2 wool 1 6 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=1] slot.hotbar.3 wool 1 9 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=1] slot.hotbar.4 wool 1 5 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}
    replaceitem entity @s[m=1] slot.hotbar.5 wool 1 14 {CanPlaceOn:["$(place_on)"],display:{Lore:["Mastermind Wool"]},HideFlags:127}

    replaceitem entity @s[m=2] slot.hotbar.6 shears 1 0 {
        CanDestroy:["minecraft:wool"],Unbreakable:1,ench:[{id:32,lvl:9001}],display:{Lore:["Mastermind Shears"]}}
    replaceitem entity @s[m=2] slot.hotbar.8 golden_apple 64 0
    replaceitem entity @s[m=1] slot.hotbar.6 sign 1 0 {ench:[{id:0,lvl:1}],HideFlags:127,display:{Name:"OP sign"}}

    @s $(initials)pl = 2


# used on dropped wool items to change their data tag
# so they can be placed on glass and immediately picked up
!mfunc change_wool
    entitydata @s {Item:{tag:{CanPlaceOn:["$(place_on)"],HideFlags:127,display:{Lore:["Mastermind Wool"]}}},PickupDelay:1s}
    @s $(initials)cl = 1

!mfunc open_doors
    fill $(fill_doors_north) air 0 replace bedrock 0
    fill $(fill_doors_south) air 0 replace bedrock 0
    
!mfunc close_doors
    fill $(fill_doors_north) bedrock 0 replace air 0
    fill $(fill_doors_south) bedrock 0 replace air 0
    
!mfunc set_buttons
    fill $(fill_buttons_north) stone_button 5 replace tripwire
    fill $(fill_buttons_south) stone_button 5 replace tripwire

    
!mfunc clear_lanes
    fill $(activate_struct_reset) redstone_block 0 replace planks 0
    fill $(activate_struct_reset) planks 0 replace redstone_block 0


# used on arbitrary to show the correct answer blocks
# for each room
!mfunc show_answer_blocks
    $for(block_num in range(1, blocks_total+1))
    @e[type=armor_stand,$(initials)Correct,$(initials)ca=$(block_num)] setblock ~ ~ ~ $(blocks[block_num-1])
    $endfor


# used on the host to show the correct answer for each room
!mfunc display_answer_actionbar
    $for(num in range(1, (total_rooms) + 1))
    title @s[$(rooms[num].select)] actionbar {"text":"","extra":[
        {"selector":"@e[type=armor_stand,score_$(initials)rn_min=$(num),score_$(initials)rn=$(num),tag=$(initials)Correct1]"},
        {"text":" "},
        {"selector":"@e[type=armor_stand,score_$(initials)rn_min=$(num),score_$(initials)rn=$(num),tag=$(initials)Correct2]"},
        {"text":" "},
        {"selector":"@e[type=armor_stand,score_$(initials)rn_min=$(num),score_$(initials)rn=$(num),tag=$(initials)Correct3]"},
        {"text":" "},
        {"selector":"@e[type=armor_stand,score_$(initials)rn_min=$(num),score_$(initials)rn=$(num),tag=$(initials)Correct4]"}
        ]}
    $endfor


!mfunc bcast_answer
    $for(num in range(1, (total_rooms) + 1))
    tellraw @a[$(initials)rn=$(num)] $(event.begin())
        {"text":"The correct combo is ","color":"green"},
        {"selector":"@e[type=armor_stand,score_$(initials)rn_min=$(num),score_$(initials)rn=$(num),tag=$(initials)Correct1]"},
        {"text":", ","color":"green"},
        {"selector":"@e[type=armor_stand,score_$(initials)rn_min=$(num),score_$(initials)rn=$(num),tag=$(initials)Correct2]"},
        {"text":", ","color":"green"},
        {"selector":"@e[type=armor_stand,score_$(initials)rn_min=$(num),score_$(initials)rn=$(num),tag=$(initials)Correct3]"},
        {"text":" and ","color":"green"},
        {"selector":"@e[type=armor_stand,score_$(initials)rn_min=$(num),score_$(initials)rn=$(num),tag=$(initials)Correct4]"},
        {"text":"!","color":"green"}
        ]}
    $endfor



!mfunc input_start_round
    @e[type=armor_stand,$(initials)Stand] $(initials)st = 1

!mfunc input_reset_round
    @e[type=armor_stand,$(initials)Stand] $(initials)st = 3

!mfunc input_open_doors
    function open_doors

!mfunc input_close_doors
    function close_doors

!mfunc input_reset_all
    @a[$(initials)pl=1..] $(initials)pl = 0

!mfunc input_reset_one
    tellraw @s $(event.begin())
    {"text":"[-]","color":"red",
        "hoverEvent":{"action":"show_text","value":{"text":"Reset one player","color":"gold"}},
        "clickEvent":{"action":"suggest_command","value":"/scoreboard players set NAME $(initials)pl 0"}
    }]}

!mfunc input_randomize
    function randomize

!mfunc input_show_answer
    function show_answer_blocks
    function bcast_answer


!mfunc book
    replaceitem entity @s slot.weapon.offhand written_book 1 0 {
        title:"$(event.full_name) Book",author:"eGO",pages:["[\"\",
            {\"text\":\"$(event.full_name) Settings\\\n\",\"bold\":\"true\"},
            {\"text\":\"\\\n\"},
            
            {\"text\":\"$(initials): \",\"color\":\"dark_gray\"},
            {\"text\":\"Start\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("init"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Starts $(event.full_name) so it can be ran\",\"color\":\"green\"}}
            },
            {\"text\":\" / \",\"color\":\"gray\"},
            {\"text\":\"Stop\\\n\",\"color\":\"red\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("term"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Ends $(event.full_name)\",\"color\":\"red\"}}
            },
            
            {\"text\":\"Round: \",\"color\":\"dark_gray\"},
            {\"text\":\"Start\",\"color\":\"dark_green\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_start_round"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Starts an individual round\",\"color\":\"dark_green\"}}
            },
            {\"text\":\" / \",\"color\":\"gray\"},
            {\"text\":\"Reset\\\n\\\n\",\"color\":\"red\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_reset_round"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Stops an individual round\",\"color\":\"gold\"}}
            },


            {\"text\":\"Doors: \",\"color\":\"dark_gray\"},
            {\"text\":\"Open\",\"color\":\"dark_green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_open_doors"))\"}},
            {\"text\":\" / \",\"color\":\"gray\"},
            {\"text\":\"Closed\\\n\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_close_doors"))\"}},
            
            {\"text\":\"Resets: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_reset_one"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Resets a single player\",\"color\":\"dark_green\"}}
            },
            {\"text\":\" / \",\"color\":\"gray\"},
            {\"text\":\"[+]\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_reset_all"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Resets all players\",\"color\":\"dark_green\"}}
            },
            
            {\"text\":\"\\\n\"},
            {\"text\":\"\\\n\"},
            
            {\"text\":\"Randomizer: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\\\n\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_randomize"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Randomizes the answer (auto sets answer if pressed)\",\"color\":\"gold\"}}
            },
            
            {\"text\":\"Bcasts answer: \",\"color\":\"dark_gray\"},
            {\"text\":\"[+]\\\n\",\"color\":\"gold\",
                \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/$(event.cmd_func("input_show_answer"))\"},
                \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Bcasts and clones answer\",\"color\":\"gold\"}}
            }
        ]"
    ]}











