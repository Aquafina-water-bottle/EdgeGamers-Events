scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1892113393
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=32,y=4,z=-108,dx=48,dy=100,dz=15] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"NR","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Nether Race","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1892113393"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Nether Race","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Nether Race","color":"red"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1892113393"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1892113393
scoreboard players set -1 g.number -1
scoreboard objectives add nr. dummy Nether Race
scoreboard objectives setdisplay sidebar nr.
scoreboard objectives add nr.cd dummy Nether Race Countdown Timer
scoreboard objectives add nr.pl dummy Nether Race Player List
scoreboard objectives add nr.pc dummy Nether Race Place
scoreboard objectives add nr.cp dummy Nether Race Checkpoint ID
scoreboard teams add nr.r Nether Race
scoreboard teams option nr.r color red
scoreboard teams option nr.r seeFriendlyInvisibles true
scoreboard teams option nr.r collisionRule never
scoreboard teams option nr.r friendlyfire false
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["nr.stand","nr.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players add @e[type=minecraft:armor_stand,tag=nr.stand] nr.cd 0
