from typing import NamedTuple, Union
from abc import ABC

from fenautils import addrepr
from lib.floo_event import FlooEvent
from lib.coords import PositionCoords, RegionCoords, Coords


__all__ = [
    "Race",
    "Fill",
    "Place",
    "Checkpoint",
    "RegularRace",
    "CheckpointRace",
    "LapRace",
    ]


class Fill(NamedTuple):
    region: Union[PositionCoords, RegionCoords]
    block: str
    term_block: str = "air"

@addrepr
class Place:
    """
    Args:
        place_num (int): The place place_num that will be shown on display
        message (str): The message that will appear when a player finishes the race,
            formatted as "(selector) message"
        select_coords (Coords): The selection region for each player
        fills (List[Fill]): Any additional commands that should be added
        additional_cmds (List[str]): Any additional commands that should be added

    Attributes:
        place_num (int): The place place_num that will be shown on display
        message (str): The message that will appear when a player finishes the race,
            formatted as "(selector) message"
        select_coords (Coords): The selection region for each player
        fills (List[Fill]): Any additional commands that should be added
        additional_cmds (List[str]): Any additional commands that should be added
    """

    default_messages = (
        "took first place!",
        "took second place!",
        "took third place!",
        "got runnerup!"
    )

    def __init__(self, place_num, message=None, select_coords=None):
        super().__init__()
        self.place_num = place_num

        # The message defaults to the messages defined in Place.default_messages
        if message is None:
            if place_num > len(Place.default_messages):
                self.message = Place.default_messages[-1]
            else:
                self.message = Place.default_messages[place_num - 1]
        else:
            self.message = message

        self.select_coords = select_coords
        self.fills = []
        self.additional_cmds = []

    def make_fill(self, *args, **kwargs):
        fill = Fill(*args, **kwargs)
        self.fills.append(fill)
        return fill


@addrepr
class Race(FlooEvent, ABC):
    """
    Storage class to contain multiple race properties

    Attributes:
        fill_air (Coords region)
        fill_block (Coords region)
        objective_disp (str): A possibly shortened version of display name to be put into objective display names
        places (List[Place]): Holds the nth place information
    """

    def __init__(self, location, objective_disp=None, **options):
        """
        Args:
            location (Location)
        """
        super().__init__(location, **options)
        self.places = []

        if objective_disp is None:
            self.objective_disp = self.location.name_str
        else:
            self.objective_disp = objective_disp

    def make_place(self, **kwargs):
        """
        See Place.__init__
        """
        place_num = len(self.places) + 1
        place = Place(place_num, **kwargs)
        self.places.append(place)
        return place


class RegularRace(Race):
    pass


class Checkpoint(NamedTuple):
    """
    Attributes:
        select_coords (Coords): Selection to set the checkpoint
        tp_coords (Coords): Teleport coordinates when they fall into water or lava

    Args:
        select_coords (Coords): Selection to set the checkpoint
        tp_coords (Coords): Teleport coordinates when they fall into water or lava
    """
    select_coords: Coords
    tp_coords: Coords


class CheckpointRace(Race):
    def __init__(self, location, spawn, objective_disp=None, **options):
        super().__init__(location, objective_disp, **options)
        self.spawn = spawn
        self.checkpoints = []

    def make_checkpoint(self, *args, **kwargs):
        checkpoint = Checkpoint(*args, **kwargs)
        self.checkpoints.append(checkpoint)
        return checkpoint


class LapRace(Race):
    """
    Container class to get lap race properties

    Args:
        location (Location)
        finish_line_coords (Coords region)
        lap_reset_coords (Coords region)
        spawn_coords (Coords region)
        required_laps (Optional[int])

    Attributes:
        select_finish_line (str): selector region when completing a lap
        finish_line_coords (Coords)
        select_lap_reset (str): selector region when resetting the lap selection
        lap_reset_coords (Coords)
        select_spawn (str): selector region for the spawn area
        spawn_coords (Coords)
        required_laps (int)
    """

    def __init__(self, location, finish_line_coords, lap_reset_coords, spawn_coords,
                 objective_disp=None, required_laps=None, **options):
        super().__init__(location, objective_disp, **options)

        self.finish_line_coords = finish_line_coords
        self.select_finish_line = finish_line_coords.selector()

        self.lap_reset_coords = lap_reset_coords
        self.select_lap_reset = lap_reset_coords.selector()

        self.spawn_coords = spawn_coords
        self.select_spawn = spawn_coords.selector()

        if required_laps is None:
            self.required_laps = 3
        else:
            self.required_laps = required_laps

