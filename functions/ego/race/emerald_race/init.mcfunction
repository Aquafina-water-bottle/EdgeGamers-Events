scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1455341280
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=219,y=5,z=-121,dx=75,dy=100,dz=51] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"ER","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Emerald Race","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1455341280"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Emerald Race","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Emerald Race","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1455341280"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1455341280
scoreboard players set -1 g.number -1
scoreboard objectives add er. dummy Emerald Race
scoreboard objectives setdisplay sidebar er.
scoreboard objectives add er.cd dummy Emerald Race Countdown Timer
scoreboard objectives add er.pl dummy Emerald Race Player List
scoreboard objectives add er.pc dummy Emerald Race Place
scoreboard objectives add er.lp dummy Emerald Race Laps
scoreboard objectives add er.ilp dummy Emerald Race Input Laps
scoreboard objectives add er.wn dummy Emerald Race Has Won
scoreboard teams add er.r Emerald Race
scoreboard teams option er.r color green
scoreboard teams option er.r seeFriendlyInvisibles true
scoreboard teams option er.r collisionRule never
scoreboard teams option er.r friendlyfire false
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["er.stand","er.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players add @e[type=minecraft:armor_stand,tag=er.stand] er.cd 0
scoreboard players set @e[type=minecraft:armor_stand,tag=er.stand] er.ilp 3
scoreboard players set @e[type=minecraft:armor_stand,tag=er.stand] er.pc 0
