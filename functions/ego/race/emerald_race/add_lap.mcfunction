scoreboard players add @s er. 1
tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"ER","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Emerald Race","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1455341280"}},{"text":"]","color":"gray"},{"text":": "},{"selector":"@s"},{"text":" has finished a lap!","color":"yellow"}]}
playsound minecraft:entity.experience_orb.pickup voice @a
scoreboard players operation @s g.temp = @s er.
scoreboard players operation @s g.temp -= @e[type=minecraft:armor_stand,c=1,tag=er.stand] er.lp
execute @s[score_g.temp_min=0,score_er.wn_min=0,score_er.wn=0] ~ ~ ~ function ego:race/emerald_race/set_place_0 if @e[type=minecraft:armor_stand,score_er.pc_min=0,score_er.pc=0,tag=er.stand]
execute @s[score_g.temp_min=0,score_er.wn_min=0,score_er.wn=0] ~ ~ ~ function ego:race/emerald_race/set_place_1 if @e[type=minecraft:armor_stand,score_er.pc_min=1,score_er.pc=1,tag=er.stand]
execute @s[score_g.temp_min=0,score_er.wn_min=0,score_er.wn=0] ~ ~ ~ function ego:race/emerald_race/set_place_2 if @e[type=minecraft:armor_stand,score_er.pc_min=2,score_er.pc=2,tag=er.stand]
execute @s[score_g.temp_min=0,score_er.wn_min=0,score_er.wn=0] ~ ~ ~ function ego:race/emerald_race/set_place_3 if @e[type=minecraft:armor_stand,score_er.pc_min=3,score_er.pc=3,tag=er.stand]
scoreboard players set @s er.lp 0
