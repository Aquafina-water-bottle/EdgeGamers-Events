execute @a[m=2,score_er.pl_min=1,score_er.pl=1] ~ ~ ~ function ego:race/emerald_race/reset_player
scoreboard players reset * g.host
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=1455341280,score_gp.cgi=1455341280,tag=gp.stand] gp.tp 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=1455341280,score_gp.cgi=1455341280,tag=gp.stand] gp.cgi 0
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"ER","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Emerald Race","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1455341280"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Emerald Race","color":"green","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Emerald Race","color":"green"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1455341280"}},{"text":" has stopped!","color":"red"}]}
scoreboard objectives remove er.
scoreboard objectives remove er.cd
scoreboard objectives remove er.pl
scoreboard objectives remove er.pc
scoreboard objectives remove er.lp
scoreboard objectives remove er.ilp
scoreboard objectives remove er.wn
scoreboard teams remove er.r
kill @e[tag=er.entity]
fill 269 19 -94 255 19 -94 emerald_block 0 replace air 0
fill 269 19 -98 255 19 -98 emerald_block 0 replace air 0
