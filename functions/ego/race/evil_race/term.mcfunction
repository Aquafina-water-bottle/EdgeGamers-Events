execute @a[m=2,score_evilr.pl_min=1,score_evilr.pl=1] ~ ~ ~ function ego:race/evil_race/reset_player
scoreboard players reset * g.host
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=296783358,score_gp.cgi=296783358,tag=gp.stand] gp.tp 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=296783358,score_gp.cgi=296783358,tag=gp.stand] gp.cgi 0
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"EvilR","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Evil Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 296783358"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Evil Race","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Evil Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 296783358"}},{"text":" has stopped!","color":"red"}]}
scoreboard objectives remove evilr.
scoreboard objectives remove evilr.cd
scoreboard objectives remove evilr.pl
scoreboard objectives remove evilr.pc
scoreboard objectives remove evilr.cp
scoreboard teams remove evilr.r
kill @e[tag=evilr.entity]
setblock 36 16 -133 air
setblock 36 16 -130 air
setblock 36 16 -136 air
fill 31 5 -129 31 5 -137 redstone_block 0 replace stonebrick 0
