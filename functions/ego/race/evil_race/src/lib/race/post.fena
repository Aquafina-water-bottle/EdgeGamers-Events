$edit_type_py_init()

!set prefix = $(race.location.prefix_str)

$py:
    from lib.consts import Effects, Sounds
    from lib.coords import RegionCoords, PositionCoords
    from lib.objective import Objectives
    from lib.team import Teams
    from lib.const_ints import ConstInts

    # general constant
    select_all = race.location.select_all

    objectives = Objectives()
    teams = Teams()
    const_ints = ConstInts()
    const_ints.add_constants(-1)

    objectives.new("""
        # main display objective
        _ _ _

        # _stand: increments as a countdown timer
        _cd _ Countdown Timer

        # players (state): Player state for this event
        # 0 = being outside the map
        # 1 = part of the race, meaning the players are properly initialized
        _pl _ Player List

        # players (state): shows the current place number they are in
        # 0 = not placed
        # 1 = first place, 2 = second place, etc.
        # _stand (state): shows the current place number (- 1) that will be
        # given to the player
        # 0 = first place, 1 = second place, etc.
        _pc _ Place

        """, race.objective_disp)

    objectives["_"].setdisplay("sidebar")

    teams.new(f"""
        # The main team that all players will join
        _r _
            color = {race.location.name.color}
            seeFriendlyInvisibles = true
            collisionRule = never
            friendlyfire = false
    """, race.objective_disp)

$edit_py_init()


# as players: clears their inventory and gives the book to the host
$macro(clear_player):
    item @s - *
    effect @s - *

    # adds all and the _stand to places
    @s _pc += 0

    # gives the book only to the host
    $(race.location.cmd_book("@s[g.host=1]"))

    $edit_type_clear_player()
    $edit_clear_player()


$macro(edit_type_term):
    # terminates all places
    $for(place in race.places):
        $for(fill in place.fills):
            $(fill.region.cmd(fill.term_block))


# adds place functions
# note that the index starts at 0, so
# set_place_0 is actually first place
$for(index, place in enumerate(race.places)):
    # players: sets their place number upon completion of the race
    # each place has its on unique function lol
    !mfunc set_place_$(index):
        tellraw @a[g.sa=1] $(race.location.surround_json(r'{"selector":"@s"},{"text":" %s","color":"green"}' % place.message))
        playsound $(Sounds.LEVEL) voice @a

        # sets the current place
        # runs any additional commands if needed
        $for(additional_cmd in place.additional_cmds):
            $(additional_cmd)

        $for(fill in place.fills):
            $py:
                assert isinstance(fill.region, (RegionCoords, PositionCoords))
            $(fill.region.cmd(fill.block))

        # note that the _pc is left at a positive number for calculations
        # for the edit_main macro, where it will be also negative by the
        # end of the main clock
        @s _pc = $(place.place_num)

        $edit_type_set_place_common()
        $edit_set_place_common()


!mfunc init:
    # initialize the floo network
    $(race.cmd_init())

    # initializes objectives and teams
    $(objectives.cmd_init())
    $(teams.cmd_init())

    $(race.summon_spawn): summon armor_stand ~ ~ ~ {Tags:["_stand","_entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
    @e[type=armor_stand,_stand] _cd += 0

    $edit_type_init()
    $edit_init()


!mfunc main debug=false:
    # the floo network clock for setting spawnpoints
    $(race.cmd_main())

    @e[type=armor_stand,_stand,_cd=..120]: function countdown
    kill @e[$(select_all),type=item]

    # adds everyone to the player list
    @a[g.sa=1,m=2] _pl += 0

    # resets all players that have either just joined the game or just died
    @a[m=2,g.de=1,_pl=1] _pl = 0
    @a[g.sa=1,m=2,_pl=0]: function init_player

    # joins the race team even if they are not initialized in the proper gamemode
    # this is so people in creative can see friendly invisible players
    team _r + @a[g.sa=1,team=!_r]

    # gives invis to all
    effect @a[g.sa=1,m=2,_pl=1] + $(Effects.INVIS) 3 0 true

    $edit_type_main()
    $edit_main()
    $edit_type_post_main()


# players: first time initializing a player by clearing and setting their
# player state to 1
!mfunc init_player:
    $clear_player()

    $edit_type_init_player()
    $edit_init_player()
    @s _pl = 1

# players: used at the end of a race to clear and teleport the players to
# the spawn
!mfunc reset_player:
    $clear_player()
    $(race.location.cmd_tp())

    $edit_type_reset_player()
    $edit_reset_player()


!mfunc term:
    # clears player and teleports them back to the spawn
    # used before the cmd_term to so g.host isn't reset
    @a[_pl=1,m=2]: function reset_player

    # terminates the floo network
    $(race.cmd_term())

    # terminates all scoreboards
    $(objectives.cmd_term())
    $(teams.cmd_term())

    # requires resets because they are singleton classes
    # when multiple files are built using `make.py build all`
    # the singleton actually carries through that lmfao
    $py:
        objectives.reset()
        teams.reset()

    # removes the area effect cloud
    kill @e[_entity]

    $edit_type_term()
    $edit_term()


# _stand: displays the countdown using tellraw and playsounds
!mfunc countdown debug=false:
    @s _cd += 1

    @s[_cd=20]: tellraw @a $(race.location.surround_json(r'{"text":"5","color":"yellow","bold":"true"}'))
    @s[_cd=20] @a: playsound $(Sounds.PLING) voice @s ~ ~ ~ 0.5

    @s[_cd=40]: tellraw @a $(race.location.surround_json(r'{"text":"4","color":"yellow","bold":"true"}'))
    @s[_cd=40] @a: playsound $(Sounds.PLING) voice @s ~ ~ ~ 1

    @s[_cd=60]: tellraw @a $(race.location.surround_json(r'{"text":"3","color":"yellow","bold":"true"}'))
    @s[_cd=60] @a: playsound $(Sounds.PLING) voice @s ~ ~ ~ 1.5

    @s[_cd=80]: tellraw @a $(race.location.surround_json(r'{"text":"2","color":"yellow","bold":"true"}'))
    @s[_cd=80] @a: playsound $(Sounds.PLING) voice @s ~ ~ ~ 2

    @s[_cd=100]: tellraw @a $(race.location.surround_json(r'{"text":"1","color":"yellow","bold":"true"}'))
    @s[_cd=100] @a: playsound $(Sounds.PLING) voice @s ~ ~ ~ 2

    @s[_cd=120]: tellraw @a $(race.location.surround_json(r'{"text":"GO!","color":"green","bold":"true"}'))
    @s[_cd=120] @a: playsound $(Sounds.WITHER) voice @s
    @s[_cd=120]: title @a title {"text":"GO!","color":"green"}

    $edit_type_countdown()
    $edit_countdown()


!folder input:
    $(race.mfunc_input())

