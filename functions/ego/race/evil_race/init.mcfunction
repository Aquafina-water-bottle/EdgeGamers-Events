scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 296783358
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=19,y=6,z=-147,dx=88,dy=100,dz=27] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"EvilR","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Evil Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 296783358"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Evil Race","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Evil Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 296783358"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 296783358
scoreboard players set -1 g.number -1
scoreboard objectives add evilr. dummy Evil Race
scoreboard objectives setdisplay sidebar evilr.
scoreboard objectives add evilr.cd dummy Evil Race Countdown Timer
scoreboard objectives add evilr.pl dummy Evil Race Player List
scoreboard objectives add evilr.pc dummy Evil Race Place
scoreboard objectives add evilr.cp dummy Evil Race Checkpoint ID
scoreboard teams add evilr.r Evil Race
scoreboard teams option evilr.r color gray
scoreboard teams option evilr.r seeFriendlyInvisibles true
scoreboard teams option evilr.r collisionRule never
scoreboard teams option evilr.r friendlyfire false
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["evilr.stand","evilr.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players add @e[type=minecraft:armor_stand,tag=evilr.stand] evilr.cd 0
