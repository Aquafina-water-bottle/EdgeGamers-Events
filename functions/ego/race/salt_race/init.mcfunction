scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 1272235179
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=419,y=4,z=-47,dx=79,dy=40,dz=32] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"SaltR","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Salt Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1272235179"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Salt Race","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Salt Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 1272235179"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 1272235179
scoreboard players set -1 g.number -1
scoreboard objectives add saltr. dummy Salt Race
scoreboard objectives setdisplay sidebar saltr.
scoreboard objectives add saltr.cd dummy Salt Race Countdown Timer
scoreboard objectives add saltr.pl dummy Salt Race Player List
scoreboard objectives add saltr.pc dummy Salt Race Place
scoreboard objectives add saltr.cp dummy Salt Race Checkpoint ID
scoreboard teams add saltr.r Salt Race
scoreboard teams option saltr.r color gray
scoreboard teams option saltr.r seeFriendlyInvisibles true
scoreboard teams option saltr.r collisionRule never
scoreboard teams option saltr.r friendlyfire false
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["saltr.stand","saltr.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players add @e[type=minecraft:armor_stand,tag=saltr.stand] saltr.cd 0
