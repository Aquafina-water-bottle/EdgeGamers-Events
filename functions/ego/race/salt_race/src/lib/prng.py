from lib.objective import Objective, Objectives
from lib.command_group import CommandGroup
from lib.const_ints import ConstInts
from lib.repr_utils import addrepr

objectives = Objectives()
const_ints = ConstInts()

@addrepr
class PRNG(CommandGroup):
    """
    Args:
        objective_name (str): objective name for storing scoreboard values
        lower (int): the minimum value (inclusive) for the resulting pseudo-random number
        upper (Optional[int]): the maximum value (non-inclusive) for the resulting pseudo-random number
            If not specified, upper will inherit lower's value and lower will be overriden with 0

    Attributes:
        objective_name (str)
        objective (Objective)
    """

    def __init__(self, objective_name, lower, upper=None):
        super().__init__()
        const_ints.add_constants(-1)
        assert isinstance(lower, int)

        if lower and not upper:
            (lower, upper) = (0, lower)

        self.objective_name = objective_name
        self.objective = Objective(objective_name, display_name="Pseudo-RNG")

        self.objective.add_score("&Multiplier", 1103515245)
        self.objective.add_score("&Increment", 12345)
        self.objective.add_score("&Offset", lower)
        self.objective.add_score("&Modulus", upper - lower)
        self.objective.add_score("&Seed", 0)

        objectives.add(self.objective)

    def cmd_init(self):
        self.commands.append('summon area_effect_cloud ~ ~ ~ {Tags:["_prng","_prng_true"],Duration:5}')
        self.commands.append('summon area_effect_cloud ~ ~ ~ {Tags:["_prng"],Duration:5}')

        for i in range(31):
            # Construct a random positive value for the seed by generating random bits.
            # Seed is stored in-game as a 32-bit signed integer, so we generate 31 random bits
            self.commands.append(f"@r[type=area_effect_cloud,_prng] @s[_prng_true]: &Seed {self.objective_name} += {1<<i}")

        return self.cmd_output()

    def cmd_next_seed(self):
        self.commands.append(f"&Seed {self.objective_name} *= &Multiplier {self.objective_name}")
        self.commands.append(f"&Seed {self.objective_name} += &Increment {self.objective_name}")
        return self.cmd_output()

    def cmd_assign(self):
        self.commands.append(f"@s {self.objective_name} = &Seed {self.objective_name}")
        self.commands.append(f"@s {self.objective_name} %= &Modulus {self.objective_name}")
        self.commands.append(f"@s[{self.objective_name}=..-1] {self.objective_name} *= -1")
        self.commands.append(f"@s {self.objective_name} += &Offset {self.objective_name}")
        return self.cmd_output()

    def cmd_main(self):
        return CommandGroup.cmd_from_args(self.cmd_next_seed(), self.cmd_assign())

    def cmd_term(self):
        return ""

