execute @a[m=2,score_sr.pl_min=1,score_sr.pl=1] ~ ~ ~ function ego:race/slow_race/reset_player
scoreboard players reset * g.host
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=526647444,score_gp.cgi=526647444,tag=gp.stand] gp.tp 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=526647444,score_gp.cgi=526647444,tag=gp.stand] gp.cgi 0
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"SR","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Slow Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 526647444"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Slow Race","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Slow Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 526647444"}},{"text":" has stopped!","color":"red"}]}
scoreboard objectives remove sr.
scoreboard objectives remove sr.cd
scoreboard objectives remove sr.pl
scoreboard objectives remove sr.pc
scoreboard teams remove sr.r
kill @e[tag=sr.entity]
setblock 70 11 -80 air
setblock 70 11 -78 air
setblock 70 11 -82 air
fill 71 5 -82 71 5 -78 redstone_block 0 replace stonebrick 0
