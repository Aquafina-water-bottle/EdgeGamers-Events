scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 526647444
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=21,y=4,z=-88,dx=61,dy=100,dz=21] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"SR","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Slow Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 526647444"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Slow Race","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Slow Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 526647444"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 526647444
scoreboard players set -1 g.number -1
scoreboard objectives add sr. dummy Slow Race
scoreboard objectives setdisplay sidebar sr.
scoreboard objectives add sr.cd dummy Slow Race Countdown Timer
scoreboard objectives add sr.pl dummy Slow Race Player List
scoreboard objectives add sr.pc dummy Slow Race Place
scoreboard teams add sr.r Slow Race
scoreboard teams option sr.r color gray
scoreboard teams option sr.r seeFriendlyInvisibles true
scoreboard teams option sr.r collisionRule never
scoreboard teams option sr.r friendlyfire false
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["sr.stand","sr.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players add @e[type=minecraft:armor_stand,tag=sr.stand] sr.cd 0
