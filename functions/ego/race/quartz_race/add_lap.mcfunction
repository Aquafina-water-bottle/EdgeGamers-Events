scoreboard players add @s qr. 1
tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"QR","color":"white","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Quartz Race","color":"white"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 365039744"}},{"text":"]","color":"gray"},{"text":": "},{"selector":"@s"},{"text":" has finished a lap!","color":"yellow"}]}
playsound minecraft:entity.experience_orb.pickup voice @a
scoreboard players operation @s g.temp = @s qr.
scoreboard players operation @s g.temp -= @e[type=minecraft:armor_stand,c=1,tag=qr.stand] qr.lp
execute @s[score_g.temp_min=0,score_qr.wn_min=0,score_qr.wn=0] ~ ~ ~ function ego:race/quartz_race/set_place_0 if @e[type=minecraft:armor_stand,score_qr.pc_min=0,score_qr.pc=0,tag=qr.stand]
execute @s[score_g.temp_min=0,score_qr.wn_min=0,score_qr.wn=0] ~ ~ ~ function ego:race/quartz_race/set_place_1 if @e[type=minecraft:armor_stand,score_qr.pc_min=1,score_qr.pc=1,tag=qr.stand]
execute @s[score_g.temp_min=0,score_qr.wn_min=0,score_qr.wn=0] ~ ~ ~ function ego:race/quartz_race/set_place_2 if @e[type=minecraft:armor_stand,score_qr.pc_min=2,score_qr.pc=2,tag=qr.stand]
execute @s[score_g.temp_min=0,score_qr.wn_min=0,score_qr.wn=0] ~ ~ ~ function ego:race/quartz_race/set_place_3 if @e[type=minecraft:armor_stand,score_qr.pc_min=3,score_qr.pc=3,tag=qr.stand]
scoreboard players set @s qr.lp 0
