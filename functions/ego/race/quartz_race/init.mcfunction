scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 365039744
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=219,y=4,z=-177,dx=77,dy=100,dz=38] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"QR","color":"white","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Quartz Race","color":"white"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 365039744"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Quartz Race","color":"white","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Quartz Race","color":"white"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 365039744"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 365039744
scoreboard players set -1 g.number -1
scoreboard objectives add qr. dummy Quartz Race
scoreboard objectives setdisplay sidebar qr.
scoreboard objectives add qr.cd dummy Quartz Race Countdown Timer
scoreboard objectives add qr.pl dummy Quartz Race Player List
scoreboard objectives add qr.pc dummy Quartz Race Place
scoreboard objectives add qr.lp dummy Quartz Race Laps
scoreboard objectives add qr.ilp dummy Quartz Race Input Laps
scoreboard objectives add qr.wn dummy Quartz Race Has Won
scoreboard teams add qr.r Quartz Race
scoreboard teams option qr.r color white
scoreboard teams option qr.r seeFriendlyInvisibles true
scoreboard teams option qr.r collisionRule never
scoreboard teams option qr.r friendlyfire false
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["qr.stand","qr.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players add @e[type=minecraft:armor_stand,tag=qr.stand] qr.cd 0
scoreboard players set @e[type=minecraft:armor_stand,tag=qr.stand] qr.ilp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=qr.stand] qr.pc 0
