execute @a[m=2,score_qr.pl_min=1,score_qr.pl=1] ~ ~ ~ function ego:race/quartz_race/reset_player
scoreboard players reset * g.host
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=365039744,score_gp.cgi=365039744,tag=gp.stand] gp.tp 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=365039744,score_gp.cgi=365039744,tag=gp.stand] gp.cgi 0
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"QR","color":"white","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Quartz Race","color":"white"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 365039744"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Quartz Race","color":"white","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Quartz Race","color":"white"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 365039744"}},{"text":" has stopped!","color":"red"}]}
scoreboard objectives remove qr.
scoreboard objectives remove qr.cd
scoreboard objectives remove qr.pl
scoreboard objectives remove qr.pc
scoreboard objectives remove qr.lp
scoreboard objectives remove qr.ilp
scoreboard objectives remove qr.wn
scoreboard teams remove qr.r
kill @e[tag=qr.entity]
fill 240 6 -160 232 6 -154 redstone_block 0 replace stonebrick 0
fill 232 4 -156 240 4 -158 stonebrick 0 replace redstone_block 0
fill 231 7 -158 231 7 -156 quartz_stairs 1
