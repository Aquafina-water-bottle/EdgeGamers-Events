execute @a[m=2,score_dr.pl_min=1,score_dr.pl=1] ~ ~ ~ function ego:race/diamond_race/reset_player
scoreboard players reset * g.host
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=401263828,score_gp.cgi=401263828,tag=gp.stand] gp.tp 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=401263828,score_gp.cgi=401263828,tag=gp.stand] gp.cgi 0
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"DR","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Diamond Race","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 401263828"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Diamond Race","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Diamond Race","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 401263828"}},{"text":" has stopped!","color":"red"}]}
scoreboard objectives remove dr.
scoreboard objectives remove dr.cd
scoreboard objectives remove dr.pl
scoreboard objectives remove dr.pc
scoreboard objectives remove dr.lp
scoreboard objectives remove dr.ilp
scoreboard objectives remove dr.wn
scoreboard teams remove dr.r
kill @e[tag=dr.entity]
fill 224 16 -34 230 16 -40 redstone_block 0 replace stonebrick 0
fill 224 14 -38 230 14 -36 stonebrick 0 replace redstone_block 0
fill 223 17 -36 223 17 -38 air 0
fill 222 17 -36 222 17 -38 stone_slab 5
