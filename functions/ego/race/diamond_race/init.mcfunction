scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 401263828
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=206,y=4,z=-57,dx=49,dy=100,dz=39] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"DR","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Diamond Race","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 401263828"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Diamond Race","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Diamond Race","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 401263828"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 401263828
scoreboard players set -1 g.number -1
scoreboard objectives add dr. dummy Diamond Race
scoreboard objectives setdisplay sidebar dr.
scoreboard objectives add dr.cd dummy Diamond Race Countdown Timer
scoreboard objectives add dr.pl dummy Diamond Race Player List
scoreboard objectives add dr.pc dummy Diamond Race Place
scoreboard objectives add dr.lp dummy Diamond Race Laps
scoreboard objectives add dr.ilp dummy Diamond Race Input Laps
scoreboard objectives add dr.wn dummy Diamond Race Has Won
scoreboard teams add dr.r Diamond Race
scoreboard teams option dr.r color aqua
scoreboard teams option dr.r seeFriendlyInvisibles true
scoreboard teams option dr.r collisionRule never
scoreboard teams option dr.r friendlyfire false
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["dr.stand","dr.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players add @e[type=minecraft:armor_stand,tag=dr.stand] dr.cd 0
scoreboard players set @e[type=minecraft:armor_stand,tag=dr.stand] dr.ilp 2
scoreboard players set @e[type=minecraft:armor_stand,tag=dr.stand] dr.pc 0
