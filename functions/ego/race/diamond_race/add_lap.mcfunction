scoreboard players add @s dr. 1
tellraw @a {"text":"","extra":[{"text":"[","color":"gray"},{"text":"DR","color":"aqua","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Diamond Race","color":"aqua"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 401263828"}},{"text":"]","color":"gray"},{"text":": "},{"selector":"@s"},{"text":" has finished a lap!","color":"yellow"}]}
playsound minecraft:entity.experience_orb.pickup voice @a
scoreboard players operation @s g.temp = @s dr.
scoreboard players operation @s g.temp -= @e[type=minecraft:armor_stand,c=1,tag=dr.stand] dr.lp
execute @s[score_g.temp_min=0,score_dr.wn_min=0,score_dr.wn=0] ~ ~ ~ function ego:race/diamond_race/set_place_0 if @e[type=minecraft:armor_stand,score_dr.pc_min=0,score_dr.pc=0,tag=dr.stand]
execute @s[score_g.temp_min=0,score_dr.wn_min=0,score_dr.wn=0] ~ ~ ~ function ego:race/diamond_race/set_place_1 if @e[type=minecraft:armor_stand,score_dr.pc_min=1,score_dr.pc=1,tag=dr.stand]
execute @s[score_g.temp_min=0,score_dr.wn_min=0,score_dr.wn=0] ~ ~ ~ function ego:race/diamond_race/set_place_2 if @e[type=minecraft:armor_stand,score_dr.pc_min=2,score_dr.pc=2,tag=dr.stand]
execute @s[score_g.temp_min=0,score_dr.wn_min=0,score_dr.wn=0] ~ ~ ~ function ego:race/diamond_race/set_place_3 if @e[type=minecraft:armor_stand,score_dr.pc_min=3,score_dr.pc=3,tag=dr.stand]
scoreboard players set @s dr.lp 0
