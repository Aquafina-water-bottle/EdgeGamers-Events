$py:
    import json
    import lib.location
    from lib.race import CheckpointRace, Checkpoint
    from lib.coords import Coords

    with open("main.json") as file:
        race_json = json.load(file)

    # gets the location from lib.location, and assumes it exists
    # private so they aren't used
    _location_str = race_json["location"]
    _race_location = getattr(lib.location, _location_str)

    # gets spawn point
    _spawn_json = race_json["spawn"]
    _spawn_checkpoint = Checkpoint(
            select_coords=Coords(_spawn_json["select_coords"]),
            tp_coords=Coords(_spawn_json["tp_coords"])
            )
    race = CheckpointRace(_race_location, _spawn_checkpoint)

    # sets checkpoint
    for checkpoint in race_json["checkpoints"]:
        race.make_checkpoint(
                select_coords=Coords(checkpoint["select_coords"]),
                tp_coords=Coords(checkpoint["tp_coords"])
                )

    for place in race_json["places"]:
        place_object = race.make_place(
                select_coords=Coords(place["select_coords"]),
                message=place["message"],
                additional_cmds=place["additional_cmds"]
                )

        for fill in place["fills"]:
            place_object.make_fill(Coords(fill["coords"]), fill["block"])


$macro(edit_clear_player):
    @s _cp += 0


$macro(edit_type_py_init):
    $py:
        objectives.new("""
            # players: holds the current checkpoint id
            # 0 = spawn, 1+ = an actual checkpoint
            _cp _ Checkpoint ID
        """, display=race.objective_disp)


    # players: set their checkpoint number when they reach a checkpoint
    $for(index, checkpoint in enumerate(race.checkpoints)):
        !mfunc set_checkpoint_$(index+1):
            playsound $(Sounds.LEVEL) voice @s
            tellraw @a[g.sa=1] $(race.location.surround_json(r'{"selector":"@s"},{"text":" has reached checkpoint %d!","color":"yellow"}' % (index+1)))
            @s _cp = $(index+1)

    # players: teleport them back to their previous checkpoint
    !mfunc teleport_back:
        $for(index, checkpoint in enumerate([race.spawn] + race.checkpoints)):
            tp @s[_cp=$(index)] $(checkpoint.tp_coords)

        effect @s + $(Effects.HP) 1 100 true
        effect @s + $(Effects.REGEN) 2 100 true
        @s: playsound $(Sounds.TP) voice @s

    !mfunc book:
        replaceitem entity @s slot.weapon.offhand written_book 1 0 {
            title:"$(race.location.name_str) Book",author:"eGO",pages:["{\"text\":\"\",\"extra\":[
                {\"text\":\"$(race.location.name_str) Settings\\n\",\"bold\":true},
                {\"text\":\"\\n\"},

                {\"text\":\"$(race.location.prefix_disp): \",\"color\":\"dark_gray\"},
                {\"text\":\"Start\",\"color\":\"dark_green\",
                    \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function input/init\"},
                    \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Starts $(race.location.name_str)\",\"color\":\"green\"}}
                },
                {\"text\":\" / \",\"color\":\"gray\"},
                {\"text\":\"Stop\\n\",\"color\":\"red\",
                    \"clickEvent\":{\"action\":\"run_command\",\"value\":\"/function term\"},
                    \"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"Ends $(race.location.name_str)\",\"color\":\"red\"}}
                }
            ]}"
        ]}


$macro(edit_type_set_place_common):
    @s _ = @s _pc
    @s _ *= -1

$macro(edit_type_main):
    # selects anyone in the spawn to be teleported back if they have a checkpoint
    @a[$(race.spawn.select_coords.selector()),m=2,_pl=1,_cp=(1..)]: function teleport_back

    # if a player is found inside a place region
    # note that this is optional for races that don't have specific slots for players
    $for(index, place in enumerate(race.places)):
        $if(place.select_coords is not None):
            @r[$(place.select_coords.selector()),m=2,_pl=1,_pc=0]: function set_place_$(index)

    # if a player is found inside a checkpoint region
    # this allows the player to be any number of checkpoints lower to set the checkpoint
    # the first checkpoint is skipped because it is the spawn
    $for(index, checkpoint in enumerate(race.checkpoints)):
        @a[$(checkpoint.select_coords.selector()),_cp=..$(index)]: function set_checkpoint_$(index+1)


$include("lib/race/main.fena")

