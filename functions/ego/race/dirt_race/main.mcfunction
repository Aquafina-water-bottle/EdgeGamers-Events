scoreboard players set @a g.sa 0
scoreboard players set @a[x=18,y=11,z=-207,dx=49,dy=20,dz=45] g.sa 1
scoreboard players add @a[score_g.sa_min=1,score_g.sa=1] gp.id 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id=-738387100] gp.id 738387099
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_gp.id_min=-738387098] gp.id 738387099
execute @e[type=minecraft:armor_stand,score_dirtr.cd=120,tag=dirtr.stand] ~ ~ ~ function ego:race/dirt_race/countdown
kill @e[x=18,y=11,z=-207,dx=49,dy=20,dz=45,type=minecraft:item]
scoreboard players add @a[m=2,score_g.sa_min=1,score_g.sa=1] dirtr.pl 0
scoreboard players set @a[m=2,score_g.de_min=1,score_g.de=1,score_dirtr.pl_min=1,score_dirtr.pl=1] dirtr.pl 0
execute @a[m=2,score_g.sa_min=1,score_g.sa=1,score_dirtr.pl_min=0,score_dirtr.pl=0] ~ ~ ~ function ego:race/dirt_race/init_player
scoreboard teams join dirtr.r @a[team=!dirtr.r,score_g.sa_min=1,score_g.sa=1]
effect @a[m=2,score_g.sa_min=1,score_g.sa=1,score_dirtr.pl_min=1,score_dirtr.pl=1] minecraft:invisibility 3 0 true
execute @a[x=19,y=14,z=-192,dx=8,dy=5,dz=5,m=2,score_dirtr.pl_min=1,score_dirtr.pl=1,score_dirtr.cp_min=1] ~ ~ ~ function ego:race/dirt_race/teleport_back
execute @r[x=63,y=15,z=-204,dx=1,dy=1,dz=0,m=2,score_dirtr.pl_min=1,score_dirtr.pl=1,score_dirtr.pc_min=0,score_dirtr.pc=0] ~ ~ ~ function ego:race/dirt_race/set_place_0
execute @r[x=66,y=15,z=-204,dx=0,dy=1,dz=0,m=2,score_dirtr.pl_min=1,score_dirtr.pl=1,score_dirtr.pc_min=0,score_dirtr.pc=0] ~ ~ ~ function ego:race/dirt_race/set_place_1
execute @r[x=61,y=15,z=-204,dx=0,dy=1,dz=0,m=2,score_dirtr.pl_min=1,score_dirtr.pl=1,score_dirtr.pc_min=0,score_dirtr.pc=0] ~ ~ ~ function ego:race/dirt_race/set_place_2
execute @a[x=48,y=25,z=-179,dx=2,dy=4,dz=0,score_dirtr.cp=0] ~ ~ ~ function ego:race/dirt_race/set_checkpoint_1
execute @a[m=2,score_g.sa_min=1,score_g.sa=1,score_dirtr.pl_min=1,score_dirtr.pl=1,score_dirtr.swim_min=1] ~ ~ ~ function ego:race/dirt_race/teleport_back
execute @a[m=2,score_g.sa_min=1,score_g.sa=1,score_dirtr.pl_min=1,score_dirtr.pl=1,score_dirtr.dive_min=1] ~ ~ ~ function ego:race/dirt_race/teleport_back
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_dirtr.swim_min=1] dirtr.swim 0
scoreboard players set @a[score_g.sa_min=1,score_g.sa=1,score_dirtr.dive_min=1] dirtr.dive 0
effect @a[m=2,score_g.sa_min=1,score_g.sa=1,score_dirtr.pl_min=1,score_dirtr.pl=1] minecraft:night_vision 20 0 true
