execute @a[m=2,score_dirtr.pl_min=1,score_dirtr.pl=1] ~ ~ ~ function ego:race/dirt_race/reset_player
scoreboard players reset * g.host
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 0
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=738387099,score_gp.cgi=738387099,tag=gp.stand] gp.tp 0
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=738387099,score_gp.cgi=738387099,tag=gp.stand] gp.cgi 0
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"DirtR","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Dirt Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 738387099"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Dirt Race","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Dirt Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 738387099"}},{"text":" has stopped!","color":"red"}]}
scoreboard objectives remove dirtr.
scoreboard objectives remove dirtr.cd
scoreboard objectives remove dirtr.pl
scoreboard objectives remove dirtr.pc
scoreboard objectives remove dirtr.cp
scoreboard objectives remove dirtr.swim
scoreboard objectives remove dirtr.dive
scoreboard teams remove dirtr.r
kill @e[tag=dirtr.entity]
fill 64 12 -203 63 12 -203 air
setblock 66 12 -203 air
setblock 61 12 -203 air
fill 28 12 -187 28 12 -192 redstone_block 0 replace stonebrick 0
