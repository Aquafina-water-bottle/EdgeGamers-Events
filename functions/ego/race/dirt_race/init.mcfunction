scoreboard players set @s g.host 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.tp 738387099
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.pvp 1
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.sat 1
gamerule naturalRegeneration true
scoreboard players set @e[type=minecraft:armor_stand,tag=gp.stand] gp.rgt 0
scoreboard players set @a g.sa 0
scoreboard players set @a[x=18,y=11,z=-207,dx=49,dy=20,dz=45] g.sa 1
tellraw @a[score_gr.ec_min=1,score_gr.ec=1] {"text":"","extra":[{"text":"[","color":"gray"},{"text":"DirtR","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Dirt Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 738387099"}},{"text":"]","color":"gray"},{"text":": "},{"text":"Dirt Race","color":"gray","bold":"true","hoverEvent":{"action":"show_text","value":{"text":"Dirt Race","color":"gray"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set @p gp.tp 738387099"}},{"text":" has started!","color":"green"}]}
scoreboard players set @e[type=minecraft:armor_stand,score_gp.cgi_min=0,score_gp.cgi=0,tag=gp.stand] gp.cgi 738387099
scoreboard players set -1 g.number -1
scoreboard objectives add dirtr. dummy Dirt Race
scoreboard objectives setdisplay sidebar dirtr.
scoreboard objectives add dirtr.cd dummy Dirt Race Countdown Timer
scoreboard objectives add dirtr.pl dummy Dirt Race Player List
scoreboard objectives add dirtr.pc dummy Dirt Race Place
scoreboard objectives add dirtr.cp dummy Dirt Race Checkpoint ID
scoreboard objectives add dirtr.swim stat.swimOneCm Dirt Race Detect Swim
scoreboard objectives add dirtr.dive stat.diveOneCm Dirt Race Detect Dive
scoreboard teams add dirtr.r Dirt Race
scoreboard teams option dirtr.r color gray
scoreboard teams option dirtr.r seeFriendlyInvisibles true
scoreboard teams option dirtr.r collisionRule never
scoreboard teams option dirtr.r friendlyfire false
execute @e[tag=gp.stand] ~ ~1 ~ summon armor_stand ~ ~ ~ {Tags:["dirtr.stand","dirtr.entity"],Invulnerable:1,PersistenceRequired:1,Invisible:1,Marker:1,NoGravity:1}
scoreboard players add @e[type=minecraft:armor_stand,tag=dirtr.stand] dirtr.cd 0
